﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class UpravljajKorisnikom : Form
    {
        List<KorisniciClass> korisniciList = new KorisniciClass().ucitajKorisnike();
        private string akcija;
        private Korisnici _korisnici;
        public UpravljajKorisnikom(Korisnici kor,string akc)
        {
            InitializeComponent();
            _korisnici = kor;
            akcija = akc;

            if (akcija == "Dodaj")
            {   lblAkcija.Text = "Novi korisnik";
                btnOdbaci.Text = "Odbaci";
            }
            else
            {   lblAkcija.Text = "Izmeni korisnika";
                btnOdbaci.Text = "Zatvori";
                prikaziKorisnika();
            }
        }
        
        private void prikaziKorisnika()
        {
            KorisniciClass selektovaniKorisnik = korisniciList.Where(x => x.UserID ==  Int32.Parse(akcija)).FirstOrDefault();
            txtUser.Text = selektovaniKorisnik.UserName;
            txtPass.Text = selektovaniKorisnik.Pass;
            txtImePrezime.Text = selektovaniKorisnik.ImePrezime;
            chkPregledInfo.Checked = selektovaniKorisnik.PregledInfo;
            chkPretraga.Checked = selektovaniKorisnik.Pretraga;
            chkUnosOdrzavanje.Checked = selektovaniKorisnik.UnosOdrzavanje;
            chkUnosBrisVozila.Checked = selektovaniKorisnik.UnosBrisVozila;
            if (selektovaniKorisnik.UserType == "Admin")
            {
                txtImePrezime.Enabled = false;
                gbOvlastenja.Enabled = false;
            }
            else
            {
                txtImePrezime.Enabled = true;
                gbOvlastenja.Enabled = true;
            }
        }

        private void btnOdbaci_Click(object sender, EventArgs e)
        {
            _korisnici.btnEnabled();
            this.Close();
        }

        private void btnPotvrdi_Click(object sender, EventArgs e)
        {
            if (txtUser.Text != "" && txtPass.Text != "")
            {
                bool _provera = false;
                for (int i = 0; i < korisniciList.Count; i++)
                {
                    if (txtUser.Text == korisniciList[i].UserName)
                        _provera = true;
                }

                if (_provera == false)
                {
                    if (akcija == "Dodaj")
                    {
                        if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate korisnika?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            KorisniciClass user = new KorisniciClass();
                            user.UserName = txtUser.Text;
                            user.Pass = txtPass.Text;
                            user.ImePrezime = txtImePrezime.Text;
                            user.PregledInfo = chkPregledInfo.Checked;
                            user.Pretraga = chkPretraga.Checked;
                            user.UnosOdrzavanje = chkUnosOdrzavanje.Checked;
                            user.UnosBrisVozila = chkUnosBrisVozila.Checked;

                            user.dodajKorisnika();
                            _korisnici.prikaziKorisnikeDGV();
                            _korisnici.btnEnabled();
                            this.Close();
                        }
                    }
                    else
                    {
                        if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate izmene?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            KorisniciClass selektovaniKorisnik = korisniciList.Where(x => x.UserID == Int32.Parse(akcija)).FirstOrDefault();
                            selektovaniKorisnik.UserName = txtUser.Text;
                            selektovaniKorisnik.Pass = txtPass.Text;
                            selektovaniKorisnik.ImePrezime = txtImePrezime.Text;
                            selektovaniKorisnik.PregledInfo = chkPregledInfo.Checked;
                            selektovaniKorisnik.Pretraga = chkPretraga.Checked;
                            selektovaniKorisnik.UnosOdrzavanje = chkUnosOdrzavanje.Checked;
                            selektovaniKorisnik.UnosBrisVozila = chkUnosBrisVozila.Checked;

                            selektovaniKorisnik.azurirajKorisnika();
                            _korisnici.btnEnabled();
                            _korisnici.prikaziKorisnikeDGV();
                        }
                    }
                }
                else MessageBox.Show("Korisničko ime već postoji.\n\nIzaberite drugo korisničko ime.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else MessageBox.Show("Niste uneli korisničok ime ili lozinku!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        
    }
}
