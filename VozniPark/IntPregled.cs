﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    class IntPregled
    {
        private string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;

        private int idPregleda;
        private Vozila vozilo;
        private DateTime datumPregleda;
        private int kilometrazaPregl;
        private string vrsioPregled;
        private string pregledano;
        private string stanjeBMB;
        private string stanjeTNGCNG;
        private string primedbe;

        public int IdPregleda
        {
            get { return idPregleda; }
            set { idPregleda = value; }
        }
        public Vozila Vozilo
        {
            get { return vozilo; }
            set { vozilo = value; }
        }
        public DateTime DatumPregleda
        {
            get { return datumPregleda; }
            set { datumPregleda = value; }
        }
        public int KilometrazaPregl
        {
            get { return kilometrazaPregl; }
            set { kilometrazaPregl = value; }
        }
        public string VrsioPregled
        {
            get { return vrsioPregled; }
            set { vrsioPregled = value; }
        }
        public string Pregledano
        {
            get { return pregledano; }
            set { pregledano = value; }
        }
        public string StanjeBMB
        {
            get { return stanjeBMB; }
            set { stanjeBMB = value; }
        }
        public string StanjeTNGCNG
        {
            get { return stanjeTNGCNG; }
            set { stanjeTNGCNG = value; }
        }
        public string Primedbe
        {
            get { return primedbe; }
            set { primedbe = value; }
        }

        public void dodajPregled()
        {
            SQLiteConnection _connection = new SQLiteConnection(conn);
            _connection.Open();
            string sqlADD = @"Insert into tbInterniPregled (DatumPregleda, KilometrazaPregl, VrsioPregled, Pregledano, StanjeBMB, StanjeTNGCNG,
                            Primedbe, IDPreglVozila)  Values (@DatumPregleda, @KilometrazaPregl, @VrsioPregled, @Pregledano, @StanjeBMB,
                            @StanjeTNGCNG, @Primedbe, @IDPreglVozila)";

            SQLiteCommand command = new SQLiteCommand(sqlADD, _connection);
            command.Parameters.Add(new SQLiteParameter("@DatumPregleda", DatumPregleda));
            command.Parameters.Add(new SQLiteParameter("@KilometrazaPregl", KilometrazaPregl));
            command.Parameters.Add(new SQLiteParameter("@VrsioPregled", VrsioPregled));
            command.Parameters.Add(new SQLiteParameter("@Pregledano", Pregledano));
            command.Parameters.Add(new SQLiteParameter("@StanjeBMB", StanjeBMB));
            command.Parameters.Add(new SQLiteParameter("@StanjeTNGCNG", StanjeTNGCNG));
            command.Parameters.Add(new SQLiteParameter("@Primedbe", Primedbe));
            command.Parameters.Add(new SQLiteParameter("@IDPreglVozila", Vozilo.SlBr));
            command.ExecuteNonQuery();
        }

        public void obrisiPregled()
        {
            string deleteSQL = "DELETE FROM tbInterniPregled WHERE IDPregleda=@IDPregleda";
            SQLiteConnection connection = new SQLiteConnection(conn);
            using (connection)
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = deleteSQL;
                command.Parameters.Add(new SQLiteParameter("@IDPregleda", IdPregleda));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<IntPregled> ucitajPreglede(int voziloID)
        {
            List<IntPregled> intPreg = new List<IntPregled>();
            string queryString = "SELECT * FROM tbInterniPregled WHERE IDPreglVozila = @SlBrVozila;";
            SQLiteConnection connection = new SQLiteConnection(conn);

            using (connection)
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@SlBrVozila", voziloID));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    IntPregled perPreg;
                    while (reader.Read())
                    {
                        perPreg = new IntPregled();
                        perPreg.IdPregleda = Int32.Parse(reader["IDPregleda"].ToString());
                        perPreg.DatumPregleda = Convert.ToDateTime(reader["DatumPregleda"].ToString());
                        perPreg.KilometrazaPregl = Int32.Parse(reader["KilometrazaPregl"].ToString());
                        perPreg.VrsioPregled = reader["VrsioPregled"].ToString();
                        perPreg.Pregledano = reader["Pregledano"].ToString();
                        perPreg.StanjeBMB = reader["StanjeBMB"].ToString();
                        perPreg.StanjeTNGCNG = reader["StanjeTNGCNG"].ToString();
                        perPreg.Primedbe = reader["Primedbe"].ToString();
                        intPreg.Add(perPreg);
                    }
                }
            }
            return intPreg;
        }

        public List<IntPregled> ucitajPregled(int voziloID, int _idPregleda)
        {
            List<IntPregled> intPreg = new List<IntPregled>();
            string queryString = "SELECT * FROM tbInterniPregled JOIN tbVozila WHERE IDPreglVozila = @SlBrVozila AND IDPregleda =@IDPregleda;";
            SQLiteConnection connection = new SQLiteConnection(conn);

            using (connection)
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@SlBrVozila", voziloID));
                command.Parameters.Add(new SQLiteParameter("@IDPregleda", _idPregleda));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    IntPregled perPreg;
                    while (reader.Read())
                    {
                        perPreg = new IntPregled();
                        perPreg.IdPregleda = Int32.Parse(reader["IDPregleda"].ToString());
                        perPreg.DatumPregleda = Convert.ToDateTime(reader["DatumPregleda"].ToString());
                        perPreg.KilometrazaPregl = Int32.Parse(reader["KilometrazaPregl"].ToString());
                        perPreg.VrsioPregled = reader["VrsioPregled"].ToString();
                        perPreg.Pregledano = reader["Pregledano"].ToString();
                        perPreg.StanjeBMB = reader["StanjeBMB"].ToString();
                        perPreg.StanjeTNGCNG = reader["StanjeTNGCNG"].ToString();
                        perPreg.Primedbe = reader["Primedbe"].ToString();
                        perPreg.Vozilo = new Vozila();
                        perPreg.Vozilo.SlBr = Int32.Parse(reader["IDPreglVozila"].ToString());
                        perPreg.Vozilo.RegistarskiBr = reader["RegistarskiBr"].ToString();
                        perPreg.Vozilo.Proizvodjac = reader["Proizvodjac"].ToString();
                        perPreg.Vozilo.Model = reader["Model"].ToString();
                        intPreg.Add(perPreg);
                    }
                }
            }
            return intPreg;
        }
    }
}