﻿namespace VozniPark
{
    partial class LogovanjaKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgLogovanja = new System.Windows.Forms.DataGridView();
            this.kolID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolDatum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolLogin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolLogout = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lblKorisnik = new System.Windows.Forms.Label();
            this.btnZatvori = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgLogovanja)).BeginInit();
            this.SuspendLayout();
            // 
            // dgLogovanja
            // 
            this.dgLogovanja.AllowUserToAddRows = false;
            this.dgLogovanja.AllowUserToDeleteRows = false;
            this.dgLogovanja.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgLogovanja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLogovanja.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kolID,
            this.kolDatum,
            this.kolLogin,
            this.kolLogout});
            this.dgLogovanja.Location = new System.Drawing.Point(12, 98);
            this.dgLogovanja.Name = "dgLogovanja";
            this.dgLogovanja.ReadOnly = true;
            this.dgLogovanja.RowTemplate.Height = 24;
            this.dgLogovanja.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgLogovanja.Size = new System.Drawing.Size(379, 291);
            this.dgLogovanja.TabIndex = 0;
            // 
            // kolID
            // 
            this.kolID.HeaderText = "Column1";
            this.kolID.Name = "kolID";
            this.kolID.ReadOnly = true;
            this.kolID.Visible = false;
            // 
            // kolDatum
            // 
            this.kolDatum.HeaderText = "Datum";
            this.kolDatum.Name = "kolDatum";
            this.kolDatum.ReadOnly = true;
            // 
            // kolLogin
            // 
            this.kolLogin.HeaderText = "Vreme Prijave";
            this.kolLogin.Name = "kolLogin";
            this.kolLogin.ReadOnly = true;
            // 
            // kolLogout
            // 
            this.kolLogout.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.kolLogout.HeaderText = "Vreme odjave";
            this.kolLogout.Name = "kolLogout";
            this.kolLogout.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Logovanja korisnika:";
            // 
            // lblKorisnik
            // 
            this.lblKorisnik.AutoSize = true;
            this.lblKorisnik.Location = new System.Drawing.Point(53, 52);
            this.lblKorisnik.Name = "lblKorisnik";
            this.lblKorisnik.Size = new System.Drawing.Size(46, 17);
            this.lblKorisnik.TabIndex = 1;
            this.lblKorisnik.Text = "label1";
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZatvori.Location = new System.Drawing.Point(280, 26);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(75, 45);
            this.btnZatvori.TabIndex = 2;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // LogovanjaKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 401);
            this.ControlBox = false;
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.lblKorisnik);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgLogovanja);
            this.Name = "LogovanjaKorisnika";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prijavljivanja korisnika";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dgLogovanja)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgLogovanja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblKorisnik;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolID;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolDatum;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolLogin;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolLogout;
    }
}