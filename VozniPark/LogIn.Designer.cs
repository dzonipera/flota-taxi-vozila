﻿namespace VozniPark
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogIn));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.tmIstReg = new System.Windows.Forms.Timer(this.components);
            this.tmSaobInsp = new System.Windows.Forms.Timer(this.components);
            this.tmTehnicki = new System.Windows.Forms.Timer(this.components);
            this.tmTaksimetar = new System.Windows.Forms.Timer(this.components);
            this.tmTaksiLeg = new System.Windows.Forms.Timer(this.components);
            this.tmOsiguranje = new System.Windows.Forms.Timer(this.components);
            this.tmKasko = new System.Windows.Forms.Timer(this.components);
            this.tmPPAparat = new System.Windows.Forms.Timer(this.components);
            this.tmParkMark = new System.Windows.Forms.Timer(this.components);
            this.chkPrikaziLozinku = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.otvoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zatvoriProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Korisnik:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(87, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lozinka:";
            // 
            // txtUser
            // 
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(205, 60);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(209, 30);
            this.txtUser.TabIndex = 1;
            // 
            // txtPass
            // 
            this.txtPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.Location = new System.Drawing.Point(205, 133);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(209, 30);
            this.txtPass.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(50)))));
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(89, 218);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(152, 76);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Prijavi se";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(50)))));
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(262, 218);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(152, 76);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Izađi";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // tmIstReg
            // 
            this.tmIstReg.Interval = 1000;
            this.tmIstReg.Tick += new System.EventHandler(this.tmIstReg_Tick);
            // 
            // tmSaobInsp
            // 
            this.tmSaobInsp.Interval = 1000;
            this.tmSaobInsp.Tick += new System.EventHandler(this.tmSaobInsp_Tick);
            // 
            // tmTehnicki
            // 
            this.tmTehnicki.Interval = 1000;
            this.tmTehnicki.Tick += new System.EventHandler(this.tmTehnicki_Tick);
            // 
            // tmTaksimetar
            // 
            this.tmTaksimetar.Interval = 1000;
            this.tmTaksimetar.Tick += new System.EventHandler(this.tmTaksimetar_Tick);
            // 
            // tmTaksiLeg
            // 
            this.tmTaksiLeg.Interval = 1000;
            this.tmTaksiLeg.Tick += new System.EventHandler(this.tmTaksiLeg_Tick);
            // 
            // tmOsiguranje
            // 
            this.tmOsiguranje.Interval = 1000;
            this.tmOsiguranje.Tick += new System.EventHandler(this.tmOsiguranje_Tick);
            // 
            // tmKasko
            // 
            this.tmKasko.Interval = 1000;
            this.tmKasko.Tick += new System.EventHandler(this.tmKasko_Tick);
            // 
            // tmPPAparat
            // 
            this.tmPPAparat.Interval = 1000;
            this.tmPPAparat.Tick += new System.EventHandler(this.tmPPAparat_Tick);
            // 
            // tmParkMark
            // 
            this.tmParkMark.Interval = 1000;
            this.tmParkMark.Tick += new System.EventHandler(this.tmParkMark_Tick);
            // 
            // chkPrikaziLozinku
            // 
            this.chkPrikaziLozinku.AutoSize = true;
            this.chkPrikaziLozinku.Location = new System.Drawing.Point(205, 169);
            this.chkPrikaziLozinku.Name = "chkPrikaziLozinku";
            this.chkPrikaziLozinku.Size = new System.Drawing.Size(120, 21);
            this.chkPrikaziLozinku.TabIndex = 3;
            this.chkPrikaziLozinku.Text = "Prikaži lozinku";
            this.chkPrikaziLozinku.UseVisualStyleBackColor = true;
            this.chkPrikaziLozinku.CheckedChanged += new System.EventHandler(this.chkPrikaziLozinku_CheckedChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otvoriToolStripMenuItem,
            this.zatvoriProgramToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(188, 52);
            // 
            // otvoriToolStripMenuItem
            // 
            this.otvoriToolStripMenuItem.Name = "otvoriToolStripMenuItem";
            this.otvoriToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.otvoriToolStripMenuItem.Text = "Otvori";
            this.otvoriToolStripMenuItem.Click += new System.EventHandler(this.otvoriToolStripMenuItem_Click);
            // 
            // zatvoriProgramToolStripMenuItem
            // 
            this.zatvoriProgramToolStripMenuItem.Name = "zatvoriProgramToolStripMenuItem";
            this.zatvoriProgramToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.zatvoriProgramToolStripMenuItem.Text = "Zatvori program";
            this.zatvoriProgramToolStripMenuItem.Click += new System.EventHandler(this.zatvoriProgramToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "iTaxi Vozni Park";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // LogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(501, 355);
            this.ControlBox = false;
            this.Controls.Add(this.chkPrikaziLozinku);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(519, 402);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(519, 402);
            this.Name = "LogIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Prijava";
            this.Load += new System.EventHandler(this.LogIn_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Timer tmIstReg;
        private System.Windows.Forms.Timer tmSaobInsp;
        private System.Windows.Forms.Timer tmTehnicki;
        private System.Windows.Forms.Timer tmTaksimetar;
        private System.Windows.Forms.Timer tmTaksiLeg;
        private System.Windows.Forms.Timer tmOsiguranje;
        private System.Windows.Forms.Timer tmKasko;
        private System.Windows.Forms.Timer tmPPAparat;
        private System.Windows.Forms.Timer tmParkMark;
        private System.Windows.Forms.CheckBox chkPrikaziLozinku;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem otvoriToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem zatvoriProgramToolStripMenuItem;
    }
}