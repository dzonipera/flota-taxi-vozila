﻿namespace VozniPark
{
    partial class RedovniServis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RedovniServis));
            this.txtOstaloTekst = new System.Windows.Forms.TextBox();
            this.chkPKKais = new System.Windows.Forms.CheckBox();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.chkUljeMenjac = new System.Windows.Forms.CheckBox();
            this.chkOstalo = new System.Windows.Forms.CheckBox();
            this.gbxOstali = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFilterTecneFaze = new System.Windows.Forms.TextBox();
            this.chkFilterTecneFaze = new System.Windows.Forms.CheckBox();
            this.chkFilterGasneFaze = new System.Windows.Forms.CheckBox();
            this.txtFilterGasneFaze = new System.Windows.Forms.TextBox();
            this.txtFilterKlime = new System.Windows.Forms.TextBox();
            this.txtPKKais = new System.Windows.Forms.TextBox();
            this.btnOdbaci = new System.Windows.Forms.Button();
            this.txtSvecice = new System.Windows.Forms.TextBox();
            this.txtZupKaisLanac = new System.Windows.Forms.TextBox();
            this.txtUljeMenjac = new System.Windows.Forms.TextBox();
            this.txtUlje = new System.Windows.Forms.TextBox();
            this.txtFilterUlja = new System.Windows.Forms.TextBox();
            this.txtFilterGoriva = new System.Windows.Forms.TextBox();
            this.txtTecnostKocenje = new System.Windows.Forms.TextBox();
            this.txtFilterVazduha = new System.Windows.Forms.TextBox();
            this.txtOstaloCena = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkZupKaisLanac = new System.Windows.Forms.CheckBox();
            this.chkFilterVazduha = new System.Windows.Forms.CheckBox();
            this.chkFilterKlime = new System.Windows.Forms.CheckBox();
            this.chkUlje = new System.Windows.Forms.CheckBox();
            this.chkSvecice = new System.Windows.Forms.CheckBox();
            this.chkTecnostKocenje = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkFilterGoriva = new System.Windows.Forms.CheckBox();
            this.chkFilterUlja = new System.Windows.Forms.CheckBox();
            this.txtUljeKolicina = new System.Windows.Forms.TextBox();
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKoJeVrsio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tpDatumServisa = new System.Windows.Forms.DateTimePicker();
            this.txtKilometrazaServ = new System.Windows.Forms.TextBox();
            this.txtSveciceKolicina = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTecnostKocKolicina = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUljeMenjacKolicina = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gbxOstali.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbxOsnovni.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtOstaloTekst
            // 
            this.txtOstaloTekst.Enabled = false;
            this.txtOstaloTekst.Location = new System.Drawing.Point(370, 65);
            this.txtOstaloTekst.Multiline = true;
            this.txtOstaloTekst.Name = "txtOstaloTekst";
            this.txtOstaloTekst.Size = new System.Drawing.Size(250, 168);
            this.txtOstaloTekst.TabIndex = 31;
            // 
            // chkPKKais
            // 
            this.chkPKKais.AutoSize = true;
            this.chkPKKais.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPKKais.Location = new System.Drawing.Point(13, 228);
            this.chkPKKais.Name = "chkPKKais";
            this.chkPKKais.Size = new System.Drawing.Size(122, 21);
            this.chkPKKais.TabIndex = 19;
            this.chkPKKais.Text = "Set PK kaiša";
            this.chkPKKais.UseVisualStyleBackColor = true;
            this.chkPKKais.CheckedChanged += new System.EventHandler(this.chkPKKais_CheckedChanged);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSacuvaj.Location = new System.Drawing.Point(389, 315);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(78, 68);
            this.btnSacuvaj.TabIndex = 32;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(255, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 15);
            this.label10.TabIndex = 63;
            this.label10.Text = "Cena u DIN";
            // 
            // chkUljeMenjac
            // 
            this.chkUljeMenjac.AutoSize = true;
            this.chkUljeMenjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUljeMenjac.Location = new System.Drawing.Point(13, 256);
            this.chkUljeMenjac.Name = "chkUljeMenjac";
            this.chkUljeMenjac.Size = new System.Drawing.Size(136, 21);
            this.chkUljeMenjac.TabIndex = 21;
            this.chkUljeMenjac.Text = "Ulje za menjač";
            this.chkUljeMenjac.UseVisualStyleBackColor = true;
            this.chkUljeMenjac.CheckedChanged += new System.EventHandler(this.chkUljeMenjac_CheckedChanged);
            // 
            // chkOstalo
            // 
            this.chkOstalo.AutoSize = true;
            this.chkOstalo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOstalo.Location = new System.Drawing.Point(370, 30);
            this.chkOstalo.Name = "chkOstalo";
            this.chkOstalo.Size = new System.Drawing.Size(77, 21);
            this.chkOstalo.TabIndex = 29;
            this.chkOstalo.Text = "Ostalo";
            this.chkOstalo.UseVisualStyleBackColor = true;
            this.chkOstalo.CheckedChanged += new System.EventHandler(this.chkOstalo_CheckedChanged);
            // 
            // gbxOstali
            // 
            this.gbxOstali.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gbxOstali.Controls.Add(this.groupBox1);
            this.gbxOstali.Controls.Add(this.txtFilterKlime);
            this.gbxOstali.Controls.Add(this.txtPKKais);
            this.gbxOstali.Controls.Add(this.btnOdbaci);
            this.gbxOstali.Controls.Add(this.txtSvecice);
            this.gbxOstali.Controls.Add(this.txtZupKaisLanac);
            this.gbxOstali.Controls.Add(this.txtUljeMenjac);
            this.gbxOstali.Controls.Add(this.btnSacuvaj);
            this.gbxOstali.Controls.Add(this.txtUlje);
            this.gbxOstali.Controls.Add(this.txtFilterUlja);
            this.gbxOstali.Controls.Add(this.txtFilterGoriva);
            this.gbxOstali.Controls.Add(this.txtTecnostKocenje);
            this.gbxOstali.Controls.Add(this.txtFilterVazduha);
            this.gbxOstali.Controls.Add(this.txtOstaloCena);
            this.gbxOstali.Controls.Add(this.txtOstaloTekst);
            this.gbxOstali.Controls.Add(this.chkPKKais);
            this.gbxOstali.Controls.Add(this.label12);
            this.gbxOstali.Controls.Add(this.chkZupKaisLanac);
            this.gbxOstali.Controls.Add(this.label10);
            this.gbxOstali.Controls.Add(this.chkUljeMenjac);
            this.gbxOstali.Controls.Add(this.chkOstalo);
            this.gbxOstali.Controls.Add(this.chkFilterVazduha);
            this.gbxOstali.Controls.Add(this.chkFilterKlime);
            this.gbxOstali.Controls.Add(this.chkUlje);
            this.gbxOstali.Controls.Add(this.chkSvecice);
            this.gbxOstali.Controls.Add(this.chkTecnostKocenje);
            this.gbxOstali.Controls.Add(this.label9);
            this.gbxOstali.Controls.Add(this.label13);
            this.gbxOstali.Controls.Add(this.label11);
            this.gbxOstali.Controls.Add(this.label8);
            this.gbxOstali.Controls.Add(this.chkFilterGoriva);
            this.gbxOstali.Controls.Add(this.chkFilterUlja);
            this.gbxOstali.Controls.Add(this.txtSveciceKolicina);
            this.gbxOstali.Controls.Add(this.txtUljeMenjacKolicina);
            this.gbxOstali.Controls.Add(this.txtTecnostKocKolicina);
            this.gbxOstali.Controls.Add(this.txtUljeKolicina);
            this.gbxOstali.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOstali.Location = new System.Drawing.Point(12, 139);
            this.gbxOstali.MinimumSize = new System.Drawing.Size(645, 425);
            this.gbxOstali.Name = "gbxOstali";
            this.gbxOstali.Size = new System.Drawing.Size(645, 425);
            this.gbxOstali.TabIndex = 77;
            this.gbxOstali.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFilterTecneFaze);
            this.groupBox1.Controls.Add(this.chkFilterTecneFaze);
            this.groupBox1.Controls.Add(this.chkFilterGasneFaze);
            this.groupBox1.Controls.Add(this.txtFilterGasneFaze);
            this.groupBox1.Location = new System.Drawing.Point(0, 337);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 89);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TNG/CNG";
            // 
            // txtFilterTecneFaze
            // 
            this.txtFilterTecneFaze.Enabled = false;
            this.txtFilterTecneFaze.Location = new System.Drawing.Point(258, 21);
            this.txtFilterTecneFaze.Name = "txtFilterTecneFaze";
            this.txtFilterTecneFaze.Size = new System.Drawing.Size(80, 22);
            this.txtFilterTecneFaze.TabIndex = 26;
            this.txtFilterTecneFaze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // chkFilterTecneFaze
            // 
            this.chkFilterTecneFaze.AutoSize = true;
            this.chkFilterTecneFaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterTecneFaze.Location = new System.Drawing.Point(13, 23);
            this.chkFilterTecneFaze.Name = "chkFilterTecneFaze";
            this.chkFilterTecneFaze.Size = new System.Drawing.Size(148, 21);
            this.chkFilterTecneFaze.TabIndex = 25;
            this.chkFilterTecneFaze.Text = "Filter tečne faze";
            this.chkFilterTecneFaze.UseVisualStyleBackColor = true;
            this.chkFilterTecneFaze.CheckedChanged += new System.EventHandler(this.chkFilterTecneFaze_CheckedChanged);
            // 
            // chkFilterGasneFaze
            // 
            this.chkFilterGasneFaze.AutoSize = true;
            this.chkFilterGasneFaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterGasneFaze.Location = new System.Drawing.Point(13, 51);
            this.chkFilterGasneFaze.Name = "chkFilterGasneFaze";
            this.chkFilterGasneFaze.Size = new System.Drawing.Size(152, 21);
            this.chkFilterGasneFaze.TabIndex = 27;
            this.chkFilterGasneFaze.Text = "Filter gasne faze";
            this.chkFilterGasneFaze.UseVisualStyleBackColor = true;
            this.chkFilterGasneFaze.CheckedChanged += new System.EventHandler(this.chkFilterGasneFaze_CheckedChanged);
            // 
            // txtFilterGasneFaze
            // 
            this.txtFilterGasneFaze.Enabled = false;
            this.txtFilterGasneFaze.Location = new System.Drawing.Point(258, 49);
            this.txtFilterGasneFaze.Name = "txtFilterGasneFaze";
            this.txtFilterGasneFaze.Size = new System.Drawing.Size(80, 22);
            this.txtFilterGasneFaze.TabIndex = 28;
            this.txtFilterGasneFaze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtFilterKlime
            // 
            this.txtFilterKlime.Enabled = false;
            this.txtFilterKlime.Location = new System.Drawing.Point(258, 114);
            this.txtFilterKlime.Name = "txtFilterKlime";
            this.txtFilterKlime.Size = new System.Drawing.Size(80, 22);
            this.txtFilterKlime.TabIndex = 11;
            this.txtFilterKlime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtPKKais
            // 
            this.txtPKKais.Enabled = false;
            this.txtPKKais.Location = new System.Drawing.Point(258, 226);
            this.txtPKKais.Name = "txtPKKais";
            this.txtPKKais.Size = new System.Drawing.Size(80, 22);
            this.txtPKKais.TabIndex = 20;
            this.txtPKKais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // btnOdbaci
            // 
            this.btnOdbaci.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOdbaci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdbaci.Location = new System.Drawing.Point(506, 314);
            this.btnOdbaci.Name = "btnOdbaci";
            this.btnOdbaci.Size = new System.Drawing.Size(78, 68);
            this.btnOdbaci.TabIndex = 33;
            this.btnOdbaci.Text = "Odbaci";
            this.btnOdbaci.UseVisualStyleBackColor = true;
            this.btnOdbaci.Click += new System.EventHandler(this.btnOdbaci_Click);
            // 
            // txtSvecice
            // 
            this.txtSvecice.Enabled = false;
            this.txtSvecice.Location = new System.Drawing.Point(258, 170);
            this.txtSvecice.Name = "txtSvecice";
            this.txtSvecice.Size = new System.Drawing.Size(80, 22);
            this.txtSvecice.TabIndex = 16;
            this.txtSvecice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtZupKaisLanac
            // 
            this.txtZupKaisLanac.Enabled = false;
            this.txtZupKaisLanac.Location = new System.Drawing.Point(258, 291);
            this.txtZupKaisLanac.Name = "txtZupKaisLanac";
            this.txtZupKaisLanac.Size = new System.Drawing.Size(80, 22);
            this.txtZupKaisLanac.TabIndex = 24;
            this.txtZupKaisLanac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtUljeMenjac
            // 
            this.txtUljeMenjac.Enabled = false;
            this.txtUljeMenjac.Location = new System.Drawing.Point(258, 254);
            this.txtUljeMenjac.Name = "txtUljeMenjac";
            this.txtUljeMenjac.Size = new System.Drawing.Size(80, 22);
            this.txtUljeMenjac.TabIndex = 22;
            this.txtUljeMenjac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtUlje
            // 
            this.txtUlje.Enabled = false;
            this.txtUlje.Location = new System.Drawing.Point(258, 142);
            this.txtUlje.Name = "txtUlje";
            this.txtUlje.Size = new System.Drawing.Size(80, 22);
            this.txtUlje.TabIndex = 14;
            this.txtUlje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtFilterUlja
            // 
            this.txtFilterUlja.Enabled = false;
            this.txtFilterUlja.Location = new System.Drawing.Point(258, 30);
            this.txtFilterUlja.Name = "txtFilterUlja";
            this.txtFilterUlja.Size = new System.Drawing.Size(80, 22);
            this.txtFilterUlja.TabIndex = 5;
            this.txtFilterUlja.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtFilterGoriva
            // 
            this.txtFilterGoriva.Enabled = false;
            this.txtFilterGoriva.Location = new System.Drawing.Point(258, 58);
            this.txtFilterGoriva.Name = "txtFilterGoriva";
            this.txtFilterGoriva.Size = new System.Drawing.Size(80, 22);
            this.txtFilterGoriva.TabIndex = 7;
            this.txtFilterGoriva.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtTecnostKocenje
            // 
            this.txtTecnostKocenje.Enabled = false;
            this.txtTecnostKocenje.Location = new System.Drawing.Point(258, 198);
            this.txtTecnostKocenje.Name = "txtTecnostKocenje";
            this.txtTecnostKocenje.Size = new System.Drawing.Size(80, 22);
            this.txtTecnostKocenje.TabIndex = 18;
            this.txtTecnostKocenje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtFilterVazduha
            // 
            this.txtFilterVazduha.Enabled = false;
            this.txtFilterVazduha.Location = new System.Drawing.Point(258, 86);
            this.txtFilterVazduha.Name = "txtFilterVazduha";
            this.txtFilterVazduha.Size = new System.Drawing.Size(80, 22);
            this.txtFilterVazduha.TabIndex = 9;
            this.txtFilterVazduha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // txtOstaloCena
            // 
            this.txtOstaloCena.Enabled = false;
            this.txtOstaloCena.Location = new System.Drawing.Point(540, 30);
            this.txtOstaloCena.Name = "txtOstaloCena";
            this.txtOstaloCena.Size = new System.Drawing.Size(80, 22);
            this.txtOstaloCena.TabIndex = 30;
            this.txtOstaloCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(537, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 15);
            this.label12.TabIndex = 63;
            this.label12.Text = "Cena u DIN";
            // 
            // chkZupKaisLanac
            // 
            this.chkZupKaisLanac.AutoSize = true;
            this.chkZupKaisLanac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZupKaisLanac.Location = new System.Drawing.Point(13, 284);
            this.chkZupKaisLanac.Name = "chkZupKaisLanac";
            this.chkZupKaisLanac.Size = new System.Drawing.Size(176, 38);
            this.chkZupKaisLanac.TabIndex = 23;
            this.chkZupKaisLanac.Text = "Set zupčastog kaiša\r\n/lanca";
            this.chkZupKaisLanac.UseVisualStyleBackColor = true;
            this.chkZupKaisLanac.CheckedChanged += new System.EventHandler(this.chkZupKaisLanac_CheckedChanged);
            // 
            // chkFilterVazduha
            // 
            this.chkFilterVazduha.AutoSize = true;
            this.chkFilterVazduha.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterVazduha.Location = new System.Drawing.Point(13, 88);
            this.chkFilterVazduha.Name = "chkFilterVazduha";
            this.chkFilterVazduha.Size = new System.Drawing.Size(133, 21);
            this.chkFilterVazduha.TabIndex = 8;
            this.chkFilterVazduha.Text = "Filter vazduha";
            this.chkFilterVazduha.UseVisualStyleBackColor = true;
            this.chkFilterVazduha.CheckedChanged += new System.EventHandler(this.chkFilterVazduha_CheckedChanged);
            // 
            // chkFilterKlime
            // 
            this.chkFilterKlime.AutoSize = true;
            this.chkFilterKlime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterKlime.Location = new System.Drawing.Point(13, 116);
            this.chkFilterKlime.Name = "chkFilterKlime";
            this.chkFilterKlime.Size = new System.Drawing.Size(109, 21);
            this.chkFilterKlime.TabIndex = 10;
            this.chkFilterKlime.Text = "Filter klime";
            this.chkFilterKlime.UseVisualStyleBackColor = true;
            this.chkFilterKlime.CheckedChanged += new System.EventHandler(this.chkFilterKlime_CheckedChanged);
            // 
            // chkUlje
            // 
            this.chkUlje.AutoSize = true;
            this.chkUlje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUlje.Location = new System.Drawing.Point(13, 144);
            this.chkUlje.Name = "chkUlje";
            this.chkUlje.Size = new System.Drawing.Size(58, 21);
            this.chkUlje.TabIndex = 12;
            this.chkUlje.Text = "Ulje";
            this.chkUlje.UseVisualStyleBackColor = true;
            this.chkUlje.CheckedChanged += new System.EventHandler(this.chkUlje_CheckedChanged);
            // 
            // chkSvecice
            // 
            this.chkSvecice.AutoSize = true;
            this.chkSvecice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSvecice.Location = new System.Drawing.Point(13, 172);
            this.chkSvecice.Name = "chkSvecice";
            this.chkSvecice.Size = new System.Drawing.Size(86, 21);
            this.chkSvecice.TabIndex = 15;
            this.chkSvecice.Text = "Svećice";
            this.chkSvecice.UseVisualStyleBackColor = true;
            this.chkSvecice.CheckedChanged += new System.EventHandler(this.chkSvecice_CheckedChanged);
            // 
            // chkTecnostKocenje
            // 
            this.chkTecnostKocenje.AutoSize = true;
            this.chkTecnostKocenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTecnostKocenje.Location = new System.Drawing.Point(13, 200);
            this.chkTecnostKocenje.Name = "chkTecnostKocenje";
            this.chkTecnostKocenje.Size = new System.Drawing.Size(171, 21);
            this.chkTecnostKocenje.TabIndex = 17;
            this.chkTecnostKocenje.Text = "Tečnost za kočenje";
            this.chkTecnostKocenje.UseVisualStyleBackColor = true;
            this.chkTecnostKocenje.CheckedChanged += new System.EventHandler(this.chkTecnostKocenje_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(108, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 18);
            this.label8.TabIndex = 63;
            this.label8.Text = "lit.";
            // 
            // chkFilterGoriva
            // 
            this.chkFilterGoriva.AutoSize = true;
            this.chkFilterGoriva.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterGoriva.Location = new System.Drawing.Point(13, 60);
            this.chkFilterGoriva.Name = "chkFilterGoriva";
            this.chkFilterGoriva.Size = new System.Drawing.Size(117, 21);
            this.chkFilterGoriva.TabIndex = 6;
            this.chkFilterGoriva.Text = "Filter goriva";
            this.chkFilterGoriva.UseVisualStyleBackColor = true;
            this.chkFilterGoriva.CheckedChanged += new System.EventHandler(this.chkFilterGoriva_CheckedChanged);
            // 
            // chkFilterUlja
            // 
            this.chkFilterUlja.AutoSize = true;
            this.chkFilterUlja.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterUlja.Location = new System.Drawing.Point(13, 34);
            this.chkFilterUlja.Name = "chkFilterUlja";
            this.chkFilterUlja.Size = new System.Drawing.Size(98, 21);
            this.chkFilterUlja.TabIndex = 4;
            this.chkFilterUlja.Text = "Filter ulja";
            this.chkFilterUlja.UseVisualStyleBackColor = true;
            this.chkFilterUlja.CheckedChanged += new System.EventHandler(this.chkFilterUlja_CheckedChanged);
            // 
            // txtUljeKolicina
            // 
            this.txtUljeKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtUljeKolicina.Enabled = false;
            this.txtUljeKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUljeKolicina.Location = new System.Drawing.Point(77, 142);
            this.txtUljeKolicina.Name = "txtUljeKolicina";
            this.txtUljeKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUljeKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtUljeKolicina.TabIndex = 13;
            this.txtUljeKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUljeKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblRegistracija);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.label7);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 121);
            this.gbxOsnovni.TabIndex = 67;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 24);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistracija.Location = new System.Drawing.Point(103, 47);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(89, 17);
            this.lblRegistracija.TabIndex = 24;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 70);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registracija:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 93);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // txtKoJeVrsio
            // 
            this.txtKoJeVrsio.Location = new System.Drawing.Point(287, 96);
            this.txtKoJeVrsio.Name = "txtKoJeVrsio";
            this.txtKoJeVrsio.Size = new System.Drawing.Size(332, 22);
            this.txtKoJeVrsio.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(290, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 17);
            this.label6.TabIndex = 69;
            this.label6.Text = "Ko je vršio servis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(487, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 17);
            this.label5.TabIndex = 70;
            this.label5.Text = "Kilometraža";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 71;
            this.label4.Text = "Datum servisa";
            // 
            // tpDatumServisa
            // 
            this.tpDatumServisa.CustomFormat = "dd.MM.yyyy.";
            this.tpDatumServisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpDatumServisa.Location = new System.Drawing.Point(287, 43);
            this.tpDatumServisa.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.tpDatumServisa.Name = "tpDatumServisa";
            this.tpDatumServisa.Size = new System.Drawing.Size(129, 22);
            this.tpDatumServisa.TabIndex = 1;
            this.tpDatumServisa.Value = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            // 
            // txtKilometrazaServ
            // 
            this.txtKilometrazaServ.Location = new System.Drawing.Point(490, 43);
            this.txtKilometrazaServ.Name = "txtKilometrazaServ";
            this.txtKilometrazaServ.Size = new System.Drawing.Size(129, 22);
            this.txtKilometrazaServ.TabIndex = 2;
            this.txtKilometrazaServ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKilometrazaServ_KeyPress);
            // 
            // txtSveciceKolicina
            // 
            this.txtSveciceKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtSveciceKolicina.Enabled = false;
            this.txtSveciceKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSveciceKolicina.Location = new System.Drawing.Point(107, 170);
            this.txtSveciceKolicina.Name = "txtSveciceKolicina";
            this.txtSveciceKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSveciceKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtSveciceKolicina.TabIndex = 13;
            this.txtSveciceKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSveciceKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(138, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 18);
            this.label9.TabIndex = 63;
            this.label9.Text = "kom.";
            // 
            // txtTecnostKocKolicina
            // 
            this.txtTecnostKocKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtTecnostKocKolicina.Enabled = false;
            this.txtTecnostKocKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTecnostKocKolicina.Location = new System.Drawing.Point(190, 198);
            this.txtTecnostKocKolicina.Name = "txtTecnostKocKolicina";
            this.txtTecnostKocKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTecnostKocKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtTecnostKocKolicina.TabIndex = 13;
            this.txtTecnostKocKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTecnostKocKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(221, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 18);
            this.label11.TabIndex = 63;
            this.label11.Text = "lit.";
            // 
            // txtUljeMenjacKolicina
            // 
            this.txtUljeMenjacKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtUljeMenjacKolicina.Enabled = false;
            this.txtUljeMenjacKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUljeMenjacKolicina.Location = new System.Drawing.Point(155, 254);
            this.txtUljeMenjacKolicina.Name = "txtUljeMenjacKolicina";
            this.txtUljeMenjacKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUljeMenjacKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtUljeMenjacKolicina.TabIndex = 13;
            this.txtUljeMenjacKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUljeMenjacKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilterUlja_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(186, 259);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 18);
            this.label13.TabIndex = 63;
            this.label13.Text = "lit.";
            // 
            // RedovniServis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(669, 576);
            this.ControlBox = false;
            this.Controls.Add(this.gbxOstali);
            this.Controls.Add(this.gbxOsnovni);
            this.Controls.Add(this.txtKoJeVrsio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tpDatumServisa);
            this.Controls.Add(this.txtKilometrazaServ);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(687, 623);
            this.Name = "RedovniServis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Redovni servis";
            this.gbxOstali.ResumeLayout(false);
            this.gbxOstali.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOstaloTekst;
        private System.Windows.Forms.CheckBox chkPKKais;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkUljeMenjac;
        private System.Windows.Forms.CheckBox chkOstalo;
        private System.Windows.Forms.GroupBox gbxOstali;
        private System.Windows.Forms.CheckBox chkFilterVazduha;
        private System.Windows.Forms.CheckBox chkFilterKlime;
        private System.Windows.Forms.CheckBox chkUlje;
        private System.Windows.Forms.CheckBox chkSvecice;
        private System.Windows.Forms.CheckBox chkTecnostKocenje;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkFilterGoriva;
        private System.Windows.Forms.CheckBox chkFilterUlja;
        private System.Windows.Forms.TextBox txtUljeKolicina;
        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKoJeVrsio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker tpDatumServisa;
        private System.Windows.Forms.TextBox txtKilometrazaServ;
        private System.Windows.Forms.Button btnOdbaci;
        private System.Windows.Forms.TextBox txtOstaloCena;
        private System.Windows.Forms.TextBox txtFilterKlime;
        private System.Windows.Forms.TextBox txtPKKais;
        private System.Windows.Forms.TextBox txtSvecice;
        private System.Windows.Forms.TextBox txtUljeMenjac;
        private System.Windows.Forms.TextBox txtUlje;
        private System.Windows.Forms.TextBox txtFilterUlja;
        private System.Windows.Forms.TextBox txtFilterGoriva;
        private System.Windows.Forms.TextBox txtTecnostKocenje;
        private System.Windows.Forms.TextBox txtFilterVazduha;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFilterTecneFaze;
        private System.Windows.Forms.CheckBox chkFilterTecneFaze;
        private System.Windows.Forms.CheckBox chkFilterGasneFaze;
        private System.Windows.Forms.TextBox txtFilterGasneFaze;
        private System.Windows.Forms.TextBox txtZupKaisLanac;
        private System.Windows.Forms.CheckBox chkZupKaisLanac;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSveciceKolicina;
        private System.Windows.Forms.TextBox txtUljeMenjacKolicina;
        private System.Windows.Forms.TextBox txtTecnostKocKolicina;
    }
}