﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class RedovniServis : Form
    {
        public float CenaServ = 0;
        public string Radovi = "";
        private int _IDVoz;
        public RedovniServis(int id, string Reg, string Proiz, string Model)
        {
            InitializeComponent();
            _IDVoz = id;
            lblSluzbeniBroj.Text = id.ToString();
            lblRegistracija.Text = Reg;
            lblProizvodjac.Text = Proiz;
            lblModel.Text = Model;
            tpDatumServisa.Value = DateTime.Today;
        }

        private void chkFilterUlja_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterUlja.Checked == true)
            {
                txtFilterUlja.Enabled = true;
            }
            else
            {
                txtFilterUlja.Text = "";
                txtFilterUlja.Enabled = false;
            }
        }
        private void chkFilterGoriva_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterGoriva.Checked == true)
            {
                txtFilterGoriva.Enabled = true;
            }
            else
            {
                txtFilterGoriva.Text = "";
                txtFilterGoriva.Enabled = false;
            }
        }
        private void chkFilterVazduha_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterVazduha.Checked == true)
            {
                txtFilterVazduha.Enabled = true;
            }
            else
            {
                txtFilterVazduha.Text = "";
                txtFilterVazduha.Enabled = false;
            }
        }
        private void chkFilterKlime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterKlime.Checked == true)
            {
                txtFilterKlime.Enabled = true;
            }
            else
            {
                txtFilterKlime.Text = "";
                txtFilterKlime.Enabled = false;
            }
        }
        private void chkUlje_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUlje.Checked == true)
            {
                txtUlje.Enabled = true;
                txtUljeKolicina.Enabled = true;
            }
            else
            {
                txtUlje.Text = "";
                txtUljeKolicina.Text = "";
                txtUlje.Enabled = false;
                txtUljeKolicina.Enabled = false;
            }
        }
        private void chkSvecice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSvecice.Checked == true)
            {
                txtSvecice.Enabled = true;
                txtSveciceKolicina.Enabled = true;
            }
            else
            {
                txtSvecice.Text = "";
                txtSveciceKolicina.Text = "";
                txtSvecice.Enabled = false;
                txtSveciceKolicina.Enabled = false;
            }
        }
        private void chkTecnostKocenje_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTecnostKocenje.Checked == true)
            {
                txtTecnostKocenje.Enabled = true;
                txtTecnostKocKolicina.Enabled = true;
            }
            else
            {
                txtTecnostKocenje.Text = "";
                txtTecnostKocKolicina.Text = "";
                txtTecnostKocenje.Enabled = false;
                txtTecnostKocKolicina.Enabled = false;
            }
        }
        private void chkPKKais_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPKKais.Checked == true)
            {
                txtPKKais.Enabled = true;
            }
            else
            {
                txtPKKais.Text = "";
                txtPKKais.Enabled = false;
            }
        }
        private void chkUljeMenjac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUljeMenjac.Checked == true)
            {
                txtUljeMenjac.Enabled = true;
                txtUljeMenjacKolicina.Enabled = true;
            }
            else
            {
                txtUljeMenjac.Text = "";
                txtUljeMenjacKolicina.Text = "";
                txtUljeMenjac.Enabled = false;
                txtUljeMenjacKolicina.Enabled = false;
            }
        }
        private void chkZupKaisLanac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZupKaisLanac.Checked == true)
            {
                txtZupKaisLanac.Enabled = true;
            }
            else
            {
                txtZupKaisLanac.Text = "";
                txtZupKaisLanac.Enabled = false;
            }
        }
        private void chkFilterTecneFaze_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterTecneFaze.Checked == true)
            {
                txtFilterTecneFaze.Enabled = true;
            }
            else
            {
                txtFilterTecneFaze.Text = "";
                txtFilterTecneFaze.Enabled = false;
            }
        }
        private void chkFilterGasneFaze_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterGasneFaze.Checked == true)
            {
                txtFilterGasneFaze.Enabled = true;
            }
            else
            {
                txtFilterGasneFaze.Text = "";
                txtFilterGasneFaze.Enabled = false;
            }
        }
        private void chkOstalo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOstalo.Checked == true)
            {
                txtOstaloCena.Enabled = true;
                txtOstaloTekst.Enabled = true;
            }
            else
            {
                txtOstaloCena.Text = "";
                txtOstaloTekst.Text = "";
                txtOstaloCena.Enabled = false;
                txtOstaloTekst.Enabled = false;
            }
        }

        private void ispitajCekirano()
        {
            CenaServ = 0;
            Radovi = "";
            string str1 = ",";
            string str2 = ".";
            if (chkFilterUlja.Checked == true)
            {
                txtFilterUlja.Text = txtFilterUlja.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterUlja.Text + "  Cena: " + txtFilterUlja.Text + "#";
                if (txtFilterUlja.Text == "") txtFilterUlja.BackColor = Color.Red; else txtFilterUlja.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterUlja.Text);
            }
            if (chkFilterGoriva.Checked == true)
            {
                txtFilterGoriva.Text = txtFilterGoriva.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterGoriva.Text + "  Cena: " + txtFilterGoriva.Text + "#";
                if (txtFilterGoriva.Text == "") txtFilterGoriva.BackColor = Color.Red; else txtFilterGoriva.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterGoriva.Text);
            }
            if (chkFilterVazduha.Checked == true)
            {
                txtFilterVazduha.Text = txtFilterVazduha.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterVazduha.Text + "  Cena: " + txtFilterVazduha.Text + "#";
                if (txtFilterVazduha.Text == "") txtFilterVazduha.BackColor = Color.Red; else txtFilterVazduha.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterVazduha.Text);
            }
            if (chkFilterKlime.Checked == true)
            {
                txtFilterKlime.Text = txtFilterKlime.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterKlime.Text + "  Cena: " + txtFilterKlime.Text + "#";
                if (txtFilterKlime.Text == "") txtFilterKlime.BackColor = Color.Red; else txtFilterKlime.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterKlime.Text);
            }
            if (chkUlje.Checked == true)
            {
                txtUlje.Text = txtUlje.Text.Replace(str1, str2);
                txtUljeKolicina.Text = txtUljeKolicina.Text.Replace(str1, str2);
                Radovi = Radovi + chkUlje.Text + "  " + txtUljeKolicina.Text + " lit.  Cena: " + txtUlje.Text + "#";
                if (txtUlje.Text == "") txtUlje.BackColor = Color.Red; else txtUlje.BackColor = Color.White;
                if (txtUljeKolicina.Text == "") txtUljeKolicina.BackColor = Color.Red; else txtUljeKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtUlje.Text);
            } 
            if (chkSvecice.Checked == true)
            {
                txtSvecice.Text = txtSvecice.Text.Replace(str1, str2);
                txtSveciceKolicina.Text = txtSveciceKolicina.Text.Replace(str1, str2);
                Radovi = Radovi + chkSvecice.Text + "  " + txtSveciceKolicina.Text + "kom.  Cena: " + txtSvecice.Text + "#";
                if (txtSvecice.Text == "") txtSvecice.BackColor = Color.Red; else txtSvecice.BackColor = Color.White;
                if (txtSveciceKolicina.Text == "") txtSveciceKolicina.BackColor = Color.Red;
                else txtSveciceKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtSvecice.Text);
            }
            if (chkTecnostKocenje.Checked == true)
            {
                txtTecnostKocenje.Text = txtTecnostKocenje.Text.Replace(str1, str2);
                txtTecnostKocKolicina.Text = txtTecnostKocKolicina.Text.Replace(str1, str2);
                Radovi = Radovi + chkTecnostKocenje.Text + "  " + txtSveciceKolicina.Text + "lit.  Cena: " + txtTecnostKocenje.Text + "#";
                if (txtTecnostKocenje.Text == "") txtTecnostKocenje.BackColor = Color.Red; else txtTecnostKocenje.BackColor = Color.White;
                if (txtTecnostKocKolicina.Text == "") txtTecnostKocKolicina.BackColor = Color.Red;
                else txtTecnostKocKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtTecnostKocenje.Text);
            }
            if (chkPKKais.Checked == true)
            {
                txtPKKais.Text = txtPKKais.Text.Replace(str1, str2);
                Radovi = Radovi + chkPKKais.Text + "  Cena: " + txtPKKais.Text + "#";
                if (txtPKKais.Text == "") txtPKKais.BackColor = Color.Red; else txtPKKais.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPKKais.Text);
            }
            if (chkUljeMenjac.Checked == true)
            {
                txtUljeMenjac.Text = txtUljeMenjac.Text.Replace(str1, str2);
                txtUljeMenjacKolicina.Text = txtUljeMenjacKolicina.Text.Replace(str1, str2);
                Radovi = Radovi + chkUljeMenjac.Text + "  " + txtUljeMenjacKolicina.Text + "lit.  Cena: " + txtUljeMenjac.Text + "#";
                if (txtUljeMenjac.Text == "") txtUljeMenjac.BackColor = Color.Red; else txtUljeMenjac.BackColor = Color.White;
                if (txtUljeMenjacKolicina.Text == "") txtUljeMenjacKolicina.BackColor = Color.Red;
                else txtUljeMenjacKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtUljeMenjac.Text);
            }
            if (chkZupKaisLanac.Checked == true)
            {
                txtZupKaisLanac.Text = txtZupKaisLanac.Text.Replace(str1, str2);
                Radovi = Radovi + chkZupKaisLanac.Text + "  Cena: " + txtZupKaisLanac.Text + "#";
                if (txtZupKaisLanac.Text == "") txtZupKaisLanac.BackColor = Color.Red; else txtZupKaisLanac.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZupKaisLanac.Text);
            }
            if (chkFilterTecneFaze.Checked == true)
            {
                txtFilterTecneFaze.Text = txtFilterTecneFaze.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterTecneFaze.Text + "  Cena: " + txtFilterTecneFaze.Text + "#";
                if (txtFilterTecneFaze.Text == "") txtFilterTecneFaze.BackColor = Color.Red; else txtFilterTecneFaze.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterTecneFaze.Text);
            }
            if (chkFilterGasneFaze.Checked == true)
            {
                txtFilterGasneFaze.Text = txtFilterGasneFaze.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterGasneFaze.Text + "  Cena: " + txtFilterGasneFaze.Text + "#";
                if (txtFilterGasneFaze.Text == "") txtFilterGasneFaze.BackColor = Color.Red; else txtFilterGasneFaze.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterGasneFaze.Text);
            }
            if (chkOstalo.Checked == true)
            {
                txtOstaloCena.Text = txtOstaloCena.Text.Replace(str1, str2);
                Radovi = Radovi + "Ostalo:\n" + txtOstaloTekst.Text + "  Cena: " + txtOstaloCena.Text + "#";
                if (txtOstaloCena.Text == "") txtOstaloCena.BackColor = Color.Red; else txtOstaloCena.BackColor = Color.White;
                if (txtOstaloTekst.Text == "") txtOstaloTekst.BackColor = Color.Red; else txtOstaloTekst.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtOstaloCena.Text);
            }
        }

        private void cuvanjeServisa()
        {
            if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Servisi serv = new Servisi();
                serv.KoJeVrsio = txtKoJeVrsio.Text;
                serv.TipServisa = "Redovni Servis";
                serv.DatumServisa = tpDatumServisa.Value.Date;
                serv.KilometrazaServ = Int32.Parse(txtKilometrazaServ.Text);
                serv.CenaServisa = CenaServ.ToString();
                serv.Radovi = Radovi;
                Vozila voz = new Vozila();
                voz.SlBr = _IDVoz;
                serv.Vozilo = voz;
                serv.dodajServis();
                this.Close();
            }
        }
        
        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (txtKilometrazaServ.Text != "" && txtKoJeVrsio.Text != "")
            {
                try
                {
                    ispitajCekirano();
                    cuvanjeServisa();
                }
                catch { MessageBox.Show("Morate popuniti obavezna polja!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
            }
            else
            {
                MessageBox.Show("Morate popuniti obavezna polja!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if( txtKilometrazaServ.Text == "") txtKilometrazaServ.BackColor = Color.Red; else txtKilometrazaServ.BackColor = Color.White;
                if (txtKoJeVrsio.Text == "") txtKoJeVrsio.BackColor = Color.Red; else txtKoJeVrsio.BackColor = Color.White;
            }
        }

        private void btnOdbaci_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da želiti da odbacite servis?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void txtKilometrazaServ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtFilterUlja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }
    }
}
