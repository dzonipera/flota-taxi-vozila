﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class UnosNovogVozila : Form
    {
        private Naslovna _naslovna;
        public UnosNovogVozila(Naslovna nas)
        {
            InitializeComponent();
            _naslovna = nas;
        }

        private void cbxStanje_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxStanje.SelectedItem.ToString() == "Nov")
            {
                txtVlasnik.Enabled = false;
                txtVlasnik.Text = "";
            }
            else
            {
                txtVlasnik.Enabled = true;
            }
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (txtSlBrVozila.Text!="" && txtProizvodjac.Text!="" && txtModel.Text!="")
            {
                if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Vozila voz = new Vozila();
                    voz.SlBr = int.Parse(txtSlBrVozila.Text);
                    voz.Proizvodjac = txtProizvodjac.Text;
                    voz.Model = txtModel.Text;
                    voz.BrSasije = txtBrSasije.Text;
                    voz.Karoserija = txtKaroserija.Text;
                    try { voz.BrVrata = int.Parse(txtBrVrata.Text); } catch { }
                    try { voz.BrSedista = int.Parse(txtBrSedista.Text); } catch { }
                    try { voz.Masa = int.Parse(txtMasa.Text); } catch { }
                    voz.Boja = txtBoja.Text;
                    try { voz.GodProizvodnje = int.Parse(txtGodProizvodnje.Text); } catch { }
                    voz.MProizvodnje = cbxMProizvodnje.Text;
                    voz.BrMotora = txtBrMotora.Text;
                    try { voz.Zapremina = int.Parse(txtZapremina.Text); } catch { }
                    voz.Standard = txtStandard.Text;
                    try { voz.KW = int.Parse(txtKW.Text); } catch { }
                    try { voz.KonjskeSnage = int.Parse(txtKonjske.Text); } catch { }
                    voz.Gorivo = txtGorivo.Text;
                    try { voz.Rezervoar = int.Parse(txtRezervoar.Text); } catch { }
                    voz.TipMenjaca = txtMenjac.Text;
                    try { voz.BrBrzina = int.Parse(txtBrBrzina.Text); } catch { }
                    voz.DatumKupovine = tpDatKupovine.Value.Date;
                    voz.Stanje = cbxStanje.Text;
                    voz.Vlasnik = txtVlasnik.Text;
                    try { voz.KilometrazaKupljenog = int.Parse(txtKilometraza.Text); } catch { }
                    voz.RegistarskiBr = txtRegistracija.Text;
                    voz.RegistrIstice = tpRegistrovanDo.Value.Date;
                    voz.dodajVozilo();
                    _naslovna.prikaziVozilaDGV();
                    _naslovna.OmoguciDugmad();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Morate popuniti obavezna polja", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (txtSlBrVozila.Text == "") txtSlBrVozila.BackColor = Color.Red; else txtSlBrVozila.BackColor = Color.White;
                if (txtProizvodjac.Text == "") txtProizvodjac.BackColor = Color.Red; else txtProizvodjac.BackColor = Color.White;
                if (txtModel.Text == "") txtModel.BackColor = Color.Red; else txtModel.BackColor = Color.White;
            }
        }

        private void txtSlBrVozila_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
