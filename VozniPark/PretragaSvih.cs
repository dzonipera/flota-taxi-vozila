﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class PretragaSvih : Form
    {
        List<Servisi> ServList = new List<Servisi>();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        public PretragaSvih()
        {
            InitializeComponent();
            this.AcceptButton = btnPretrazi;
            tpStart.MaxDate = DateTime.Today;
            tpEnd.MinDate = tpStart.Value;
            tpEnd.Value = DateTime.Today;
            dgPretrazeni.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            PrikaziDGV();
        }

        private void PrikaziDGV()
        {
            ServList = new Servisi().ucitajServiseOdDo(txtPojam.Text, tpStart.Value, tpEnd.Value);
            dt.Columns.Add("ID");
            dt.Columns.Add("kolRadjeno");
            dt.Columns.Add("kolSlBr");
            dt.Columns.Add("kolRegBr");
            dt.Columns.Add("kolVrsta");
            dt.Columns.Add("kolDatum");
            dt.Columns.Add("kolDatumPoredjenje");

            for (int i = 0; i < ServList.Count; i++)
            {
                string dateString = ServList[i].DatumServisa.Day + ". " + ServList[i].DatumServisa.Month + ". " + ServList[i].DatumServisa.Year + ".";
                int datePoredjenje = +ServList[i].DatumServisa.Year*10000 +ServList[i].DatumServisa.Month*100 +ServList[i].DatumServisa.Day;
                dt.Rows.Add(ServList[i].IdServisa, ServList[i].Radovi,
                        ServList[i].Vozilo.SlBr, ServList[i].Vozilo.RegistarskiBr, ServList[i].TipServisa, dateString, datePoredjenje);
            }
            
            dgPretrazeni.Rows.Clear();
            dgPretrazeni.DataSource = dt;
            ds.Tables.Add(dt);
            dgPretrazeni.Columns["ID"].Visible = false;
            dgPretrazeni.Columns["kolRadjeno"].Visible = false;
            dgPretrazeni.Columns["kolSlBr"].HeaderText = "Službeni broj";
            dgPretrazeni.Columns["kolRegBr"].HeaderText = "Registarski broj";
            dgPretrazeni.Columns["kolVrsta"].HeaderText = "Tip servisa";
            dgPretrazeni.Columns["kolDatum"].HeaderText = "Datum servisa";
            dgPretrazeni.Columns["kolDatumPoredjenje"].Visible = false;
            dgPretrazeni.CurrentCell = null;
        }
        
        private void txtPojam_TextChanged(object sender, EventArgs e)
        {
            int poredjStart = tpStart.Value.Year * 10000 + tpStart.Value.Month * 100 + tpStart.Value.Day;
            int poredjEnd = tpEnd.Value.Year * 10000 + tpEnd.Value.Month * 100 + tpEnd.Value.Day;
            DataView dv = ds.Tables[0].DefaultView;
            dv.RowFilter = string.Format("kolRadjeno LIKE '%{0}%' AND kolDatumPoredjenje >={1} AND kolDatumPoredjenje<={2}", txtPojam.Text, poredjStart, poredjEnd);
            dgPretrazeni.DataSource = dv;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (dgPretrazeni.SelectedRows.Count > 0)
            {
                int _idVoz = Int32.Parse(dgPretrazeni.SelectedRows[0].Cells["kolSlBr"].Value.ToString());
                int _idServisa = Int32.Parse(dgPretrazeni.SelectedRows[0].Cells["ID"].Value.ToString());
                OServisu oServ = new OServisu(_idVoz, _idServisa);
                oServ.Show();
            }
            else { MessageBox.Show("Nema podataka ili nijedan red nije odabran!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }
        
    }
}
