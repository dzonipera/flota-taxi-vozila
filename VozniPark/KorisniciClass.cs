﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Configuration;

namespace VozniPark
{
    class KorisniciClass
    {
        private int userID;
        private string userName;
        private string pass;
        private string imePrezime;
        private bool pregledInfo;
        private bool pretraga;
        private bool unosOdrzavanje;
        private bool unosBrisVozila;
        private string userType;

        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public string Pass
        {
            get { return pass; }
            set { pass = value; }
        }
        public string ImePrezime
        {
            get { return imePrezime; }
            set { imePrezime = value; }
        }
        public bool PregledInfo
        {
            get { return pregledInfo; }
            set { pregledInfo = value; }
        }
        public bool Pretraga
        {
            get { return pretraga; }
            set { pretraga = value; }
        }
        public bool UnosOdrzavanje
        {
            get { return unosOdrzavanje; }
            set { unosOdrzavanje = value; }
        }
        public bool UnosBrisVozila
        {
            get { return unosBrisVozila; }
            set { unosBrisVozila = value; }
        }
        public string UserType
        {
            get { return userType; }
            set { userType = value; }
        }


        public void dodajKorisnika()
        {
            SQLiteConnection _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlADD = @"Insert into tbLogin (UserName, Password, ImePrezime, PregledInfo, Pretraga, UnosOdrzavanje, UnosBrisVozila) 
                            Values (@UserName, @Password, @ImePrezime, @PregledInfo, @Pretraga, @UnosOdrzavanje, @UnosBrisVozila)";

            SQLiteCommand command = new SQLiteCommand(sqlADD, _connection);
            command.Parameters.Add(new SQLiteParameter("@UserName", UserName));
            command.Parameters.Add(new SQLiteParameter("@Password", Pass));
            command.Parameters.Add(new SQLiteParameter("@ImePrezime", ImePrezime));
            command.Parameters.Add(new SQLiteParameter("@PregledInfo", PregledInfo));
            command.Parameters.Add(new SQLiteParameter("@Pretraga", Pretraga));
            command.Parameters.Add(new SQLiteParameter("@UnosOdrzavanje", UnosOdrzavanje));
            command.Parameters.Add(new SQLiteParameter("@UnosBrisVozila", unosBrisVozila));
            command.ExecuteNonQuery();
        }

        public void azurirajKorisnika()
        {
            SQLiteConnection _connection;
            _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlUpdate = @"Update tbLogin SET UserName=@UserName, Password=@Password, ImePrezime=@ImePrezime, PregledInfo=@PregledInfo,
                                Pretraga=@Pretraga, UnosOdrzavanje=@UnosOdrzavanje, UnosBrisVozila=@UnosBrisVozila WHERE UserID=@UserID";

            SQLiteCommand command = new SQLiteCommand(sqlUpdate, _connection);
            command.Parameters.Add(new SQLiteParameter("@UserID", UserID));
            command.Parameters.Add(new SQLiteParameter("@UserName", UserName));
            command.Parameters.Add(new SQLiteParameter("@Password", Pass));
            command.Parameters.Add(new SQLiteParameter("@ImePrezime", ImePrezime));
            command.Parameters.Add(new SQLiteParameter("@PregledInfo", PregledInfo));
            command.Parameters.Add(new SQLiteParameter("@Pretraga", Pretraga));
            command.Parameters.Add(new SQLiteParameter("@UnosOdrzavanje", UnosOdrzavanje));
            command.Parameters.Add(new SQLiteParameter("@UnosBrisVozila", unosBrisVozila));
            command.ExecuteNonQuery();
        }

        public void obrisiKorisnika()
        {
            string deleteSql = "DELETE FROM tbLogin WHERE UserID=@UserID; DELETE FROM tbLogovanja WHERE KorisnikID = @UserID;";
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SQLiteParameter("@UserID", UserID));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<KorisniciClass> ucitajKorisnike()
        {
            List<KorisniciClass> korisnici = new List<KorisniciClass>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbLogin;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    KorisniciClass users;
                    while (reader.Read())
                    {
                        users = new KorisniciClass();
                        users.UserID = Int32.Parse(reader["UserID"].ToString());
                        users.UserName = reader["UserName"].ToString();
                        users.Pass = reader["Password"].ToString();
                        users.ImePrezime = reader["ImePrezime"].ToString();
                        users.UserType = reader["UserType"].ToString();
                        users.PregledInfo = Convert.ToBoolean(reader["PregledInfo"]);
                        users.Pretraga = Convert.ToBoolean(reader["Pretraga"]);
                        users.UnosOdrzavanje = Convert.ToBoolean(reader["UnosOdrzavanje"]);
                        users.UnosBrisVozila = Convert.ToBoolean(reader["UnosBrisVozila"]);
                        korisnici.Add(users);
                    }
                }
            }
            return korisnici;
        }

        public List<KorisniciClass> ucitajKorisnika(string _korisnik)
        {
            List<KorisniciClass> korisnik = new List<KorisniciClass>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "Select* from tbLogin where UserName = @UserName;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@UserName", _korisnik));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    KorisniciClass user;
                    while (reader.Read())
                    {
                        user = new KorisniciClass();
                        //user.UserID = Int32.Parse(reader["UserID"].ToString());
                        user.UserName = reader["UserName"].ToString();
                        //user.Pass = reader["Password"].ToString();
                        //user.ImePrezime = reader["ImePrezime"].ToString();
                        user.UserType = reader["UserType"].ToString();
                        user.PregledInfo = Convert.ToBoolean(reader["PregledInfo"]);
                        user.Pretraga = Convert.ToBoolean(reader["Pretraga"]);
                        user.UnosOdrzavanje = Convert.ToBoolean(reader["UnosOdrzavanje"]);
                        user.UnosBrisVozila = Convert.ToBoolean(reader["UnosBrisVozila"]);
                        korisnik.Add(user);
                    }
                }
            }
            return korisnik;
        }
    }
}
