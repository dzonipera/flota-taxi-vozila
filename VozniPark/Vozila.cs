﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    public class Vozila
    {
        private int slbr;
        private string proizvodjac;
        private string model;
        private string brSasije;
        private string karoserija;
        private int brVrata;
        private int brSedista;
        private int masa;
        private string boja;
        private int godProizvodnje;
        private string mProizvodnje;
        private string brMotora;
        private int zapremina;
        private string standard;
        private int kW;
        private int konjskeSnage;
        private string gorivo;
        private int rezervoar;
        private string tipMenjaca;
        private int brBrzina;
        private DateTime datumKupovine;
        private string stanje;
        private string vlasnik;
        private int predjKilometraza;
        private int kilometrazaKupljenog;
        private string registarskiBr;
        private DateTime registrIstice;
        private string komunalniBroj;
        private DateTime saobrInspektor;
        private DateTime tehnickiPregled;
        private DateTime pregledTaksimetra;
        private DateTime taksiLegitimacija;
        private DateTime osiguranjeAOOP;
        private DateTime kasko;
        private DateTime ppAparat;
        private DateTime parkMarkice;

        public int SlBr
        {
            get { return slbr; }
            set { slbr = value; }
        }
        public string Proizvodjac
        {
            get { return proizvodjac; }
            set { proizvodjac = value; }
        }
        public string Model
        {
            get { return model; }
            set { model = value; }
        }
        public string BrSasije
        {
            get { return brSasije; }
            set { brSasije = value; }
        }
        public string Karoserija
        {
            get { return karoserija; }
            set { karoserija = value; }
        }
        public int BrVrata
        {
            get { return brVrata; }
            set { brVrata = value; }
        }
        public int BrSedista
        {
            get { return brSedista; }
            set { brSedista = value; }
        }
        public int Masa
        {
            get { return masa; }
            set { masa = value; }
        }
        public string Boja
        {
            get { return boja; }
            set { boja = value; }
        }
        public int GodProizvodnje
        {
            get { return godProizvodnje; }
            set { godProizvodnje = value; }
        }
        public string MProizvodnje
        {
            get { return mProizvodnje; }
            set { mProizvodnje = value; }
        }
        public string BrMotora
        {
            get { return brMotora; }
            set { brMotora = value; }
        }
        public int Zapremina
        {
            get { return zapremina; }
            set { zapremina = value; }
        }
        public string Standard
        {
            get { return standard; }
            set { standard = value; }
        }
        public int KW
        {
            get { return kW; }
            set { kW = value; }
        }
        public int KonjskeSnage
        {
            get { return konjskeSnage; }
            set { konjskeSnage = value; }
        }
        public string Gorivo
        {
            get { return gorivo; }
            set { gorivo = value; }
        }
        public int Rezervoar
        {
            get { return rezervoar; }
            set { rezervoar = value; }
        }
        public string TipMenjaca
        {
            get { return tipMenjaca; }
            set { tipMenjaca = value; }
        }
        public int BrBrzina
        {
            get { return brBrzina; }
            set { brBrzina = value; }
        }
        public DateTime DatumKupovine
        {
            get { return datumKupovine; }
            set { datumKupovine = value; }
        }
        public string Stanje
        {
            get { return stanje; }
            set { stanje = value; }
        }
        public string Vlasnik
        {
            get { return vlasnik; }
            set { vlasnik = value; }
        }
        public int PredjKilometraza
        {
            get { return predjKilometraza; }
            set { predjKilometraza = value; }
        }
        public int KilometrazaKupljenog
        {
            get { return kilometrazaKupljenog; }
            set { kilometrazaKupljenog = value; }
        }
        public string RegistarskiBr
        {
            get { return registarskiBr; }
            set { registarskiBr = value; }
        }
        public DateTime RegistrIstice
        {
            get { return registrIstice; }
            set { registrIstice = value; }
        }
        public string KomunalniBroj
        {
            get { return komunalniBroj; }
            set { komunalniBroj = value; }
        }
        public DateTime SaobrInspektor
        {
            get { return saobrInspektor; }
            set { saobrInspektor = value; }
        }
        public DateTime TehnickiPregled
        {
            get { return tehnickiPregled; }
            set { tehnickiPregled = value; }
        }
        public DateTime PregledTaksimetra
        {
            get { return pregledTaksimetra; }
            set { pregledTaksimetra = value; }
        }
        public DateTime TaksiLegitimacija
        {
            get { return taksiLegitimacija; }
            set { taksiLegitimacija = value; }
        }
        public DateTime OsiguranjeAOOP
        {
            get { return osiguranjeAOOP; }
            set { osiguranjeAOOP = value; }
        }
        public DateTime Kasko
        {
            get { return kasko; }
            set { kasko = value; }
        }
        public DateTime PPAparat
        {
            get { return ppAparat; }
            set { ppAparat = value; }
        }
        public DateTime ParkMarkice
        {
            get { return parkMarkice; }
            set { parkMarkice = value; }
        }
        
        public void dodajVozilo()
        {
            SQLiteConnection m_dbConnection;
            m_dbConnection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            m_dbConnection.Open();
            string sqlADD = @"insert into tbVozila  
                (SlBrVozila, Proizvodjac, Model, BrSasije, Karoserija, BrVrata, BrSedista, Masa, Boja, 
                GodProizvodnje, MProizvodnje, BrMotora, Zapremina, Standard, kW, KonjskeSnage, Gorivo, Rezervoar, 
                TipMenjaca, BrBrzina, DatumKupovine, Stanje, Vlasnik, KilometrazaKupljen, RegistarskiBr, RegistrIstice) VALUES 
                 (@SlBrVozila, @Proizvodjac, @Model, @BrSasije, @Karoserija, @BrVrata, @BrSedista, @Masa, @Boja, @GodProizvodnje, 
                 @MProizvodnje, @BrMotora, @Zapremina, @Standard, @kW, @KonjskeSnage, @Gorivo, @Rezervoar, @TipMenjaca, @BrBrzina, 
                 @DatumKupovine, @Stanje, @Vlasnik, @KilometrazaKupljen, @RegistarskiBr, @RegistrIstice)";
            
            SQLiteCommand command = new SQLiteCommand(sqlADD, m_dbConnection);
            command.Parameters.Add(new SQLiteParameter("@SlBrVozila", SlBr));
            command.Parameters.Add(new SQLiteParameter("@Proizvodjac", Proizvodjac));
            command.Parameters.Add(new SQLiteParameter("@Model", Model));
            command.Parameters.Add(new SQLiteParameter("@BrSasije", BrSasije));
            command.Parameters.Add(new SQLiteParameter("@Karoserija", Karoserija));
            command.Parameters.Add(new SQLiteParameter("@BrVrata", BrVrata));
            command.Parameters.Add(new SQLiteParameter("@BrSedista", BrSedista));
            command.Parameters.Add(new SQLiteParameter("@Masa", Masa));
            command.Parameters.Add(new SQLiteParameter("@Boja", Boja));
            command.Parameters.Add(new SQLiteParameter("@GodProizvodnje", GodProizvodnje));
            command.Parameters.Add(new SQLiteParameter("@MProizvodnje", MProizvodnje));
            command.Parameters.Add(new SQLiteParameter("@BrMotora", BrMotora));
            command.Parameters.Add(new SQLiteParameter("@Zapremina", Zapremina));
            command.Parameters.Add(new SQLiteParameter("@Standard", Standard));
            command.Parameters.Add(new SQLiteParameter("@kW", KW));
            command.Parameters.Add(new SQLiteParameter("@KonjskeSnage", KonjskeSnage));
            command.Parameters.Add(new SQLiteParameter("@Gorivo", Gorivo));
            command.Parameters.Add(new SQLiteParameter("@Rezervoar", Rezervoar));
            command.Parameters.Add(new SQLiteParameter("@TipMenjaca", TipMenjaca));
            command.Parameters.Add(new SQLiteParameter("@BrBrzina", BrBrzina));
            command.Parameters.Add(new SQLiteParameter("@DatumKupovine", DatumKupovine));
            command.Parameters.Add(new SQLiteParameter("@Stanje", Stanje));
            command.Parameters.Add(new SQLiteParameter("@Vlasnik", Vlasnik));
            command.Parameters.Add(new SQLiteParameter("@KilometrazaKupljen", KilometrazaKupljenog));
            command.Parameters.Add(new SQLiteParameter("@RegistarskiBr", RegistarskiBr));
            command.Parameters.Add(new SQLiteParameter("@RegistrIstice", RegistrIstice));
            command.ExecuteNonQuery();
            
        }

        public void azurirajVozilo()
        {
            SQLiteConnection m_dbConnection;
            m_dbConnection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            m_dbConnection.Open();
            string sqlUpdate = @"update tbVozila 
                SET BrSasije=@BrSasije, Karoserija=@Karoserija, BrVrata=@BrVrata,
                BrSedista=@BrSedista, Masa=@Masa, Boja=@Boja, GodProizvodnje=@GodProizvodnje, MProizvodnje=@MProizvodnje,
                BrMotora=@BrMotora, Zapremina=@Zapremina, Standard=@Standard, kW=@kW, KonjskeSnage=@KonjskeSnage, Gorivo=@Gorivo,
                Rezervoar=@Rezervoar, TipMenjaca=@TipMenjaca, BrBrzina=@BrBrzina, DatumKupovine=@DatumKupovine, Stanje=@Stanje,
                Vlasnik=@Vlasnik, KilometrazaKupljen=@KilometrazaKupljen, PredjKilometraza=@PredjKilometraza, RegistarskiBr=@RegistarskiBr, 
                RegistrIstice=@RegistrIstice, KomunalniBroj=@KomunalniBroj, PregledInspektora=@PregledInspektora, Tehnicki=@Tehnicki, 
                PPAparat=@PPAparat, PregledTaksimetra=@PregledTaksimetra, TaksiLeg=@TaksiLeg, Osiguranje=@Osiguranje, KaskoOsig=@KaskoOsig,
                ParkingMarkice=@ParkingMarkice       WHERE  SlBrVozila=@SlBrVozila;";
            
            SQLiteCommand command = new SQLiteCommand(sqlUpdate, m_dbConnection);
            command.Parameters.Add(new SQLiteParameter("@SlBrVozila", SlBr));
            command.Parameters.Add(new SQLiteParameter("@Proizvodjac", Proizvodjac));
            command.Parameters.Add(new SQLiteParameter("@Model", Model));
            command.Parameters.Add(new SQLiteParameter("@BrSasije", BrSasije));
            command.Parameters.Add(new SQLiteParameter("@Karoserija", Karoserija));
            command.Parameters.Add(new SQLiteParameter("@BrVrata", BrVrata));
            command.Parameters.Add(new SQLiteParameter("@BrSedista", BrSedista));
            command.Parameters.Add(new SQLiteParameter("@Masa", Masa));
            command.Parameters.Add(new SQLiteParameter("@Boja", Boja));
            command.Parameters.Add(new SQLiteParameter("@GodProizvodnje", GodProizvodnje));
            command.Parameters.Add(new SQLiteParameter("@MProizvodnje", MProizvodnje));
            command.Parameters.Add(new SQLiteParameter("@BrMotora", BrMotora));
            command.Parameters.Add(new SQLiteParameter("@Zapremina", Zapremina));
            command.Parameters.Add(new SQLiteParameter("@Standard", Standard));
            command.Parameters.Add(new SQLiteParameter("@kW", KW));
            command.Parameters.Add(new SQLiteParameter("@KonjskeSnage", KonjskeSnage));
            command.Parameters.Add(new SQLiteParameter("@Gorivo", Gorivo));
            command.Parameters.Add(new SQLiteParameter("@Rezervoar", Rezervoar));
            command.Parameters.Add(new SQLiteParameter("@TipMenjaca", TipMenjaca));
            command.Parameters.Add(new SQLiteParameter("@BrBrzina", BrBrzina));
            command.Parameters.Add(new SQLiteParameter("@DatumKupovine", DatumKupovine));
            command.Parameters.Add(new SQLiteParameter("@Stanje", Stanje));
            command.Parameters.Add(new SQLiteParameter("@Vlasnik", Vlasnik));
            command.Parameters.Add(new SQLiteParameter("@KilometrazaKupljen", KilometrazaKupljenog));
            command.Parameters.Add(new SQLiteParameter("@PredjKilometraza", PredjKilometraza));
            command.Parameters.Add(new SQLiteParameter("@RegistarskiBr", RegistarskiBr));
            command.Parameters.Add(new SQLiteParameter("@RegistrIstice", RegistrIstice));
            command.Parameters.Add(new SQLiteParameter("@KomunalniBroj", KomunalniBroj));
            command.Parameters.Add(new SQLiteParameter("@PregledInspektora", SaobrInspektor));
            command.Parameters.Add(new SQLiteParameter("@Tehnicki", TehnickiPregled));
            command.Parameters.Add(new SQLiteParameter("@PPAparat", PPAparat));
            command.Parameters.Add(new SQLiteParameter("@PregledTaksimetra", PregledTaksimetra));
            command.Parameters.Add(new SQLiteParameter("@TaksiLeg", TaksiLegitimacija));
            command.Parameters.Add(new SQLiteParameter("@Osiguranje", OsiguranjeAOOP));
            command.Parameters.Add(new SQLiteParameter("@KaskoOsig", Kasko));
            command.Parameters.Add(new SQLiteParameter("@ParkingMarkice", ParkMarkice));
            command.ExecuteNonQuery();
        }
        
        public void obrisiVozilo()
        {
            string deleteSql = "DELETE FROM tbVozila WHERE SlBrVozila = @SlBr; DELETE FROM tbServisi WHERE IDVozila = @SlBr;"+
                                " DELETE FROM tbInterniPregled WHERE IDPreglVozila = @SlBr";
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SQLiteParameter("@SlBr", SlBr));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        
        public List<Vozila> ucitajVozila()
        {
            List<Vozila> vozila = new List<Vozila>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbVozila;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);               
                connection.Open();
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    Vozila cars;
                    while (reader.Read())
                    {
                        cars = new Vozila();
                        cars.SlBr = Int32.Parse(reader["SlBrVozila"].ToString());
                        cars.Proizvodjac = reader["Proizvodjac"].ToString();
                        cars.Model = reader["Model"].ToString();
                        cars.BrSasije = reader["BrSasije"].ToString();
                        cars.Karoserija = reader["Karoserija"].ToString();
                        cars.BrVrata = Int32.Parse(reader["BrVrata"].ToString());
                        cars.BrSedista = Int32.Parse(reader["BrSedista"].ToString());
                        cars.Masa = Int32.Parse(reader["Masa"].ToString());
                        cars.Boja = reader["Boja"].ToString();
                        cars.GodProizvodnje = Int32.Parse(reader["GodProizvodnje"].ToString());
                        cars.MProizvodnje = reader["MProizvodnje"].ToString();
                        cars.BrMotora = reader["BrMotora"].ToString();
                        cars.Zapremina = Int32.Parse(reader["Zapremina"].ToString());
                        cars.Standard = reader["Standard"].ToString();
                        cars.KW = Int32.Parse(reader["kW"].ToString());
                        cars.KonjskeSnage = Int32.Parse(reader["KonjskeSnage"].ToString());
                        cars.Gorivo = reader["Gorivo"].ToString();
                        cars.Rezervoar = Int32.Parse(reader["Rezervoar"].ToString());
                        cars.TipMenjaca = reader["TipMenjaca"].ToString();
                        cars.BrBrzina = Int32.Parse(reader["BrBrzina"].ToString());
                        cars.DatumKupovine = Convert.ToDateTime(reader["DatumKupovine"]);
                        cars.Stanje = reader["Stanje"].ToString();
                        cars.Vlasnik = reader["Vlasnik"].ToString();
                        cars.KilometrazaKupljenog = Int32.Parse(reader["KilometrazaKupljen"].ToString());
                        try { cars.PredjKilometraza = Int32.Parse(reader["PredjKilometraza"].ToString()); } catch { }
                        cars.RegistarskiBr = reader["RegistarskiBr"].ToString();
                        cars.RegistrIstice = Convert.ToDateTime(reader["RegistrIstice"]);
                        cars.KomunalniBroj = reader["KomunalniBroj"].ToString();
                        try { cars.SaobrInspektor = Convert.ToDateTime(reader["PregledInspektora"]); } catch { }
                        try { cars.TehnickiPregled =  Convert.ToDateTime(reader["Tehnicki"]); } catch { }
                        try { cars.PregledTaksimetra =  Convert.ToDateTime(reader["PregledTaksimetra"]); } catch { }
                        try { cars.TaksiLegitimacija =  Convert.ToDateTime(reader["TaksiLeg"]); } catch { }
                        try { cars.OsiguranjeAOOP =  Convert.ToDateTime(reader["Osiguranje"]); } catch { }
                        try { cars.Kasko =  Convert.ToDateTime(reader["KaskoOsig"]); } catch { }
                        try { cars.PPAparat =  Convert.ToDateTime(reader["PPAparat"]); } catch { }
                        try { cars.ParkMarkice =  Convert.ToDateTime(reader["ParkingMarkice"]); } catch { }

                        vozila.Add(cars);
                    }
                }
            }
            return vozila;
        }
    }
}
