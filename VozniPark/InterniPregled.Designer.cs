﻿namespace VozniPark
{
    partial class InterniPregled
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKilometrazaPregl = new System.Windows.Forms.TextBox();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.btnOdbaci = new System.Windows.Forms.Button();
            this.txtBMB = new System.Windows.Forms.TextBox();
            this.txtPregledVrsio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrimedbe = new System.Windows.Forms.TextBox();
            this.gbxOstali = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTNGCNG = new System.Windows.Forms.TextBox();
            this.chkSpoljOstecenje = new System.Windows.Forms.CheckBox();
            this.chkUnutOstecenje = new System.Windows.Forms.CheckBox();
            this.chkOstecenjeBrenda = new System.Windows.Forms.CheckBox();
            this.chkObOprema = new System.Windows.Forms.CheckBox();
            this.chkPPAparat = new System.Windows.Forms.CheckBox();
            this.tpDatumPregleda = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.gbxOsnovni.SuspendLayout();
            this.gbxOstali.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblRegistracija);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.label7);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 121);
            this.gbxOsnovni.TabIndex = 78;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 24);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistracija.Location = new System.Drawing.Point(103, 47);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(89, 17);
            this.lblRegistracija.TabIndex = 24;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 70);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registracija:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 93);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 17);
            this.label4.TabIndex = 82;
            this.label4.Text = "Datum pregleda";
            // 
            // txtKilometrazaPregl
            // 
            this.txtKilometrazaPregl.Location = new System.Drawing.Point(490, 43);
            this.txtKilometrazaPregl.Name = "txtKilometrazaPregl";
            this.txtKilometrazaPregl.Size = new System.Drawing.Size(129, 22);
            this.txtKilometrazaPregl.TabIndex = 2;
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSacuvaj.Location = new System.Drawing.Point(335, 245);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(78, 68);
            this.btnSacuvaj.TabIndex = 12;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnOdbaci
            // 
            this.btnOdbaci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdbaci.Location = new System.Drawing.Point(469, 245);
            this.btnOdbaci.Name = "btnOdbaci";
            this.btnOdbaci.Size = new System.Drawing.Size(78, 68);
            this.btnOdbaci.TabIndex = 13;
            this.btnOdbaci.Text = "Odbaci";
            this.btnOdbaci.UseVisualStyleBackColor = true;
            this.btnOdbaci.Click += new System.EventHandler(this.btnOdbaci_Click);
            // 
            // txtBMB
            // 
            this.txtBMB.Location = new System.Drawing.Point(13, 233);
            this.txtBMB.Name = "txtBMB";
            this.txtBMB.Size = new System.Drawing.Size(221, 22);
            this.txtBMB.TabIndex = 9;
            // 
            // txtPregledVrsio
            // 
            this.txtPregledVrsio.Location = new System.Drawing.Point(287, 96);
            this.txtPregledVrsio.Name = "txtPregledVrsio";
            this.txtPregledVrsio.Size = new System.Drawing.Size(332, 22);
            this.txtPregledVrsio.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(290, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 17);
            this.label6.TabIndex = 80;
            this.label6.Text = "Pregledao vozilo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(318, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 17);
            this.label5.TabIndex = 81;
            this.label5.Text = "Primedbe:";
            // 
            // txtPrimedbe
            // 
            this.txtPrimedbe.Location = new System.Drawing.Point(281, 38);
            this.txtPrimedbe.Multiline = true;
            this.txtPrimedbe.Name = "txtPrimedbe";
            this.txtPrimedbe.Size = new System.Drawing.Size(315, 168);
            this.txtPrimedbe.TabIndex = 11;
            // 
            // gbxOstali
            // 
            this.gbxOstali.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gbxOstali.Controls.Add(this.btnOdbaci);
            this.gbxOstali.Controls.Add(this.label10);
            this.gbxOstali.Controls.Add(this.label9);
            this.gbxOstali.Controls.Add(this.label8);
            this.gbxOstali.Controls.Add(this.btnSacuvaj);
            this.gbxOstali.Controls.Add(this.label11);
            this.gbxOstali.Controls.Add(this.label5);
            this.gbxOstali.Controls.Add(this.txtTNGCNG);
            this.gbxOstali.Controls.Add(this.txtBMB);
            this.gbxOstali.Controls.Add(this.txtPrimedbe);
            this.gbxOstali.Controls.Add(this.chkSpoljOstecenje);
            this.gbxOstali.Controls.Add(this.chkUnutOstecenje);
            this.gbxOstali.Controls.Add(this.chkOstecenjeBrenda);
            this.gbxOstali.Controls.Add(this.chkObOprema);
            this.gbxOstali.Controls.Add(this.chkPPAparat);
            this.gbxOstali.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOstali.Location = new System.Drawing.Point(12, 139);
            this.gbxOstali.Name = "gbxOstali";
            this.gbxOstali.Size = new System.Drawing.Size(616, 352);
            this.gbxOstali.TabIndex = 85;
            this.gbxOstali.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 17);
            this.label10.TabIndex = 82;
            this.label10.Text = "TNG / CNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 17);
            this.label9.TabIndex = 82;
            this.label9.Text = "BMB";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 20);
            this.label8.TabIndex = 82;
            this.label8.Text = "Stanje goriva:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(219, 17);
            this.label11.TabIndex = 81;
            this.label11.Text = "Čekirati stavke koje su uredu";
            // 
            // txtTNGCNG
            // 
            this.txtTNGCNG.Location = new System.Drawing.Point(13, 291);
            this.txtTNGCNG.Name = "txtTNGCNG";
            this.txtTNGCNG.Size = new System.Drawing.Size(221, 22);
            this.txtTNGCNG.TabIndex = 10;
            // 
            // chkSpoljOstecenje
            // 
            this.chkSpoljOstecenje.AutoSize = true;
            this.chkSpoljOstecenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSpoljOstecenje.Location = new System.Drawing.Point(13, 86);
            this.chkSpoljOstecenje.Name = "chkSpoljOstecenje";
            this.chkSpoljOstecenje.Size = new System.Drawing.Size(159, 21);
            this.chkSpoljOstecenje.TabIndex = 6;
            this.chkSpoljOstecenje.Text = "Spoljno oštećenje";
            this.chkSpoljOstecenje.UseVisualStyleBackColor = true;
            // 
            // chkUnutOstecenje
            // 
            this.chkUnutOstecenje.AutoSize = true;
            this.chkUnutOstecenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUnutOstecenje.Location = new System.Drawing.Point(13, 114);
            this.chkUnutOstecenje.Name = "chkUnutOstecenje";
            this.chkUnutOstecenje.Size = new System.Drawing.Size(202, 21);
            this.chkUnutOstecenje.TabIndex = 7;
            this.chkUnutOstecenje.Text = "Oštećenje unutrašnjosti";
            this.chkUnutOstecenje.UseVisualStyleBackColor = true;
            // 
            // chkOstecenjeBrenda
            // 
            this.chkOstecenjeBrenda.AutoSize = true;
            this.chkOstecenjeBrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOstecenjeBrenda.Location = new System.Drawing.Point(13, 142);
            this.chkOstecenjeBrenda.Name = "chkOstecenjeBrenda";
            this.chkOstecenjeBrenda.Size = new System.Drawing.Size(159, 21);
            this.chkOstecenjeBrenda.TabIndex = 8;
            this.chkOstecenjeBrenda.Text = "Oštećenje brenda";
            this.chkOstecenjeBrenda.UseVisualStyleBackColor = true;
            // 
            // chkObOprema
            // 
            this.chkObOprema.AutoSize = true;
            this.chkObOprema.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkObOprema.Location = new System.Drawing.Point(13, 58);
            this.chkObOprema.Name = "chkObOprema";
            this.chkObOprema.Size = new System.Drawing.Size(162, 21);
            this.chkObOprema.TabIndex = 5;
            this.chkObOprema.Text = "Obavezna oprema";
            this.chkObOprema.UseVisualStyleBackColor = true;
            // 
            // chkPPAparat
            // 
            this.chkPPAparat.AutoSize = true;
            this.chkPPAparat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPPAparat.Location = new System.Drawing.Point(13, 32);
            this.chkPPAparat.Name = "chkPPAparat";
            this.chkPPAparat.Size = new System.Drawing.Size(103, 21);
            this.chkPPAparat.TabIndex = 4;
            this.chkPPAparat.Text = "PP Aparat";
            this.chkPPAparat.UseVisualStyleBackColor = true;
            // 
            // tpDatumPregleda
            // 
            this.tpDatumPregleda.CustomFormat = "dd.MM.yyyy.";
            this.tpDatumPregleda.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpDatumPregleda.Location = new System.Drawing.Point(287, 43);
            this.tpDatumPregleda.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.tpDatumPregleda.Name = "tpDatumPregleda";
            this.tpDatumPregleda.Size = new System.Drawing.Size(129, 22);
            this.tpDatumPregleda.TabIndex = 1;
            this.tpDatumPregleda.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(487, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 17);
            this.label12.TabIndex = 82;
            this.label12.Text = "Kilometraža";
            // 
            // InterniPregled
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(643, 503);
            this.ControlBox = false;
            this.Controls.Add(this.gbxOsnovni);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtKilometrazaPregl);
            this.Controls.Add(this.txtPregledVrsio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.gbxOstali);
            this.Controls.Add(this.tpDatumPregleda);
            this.MaximumSize = new System.Drawing.Size(661, 550);
            this.MinimumSize = new System.Drawing.Size(661, 550);
            this.Name = "InterniPregled";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Interni periodični pregled";
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            this.gbxOstali.ResumeLayout(false);
            this.gbxOstali.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKilometrazaPregl;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Button btnOdbaci;
        private System.Windows.Forms.TextBox txtBMB;
        private System.Windows.Forms.TextBox txtPregledVrsio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrimedbe;
        private System.Windows.Forms.GroupBox gbxOstali;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTNGCNG;
        private System.Windows.Forms.CheckBox chkSpoljOstecenje;
        private System.Windows.Forms.CheckBox chkUnutOstecenje;
        private System.Windows.Forms.CheckBox chkOstecenjeBrenda;
        private System.Windows.Forms.CheckBox chkObOprema;
        private System.Windows.Forms.CheckBox chkPPAparat;
        private System.Windows.Forms.DateTimePicker tpDatumPregleda;
        private System.Windows.Forms.Label label12;
    }
}