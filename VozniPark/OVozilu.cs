﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using itextsharp.pdfa;
using iTextSharp.xtra;
using System.IO;

namespace VozniPark
{
    public partial class OVozilu : Form
    {
        List<Vozila> vozilaList = new Vozila().ucitajVozila();
        List<KorisniciClass> korLIst = new List<KorisniciClass>();
        public int IDVozila;
        private Naslovna _naslovna;
        string userName;
        public OVozilu(int id, Naslovna nas,string UN)
        {
            userName = UN;
            IDVozila = id;
            _naslovna = nas;
            InitializeComponent();
            ispitajKorisnika();
            lblSluzbeniBroj.Text = IDVozila.ToString();
            prikaziVoziloTxt();
            txtDisabled();
        }

        private void ispitajKorisnika()
        {
            korLIst = new KorisniciClass().ucitajKorisnika(userName);
            if (korLIst[0].UserType != "Admin")
            {
                Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
                lblProizvodjac.Text = selektovanoVozilo.Proizvodjac;
                lblModel.Text = selektovanoVozilo.Model;
                btnIzmeni.Enabled = false;
                btnSnimi.Enabled = false;

                if (korLIst[0].PregledInfo == false)
                {
                    gbxOstali.Visible = false;
                    btnZatvori.Visible = true;
                    btnPDF.Visible = false;
                    lblPristup.Visible = true;
                }

                if (korLIst[0].UnosOdrzavanje == false)
                {
                    btnRedovni.Enabled = false;
                    btnVanredni.Enabled = false;
                    btnPeriodicniPregled.Enabled = false;
                }

                if (korLIst[0].Pretraga == false)
                {
                    btnListaPreg.Visible = false;
                    btnListaServisa.Visible = false;
                }
            }
        }
        
        private void cbxStanje_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxStanje.SelectedItem.ToString() == "Nov")
            {
                txtVlasnik.Enabled = false;
                txtVlasnik.Text = "";
            }
            else
            {
                txtVlasnik.Enabled = true;
                Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
                txtVlasnik.Text = selektovanoVozilo.Vlasnik;
            }
        }

        private void txtDisabled()
        { 
            string str1 = tpDatKupovine.Value.Date.Day.ToString() +"."+tpDatKupovine.Value.Date.Month.ToString()+"."+tpDatKupovine.Value.Date.Year.ToString();
            string str2 = tpRegistrovanDo.Value.Date.Day.ToString()+"."+tpRegistrovanDo.Value.Date.Month.ToString()+"."+tpRegistrovanDo.Value.Date.Year.ToString();
            string str3 = tpSaobrInspektor.Value.Date.Day.ToString() + "." + tpSaobrInspektor.Value.Date.Month.ToString() + "." + tpSaobrInspektor.Value.Date.Year.ToString();
            string str4 = tpTehnickiPregled.Value.Date.Day.ToString() + "." + tpTehnickiPregled.Value.Date.Month.ToString() + "." + tpTehnickiPregled.Value.Date.Year.ToString();
            string str5 = tpPregledTaksimetra.Value.Date.Day.ToString() + "." + tpPregledTaksimetra.Value.Date.Month.ToString() + "." + tpPregledTaksimetra.Value.Date.Year.ToString();
            string str6 = tpTaksiLegitimacija.Value.Date.Day.ToString() + "." + tpTaksiLegitimacija.Value.Date.Month.ToString() + "." + tpTaksiLegitimacija.Value.Date.Year.ToString();
            string str7 = tpOsiguranjeAOiOP.Value.Date.Day.ToString() + "." + tpOsiguranjeAOiOP.Value.Date.Month.ToString() + "." + tpOsiguranjeAOiOP.Value.Date.Year.ToString();
            string str8 = tpKasko.Value.Date.Day.ToString() + "." + tpKasko.Value.Date.Month.ToString() + "." + tpKasko.Value.Date.Year.ToString();
            string str9 = tpPPAparat.Value.Date.Day.ToString() + "." + tpPPAparat.Value.Date.Month.ToString() + "." + tpPPAparat.Value.Date.Year.ToString();
            string str10 = tpParkMarkice.Value.Date.Day.ToString() + "." + tpParkMarkice.Value.Date.Month.ToString() + "." + tpParkMarkice.Value.Date.Year.ToString();
            
            if (str1 == "1.1.2000") tpDatKupovine.Visible = false;
            if (str2 == "1.1.2000") tpRegistrovanDo.Visible = false;
            if (str3 == "1.1.2000") tpSaobrInspektor.Visible = false;
            if (str4 == "1.1.2000") tpTehnickiPregled.Visible = false; 
            if (str5 == "1.1.2000") tpPregledTaksimetra.Visible = false; 
            if (str6 == "1.1.2000") tpTaksiLegitimacija.Visible = false; 
            if (str7 == "1.1.2000") tpOsiguranjeAOiOP.Visible = false; 
            if (str8 == "1.1.2000") tpKasko.Visible = false; 
            if (str9 == "1.1.2000") tpPPAparat.Visible = false; 
            if (str10 == "1.1.2000") tpParkMarkice.Visible = false;
            txtBrSasije.Enabled = false;
            txtKaroserija.Enabled = false;
            txtBrVrata.Enabled = false;
            txtBrSedista.Enabled = false;
            txtMasa.Enabled = false;
            txtBoja.Enabled = false;
            txtGodProizvodnje.Enabled = false;
            cbxMProizvodnje.Enabled = false;
            txtBrMotora.Enabled = false;
            txtZapremina.Enabled = false;
            txtStandard.Enabled = false;
            txtKW.Enabled = false;
            txtKonjske.Enabled = false;
            txtGorivo.Enabled = false;
            txtRezervoar.Enabled = false;
            txtMenjac.Enabled = false;
            txtBrBrzina.Enabled = false;
            tpDatKupovine.Enabled = false;
            cbxStanje.Enabled = false;
            txtVlasnik.Enabled = false;
            txtKilometrazaKupljen.Enabled = false;
            txtKilometraza.Enabled = false;
            txtRegistracija.Enabled = false;
            tpRegistrovanDo.Enabled = false;
            txtKomunalniBr.Enabled = false;
            tpSaobrInspektor.Enabled = false;
            tpTehnickiPregled.Enabled = false;
            tpPregledTaksimetra.Enabled = false;
            tpTaksiLegitimacija.Enabled = false;
            tpOsiguranjeAOiOP.Enabled = false;
            tpKasko.Enabled = false;
            tpPPAparat.Enabled = false;
            tpParkMarkice.Enabled = false;
        }

        private void txtEnabled()
        {
            tpDatKupovine.Visible = true;
            tpRegistrovanDo.Visible = true;
            tpSaobrInspektor.Visible = true;
            tpTehnickiPregled.Visible = true;
            tpPregledTaksimetra.Visible = true;
            tpTaksiLegitimacija.Visible = true;
            tpOsiguranjeAOiOP.Visible = true;
            tpKasko.Visible = true;
            tpPPAparat.Visible = true;
            tpParkMarkice.Visible = true;
            txtBrSasije.Enabled = true;
            txtKaroserija.Enabled = true;
            txtBrVrata.Enabled = true;
            txtBrSedista.Enabled = true;
            txtMasa.Enabled = true;
            txtBoja.Enabled = true;
            txtGodProizvodnje.Enabled = true;
            cbxMProizvodnje.Enabled = true;
            txtBrMotora.Enabled = true;
            txtZapremina.Enabled = true;
            txtStandard.Enabled = true;
            txtKW.Enabled = true;
            txtKonjske.Enabled = true;
            txtGorivo.Enabled = true;
            txtRezervoar.Enabled = true;
            txtMenjac.Enabled = true;
            txtBrBrzina.Enabled = true;
            tpDatKupovine.Enabled = true;
            cbxStanje.Enabled = true;
            txtVlasnik.Enabled = true;
            txtKilometrazaKupljen.Enabled = true;
            txtKilometraza.Enabled = true;
            txtRegistracija.Enabled = true;
            tpRegistrovanDo.Enabled = true;
            txtKomunalniBr.Enabled = true;
            tpSaobrInspektor.Enabled = true;
            tpTehnickiPregled.Enabled = true;
            tpPregledTaksimetra.Enabled = true;
            tpTaksiLegitimacija.Enabled = true;
            tpOsiguranjeAOiOP.Enabled = true;
            tpKasko.Enabled = true;
            tpPPAparat.Enabled = true;
            tpParkMarkice.Enabled = true;
        }

        private void prikaziVoziloTxt()
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            
                lblProizvodjac.Text = selektovanoVozilo.Proizvodjac;
                lblModel.Text = selektovanoVozilo.Model;
                txtBrSasije.Text = selektovanoVozilo.BrSasije;
                txtKaroserija.Text = selektovanoVozilo.Karoserija;
                if (selektovanoVozilo.BrVrata !=0) txtBrVrata.Text = selektovanoVozilo.BrVrata.ToString();
                if (selektovanoVozilo.BrSedista != 0) txtBrSedista.Text = selektovanoVozilo.BrSedista.ToString();
                if (selektovanoVozilo.Masa != 0) txtMasa.Text = selektovanoVozilo.Masa.ToString();
                txtBoja.Text = selektovanoVozilo.Boja;
                if (selektovanoVozilo.GodProizvodnje != 0) txtGodProizvodnje.Text = selektovanoVozilo.GodProizvodnje.ToString();
                cbxMProizvodnje.Text = selektovanoVozilo.MProizvodnje;
                txtBrMotora.Text = selektovanoVozilo.BrMotora;
                if (selektovanoVozilo.Zapremina != 0) txtZapremina.Text = selektovanoVozilo.Zapremina.ToString();
                txtStandard.Text = selektovanoVozilo.Standard;
                if (selektovanoVozilo.KW != 0) txtKW.Text = selektovanoVozilo.KW.ToString();
                if (selektovanoVozilo.KonjskeSnage != 0) txtKonjske.Text = selektovanoVozilo.KonjskeSnage.ToString();
                txtGorivo.Text = selektovanoVozilo.Gorivo;
                if (selektovanoVozilo.Rezervoar != 0) txtRezervoar.Text = selektovanoVozilo.Rezervoar.ToString();
                txtMenjac.Text = selektovanoVozilo.TipMenjaca;
                if (selektovanoVozilo.BrBrzina != 0) txtBrBrzina.Text = selektovanoVozilo.BrBrzina.ToString();
                tpDatKupovine.Value = selektovanoVozilo.DatumKupovine;
                cbxStanje.Text = selektovanoVozilo.Stanje;
                txtVlasnik.Text = selektovanoVozilo.Vlasnik;
                if (selektovanoVozilo.KilometrazaKupljenog != 0) txtKilometrazaKupljen.Text = selektovanoVozilo.KilometrazaKupljenog.ToString();
                if (selektovanoVozilo.PredjKilometraza != 0) txtKilometraza.Text = selektovanoVozilo.PredjKilometraza.ToString();
                txtRegistracija.Text = selektovanoVozilo.RegistarskiBr;
                tpRegistrovanDo.Value = selektovanoVozilo.RegistrIstice;
                txtKomunalniBr.Text = selektovanoVozilo.KomunalniBroj;
                try { tpSaobrInspektor.Value = selektovanoVozilo.SaobrInspektor; } catch { }
                try { tpTehnickiPregled.Value = selektovanoVozilo.TehnickiPregled; } catch { }
                try { tpPregledTaksimetra.Value = selektovanoVozilo.PregledTaksimetra; } catch { }
                try { tpTaksiLegitimacija.Value = selektovanoVozilo.TaksiLegitimacija; } catch { }
                try { tpOsiguranjeAOiOP.Value = selektovanoVozilo.OsiguranjeAOOP; } catch { }
                try { tpKasko.Value = selektovanoVozilo.Kasko; } catch { }
                try { tpPPAparat.Value = selektovanoVozilo.PPAparat; } catch { }
                try { tpParkMarkice.Value = selektovanoVozilo.ParkMarkice; } catch { }
        }
        
        private void btnZatvori_Click(object sender, EventArgs e)
        {
            if (btnSnimi.Enabled == true)
            {
                DialogResult r = MessageBox.Show("Da li želite da sačuvate izmene?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (r == DialogResult.Yes)
                {
                    snimiVozilo();
                    this.Close();
                }
                else if (r == DialogResult.No)
                    this.Close();
            }
            else this.Close();
            _naslovna.OmoguciDugmad();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            txtEnabled();
            btnSnimi.Enabled = true;
            btnIzmeni.Enabled = false;
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            txtDisabled();
            btnSnimi.Enabled = false;
            btnIzmeni.Enabled = true;
            snimiVozilo();
        }

        private void snimiVozilo()
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            selektovanoVozilo.BrSasije = txtBrSasije.Text;
            selektovanoVozilo.Karoserija = txtKaroserija.Text;
            try { selektovanoVozilo.BrVrata = int.Parse(txtBrVrata.Text); } catch { }
            try { selektovanoVozilo.BrSedista = int.Parse(txtBrSedista.Text); } catch { }
            try { selektovanoVozilo.Masa = int.Parse(txtMasa.Text); } catch { }
            selektovanoVozilo.Boja = txtBoja.Text;
            try { selektovanoVozilo.GodProizvodnje = int.Parse(txtGodProizvodnje.Text); } catch { }
            selektovanoVozilo.MProizvodnje = cbxMProizvodnje.Text;
            selektovanoVozilo.BrMotora = txtBrMotora.Text;
            try { selektovanoVozilo.Zapremina = int.Parse(txtZapremina.Text); } catch { }
            selektovanoVozilo.Standard = txtStandard.Text;
            try { selektovanoVozilo.KW = int.Parse(txtKW.Text); } catch { }
            try { selektovanoVozilo.KonjskeSnage = int.Parse(txtKonjske.Text); } catch { }
            selektovanoVozilo.Gorivo = txtGorivo.Text;
            try { selektovanoVozilo.Rezervoar = int.Parse(txtRezervoar.Text); } catch { }
            selektovanoVozilo.TipMenjaca = txtMenjac.Text;
            try { selektovanoVozilo.BrBrzina = int.Parse(txtBrBrzina.Text); } catch { }
            selektovanoVozilo.DatumKupovine = tpDatKupovine.Value.Date;
            selektovanoVozilo.Stanje = cbxStanje.Text;
            selektovanoVozilo.Vlasnik = txtVlasnik.Text;
            try { selektovanoVozilo.KilometrazaKupljenog = int.Parse(txtKilometrazaKupljen.Text); } catch { }
            try { selektovanoVozilo.PredjKilometraza = int.Parse(txtKilometraza.Text); } catch { }
            selektovanoVozilo.RegistarskiBr = txtRegistracija.Text;
            selektovanoVozilo.RegistrIstice = tpRegistrovanDo.Value.Date;
            selektovanoVozilo.KomunalniBroj = txtKomunalniBr.Text;
            selektovanoVozilo.SaobrInspektor = tpSaobrInspektor.Value;
            selektovanoVozilo.TehnickiPregled = tpTehnickiPregled.Value;
            selektovanoVozilo.PregledTaksimetra = tpPregledTaksimetra.Value;
            selektovanoVozilo.TaksiLegitimacija = tpTaksiLegitimacija.Value;
            selektovanoVozilo.OsiguranjeAOOP = tpOsiguranjeAOiOP.Value;
            selektovanoVozilo.Kasko = tpKasko.Value;
            selektovanoVozilo.PPAparat = tpPPAparat.Value;
            selektovanoVozilo.ParkMarkice = tpParkMarkice.Value;
            selektovanoVozilo.azurirajVozilo();
            _naslovna.prikaziVozilaDGV();
        }

        private void btnRedovni_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            RedovniServis redServ = new 
                RedovniServis(IDVozila, selektovanoVozilo.RegistarskiBr, selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            redServ.Show();
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            SaveFileDialog svg = new SaveFileDialog();
            svg.InitialDirectory = @"c:\User\Desktop\";
            svg.FileName = "O vozilu-"+selektovanoVozilo.SlBr+" - "+selektovanoVozilo.RegistarskiBr+" - "+selektovanoVozilo.Proizvodjac+" "+selektovanoVozilo.Model;
            svg.ShowDialog();

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(svg.FileName + ".pdf", FileMode.Create));
            doc.Open();

            string ARIAL = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIALUNI.TTF");
            BaseFont bf = BaseFont.CreateFont(ARIAL, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            iTextSharp.text.Image Logo = iTextSharp.text.Image.GetInstance("Logo.jpg");
            Logo.ScalePercent(50);
            Logo.Alignment = iTextSharp.text.Image.ALIGN_RIGHT | iTextSharp.text.Image.TEXTWRAP;
            
            iTextSharp.text.Font fontOsnovni = new iTextSharp.text.Font(bf, 14, iTextSharp.text.Font.BOLDITALIC);
            iTextSharp.text.Font fontOstali = new iTextSharp.text.Font(bf, 11, iTextSharp.text.Font.NORMAL);

            string strOsnovni = string.Format("OSNOVNI PODACI O VOZILU{0}Službeni broj vozila: {1}{0}Registarski broj: {2}{0}Proizvođač: "+
                            "{3}{0}Model: {4}{0}", System.Environment.NewLine, IDVozila.ToString(), selektovanoVozilo.RegistarskiBr, 
                            selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            string strDatum = string.Format("{1}{1}U Zrenjaninu {0:dd.MM.yyyy.}", DateTime.Today, Environment.NewLine);

            Phrase Osnovni = new Phrase(strOsnovni, fontOsnovni);
            Paragraph parDatum = new Paragraph(strDatum);
                        
            string strOstali1 = string.Format("Broj šasije: {1}{0}{0}Tip Karoserije {2}{0}{0}Broj vrata; {3}{0}{0}Broj sedišta: {4}{0}{0}" +
                            "Masa: {5}{0}{0}Boja: {6}{0}{0}Godina proizvodnje: {7}{0}{0}Mesec proizvodnje: {8}{0}{0}Broj motora: {9}{0}{0}" +
                            "Zapremina ccm: {10}{0}{0}Standard: {11}{0}{0}Broj kW: {12}{0}{0}Broj konjskih snaga: {13}{0}{0}Gorivo: {14}{0}{0}"+
                            "Zapremina rezervoara: {15} lit.{0}{0}Tip menjača: {16}{0}{0}Broj stepeni prenosa: {17}", 
                            System.Environment.NewLine, selektovanoVozilo.BrSasije, selektovanoVozilo.Karoserija,selektovanoVozilo.BrVrata,
                            selektovanoVozilo.BrSedista, selektovanoVozilo.Masa, selektovanoVozilo.Boja, selektovanoVozilo.GodProizvodnje,
                            selektovanoVozilo.MProizvodnje, selektovanoVozilo.BrMotora, selektovanoVozilo.Zapremina, selektovanoVozilo.Standard,
                            selektovanoVozilo.KW, selektovanoVozilo.KonjskeSnage, selektovanoVozilo.Gorivo, selektovanoVozilo.Rezervoar,
                            selektovanoVozilo.TipMenjaca, selektovanoVozilo.BrBrzina);

            string strOstali2 = string.Format("Datum kupovine: {1:dd.MM.yyyy.}{0}{0}Stanje kupljenog: {2}{0}{0}Prethodni vlasnik: {3}{0}{0}" +
                            "Kilometraža kupljenog: {4}{0}{0}Registrovan do: {5:dd.MM.yyyy.}{0}{0}Komunalni broj: {6}{0}{0}Pređena kilometraža: {7}{0}{0}" +
                            "Sledeći pregled saobraćajnog inspektora: {8:dd.MM.yyyy.}{0}{0}Tehnički pregled: {9:dd.MM.yyyy.}{0}{0}" +
                            "Pregled taksimetra: {10:dd.MM.yyyy.}{0}{0}Taksi legitimacija: {11:dd.MM.yyyy.}{0}{0}Osiguranje AO i OP: {12:dd.MM.yyyy.}{0}{0}" +
                            "Kasko osiguranje: {13:dd.MM.yyyy.}{0}{0}PP aparat: {14:dd.MM.yyyy.}{0}{0}Parking markice: {15:dd.MM.yyyy.}",
                            System.Environment.NewLine, selektovanoVozilo.DatumKupovine, selektovanoVozilo.Stanje, selektovanoVozilo.Vlasnik,
                            selektovanoVozilo.KilometrazaKupljenog, selektovanoVozilo.RegistrIstice, selektovanoVozilo.KomunalniBroj,
                            selektovanoVozilo.PredjKilometraza, selektovanoVozilo.SaobrInspektor, selektovanoVozilo.TehnickiPregled,
                            selektovanoVozilo.PregledTaksimetra, selektovanoVozilo.TaksiLegitimacija, selektovanoVozilo.OsiguranjeAOOP,
                            selektovanoVozilo.Kasko, selektovanoVozilo.PPAparat, selektovanoVozilo.ParkMarkice);

            Phrase Ostali1 = new Phrase(strOstali1, fontOstali);
            Phrase Ostali2 = new Phrase(strOstali2, fontOstali);
            
            PdfPTable table = new PdfPTable(2);
            table.TotalWidth = doc.PageSize.Width-72;
            table.LockedWidth = true;

            table.AddCell(Ostali1);
            table.AddCell(Ostali2);

            

            doc.Add(Logo);
            doc.Add(Osnovni);
            doc.Add(table);
            doc.Add(parDatum);
            doc.Close();


        }

        private void btnListaServisa_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            ListaServisa listServ = new 
                ListaServisa(IDVozila, selektovanoVozilo.RegistarskiBr, selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            listServ.Show();
        }

        private void btnVanredni_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            VanredniServis redServ = new
                VanredniServis(IDVozila, selektovanoVozilo.RegistarskiBr, selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            redServ.Show();
        }

        private void btnPeriodicniPregled_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            InterniPregled intPr = new
                InterniPregled(IDVozila, selektovanoVozilo.RegistarskiBr, selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            intPr.Show();
        }

        private void btnListaPreg_Click(object sender, EventArgs e)
        {
            Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == IDVozila).FirstOrDefault();
            ListaPregleda lp = new ListaPregleda(IDVozila, selektovanoVozilo.RegistarskiBr, selektovanoVozilo.Proizvodjac, selektovanoVozilo.Model);
            lp.Show();
        }

        private void txtBrVrata_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        
    }
}
