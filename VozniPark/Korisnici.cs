﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class Korisnici : Form
    {
        List<KorisniciClass> korisniciList = new List<KorisniciClass>();
        Naslovna nas;
        public Korisnici(Naslovna naslovna)
        {
            nas = naslovna;
            InitializeComponent();
            dgKorisnici.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            prikaziKorisnikeDGV();
        }

        public void btnEnabled()
        {
            btnDodaj.Enabled = true;
            btnIzmeni.Enabled = true;
            btnObrisi.Enabled = true;
            btnZatvori.Enabled = true;
            btnLogovanja.Enabled = true;
        }

        public void dgKorisnici_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgKorisnici.SelectedRows[0].Cells["kolTipKorisnika"].Value.ToString() == "Admin")
                { btnObrisi.Enabled = false; }
                else { btnObrisi.Enabled = true; }
            }
            catch { }
        }

        public void prikaziKorisnikeDGV()
        {
            korisniciList = new KorisniciClass().ucitajKorisnike();
            dgKorisnici.Rows.Clear();
            for (int i = 0; i < korisniciList.Count; i++)
            {
                dgKorisnici.Rows.Add();
                dgKorisnici.Rows[i].Cells["ID"].Value = korisniciList[i].UserID;
                dgKorisnici.Rows[i].Cells["kolKorisnik"].Value = korisniciList[i].UserName;
                dgKorisnici.Rows[i].Cells["kolTipKorisnika"].Value = korisniciList[i].UserType;
                dgKorisnici.Rows[i].Cells["kolImePrezime"].Value = korisniciList[i].ImePrezime;

                dgKorisnici.CurrentCell = null;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            btnDodaj.Enabled = false;
            btnObrisi.Enabled = false;
            btnIzmeni.Enabled = false;
            btnZatvori.Enabled = false;
            btnLogovanja.Enabled = false;
            UpravljajKorisnikom Dodaj = new UpravljajKorisnikom(this,"Dodaj");
            Dodaj.FormBorderStyle = FormBorderStyle.None;
            Dodaj.TopLevel = false;
            Dodaj.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(Dodaj);
            Dodaj.Show();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgKorisnici.SelectedRows.Count > 0)
            {
                int idSelektovanog = (int)dgKorisnici.SelectedRows[0].Cells["ID"].Value;

                if (MessageBox.Show("Da li zelite da obrisete odabranog korisnika?",
                    "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    

                    KorisniciClass selektovaniKorisnik = korisniciList.Where(x => x.UserID == idSelektovanog).FirstOrDefault();

                    if (selektovaniKorisnik != null)
                    {
                        selektovaniKorisnik.obrisiKorisnika();
                    }
                    prikaziKorisnikeDGV();
                }
            }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            btnIzmeni.Enabled = false;
            btnZatvori.Enabled = false;
            btnDodaj.Enabled = false;
            btnObrisi.Enabled = false;
            btnLogovanja.Enabled = false;
            UpravljajKorisnikom Dodaj = new UpravljajKorisnikom(this, dgKorisnici.SelectedRows[0].Cells["ID"].Value.ToString());
            Dodaj.FormBorderStyle = FormBorderStyle.None;
            Dodaj.TopLevel = false;
            Dodaj.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(Dodaj);
            Dodaj.Show();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            nas.OmoguciDugmad();
            this.Close();
        }

        private void btnLogovanja_Click(object sender, EventArgs e)
        {
            int idSelektovanog = (int)dgKorisnici.SelectedRows[0].Cells["ID"].Value;
            btnDodaj.Enabled = false;
            btnObrisi.Enabled = false;
            btnIzmeni.Enabled = false;
            btnZatvori.Enabled = false;
            btnLogovanja.Enabled = false;
            LogovanjaKorisnika Logovanja = new LogovanjaKorisnika(this, idSelektovanog);
            Logovanja.FormBorderStyle = FormBorderStyle.None;
            Logovanja.TopLevel = false;
            Logovanja.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(Logovanja);
            Logovanja.Show();
        }
    }
}
