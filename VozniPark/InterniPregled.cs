﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class InterniPregled : Form
    {
        public string Pregled = "";
        private int _IDVoz;
        public InterniPregled(int id, string Reg, string Proiz, string Model)
        {
            InitializeComponent();
            _IDVoz = id;
            lblSluzbeniBroj.Text = id.ToString();
            lblRegistracija.Text = Reg;
            lblProizvodjac.Text = Proiz;
            lblModel.Text = Model;
            tpDatumPregleda.Value = DateTime.Today;
        }

        private void ispitajCekirano()
        {
            Pregled += "PPAparat: ";
            if (chkPPAparat.Checked == true)
                Pregled += " OK#";
            else Pregled += "  -#";
            Pregled += "Obavezna oprema: ";
            if (chkObOprema.Checked == true)
                Pregled += " OK#";
            else Pregled += "  -#";
            Pregled += "Spoljno oštećenje: ";
            if (chkSpoljOstecenje.Checked == true)
                Pregled += " Nema#";
            else Pregled += "  -#";
            Pregled += "Oštećenje unutrašnjosti: ";
            if (chkUnutOstecenje.Checked == true)
                Pregled += " Nema#";
            else Pregled += "  -#";
            Pregled += "OŠtećenje brenda: ";
            if (chkOstecenjeBrenda.Checked == true)
                Pregled += " Nema#";
            else Pregled += "  -#";
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (txtKilometrazaPregl.Text != "" && txtPregledVrsio.Text != "")
            {
                if (MessageBox.Show("Da li ste sigurni da želite da sačuvate?", "Potvrda čuvanja", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ispitajCekirano();
                    IntPregled intPreg = new IntPregled();
                    intPreg.DatumPregleda = tpDatumPregleda.Value.Date;
                    try { intPreg.KilometrazaPregl = Int32.Parse(txtKilometrazaPregl.Text); }
                    catch { MessageBox.Show("Pogrešan format kilometraže.\nMorate uneti tačan BROJ pređenih kilometara pri pregledu.",
                                            "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
                    intPreg.VrsioPregled = txtPregledVrsio.Text;
                    intPreg.Pregledano = Pregled;
                    intPreg.StanjeBMB = txtBMB.Text;
                    intPreg.StanjeTNGCNG = txtTNGCNG.Text;
                    intPreg.Primedbe = txtPrimedbe.Text;
                    Vozila voz = new Vozila();
                    voz.SlBr = _IDVoz;
                    intPreg.Vozilo = voz;
                    intPreg.dodajPregled();
                    this.Close();
                }
            }
            else MessageBox.Show("Morate uneti kilometražu i ko je vršio pregled vozila.",
                                    "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnOdbaci_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da želite da odbacite pregled?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                this.Close();
        }
    }
}
