﻿namespace VozniPark
{
    partial class VanredniServis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilterKlime = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSenzorPritGasa = new System.Windows.Forms.TextBox();
            this.chkSenzorPritGasa = new System.Windows.Forms.CheckBox();
            this.txtInjector = new System.Windows.Forms.TextBox();
            this.chkInjector = new System.Windows.Forms.CheckBox();
            this.txtSetDelovaRemont = new System.Windows.Forms.TextBox();
            this.chkSetDelRemontIspa = new System.Windows.Forms.CheckBox();
            this.chkSenzorPokGasa = new System.Windows.Forms.CheckBox();
            this.txtSetCrevaVoda = new System.Windows.Forms.TextBox();
            this.chkMultiventil = new System.Windows.Forms.CheckBox();
            this.chkSetCrevaVoda = new System.Windows.Forms.CheckBox();
            this.chkPriljucakUtakGasa = new System.Windows.Forms.CheckBox();
            this.txtSenzorPokazGasa = new System.Windows.Forms.TextBox();
            this.txtFilterTecneFaze = new System.Windows.Forms.TextBox();
            this.txtMultiventil = new System.Windows.Forms.TextBox();
            this.chkGumCrevaGasFaze = new System.Windows.Forms.CheckBox();
            this.txtPrikljucakUtakGasa = new System.Windows.Forms.TextBox();
            this.chkFilterTecneFaze = new System.Windows.Forms.CheckBox();
            this.txtGumCrevaGasFaze = new System.Windows.Forms.TextBox();
            this.chkFilterGasneFaze = new System.Windows.Forms.CheckBox();
            this.txtFilterGasneFaze = new System.Windows.Forms.TextBox();
            this.txtPKKais = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKilometrazaServ = new System.Windows.Forms.TextBox();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.btnOdbaci = new System.Windows.Forms.Button();
            this.txtSvecice = new System.Windows.Forms.TextBox();
            this.txtZupKaisLanac = new System.Windows.Forms.TextBox();
            this.txtUljeMenjac = new System.Windows.Forms.TextBox();
            this.txtUlje = new System.Windows.Forms.TextBox();
            this.txtFilterUlja = new System.Windows.Forms.TextBox();
            this.txtFilterGoriva = new System.Windows.Forms.TextBox();
            this.txtTecnostKocenje = new System.Windows.Forms.TextBox();
            this.txtFilterVazduha = new System.Windows.Forms.TextBox();
            this.txtZamenaGuma = new System.Windows.Forms.TextBox();
            this.txtKoJeVrsio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOstaloTekst = new System.Windows.Forms.TextBox();
            this.chkPKKais = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkUljeMenjac = new System.Windows.Forms.CheckBox();
            this.chkZamenaGuma = new System.Windows.Forms.CheckBox();
            this.gbxOstali = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMaskaPr = new System.Windows.Forms.TextBox();
            this.chkPrLeviFar = new System.Windows.Forms.CheckBox();
            this.chkPrednjaMaska = new System.Windows.Forms.CheckBox();
            this.chkZadnjiBranik = new System.Windows.Forms.CheckBox();
            this.txtMigavacLe = new System.Windows.Forms.TextBox();
            this.txtKrov = new System.Windows.Forms.TextBox();
            this.txtPoklMotora = new System.Windows.Forms.TextBox();
            this.txtRatkapaZaDe = new System.Windows.Forms.TextBox();
            this.txtPrednjiBranik = new System.Windows.Forms.TextBox();
            this.txtRatkapaPrDe = new System.Windows.Forms.TextBox();
            this.txtPragDe = new System.Windows.Forms.TextBox();
            this.txtStopLe = new System.Windows.Forms.TextBox();
            this.txtRetrovizorDe = new System.Windows.Forms.TextBox();
            this.txtMaglZaDe = new System.Windows.Forms.TextBox();
            this.txtZaDeKrilo = new System.Windows.Forms.TextBox();
            this.chkKrov = new System.Windows.Forms.CheckBox();
            this.txtVrataZaLe = new System.Windows.Forms.TextBox();
            this.txtFarPrLe = new System.Windows.Forms.TextBox();
            this.chkPrednjiBranik = new System.Windows.Forms.CheckBox();
            this.txtFarPrDe = new System.Windows.Forms.TextBox();
            this.txtZadnjiBranik = new System.Windows.Forms.TextBox();
            this.txtSvetloTablice = new System.Windows.Forms.TextBox();
            this.txtPrLeKrilo = new System.Windows.Forms.TextBox();
            this.txtMaglPrLe = new System.Windows.Forms.TextBox();
            this.txtVrataZaDe = new System.Windows.Forms.TextBox();
            this.txtRatkapaZaLe = new System.Windows.Forms.TextBox();
            this.txtPrDeKrilo = new System.Windows.Forms.TextBox();
            this.txtRatkapaPrLe = new System.Windows.Forms.TextBox();
            this.txtPragLe = new System.Windows.Forms.TextBox();
            this.chkZaDeSvetlo = new System.Windows.Forms.CheckBox();
            this.txtRetrovizorLe = new System.Windows.Forms.TextBox();
            this.chkZaLeMagl = new System.Windows.Forms.CheckBox();
            this.chkPrLevaVrata = new System.Windows.Forms.CheckBox();
            this.chkPrDesniFar = new System.Windows.Forms.CheckBox();
            this.chkPrDesnaVrata = new System.Windows.Forms.CheckBox();
            this.chkPrDesnaMagl = new System.Windows.Forms.CheckBox();
            this.chkPrLevoKrilo = new System.Windows.Forms.CheckBox();
            this.txtMigavacDe = new System.Windows.Forms.TextBox();
            this.chkZaLevoKrilo = new System.Windows.Forms.CheckBox();
            this.chkPrLevaMagl = new System.Windows.Forms.CheckBox();
            this.txtPoklPrtljaznika = new System.Windows.Forms.TextBox();
            this.txtMaglPrDe = new System.Windows.Forms.TextBox();
            this.chkPrDesnoKrilo = new System.Windows.Forms.CheckBox();
            this.chkZaLeSvetlo = new System.Windows.Forms.CheckBox();
            this.txtZaLeKrilo = new System.Windows.Forms.TextBox();
            this.txtStopDe = new System.Windows.Forms.TextBox();
            this.chkZaDesnoKrilo = new System.Windows.Forms.CheckBox();
            this.txtMaglZaLe = new System.Windows.Forms.TextBox();
            this.txtVrataPrLe = new System.Windows.Forms.TextBox();
            this.chkZaLeRatkapa = new System.Windows.Forms.CheckBox();
            this.txtVrataPrDe = new System.Windows.Forms.TextBox();
            this.chkDesniMigavac = new System.Windows.Forms.CheckBox();
            this.chkLeviPrag = new System.Windows.Forms.CheckBox();
            this.chkPrLeRatkapa = new System.Windows.Forms.CheckBox();
            this.chkPoklPrtljaznika = new System.Windows.Forms.CheckBox();
            this.chkLeviMigavac = new System.Windows.Forms.CheckBox();
            this.chkLeviRetrovizor = new System.Windows.Forms.CheckBox();
            this.chkSvetloTablice = new System.Windows.Forms.CheckBox();
            this.chkPoklMotora = new System.Windows.Forms.CheckBox();
            this.chkZaDeRatkapa = new System.Windows.Forms.CheckBox();
            this.chkZaDesnaVrata = new System.Windows.Forms.CheckBox();
            this.chkZaDeMagl = new System.Windows.Forms.CheckBox();
            this.chkDesniPrag = new System.Windows.Forms.CheckBox();
            this.chkPrDeRatkapa = new System.Windows.Forms.CheckBox();
            this.chkZaLevaVrata = new System.Windows.Forms.CheckBox();
            this.chkDesniRetrovizor = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkFilterUlja = new System.Windows.Forms.CheckBox();
            this.txtUljeKolicina = new System.Windows.Forms.TextBox();
            this.chkFilterGoriva = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkTecnostKocenje = new System.Windows.Forms.CheckBox();
            this.chkSvecice = new System.Windows.Forms.CheckBox();
            this.chkUlje = new System.Windows.Forms.CheckBox();
            this.chkFilterKlime = new System.Windows.Forms.CheckBox();
            this.chkFilterVazduha = new System.Windows.Forms.CheckBox();
            this.chkZupKaisLanac = new System.Windows.Forms.CheckBox();
            this.txtPotrosniMaterijal = new System.Windows.Forms.TextBox();
            this.txtLezajTockaZa = new System.Windows.Forms.TextBox();
            this.txtGumiceBalansSta = new System.Windows.Forms.TextBox();
            this.txtLezajTockaPr = new System.Windows.Forms.TextBox();
            this.txtPopravkaBrenda = new System.Windows.Forms.TextBox();
            this.txtLamela = new System.Windows.Forms.TextBox();
            this.txtKrajSpone = new System.Windows.Forms.TextBox();
            this.txtStabilizatorZa = new System.Windows.Forms.TextBox();
            this.txtStabilizatorPr = new System.Windows.Forms.TextBox();
            this.txtSijalica = new System.Windows.Forms.TextBox();
            this.txtBaterijaKljuca = new System.Windows.Forms.TextBox();
            this.txtSetKvacila = new System.Windows.Forms.TextBox();
            this.txtAmortizeriZa = new System.Windows.Forms.TextBox();
            this.chkLezajTockaZa = new System.Windows.Forms.CheckBox();
            this.chkGumiceBalansSta = new System.Windows.Forms.CheckBox();
            this.chkPotrosniMaterijal = new System.Windows.Forms.CheckBox();
            this.chkLezajTockaPr = new System.Windows.Forms.CheckBox();
            this.txtRashlTecMotora = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.chkKrajSpone = new System.Windows.Forms.CheckBox();
            this.chkPopravkaBrenda = new System.Windows.Forms.CheckBox();
            this.chkStabilizatorZa = new System.Windows.Forms.CheckBox();
            this.chkLamela = new System.Windows.Forms.CheckBox();
            this.chkStabilizatorPr = new System.Windows.Forms.CheckBox();
            this.txtAmortizeriPr = new System.Windows.Forms.TextBox();
            this.chkSijalica = new System.Windows.Forms.CheckBox();
            this.txtTecnostStakla = new System.Windows.Forms.TextBox();
            this.chkBaterijaKljuca = new System.Windows.Forms.CheckBox();
            this.chkSetKvacila = new System.Windows.Forms.CheckBox();
            this.txtAlternator = new System.Windows.Forms.TextBox();
            this.txtPodloskaCepa = new System.Windows.Forms.TextBox();
            this.chkAmortizerZa = new System.Windows.Forms.CheckBox();
            this.chkRashlTecMotora = new System.Windows.Forms.CheckBox();
            this.txtAnlaser = new System.Windows.Forms.TextBox();
            this.txtMetliceBrisZa = new System.Windows.Forms.TextBox();
            this.chkAmortizerPr = new System.Windows.Forms.CheckBox();
            this.chkTecnostStakla = new System.Windows.Forms.CheckBox();
            this.txtZamajac = new System.Windows.Forms.TextBox();
            this.chkAlternator = new System.Windows.Forms.CheckBox();
            this.txtMetliceBrisPr = new System.Windows.Forms.TextBox();
            this.chkPodloska = new System.Windows.Forms.CheckBox();
            this.chkAnlaser = new System.Windows.Forms.CheckBox();
            this.txtPlocicePakneZa = new System.Windows.Forms.TextBox();
            this.chkMetliceBrisZa = new System.Windows.Forms.CheckBox();
            this.chkZamajac = new System.Windows.Forms.CheckBox();
            this.txtDiskDobosiZa = new System.Windows.Forms.TextBox();
            this.chkMetliceBrisPr = new System.Windows.Forms.CheckBox();
            this.txtDiskoviPr = new System.Windows.Forms.TextBox();
            this.chkPlocicePakneZa = new System.Windows.Forms.CheckBox();
            this.txtDiskPlocicePr = new System.Windows.Forms.TextBox();
            this.chkDiskDobosiZa = new System.Windows.Forms.CheckBox();
            this.txtAkumulator = new System.Windows.Forms.TextBox();
            this.chkDiskoviPr = new System.Windows.Forms.CheckBox();
            this.txtTecnostKlime = new System.Windows.Forms.TextBox();
            this.txtOstaloCena = new System.Windows.Forms.TextBox();
            this.chkDiskPlocicePr = new System.Windows.Forms.CheckBox();
            this.chkAkumulator = new System.Windows.Forms.CheckBox();
            this.chkOstalo = new System.Windows.Forms.CheckBox();
            this.chkTecnostKlime = new System.Windows.Forms.CheckBox();
            this.tpDatumServisa = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSveciceKolicina = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUljeMenjacKolicina = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTecnostKocKolicina = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTecnostKlimeKol = new System.Windows.Forms.TextBox();
            this.txtBrisaciKolicina = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTecnostStaklaKol = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRashlTecMotKol = new System.Windows.Forms.TextBox();
            this.txtSijalicaKolicina = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtStabilizatorPrKol = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtStabilizatorZaKol = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLezajTockaPrKol = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtLezajTockaZaKol = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtKrajSponeKol = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtGumeKolicina = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtInjectorKol = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.gbxOsnovni.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbxOstali.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblRegistracija);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.label7);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 121);
            this.gbxOsnovni.TabIndex = 78;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 24);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistracija.Location = new System.Drawing.Point(103, 47);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(89, 17);
            this.lblRegistracija.TabIndex = 24;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 70);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registracija:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 93);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // txtFilterKlime
            // 
            this.txtFilterKlime.Enabled = false;
            this.txtFilterKlime.Location = new System.Drawing.Point(200, 102);
            this.txtFilterKlime.Name = "txtFilterKlime";
            this.txtFilterKlime.Size = new System.Drawing.Size(80, 22);
            this.txtFilterKlime.TabIndex = 78;
            this.txtFilterKlime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.MediumTurquoise;
            this.groupBox1.Controls.Add(this.txtSenzorPritGasa);
            this.groupBox1.Controls.Add(this.chkSenzorPritGasa);
            this.groupBox1.Controls.Add(this.txtInjector);
            this.groupBox1.Controls.Add(this.chkInjector);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtSetDelovaRemont);
            this.groupBox1.Controls.Add(this.chkSetDelRemontIspa);
            this.groupBox1.Controls.Add(this.chkSenzorPokGasa);
            this.groupBox1.Controls.Add(this.txtSetCrevaVoda);
            this.groupBox1.Controls.Add(this.chkMultiventil);
            this.groupBox1.Controls.Add(this.chkSetCrevaVoda);
            this.groupBox1.Controls.Add(this.chkPriljucakUtakGasa);
            this.groupBox1.Controls.Add(this.txtSenzorPokazGasa);
            this.groupBox1.Controls.Add(this.txtFilterTecneFaze);
            this.groupBox1.Controls.Add(this.txtInjectorKol);
            this.groupBox1.Controls.Add(this.txtMultiventil);
            this.groupBox1.Controls.Add(this.chkGumCrevaGasFaze);
            this.groupBox1.Controls.Add(this.txtPrikljucakUtakGasa);
            this.groupBox1.Controls.Add(this.chkFilterTecneFaze);
            this.groupBox1.Controls.Add(this.txtGumCrevaGasFaze);
            this.groupBox1.Controls.Add(this.chkFilterGasneFaze);
            this.groupBox1.Controls.Add(this.txtFilterGasneFaze);
            this.groupBox1.Location = new System.Drawing.Point(0, 333);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 366);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TNG/CNG";
            // 
            // txtSenzorPritGasa
            // 
            this.txtSenzorPritGasa.Enabled = false;
            this.txtSenzorPritGasa.Location = new System.Drawing.Point(238, 297);
            this.txtSenzorPritGasa.Name = "txtSenzorPritGasa";
            this.txtSenzorPritGasa.Size = new System.Drawing.Size(80, 22);
            this.txtSenzorPritGasa.TabIndex = 78;
            this.txtSenzorPritGasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkSenzorPritGasa
            // 
            this.chkSenzorPritGasa.AutoSize = true;
            this.chkSenzorPritGasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSenzorPritGasa.Location = new System.Drawing.Point(13, 299);
            this.chkSenzorPritGasa.Name = "chkSenzorPritGasa";
            this.chkSenzorPritGasa.Size = new System.Drawing.Size(179, 21);
            this.chkSenzorPritGasa.TabIndex = 79;
            this.chkSenzorPritGasa.Text = "Senzor pritiska gasa";
            this.chkSenzorPritGasa.UseVisualStyleBackColor = true;
            this.chkSenzorPritGasa.CheckedChanged += new System.EventHandler(this.chkSenzorPritGasa_CheckedChanged);
            // 
            // txtInjector
            // 
            this.txtInjector.Enabled = false;
            this.txtInjector.Location = new System.Drawing.Point(238, 228);
            this.txtInjector.Name = "txtInjector";
            this.txtInjector.Size = new System.Drawing.Size(80, 22);
            this.txtInjector.TabIndex = 78;
            this.txtInjector.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkInjector
            // 
            this.chkInjector.AutoSize = true;
            this.chkInjector.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInjector.Location = new System.Drawing.Point(13, 230);
            this.chkInjector.Name = "chkInjector";
            this.chkInjector.Size = new System.Drawing.Size(84, 21);
            this.chkInjector.TabIndex = 79;
            this.chkInjector.Text = "Injector";
            this.chkInjector.UseVisualStyleBackColor = true;
            this.chkInjector.CheckedChanged += new System.EventHandler(this.chkInjector_CheckedChanged);
            // 
            // txtSetDelovaRemont
            // 
            this.txtSetDelovaRemont.Enabled = false;
            this.txtSetDelovaRemont.Location = new System.Drawing.Point(238, 151);
            this.txtSetDelovaRemont.Name = "txtSetDelovaRemont";
            this.txtSetDelovaRemont.Size = new System.Drawing.Size(80, 22);
            this.txtSetDelovaRemont.TabIndex = 78;
            this.txtSetDelovaRemont.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkSetDelRemontIspa
            // 
            this.chkSetDelRemontIspa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSetDelRemontIspa.Location = new System.Drawing.Point(13, 144);
            this.chkSetDelRemontIspa.Name = "chkSetDelRemontIspa";
            this.chkSetDelRemontIspa.Size = new System.Drawing.Size(184, 38);
            this.chkSetDelRemontIspa.TabIndex = 79;
            this.chkSetDelRemontIspa.Text = "Set delova za remont isparivača";
            this.chkSetDelRemontIspa.UseVisualStyleBackColor = true;
            this.chkSetDelRemontIspa.CheckedChanged += new System.EventHandler(this.chkSetDelRemontIspa_CheckedChanged);
            // 
            // chkSenzorPokGasa
            // 
            this.chkSenzorPokGasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSenzorPokGasa.Location = new System.Drawing.Point(13, 325);
            this.chkSenzorPokGasa.Name = "chkSenzorPokGasa";
            this.chkSenzorPokGasa.Size = new System.Drawing.Size(167, 38);
            this.chkSenzorPokGasa.TabIndex = 79;
            this.chkSenzorPokGasa.Text = "Senzor pokazivača nivoa gasa";
            this.chkSenzorPokGasa.UseVisualStyleBackColor = true;
            this.chkSenzorPokGasa.CheckedChanged += new System.EventHandler(this.chkSenzorPokGasa_CheckedChanged);
            // 
            // txtSetCrevaVoda
            // 
            this.txtSetCrevaVoda.Enabled = false;
            this.txtSetCrevaVoda.Location = new System.Drawing.Point(238, 73);
            this.txtSetCrevaVoda.Name = "txtSetCrevaVoda";
            this.txtSetCrevaVoda.Size = new System.Drawing.Size(80, 22);
            this.txtSetCrevaVoda.TabIndex = 78;
            this.txtSetCrevaVoda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkMultiventil
            // 
            this.chkMultiventil.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMultiventil.Location = new System.Drawing.Point(13, 256);
            this.chkMultiventil.Name = "chkMultiventil";
            this.chkMultiventil.Size = new System.Drawing.Size(140, 38);
            this.chkMultiventil.TabIndex = 79;
            this.chkMultiventil.Text = "Multiventil sa elektroventilom";
            this.chkMultiventil.UseVisualStyleBackColor = true;
            this.chkMultiventil.CheckedChanged += new System.EventHandler(this.chkMultiventil_CheckedChanged);
            // 
            // chkSetCrevaVoda
            // 
            this.chkSetCrevaVoda.AutoSize = true;
            this.chkSetCrevaVoda.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSetCrevaVoda.Location = new System.Drawing.Point(13, 75);
            this.chkSetCrevaVoda.Name = "chkSetCrevaVoda";
            this.chkSetCrevaVoda.Size = new System.Drawing.Size(161, 21);
            this.chkSetCrevaVoda.TabIndex = 79;
            this.chkSetCrevaVoda.Text = "Set creva za vodu";
            this.chkSetCrevaVoda.UseVisualStyleBackColor = true;
            this.chkSetCrevaVoda.CheckedChanged += new System.EventHandler(this.chkSetCrevaVoda_CheckedChanged);
            // 
            // chkPriljucakUtakGasa
            // 
            this.chkPriljucakUtakGasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPriljucakUtakGasa.Location = new System.Drawing.Point(13, 187);
            this.chkPriljucakUtakGasa.Name = "chkPriljucakUtakGasa";
            this.chkPriljucakUtakGasa.Size = new System.Drawing.Size(166, 38);
            this.chkPriljucakUtakGasa.TabIndex = 79;
            this.chkPriljucakUtakGasa.Text = "Priključak spoljnog utakanja gasa";
            this.chkPriljucakUtakGasa.UseVisualStyleBackColor = true;
            this.chkPriljucakUtakGasa.CheckedChanged += new System.EventHandler(this.chkPriljucakUtakGasa_CheckedChanged);
            // 
            // txtSenzorPokazGasa
            // 
            this.txtSenzorPokazGasa.Enabled = false;
            this.txtSenzorPokazGasa.Location = new System.Drawing.Point(238, 332);
            this.txtSenzorPokazGasa.Name = "txtSenzorPokazGasa";
            this.txtSenzorPokazGasa.Size = new System.Drawing.Size(80, 22);
            this.txtSenzorPokazGasa.TabIndex = 78;
            this.txtSenzorPokazGasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtFilterTecneFaze
            // 
            this.txtFilterTecneFaze.Enabled = false;
            this.txtFilterTecneFaze.Location = new System.Drawing.Point(238, 21);
            this.txtFilterTecneFaze.Name = "txtFilterTecneFaze";
            this.txtFilterTecneFaze.Size = new System.Drawing.Size(80, 22);
            this.txtFilterTecneFaze.TabIndex = 78;
            this.txtFilterTecneFaze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtMultiventil
            // 
            this.txtMultiventil.Enabled = false;
            this.txtMultiventil.Location = new System.Drawing.Point(238, 263);
            this.txtMultiventil.Name = "txtMultiventil";
            this.txtMultiventil.Size = new System.Drawing.Size(80, 22);
            this.txtMultiventil.TabIndex = 78;
            this.txtMultiventil.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkGumCrevaGasFaze
            // 
            this.chkGumCrevaGasFaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGumCrevaGasFaze.Location = new System.Drawing.Point(13, 101);
            this.chkGumCrevaGasFaze.Name = "chkGumCrevaGasFaze";
            this.chkGumCrevaGasFaze.Size = new System.Drawing.Size(135, 38);
            this.chkGumCrevaGasFaze.TabIndex = 79;
            this.chkGumCrevaGasFaze.Text = "Gumena creva gasne faze";
            this.chkGumCrevaGasFaze.UseVisualStyleBackColor = true;
            this.chkGumCrevaGasFaze.CheckedChanged += new System.EventHandler(this.chkGumCrevaGasFaze_CheckedChanged);
            // 
            // txtPrikljucakUtakGasa
            // 
            this.txtPrikljucakUtakGasa.Enabled = false;
            this.txtPrikljucakUtakGasa.Location = new System.Drawing.Point(238, 194);
            this.txtPrikljucakUtakGasa.Name = "txtPrikljucakUtakGasa";
            this.txtPrikljucakUtakGasa.Size = new System.Drawing.Size(80, 22);
            this.txtPrikljucakUtakGasa.TabIndex = 78;
            this.txtPrikljucakUtakGasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkFilterTecneFaze
            // 
            this.chkFilterTecneFaze.AutoSize = true;
            this.chkFilterTecneFaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterTecneFaze.Location = new System.Drawing.Point(13, 23);
            this.chkFilterTecneFaze.Name = "chkFilterTecneFaze";
            this.chkFilterTecneFaze.Size = new System.Drawing.Size(148, 21);
            this.chkFilterTecneFaze.TabIndex = 79;
            this.chkFilterTecneFaze.Text = "Filter tečne faze";
            this.chkFilterTecneFaze.UseVisualStyleBackColor = true;
            this.chkFilterTecneFaze.CheckedChanged += new System.EventHandler(this.chkFilterTecneFaze_CheckedChanged);
            // 
            // txtGumCrevaGasFaze
            // 
            this.txtGumCrevaGasFaze.Enabled = false;
            this.txtGumCrevaGasFaze.Location = new System.Drawing.Point(238, 108);
            this.txtGumCrevaGasFaze.Name = "txtGumCrevaGasFaze";
            this.txtGumCrevaGasFaze.Size = new System.Drawing.Size(80, 22);
            this.txtGumCrevaGasFaze.TabIndex = 78;
            this.txtGumCrevaGasFaze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkFilterGasneFaze
            // 
            this.chkFilterGasneFaze.AutoSize = true;
            this.chkFilterGasneFaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterGasneFaze.Location = new System.Drawing.Point(13, 49);
            this.chkFilterGasneFaze.Name = "chkFilterGasneFaze";
            this.chkFilterGasneFaze.Size = new System.Drawing.Size(152, 21);
            this.chkFilterGasneFaze.TabIndex = 79;
            this.chkFilterGasneFaze.Text = "Filter gasne faze";
            this.chkFilterGasneFaze.UseVisualStyleBackColor = true;
            this.chkFilterGasneFaze.CheckedChanged += new System.EventHandler(this.chkFilterGasneFaze_CheckedChanged);
            // 
            // txtFilterGasneFaze
            // 
            this.txtFilterGasneFaze.Enabled = false;
            this.txtFilterGasneFaze.Location = new System.Drawing.Point(238, 47);
            this.txtFilterGasneFaze.Name = "txtFilterGasneFaze";
            this.txtFilterGasneFaze.Size = new System.Drawing.Size(80, 22);
            this.txtFilterGasneFaze.TabIndex = 78;
            this.txtFilterGasneFaze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPKKais
            // 
            this.txtPKKais.Enabled = false;
            this.txtPKKais.Location = new System.Drawing.Point(200, 228);
            this.txtPKKais.Name = "txtPKKais";
            this.txtPKKais.Size = new System.Drawing.Size(80, 22);
            this.txtPKKais.TabIndex = 78;
            this.txtPKKais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 82;
            this.label4.Text = "Datum servisa";
            // 
            // txtKilometrazaServ
            // 
            this.txtKilometrazaServ.Location = new System.Drawing.Point(392, 74);
            this.txtKilometrazaServ.Name = "txtKilometrazaServ";
            this.txtKilometrazaServ.Size = new System.Drawing.Size(129, 22);
            this.txtKilometrazaServ.TabIndex = 2;
            this.txtKilometrazaServ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKilometrazaServ_KeyPress);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSacuvaj.Location = new System.Drawing.Point(1032, 33);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(78, 68);
            this.btnSacuvaj.TabIndex = 4;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnOdbaci
            // 
            this.btnOdbaci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdbaci.Location = new System.Drawing.Point(1149, 32);
            this.btnOdbaci.Name = "btnOdbaci";
            this.btnOdbaci.Size = new System.Drawing.Size(78, 68);
            this.btnOdbaci.TabIndex = 5;
            this.btnOdbaci.Text = "Odbaci";
            this.btnOdbaci.UseVisualStyleBackColor = true;
            this.btnOdbaci.Click += new System.EventHandler(this.btnOdbaci_Click);
            // 
            // txtSvecice
            // 
            this.txtSvecice.Enabled = false;
            this.txtSvecice.Location = new System.Drawing.Point(200, 156);
            this.txtSvecice.Name = "txtSvecice";
            this.txtSvecice.Size = new System.Drawing.Size(80, 22);
            this.txtSvecice.TabIndex = 78;
            this.txtSvecice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtZupKaisLanac
            // 
            this.txtZupKaisLanac.Enabled = false;
            this.txtZupKaisLanac.Location = new System.Drawing.Point(200, 308);
            this.txtZupKaisLanac.Name = "txtZupKaisLanac";
            this.txtZupKaisLanac.Size = new System.Drawing.Size(80, 22);
            this.txtZupKaisLanac.TabIndex = 78;
            this.txtZupKaisLanac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtUljeMenjac
            // 
            this.txtUljeMenjac.Enabled = false;
            this.txtUljeMenjac.Location = new System.Drawing.Point(200, 267);
            this.txtUljeMenjac.Name = "txtUljeMenjac";
            this.txtUljeMenjac.Size = new System.Drawing.Size(80, 22);
            this.txtUljeMenjac.TabIndex = 78;
            this.txtUljeMenjac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtUlje
            // 
            this.txtUlje.Enabled = false;
            this.txtUlje.Location = new System.Drawing.Point(200, 129);
            this.txtUlje.Name = "txtUlje";
            this.txtUlje.Size = new System.Drawing.Size(80, 22);
            this.txtUlje.TabIndex = 78;
            this.txtUlje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtFilterUlja
            // 
            this.txtFilterUlja.Enabled = false;
            this.txtFilterUlja.Location = new System.Drawing.Point(200, 21);
            this.txtFilterUlja.Name = "txtFilterUlja";
            this.txtFilterUlja.Size = new System.Drawing.Size(80, 22);
            this.txtFilterUlja.TabIndex = 78;
            this.txtFilterUlja.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtFilterGoriva
            // 
            this.txtFilterGoriva.Enabled = false;
            this.txtFilterGoriva.Location = new System.Drawing.Point(200, 48);
            this.txtFilterGoriva.Name = "txtFilterGoriva";
            this.txtFilterGoriva.Size = new System.Drawing.Size(80, 22);
            this.txtFilterGoriva.TabIndex = 78;
            this.txtFilterGoriva.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtTecnostKocenje
            // 
            this.txtTecnostKocenje.Enabled = false;
            this.txtTecnostKocenje.Location = new System.Drawing.Point(200, 194);
            this.txtTecnostKocenje.Name = "txtTecnostKocenje";
            this.txtTecnostKocenje.Size = new System.Drawing.Size(80, 22);
            this.txtTecnostKocenje.TabIndex = 78;
            this.txtTecnostKocenje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtFilterVazduha
            // 
            this.txtFilterVazduha.Enabled = false;
            this.txtFilterVazduha.Location = new System.Drawing.Point(200, 75);
            this.txtFilterVazduha.Name = "txtFilterVazduha";
            this.txtFilterVazduha.Size = new System.Drawing.Size(80, 22);
            this.txtFilterVazduha.TabIndex = 78;
            this.txtFilterVazduha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtZamenaGuma
            // 
            this.txtZamenaGuma.Enabled = false;
            this.txtZamenaGuma.Location = new System.Drawing.Point(1181, 201);
            this.txtZamenaGuma.Name = "txtZamenaGuma";
            this.txtZamenaGuma.Size = new System.Drawing.Size(80, 22);
            this.txtZamenaGuma.TabIndex = 78;
            this.txtZamenaGuma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtKoJeVrsio
            // 
            this.txtKoJeVrsio.Location = new System.Drawing.Point(575, 72);
            this.txtKoJeVrsio.Name = "txtKoJeVrsio";
            this.txtKoJeVrsio.Size = new System.Drawing.Size(332, 22);
            this.txtKoJeVrsio.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(578, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 17);
            this.label6.TabIndex = 80;
            this.label6.Text = "Ko je vršio servis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(300, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 17);
            this.label5.TabIndex = 81;
            this.label5.Text = "Kilometraža";
            // 
            // txtOstaloTekst
            // 
            this.txtOstaloTekst.Enabled = false;
            this.txtOstaloTekst.Location = new System.Drawing.Point(989, 260);
            this.txtOstaloTekst.Multiline = true;
            this.txtOstaloTekst.Name = "txtOstaloTekst";
            this.txtOstaloTekst.Size = new System.Drawing.Size(272, 86);
            this.txtOstaloTekst.TabIndex = 91;
            // 
            // chkPKKais
            // 
            this.chkPKKais.AutoSize = true;
            this.chkPKKais.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPKKais.Location = new System.Drawing.Point(13, 232);
            this.chkPKKais.Name = "chkPKKais";
            this.chkPKKais.Size = new System.Drawing.Size(122, 21);
            this.chkPKKais.TabIndex = 90;
            this.chkPKKais.Text = "Set PK kaiša";
            this.chkPKKais.UseVisualStyleBackColor = true;
            this.chkPKKais.CheckedChanged += new System.EventHandler(this.chkPKKais_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(237, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 15);
            this.label12.TabIndex = 63;
            this.label12.Text = "Cena u DIN";
            // 
            // chkUljeMenjac
            // 
            this.chkUljeMenjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUljeMenjac.Location = new System.Drawing.Point(13, 256);
            this.chkUljeMenjac.Name = "chkUljeMenjac";
            this.chkUljeMenjac.Size = new System.Drawing.Size(100, 45);
            this.chkUljeMenjac.TabIndex = 79;
            this.chkUljeMenjac.Text = "Ulje za menjač";
            this.chkUljeMenjac.UseVisualStyleBackColor = true;
            this.chkUljeMenjac.CheckedChanged += new System.EventHandler(this.chkUljeMenjac_CheckedChanged);
            // 
            // chkZamenaGuma
            // 
            this.chkZamenaGuma.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZamenaGuma.Location = new System.Drawing.Point(989, 192);
            this.chkZamenaGuma.Name = "chkZamenaGuma";
            this.chkZamenaGuma.Size = new System.Drawing.Size(98, 40);
            this.chkZamenaGuma.TabIndex = 77;
            this.chkZamenaGuma.Text = "Zamena guma";
            this.chkZamenaGuma.UseVisualStyleBackColor = true;
            this.chkZamenaGuma.CheckedChanged += new System.EventHandler(this.chkZamenaGuma_CheckedChanged);
            // 
            // gbxOstali
            // 
            this.gbxOstali.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gbxOstali.Controls.Add(this.groupBox3);
            this.gbxOstali.Controls.Add(this.groupBox1);
            this.gbxOstali.Controls.Add(this.groupBox2);
            this.gbxOstali.Controls.Add(this.txtPotrosniMaterijal);
            this.gbxOstali.Controls.Add(this.label26);
            this.gbxOstali.Controls.Add(this.label24);
            this.gbxOstali.Controls.Add(this.label23);
            this.gbxOstali.Controls.Add(this.label21);
            this.gbxOstali.Controls.Add(this.label25);
            this.gbxOstali.Controls.Add(this.label22);
            this.gbxOstali.Controls.Add(this.label20);
            this.gbxOstali.Controls.Add(this.label17);
            this.gbxOstali.Controls.Add(this.txtLezajTockaZa);
            this.gbxOstali.Controls.Add(this.txtGumeKolicina);
            this.gbxOstali.Controls.Add(this.txtLezajTockaZaKol);
            this.gbxOstali.Controls.Add(this.txtLezajTockaPrKol);
            this.gbxOstali.Controls.Add(this.txtStabilizatorPrKol);
            this.gbxOstali.Controls.Add(this.txtKrajSponeKol);
            this.gbxOstali.Controls.Add(this.txtStabilizatorZaKol);
            this.gbxOstali.Controls.Add(this.txtSijalicaKolicina);
            this.gbxOstali.Controls.Add(this.txtGumiceBalansSta);
            this.gbxOstali.Controls.Add(this.txtBrisaciKolicina);
            this.gbxOstali.Controls.Add(this.txtLezajTockaPr);
            this.gbxOstali.Controls.Add(this.txtRashlTecMotKol);
            this.gbxOstali.Controls.Add(this.txtTecnostStaklaKol);
            this.gbxOstali.Controls.Add(this.txtTecnostKlimeKol);
            this.gbxOstali.Controls.Add(this.txtPopravkaBrenda);
            this.gbxOstali.Controls.Add(this.label19);
            this.gbxOstali.Controls.Add(this.txtLamela);
            this.gbxOstali.Controls.Add(this.label18);
            this.gbxOstali.Controls.Add(this.txtKrajSpone);
            this.gbxOstali.Controls.Add(this.label16);
            this.gbxOstali.Controls.Add(this.txtStabilizatorZa);
            this.gbxOstali.Controls.Add(this.txtStabilizatorPr);
            this.gbxOstali.Controls.Add(this.txtSijalica);
            this.gbxOstali.Controls.Add(this.txtBaterijaKljuca);
            this.gbxOstali.Controls.Add(this.txtSetKvacila);
            this.gbxOstali.Controls.Add(this.txtAmortizeriZa);
            this.gbxOstali.Controls.Add(this.chkLezajTockaZa);
            this.gbxOstali.Controls.Add(this.chkGumiceBalansSta);
            this.gbxOstali.Controls.Add(this.chkPotrosniMaterijal);
            this.gbxOstali.Controls.Add(this.chkLezajTockaPr);
            this.gbxOstali.Controls.Add(this.txtRashlTecMotora);
            this.gbxOstali.Controls.Add(this.label13);
            this.gbxOstali.Controls.Add(this.label11);
            this.gbxOstali.Controls.Add(this.label9);
            this.gbxOstali.Controls.Add(this.label12);
            this.gbxOstali.Controls.Add(this.chkKrajSpone);
            this.gbxOstali.Controls.Add(this.chkPopravkaBrenda);
            this.gbxOstali.Controls.Add(this.chkStabilizatorZa);
            this.gbxOstali.Controls.Add(this.chkLamela);
            this.gbxOstali.Controls.Add(this.chkStabilizatorPr);
            this.gbxOstali.Controls.Add(this.txtAmortizeriPr);
            this.gbxOstali.Controls.Add(this.chkSijalica);
            this.gbxOstali.Controls.Add(this.txtTecnostStakla);
            this.gbxOstali.Controls.Add(this.chkBaterijaKljuca);
            this.gbxOstali.Controls.Add(this.chkSetKvacila);
            this.gbxOstali.Controls.Add(this.txtAlternator);
            this.gbxOstali.Controls.Add(this.txtPodloskaCepa);
            this.gbxOstali.Controls.Add(this.chkAmortizerZa);
            this.gbxOstali.Controls.Add(this.chkRashlTecMotora);
            this.gbxOstali.Controls.Add(this.txtAnlaser);
            this.gbxOstali.Controls.Add(this.txtMetliceBrisZa);
            this.gbxOstali.Controls.Add(this.chkAmortizerPr);
            this.gbxOstali.Controls.Add(this.chkTecnostStakla);
            this.gbxOstali.Controls.Add(this.txtZamajac);
            this.gbxOstali.Controls.Add(this.chkAlternator);
            this.gbxOstali.Controls.Add(this.txtMetliceBrisPr);
            this.gbxOstali.Controls.Add(this.chkPodloska);
            this.gbxOstali.Controls.Add(this.chkAnlaser);
            this.gbxOstali.Controls.Add(this.txtPlocicePakneZa);
            this.gbxOstali.Controls.Add(this.chkMetliceBrisZa);
            this.gbxOstali.Controls.Add(this.chkZamajac);
            this.gbxOstali.Controls.Add(this.txtDiskDobosiZa);
            this.gbxOstali.Controls.Add(this.chkMetliceBrisPr);
            this.gbxOstali.Controls.Add(this.txtDiskoviPr);
            this.gbxOstali.Controls.Add(this.chkPlocicePakneZa);
            this.gbxOstali.Controls.Add(this.txtDiskPlocicePr);
            this.gbxOstali.Controls.Add(this.chkDiskDobosiZa);
            this.gbxOstali.Controls.Add(this.txtAkumulator);
            this.gbxOstali.Controls.Add(this.chkDiskoviPr);
            this.gbxOstali.Controls.Add(this.txtTecnostKlime);
            this.gbxOstali.Controls.Add(this.txtOstaloCena);
            this.gbxOstali.Controls.Add(this.chkDiskPlocicePr);
            this.gbxOstali.Controls.Add(this.txtZamenaGuma);
            this.gbxOstali.Controls.Add(this.chkAkumulator);
            this.gbxOstali.Controls.Add(this.txtOstaloTekst);
            this.gbxOstali.Controls.Add(this.chkOstalo);
            this.gbxOstali.Controls.Add(this.chkTecnostKlime);
            this.gbxOstali.Controls.Add(this.chkZamenaGuma);
            this.gbxOstali.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOstali.Location = new System.Drawing.Point(12, 139);
            this.gbxOstali.Name = "gbxOstali";
            this.gbxOstali.Size = new System.Drawing.Size(1276, 699);
            this.gbxOstali.TabIndex = 85;
            this.gbxOstali.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.groupBox3.Controls.Add(this.txtMaskaPr);
            this.groupBox3.Controls.Add(this.chkPrLeviFar);
            this.groupBox3.Controls.Add(this.chkPrednjaMaska);
            this.groupBox3.Controls.Add(this.chkZadnjiBranik);
            this.groupBox3.Controls.Add(this.txtMigavacLe);
            this.groupBox3.Controls.Add(this.txtKrov);
            this.groupBox3.Controls.Add(this.txtPoklMotora);
            this.groupBox3.Controls.Add(this.txtRatkapaZaDe);
            this.groupBox3.Controls.Add(this.txtPrednjiBranik);
            this.groupBox3.Controls.Add(this.txtRatkapaPrDe);
            this.groupBox3.Controls.Add(this.txtPragDe);
            this.groupBox3.Controls.Add(this.txtStopLe);
            this.groupBox3.Controls.Add(this.txtRetrovizorDe);
            this.groupBox3.Controls.Add(this.txtMaglZaDe);
            this.groupBox3.Controls.Add(this.txtZaDeKrilo);
            this.groupBox3.Controls.Add(this.chkKrov);
            this.groupBox3.Controls.Add(this.txtVrataZaLe);
            this.groupBox3.Controls.Add(this.txtFarPrLe);
            this.groupBox3.Controls.Add(this.chkPrednjiBranik);
            this.groupBox3.Controls.Add(this.txtFarPrDe);
            this.groupBox3.Controls.Add(this.txtZadnjiBranik);
            this.groupBox3.Controls.Add(this.txtSvetloTablice);
            this.groupBox3.Controls.Add(this.txtPrLeKrilo);
            this.groupBox3.Controls.Add(this.txtMaglPrLe);
            this.groupBox3.Controls.Add(this.txtVrataZaDe);
            this.groupBox3.Controls.Add(this.txtRatkapaZaLe);
            this.groupBox3.Controls.Add(this.txtPrDeKrilo);
            this.groupBox3.Controls.Add(this.txtRatkapaPrLe);
            this.groupBox3.Controls.Add(this.txtPragLe);
            this.groupBox3.Controls.Add(this.chkZaDeSvetlo);
            this.groupBox3.Controls.Add(this.txtRetrovizorLe);
            this.groupBox3.Controls.Add(this.chkZaLeMagl);
            this.groupBox3.Controls.Add(this.chkPrLevaVrata);
            this.groupBox3.Controls.Add(this.chkPrDesniFar);
            this.groupBox3.Controls.Add(this.chkPrDesnaVrata);
            this.groupBox3.Controls.Add(this.chkPrDesnaMagl);
            this.groupBox3.Controls.Add(this.chkPrLevoKrilo);
            this.groupBox3.Controls.Add(this.txtMigavacDe);
            this.groupBox3.Controls.Add(this.chkZaLevoKrilo);
            this.groupBox3.Controls.Add(this.chkPrLevaMagl);
            this.groupBox3.Controls.Add(this.txtPoklPrtljaznika);
            this.groupBox3.Controls.Add(this.txtMaglPrDe);
            this.groupBox3.Controls.Add(this.chkPrDesnoKrilo);
            this.groupBox3.Controls.Add(this.chkZaLeSvetlo);
            this.groupBox3.Controls.Add(this.txtZaLeKrilo);
            this.groupBox3.Controls.Add(this.txtStopDe);
            this.groupBox3.Controls.Add(this.chkZaDesnoKrilo);
            this.groupBox3.Controls.Add(this.txtMaglZaLe);
            this.groupBox3.Controls.Add(this.txtVrataPrLe);
            this.groupBox3.Controls.Add(this.chkZaLeRatkapa);
            this.groupBox3.Controls.Add(this.txtVrataPrDe);
            this.groupBox3.Controls.Add(this.chkDesniMigavac);
            this.groupBox3.Controls.Add(this.chkLeviPrag);
            this.groupBox3.Controls.Add(this.chkPrLeRatkapa);
            this.groupBox3.Controls.Add(this.chkPoklPrtljaznika);
            this.groupBox3.Controls.Add(this.chkLeviMigavac);
            this.groupBox3.Controls.Add(this.chkLeviRetrovizor);
            this.groupBox3.Controls.Add(this.chkSvetloTablice);
            this.groupBox3.Controls.Add(this.chkPoklMotora);
            this.groupBox3.Controls.Add(this.chkZaDeRatkapa);
            this.groupBox3.Controls.Add(this.chkZaDesnaVrata);
            this.groupBox3.Controls.Add(this.chkZaDeMagl);
            this.groupBox3.Controls.Add(this.chkDesniPrag);
            this.groupBox3.Controls.Add(this.chkPrDeRatkapa);
            this.groupBox3.Controls.Add(this.chkZaLevaVrata);
            this.groupBox3.Controls.Add(this.chkDesniRetrovizor);
            this.groupBox3.Location = new System.Drawing.Point(334, 209);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(644, 490);
            this.groupBox3.TabIndex = 94;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ispravnost karoserije";
            // 
            // txtMaskaPr
            // 
            this.txtMaskaPr.Enabled = false;
            this.txtMaskaPr.Location = new System.Drawing.Point(554, 26);
            this.txtMaskaPr.Name = "txtMaskaPr";
            this.txtMaskaPr.Size = new System.Drawing.Size(80, 22);
            this.txtMaskaPr.TabIndex = 78;
            this.txtMaskaPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkPrLeviFar
            // 
            this.chkPrLeviFar.AutoSize = true;
            this.chkPrLeviFar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrLeviFar.Location = new System.Drawing.Point(324, 81);
            this.chkPrLeviFar.Name = "chkPrLeviFar";
            this.chkPrLeviFar.Size = new System.Drawing.Size(136, 21);
            this.chkPrLeviFar.TabIndex = 77;
            this.chkPrLeviFar.Text = "Prednji levi far";
            this.chkPrLeviFar.UseVisualStyleBackColor = true;
            this.chkPrLeviFar.CheckedChanged += new System.EventHandler(this.chkPrLeviFar_CheckedChanged);
            // 
            // chkPrednjaMaska
            // 
            this.chkPrednjaMaska.AutoSize = true;
            this.chkPrednjaMaska.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrednjaMaska.Location = new System.Drawing.Point(324, 27);
            this.chkPrednjaMaska.Name = "chkPrednjaMaska";
            this.chkPrednjaMaska.Size = new System.Drawing.Size(137, 21);
            this.chkPrednjaMaska.TabIndex = 77;
            this.chkPrednjaMaska.Text = "Prednja maska";
            this.chkPrednjaMaska.UseVisualStyleBackColor = true;
            this.chkPrednjaMaska.CheckedChanged += new System.EventHandler(this.chkPrednjaMaska_CheckedChanged);
            // 
            // chkZadnjiBranik
            // 
            this.chkZadnjiBranik.AutoSize = true;
            this.chkZadnjiBranik.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZadnjiBranik.Location = new System.Drawing.Point(10, 63);
            this.chkZadnjiBranik.Name = "chkZadnjiBranik";
            this.chkZadnjiBranik.Size = new System.Drawing.Size(125, 21);
            this.chkZadnjiBranik.TabIndex = 77;
            this.chkZadnjiBranik.Text = "Zadnji branik";
            this.chkZadnjiBranik.UseVisualStyleBackColor = true;
            this.chkZadnjiBranik.CheckedChanged += new System.EventHandler(this.chkZadnjiBranik_CheckedChanged);
            // 
            // txtMigavacLe
            // 
            this.txtMigavacLe.Enabled = false;
            this.txtMigavacLe.Location = new System.Drawing.Point(554, 323);
            this.txtMigavacLe.Name = "txtMigavacLe";
            this.txtMigavacLe.Size = new System.Drawing.Size(80, 22);
            this.txtMigavacLe.TabIndex = 78;
            this.txtMigavacLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtKrov
            // 
            this.txtKrov.Enabled = false;
            this.txtKrov.Location = new System.Drawing.Point(554, 53);
            this.txtKrov.Name = "txtKrov";
            this.txtKrov.Size = new System.Drawing.Size(80, 22);
            this.txtKrov.TabIndex = 78;
            this.txtKrov.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPoklMotora
            // 
            this.txtPoklMotora.Enabled = false;
            this.txtPoklMotora.Location = new System.Drawing.Point(220, 314);
            this.txtPoklMotora.Name = "txtPoklMotora";
            this.txtPoklMotora.Size = new System.Drawing.Size(80, 22);
            this.txtPoklMotora.TabIndex = 78;
            this.txtPoklMotora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtRatkapaZaDe
            // 
            this.txtRatkapaZaDe.Enabled = false;
            this.txtRatkapaZaDe.Location = new System.Drawing.Point(554, 458);
            this.txtRatkapaZaDe.Name = "txtRatkapaZaDe";
            this.txtRatkapaZaDe.Size = new System.Drawing.Size(80, 22);
            this.txtRatkapaZaDe.TabIndex = 78;
            this.txtRatkapaZaDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPrednjiBranik
            // 
            this.txtPrednjiBranik.Enabled = false;
            this.txtPrednjiBranik.Location = new System.Drawing.Point(220, 34);
            this.txtPrednjiBranik.Name = "txtPrednjiBranik";
            this.txtPrednjiBranik.Size = new System.Drawing.Size(80, 22);
            this.txtPrednjiBranik.TabIndex = 78;
            this.txtPrednjiBranik.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtRatkapaPrDe
            // 
            this.txtRatkapaPrDe.Enabled = false;
            this.txtRatkapaPrDe.Location = new System.Drawing.Point(554, 404);
            this.txtRatkapaPrDe.Name = "txtRatkapaPrDe";
            this.txtRatkapaPrDe.Size = new System.Drawing.Size(80, 22);
            this.txtRatkapaPrDe.TabIndex = 78;
            this.txtRatkapaPrDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPragDe
            // 
            this.txtPragDe.Enabled = false;
            this.txtPragDe.Location = new System.Drawing.Point(220, 454);
            this.txtPragDe.Name = "txtPragDe";
            this.txtPragDe.Size = new System.Drawing.Size(80, 22);
            this.txtPragDe.TabIndex = 78;
            this.txtPragDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtStopLe
            // 
            this.txtStopLe.Enabled = false;
            this.txtStopLe.Location = new System.Drawing.Point(554, 188);
            this.txtStopLe.Name = "txtStopLe";
            this.txtStopLe.Size = new System.Drawing.Size(80, 22);
            this.txtStopLe.TabIndex = 78;
            this.txtStopLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtRetrovizorDe
            // 
            this.txtRetrovizorDe.Enabled = false;
            this.txtRetrovizorDe.Location = new System.Drawing.Point(220, 398);
            this.txtRetrovizorDe.Name = "txtRetrovizorDe";
            this.txtRetrovizorDe.Size = new System.Drawing.Size(80, 22);
            this.txtRetrovizorDe.TabIndex = 78;
            this.txtRetrovizorDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtMaglZaDe
            // 
            this.txtMaglZaDe.Enabled = false;
            this.txtMaglZaDe.Location = new System.Drawing.Point(554, 269);
            this.txtMaglZaDe.Name = "txtMaglZaDe";
            this.txtMaglZaDe.Size = new System.Drawing.Size(80, 22);
            this.txtMaglZaDe.TabIndex = 78;
            this.txtMaglZaDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtZaDeKrilo
            // 
            this.txtZaDeKrilo.Enabled = false;
            this.txtZaDeKrilo.Location = new System.Drawing.Point(220, 174);
            this.txtZaDeKrilo.Name = "txtZaDeKrilo";
            this.txtZaDeKrilo.Size = new System.Drawing.Size(80, 22);
            this.txtZaDeKrilo.TabIndex = 78;
            this.txtZaDeKrilo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkKrov
            // 
            this.chkKrov.AutoSize = true;
            this.chkKrov.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkKrov.Location = new System.Drawing.Point(324, 53);
            this.chkKrov.Name = "chkKrov";
            this.chkKrov.Size = new System.Drawing.Size(63, 21);
            this.chkKrov.TabIndex = 77;
            this.chkKrov.Text = "Krov";
            this.chkKrov.UseVisualStyleBackColor = true;
            this.chkKrov.CheckedChanged += new System.EventHandler(this.chkKrov_CheckedChanged);
            // 
            // txtVrataZaLe
            // 
            this.txtVrataZaLe.Enabled = false;
            this.txtVrataZaLe.Location = new System.Drawing.Point(220, 258);
            this.txtVrataZaLe.Name = "txtVrataZaLe";
            this.txtVrataZaLe.Size = new System.Drawing.Size(80, 22);
            this.txtVrataZaLe.TabIndex = 78;
            this.txtVrataZaLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtFarPrLe
            // 
            this.txtFarPrLe.Enabled = false;
            this.txtFarPrLe.Location = new System.Drawing.Point(554, 80);
            this.txtFarPrLe.Name = "txtFarPrLe";
            this.txtFarPrLe.Size = new System.Drawing.Size(80, 22);
            this.txtFarPrLe.TabIndex = 78;
            this.txtFarPrLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkPrednjiBranik
            // 
            this.chkPrednjiBranik.AutoSize = true;
            this.chkPrednjiBranik.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrednjiBranik.Location = new System.Drawing.Point(10, 35);
            this.chkPrednjiBranik.Name = "chkPrednjiBranik";
            this.chkPrednjiBranik.Size = new System.Drawing.Size(131, 21);
            this.chkPrednjiBranik.TabIndex = 77;
            this.chkPrednjiBranik.Text = "Prednji branik";
            this.chkPrednjiBranik.UseVisualStyleBackColor = true;
            this.chkPrednjiBranik.CheckedChanged += new System.EventHandler(this.chkPrednjiBranik_CheckedChanged);
            // 
            // txtFarPrDe
            // 
            this.txtFarPrDe.Enabled = false;
            this.txtFarPrDe.Location = new System.Drawing.Point(554, 107);
            this.txtFarPrDe.Name = "txtFarPrDe";
            this.txtFarPrDe.Size = new System.Drawing.Size(80, 22);
            this.txtFarPrDe.TabIndex = 78;
            this.txtFarPrDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtZadnjiBranik
            // 
            this.txtZadnjiBranik.Enabled = false;
            this.txtZadnjiBranik.Location = new System.Drawing.Point(220, 62);
            this.txtZadnjiBranik.Name = "txtZadnjiBranik";
            this.txtZadnjiBranik.Size = new System.Drawing.Size(80, 22);
            this.txtZadnjiBranik.TabIndex = 78;
            this.txtZadnjiBranik.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtSvetloTablice
            // 
            this.txtSvetloTablice.Enabled = false;
            this.txtSvetloTablice.Location = new System.Drawing.Point(554, 296);
            this.txtSvetloTablice.Name = "txtSvetloTablice";
            this.txtSvetloTablice.Size = new System.Drawing.Size(80, 22);
            this.txtSvetloTablice.TabIndex = 78;
            this.txtSvetloTablice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPrLeKrilo
            // 
            this.txtPrLeKrilo.Enabled = false;
            this.txtPrLeKrilo.Location = new System.Drawing.Point(220, 90);
            this.txtPrLeKrilo.Name = "txtPrLeKrilo";
            this.txtPrLeKrilo.Size = new System.Drawing.Size(80, 22);
            this.txtPrLeKrilo.TabIndex = 78;
            this.txtPrLeKrilo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtMaglPrLe
            // 
            this.txtMaglPrLe.Enabled = false;
            this.txtMaglPrLe.Location = new System.Drawing.Point(554, 134);
            this.txtMaglPrLe.Name = "txtMaglPrLe";
            this.txtMaglPrLe.Size = new System.Drawing.Size(80, 22);
            this.txtMaglPrLe.TabIndex = 78;
            this.txtMaglPrLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtVrataZaDe
            // 
            this.txtVrataZaDe.Enabled = false;
            this.txtVrataZaDe.Location = new System.Drawing.Point(220, 286);
            this.txtVrataZaDe.Name = "txtVrataZaDe";
            this.txtVrataZaDe.Size = new System.Drawing.Size(80, 22);
            this.txtVrataZaDe.TabIndex = 78;
            this.txtVrataZaDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtRatkapaZaLe
            // 
            this.txtRatkapaZaLe.Enabled = false;
            this.txtRatkapaZaLe.Location = new System.Drawing.Point(554, 431);
            this.txtRatkapaZaLe.Name = "txtRatkapaZaLe";
            this.txtRatkapaZaLe.Size = new System.Drawing.Size(80, 22);
            this.txtRatkapaZaLe.TabIndex = 78;
            this.txtRatkapaZaLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPrDeKrilo
            // 
            this.txtPrDeKrilo.Enabled = false;
            this.txtPrDeKrilo.Location = new System.Drawing.Point(220, 118);
            this.txtPrDeKrilo.Name = "txtPrDeKrilo";
            this.txtPrDeKrilo.Size = new System.Drawing.Size(80, 22);
            this.txtPrDeKrilo.TabIndex = 78;
            this.txtPrDeKrilo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtRatkapaPrLe
            // 
            this.txtRatkapaPrLe.Enabled = false;
            this.txtRatkapaPrLe.Location = new System.Drawing.Point(554, 377);
            this.txtRatkapaPrLe.Name = "txtRatkapaPrLe";
            this.txtRatkapaPrLe.Size = new System.Drawing.Size(80, 22);
            this.txtRatkapaPrLe.TabIndex = 78;
            this.txtRatkapaPrLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPragLe
            // 
            this.txtPragLe.Enabled = false;
            this.txtPragLe.Location = new System.Drawing.Point(220, 426);
            this.txtPragLe.Name = "txtPragLe";
            this.txtPragLe.Size = new System.Drawing.Size(80, 22);
            this.txtPragLe.TabIndex = 78;
            this.txtPragLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkZaDeSvetlo
            // 
            this.chkZaDeSvetlo.AutoSize = true;
            this.chkZaDeSvetlo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaDeSvetlo.Location = new System.Drawing.Point(324, 216);
            this.chkZaDeSvetlo.Name = "chkZaDeSvetlo";
            this.chkZaDeSvetlo.Size = new System.Drawing.Size(177, 21);
            this.chkZaDeSvetlo.TabIndex = 77;
            this.chkZaDeSvetlo.Text = "Zadnja desna svetla";
            this.chkZaDeSvetlo.UseVisualStyleBackColor = true;
            this.chkZaDeSvetlo.CheckedChanged += new System.EventHandler(this.chkZaDeSvetlo_CheckedChanged);
            // 
            // txtRetrovizorLe
            // 
            this.txtRetrovizorLe.Enabled = false;
            this.txtRetrovizorLe.Location = new System.Drawing.Point(220, 370);
            this.txtRetrovizorLe.Name = "txtRetrovizorLe";
            this.txtRetrovizorLe.Size = new System.Drawing.Size(80, 22);
            this.txtRetrovizorLe.TabIndex = 78;
            this.txtRetrovizorLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkZaLeMagl
            // 
            this.chkZaLeMagl.AutoSize = true;
            this.chkZaLeMagl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaLeMagl.Location = new System.Drawing.Point(324, 243);
            this.chkZaLeMagl.Name = "chkZaLeMagl";
            this.chkZaLeMagl.Size = new System.Drawing.Size(188, 21);
            this.chkZaLeMagl.TabIndex = 77;
            this.chkZaLeMagl.Text = "Zadnja leva maglovka";
            this.chkZaLeMagl.UseVisualStyleBackColor = true;
            this.chkZaLeMagl.CheckedChanged += new System.EventHandler(this.chkZaLeMagl_CheckedChanged);
            // 
            // chkPrLevaVrata
            // 
            this.chkPrLevaVrata.AutoSize = true;
            this.chkPrLevaVrata.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrLevaVrata.Location = new System.Drawing.Point(10, 203);
            this.chkPrLevaVrata.Name = "chkPrLevaVrata";
            this.chkPrLevaVrata.Size = new System.Drawing.Size(163, 21);
            this.chkPrLevaVrata.TabIndex = 77;
            this.chkPrLevaVrata.Text = "Prednja leva vrata";
            this.chkPrLevaVrata.UseVisualStyleBackColor = true;
            this.chkPrLevaVrata.CheckedChanged += new System.EventHandler(this.chkPrLevaVrata_CheckedChanged);
            // 
            // chkPrDesniFar
            // 
            this.chkPrDesniFar.AutoSize = true;
            this.chkPrDesniFar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrDesniFar.Location = new System.Drawing.Point(324, 108);
            this.chkPrDesniFar.Name = "chkPrDesniFar";
            this.chkPrDesniFar.Size = new System.Drawing.Size(150, 21);
            this.chkPrDesniFar.TabIndex = 77;
            this.chkPrDesniFar.Text = "Prednji desni far";
            this.chkPrDesniFar.UseVisualStyleBackColor = true;
            this.chkPrDesniFar.CheckedChanged += new System.EventHandler(this.chkPrDesniFar_CheckedChanged);
            // 
            // chkPrDesnaVrata
            // 
            this.chkPrDesnaVrata.AutoSize = true;
            this.chkPrDesnaVrata.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrDesnaVrata.Location = new System.Drawing.Point(10, 231);
            this.chkPrDesnaVrata.Name = "chkPrDesnaVrata";
            this.chkPrDesnaVrata.Size = new System.Drawing.Size(177, 21);
            this.chkPrDesnaVrata.TabIndex = 77;
            this.chkPrDesnaVrata.Text = "Prednja desna vrata";
            this.chkPrDesnaVrata.UseVisualStyleBackColor = true;
            this.chkPrDesnaVrata.CheckedChanged += new System.EventHandler(this.chkPrDesnaVrata_CheckedChanged);
            // 
            // chkPrDesnaMagl
            // 
            this.chkPrDesnaMagl.AutoSize = true;
            this.chkPrDesnaMagl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrDesnaMagl.Location = new System.Drawing.Point(324, 162);
            this.chkPrDesnaMagl.Name = "chkPrDesnaMagl";
            this.chkPrDesnaMagl.Size = new System.Drawing.Size(208, 21);
            this.chkPrDesnaMagl.TabIndex = 77;
            this.chkPrDesnaMagl.Text = "Prednja desna maglovka";
            this.chkPrDesnaMagl.UseVisualStyleBackColor = true;
            this.chkPrDesnaMagl.CheckedChanged += new System.EventHandler(this.chkPrDesnaMagl_CheckedChanged);
            // 
            // chkPrLevoKrilo
            // 
            this.chkPrLevoKrilo.AutoSize = true;
            this.chkPrLevoKrilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrLevoKrilo.Location = new System.Drawing.Point(10, 91);
            this.chkPrLevoKrilo.Name = "chkPrLevoKrilo";
            this.chkPrLevoKrilo.Size = new System.Drawing.Size(157, 21);
            this.chkPrLevoKrilo.TabIndex = 77;
            this.chkPrLevoKrilo.Text = "Prednje levo krilo";
            this.chkPrLevoKrilo.UseVisualStyleBackColor = true;
            this.chkPrLevoKrilo.CheckedChanged += new System.EventHandler(this.chkPrLevoKrilo_CheckedChanged);
            // 
            // txtMigavacDe
            // 
            this.txtMigavacDe.Enabled = false;
            this.txtMigavacDe.Location = new System.Drawing.Point(554, 350);
            this.txtMigavacDe.Name = "txtMigavacDe";
            this.txtMigavacDe.Size = new System.Drawing.Size(80, 22);
            this.txtMigavacDe.TabIndex = 78;
            this.txtMigavacDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkZaLevoKrilo
            // 
            this.chkZaLevoKrilo.AutoSize = true;
            this.chkZaLevoKrilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaLevoKrilo.Location = new System.Drawing.Point(10, 147);
            this.chkZaLevoKrilo.Name = "chkZaLevoKrilo";
            this.chkZaLevoKrilo.Size = new System.Drawing.Size(151, 21);
            this.chkZaLevoKrilo.TabIndex = 77;
            this.chkZaLevoKrilo.Text = "Zadnje levo krilo";
            this.chkZaLevoKrilo.UseVisualStyleBackColor = true;
            this.chkZaLevoKrilo.CheckedChanged += new System.EventHandler(this.chkZaLevoKrilo_CheckedChanged);
            // 
            // chkPrLevaMagl
            // 
            this.chkPrLevaMagl.AutoSize = true;
            this.chkPrLevaMagl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrLevaMagl.Location = new System.Drawing.Point(324, 135);
            this.chkPrLevaMagl.Name = "chkPrLevaMagl";
            this.chkPrLevaMagl.Size = new System.Drawing.Size(194, 21);
            this.chkPrLevaMagl.TabIndex = 77;
            this.chkPrLevaMagl.Text = "Prednja leva maglovka";
            this.chkPrLevaMagl.UseVisualStyleBackColor = true;
            this.chkPrLevaMagl.CheckedChanged += new System.EventHandler(this.chkPrLevaMagl_CheckedChanged);
            // 
            // txtPoklPrtljaznika
            // 
            this.txtPoklPrtljaznika.Enabled = false;
            this.txtPoklPrtljaznika.Location = new System.Drawing.Point(220, 342);
            this.txtPoklPrtljaznika.Name = "txtPoklPrtljaznika";
            this.txtPoklPrtljaznika.Size = new System.Drawing.Size(80, 22);
            this.txtPoklPrtljaznika.TabIndex = 78;
            this.txtPoklPrtljaznika.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtMaglPrDe
            // 
            this.txtMaglPrDe.Enabled = false;
            this.txtMaglPrDe.Location = new System.Drawing.Point(554, 161);
            this.txtMaglPrDe.Name = "txtMaglPrDe";
            this.txtMaglPrDe.Size = new System.Drawing.Size(80, 22);
            this.txtMaglPrDe.TabIndex = 78;
            this.txtMaglPrDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkPrDesnoKrilo
            // 
            this.chkPrDesnoKrilo.AutoSize = true;
            this.chkPrDesnoKrilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrDesnoKrilo.Location = new System.Drawing.Point(10, 119);
            this.chkPrDesnoKrilo.Name = "chkPrDesnoKrilo";
            this.chkPrDesnoKrilo.Size = new System.Drawing.Size(171, 21);
            this.chkPrDesnoKrilo.TabIndex = 77;
            this.chkPrDesnoKrilo.Text = "Prednje desno krilo";
            this.chkPrDesnoKrilo.UseVisualStyleBackColor = true;
            this.chkPrDesnoKrilo.CheckedChanged += new System.EventHandler(this.chkPrDesnoKrilo_CheckedChanged);
            // 
            // chkZaLeSvetlo
            // 
            this.chkZaLeSvetlo.AutoSize = true;
            this.chkZaLeSvetlo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaLeSvetlo.Location = new System.Drawing.Point(324, 189);
            this.chkZaLeSvetlo.Name = "chkZaLeSvetlo";
            this.chkZaLeSvetlo.Size = new System.Drawing.Size(163, 21);
            this.chkZaLeSvetlo.TabIndex = 77;
            this.chkZaLeSvetlo.Text = "Zadnja leva svetla";
            this.chkZaLeSvetlo.UseVisualStyleBackColor = true;
            this.chkZaLeSvetlo.CheckedChanged += new System.EventHandler(this.chkZaLeSvetlo_CheckedChanged);
            // 
            // txtZaLeKrilo
            // 
            this.txtZaLeKrilo.Enabled = false;
            this.txtZaLeKrilo.Location = new System.Drawing.Point(220, 146);
            this.txtZaLeKrilo.Name = "txtZaLeKrilo";
            this.txtZaLeKrilo.Size = new System.Drawing.Size(80, 22);
            this.txtZaLeKrilo.TabIndex = 78;
            this.txtZaLeKrilo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtStopDe
            // 
            this.txtStopDe.Enabled = false;
            this.txtStopDe.Location = new System.Drawing.Point(554, 215);
            this.txtStopDe.Name = "txtStopDe";
            this.txtStopDe.Size = new System.Drawing.Size(80, 22);
            this.txtStopDe.TabIndex = 77;
            this.txtStopDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkZaDesnoKrilo
            // 
            this.chkZaDesnoKrilo.AutoSize = true;
            this.chkZaDesnoKrilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaDesnoKrilo.Location = new System.Drawing.Point(10, 175);
            this.chkZaDesnoKrilo.Name = "chkZaDesnoKrilo";
            this.chkZaDesnoKrilo.Size = new System.Drawing.Size(165, 21);
            this.chkZaDesnoKrilo.TabIndex = 77;
            this.chkZaDesnoKrilo.Text = "Zadnje desno krilo";
            this.chkZaDesnoKrilo.UseVisualStyleBackColor = true;
            this.chkZaDesnoKrilo.CheckedChanged += new System.EventHandler(this.chkZaDesnoKrilo_CheckedChanged);
            // 
            // txtMaglZaLe
            // 
            this.txtMaglZaLe.Enabled = false;
            this.txtMaglZaLe.Location = new System.Drawing.Point(554, 242);
            this.txtMaglZaLe.Name = "txtMaglZaLe";
            this.txtMaglZaLe.Size = new System.Drawing.Size(80, 22);
            this.txtMaglZaLe.TabIndex = 78;
            this.txtMaglZaLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtVrataPrLe
            // 
            this.txtVrataPrLe.Enabled = false;
            this.txtVrataPrLe.Location = new System.Drawing.Point(220, 202);
            this.txtVrataPrLe.Name = "txtVrataPrLe";
            this.txtVrataPrLe.Size = new System.Drawing.Size(80, 22);
            this.txtVrataPrLe.TabIndex = 78;
            this.txtVrataPrLe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkZaLeRatkapa
            // 
            this.chkZaLeRatkapa.AutoSize = true;
            this.chkZaLeRatkapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaLeRatkapa.Location = new System.Drawing.Point(324, 432);
            this.chkZaLeRatkapa.Name = "chkZaLeRatkapa";
            this.chkZaLeRatkapa.Size = new System.Drawing.Size(208, 21);
            this.chkZaLeRatkapa.TabIndex = 77;
            this.chkZaLeRatkapa.Text = "Zadnji L. poklopac točka";
            this.chkZaLeRatkapa.UseVisualStyleBackColor = true;
            this.chkZaLeRatkapa.CheckedChanged += new System.EventHandler(this.chkZaLeRatkapa_CheckedChanged);
            // 
            // txtVrataPrDe
            // 
            this.txtVrataPrDe.Enabled = false;
            this.txtVrataPrDe.Location = new System.Drawing.Point(220, 230);
            this.txtVrataPrDe.Name = "txtVrataPrDe";
            this.txtVrataPrDe.Size = new System.Drawing.Size(80, 22);
            this.txtVrataPrDe.TabIndex = 78;
            this.txtVrataPrDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkDesniMigavac
            // 
            this.chkDesniMigavac.AutoSize = true;
            this.chkDesniMigavac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDesniMigavac.Location = new System.Drawing.Point(324, 351);
            this.chkDesniMigavac.Name = "chkDesniMigavac";
            this.chkDesniMigavac.Size = new System.Drawing.Size(179, 21);
            this.chkDesniMigavac.TabIndex = 77;
            this.chkDesniMigavac.Text = "Desni bočni migavac";
            this.chkDesniMigavac.UseVisualStyleBackColor = true;
            this.chkDesniMigavac.CheckedChanged += new System.EventHandler(this.chkDesniMigavac_CheckedChanged);
            // 
            // chkLeviPrag
            // 
            this.chkLeviPrag.AutoSize = true;
            this.chkLeviPrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLeviPrag.Location = new System.Drawing.Point(10, 427);
            this.chkLeviPrag.Name = "chkLeviPrag";
            this.chkLeviPrag.Size = new System.Drawing.Size(98, 21);
            this.chkLeviPrag.TabIndex = 77;
            this.chkLeviPrag.Text = "Levi prag";
            this.chkLeviPrag.UseVisualStyleBackColor = true;
            this.chkLeviPrag.CheckedChanged += new System.EventHandler(this.chkLeviPrag_CheckedChanged);
            // 
            // chkPrLeRatkapa
            // 
            this.chkPrLeRatkapa.AutoSize = true;
            this.chkPrLeRatkapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrLeRatkapa.Location = new System.Drawing.Point(324, 378);
            this.chkPrLeRatkapa.Name = "chkPrLeRatkapa";
            this.chkPrLeRatkapa.Size = new System.Drawing.Size(214, 21);
            this.chkPrLeRatkapa.TabIndex = 77;
            this.chkPrLeRatkapa.Text = "Prednji L. poklopac točka";
            this.chkPrLeRatkapa.UseVisualStyleBackColor = true;
            this.chkPrLeRatkapa.CheckedChanged += new System.EventHandler(this.chkPrLeRatkapa_CheckedChanged);
            // 
            // chkPoklPrtljaznika
            // 
            this.chkPoklPrtljaznika.AutoSize = true;
            this.chkPoklPrtljaznika.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPoklPrtljaznika.Location = new System.Drawing.Point(10, 343);
            this.chkPoklPrtljaznika.Name = "chkPoklPrtljaznika";
            this.chkPoklPrtljaznika.Size = new System.Drawing.Size(176, 21);
            this.chkPoklPrtljaznika.TabIndex = 77;
            this.chkPoklPrtljaznika.Text = "Poklopac prtljažnika";
            this.chkPoklPrtljaznika.UseVisualStyleBackColor = true;
            this.chkPoklPrtljaznika.CheckedChanged += new System.EventHandler(this.chkPoklPrtljaznika_CheckedChanged);
            // 
            // chkLeviMigavac
            // 
            this.chkLeviMigavac.AutoSize = true;
            this.chkLeviMigavac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLeviMigavac.Location = new System.Drawing.Point(324, 324);
            this.chkLeviMigavac.Name = "chkLeviMigavac";
            this.chkLeviMigavac.Size = new System.Drawing.Size(168, 21);
            this.chkLeviMigavac.TabIndex = 77;
            this.chkLeviMigavac.Text = "Levi bočni migavac";
            this.chkLeviMigavac.UseVisualStyleBackColor = true;
            this.chkLeviMigavac.CheckedChanged += new System.EventHandler(this.chkLeviMigavac_CheckedChanged);
            // 
            // chkLeviRetrovizor
            // 
            this.chkLeviRetrovizor.AutoSize = true;
            this.chkLeviRetrovizor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLeviRetrovizor.Location = new System.Drawing.Point(10, 371);
            this.chkLeviRetrovizor.Name = "chkLeviRetrovizor";
            this.chkLeviRetrovizor.Size = new System.Drawing.Size(135, 21);
            this.chkLeviRetrovizor.TabIndex = 77;
            this.chkLeviRetrovizor.Text = "Levi retrovizor";
            this.chkLeviRetrovizor.UseVisualStyleBackColor = true;
            this.chkLeviRetrovizor.CheckedChanged += new System.EventHandler(this.chkLeviRetrovizor_CheckedChanged);
            // 
            // chkSvetloTablice
            // 
            this.chkSvetloTablice.AutoSize = true;
            this.chkSvetloTablice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSvetloTablice.Location = new System.Drawing.Point(324, 297);
            this.chkSvetloTablice.Name = "chkSvetloTablice";
            this.chkSvetloTablice.Size = new System.Drawing.Size(128, 21);
            this.chkSvetloTablice.TabIndex = 77;
            this.chkSvetloTablice.Text = "Svetlo tablice";
            this.chkSvetloTablice.UseVisualStyleBackColor = true;
            this.chkSvetloTablice.CheckedChanged += new System.EventHandler(this.chkSvetloTablice_CheckedChanged);
            // 
            // chkPoklMotora
            // 
            this.chkPoklMotora.AutoSize = true;
            this.chkPoklMotora.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPoklMotora.Location = new System.Drawing.Point(10, 315);
            this.chkPoklMotora.Name = "chkPoklMotora";
            this.chkPoklMotora.Size = new System.Drawing.Size(151, 21);
            this.chkPoklMotora.TabIndex = 77;
            this.chkPoklMotora.Text = "Poklopac motora";
            this.chkPoklMotora.UseVisualStyleBackColor = true;
            this.chkPoklMotora.CheckedChanged += new System.EventHandler(this.chkPoklMotora_CheckedChanged);
            // 
            // chkZaDeRatkapa
            // 
            this.chkZaDeRatkapa.AutoSize = true;
            this.chkZaDeRatkapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaDeRatkapa.Location = new System.Drawing.Point(324, 459);
            this.chkZaDeRatkapa.Name = "chkZaDeRatkapa";
            this.chkZaDeRatkapa.Size = new System.Drawing.Size(210, 21);
            this.chkZaDeRatkapa.TabIndex = 77;
            this.chkZaDeRatkapa.Text = "Zadnji D. poklopac točka";
            this.chkZaDeRatkapa.UseVisualStyleBackColor = true;
            this.chkZaDeRatkapa.CheckedChanged += new System.EventHandler(this.chkZaDeRatkapa_CheckedChanged);
            // 
            // chkZaDesnaVrata
            // 
            this.chkZaDesnaVrata.AutoSize = true;
            this.chkZaDesnaVrata.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaDesnaVrata.Location = new System.Drawing.Point(10, 287);
            this.chkZaDesnaVrata.Name = "chkZaDesnaVrata";
            this.chkZaDesnaVrata.Size = new System.Drawing.Size(171, 21);
            this.chkZaDesnaVrata.TabIndex = 77;
            this.chkZaDesnaVrata.Text = "Zadnja desna vrata";
            this.chkZaDesnaVrata.UseVisualStyleBackColor = true;
            this.chkZaDesnaVrata.CheckedChanged += new System.EventHandler(this.chkZaDesnaVrata_CheckedChanged);
            // 
            // chkZaDeMagl
            // 
            this.chkZaDeMagl.AutoSize = true;
            this.chkZaDeMagl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaDeMagl.Location = new System.Drawing.Point(324, 270);
            this.chkZaDeMagl.Name = "chkZaDeMagl";
            this.chkZaDeMagl.Size = new System.Drawing.Size(202, 21);
            this.chkZaDeMagl.TabIndex = 77;
            this.chkZaDeMagl.Text = "Zadnja desna maglovka";
            this.chkZaDeMagl.UseVisualStyleBackColor = true;
            this.chkZaDeMagl.CheckedChanged += new System.EventHandler(this.chkZaDeMagl_CheckedChanged);
            // 
            // chkDesniPrag
            // 
            this.chkDesniPrag.AutoSize = true;
            this.chkDesniPrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDesniPrag.Location = new System.Drawing.Point(10, 455);
            this.chkDesniPrag.Name = "chkDesniPrag";
            this.chkDesniPrag.Size = new System.Drawing.Size(109, 21);
            this.chkDesniPrag.TabIndex = 77;
            this.chkDesniPrag.Text = "Desni prag";
            this.chkDesniPrag.UseVisualStyleBackColor = true;
            this.chkDesniPrag.CheckedChanged += new System.EventHandler(this.chkDesniPrag_CheckedChanged);
            // 
            // chkPrDeRatkapa
            // 
            this.chkPrDeRatkapa.AutoSize = true;
            this.chkPrDeRatkapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPrDeRatkapa.Location = new System.Drawing.Point(324, 405);
            this.chkPrDeRatkapa.Name = "chkPrDeRatkapa";
            this.chkPrDeRatkapa.Size = new System.Drawing.Size(216, 21);
            this.chkPrDeRatkapa.TabIndex = 77;
            this.chkPrDeRatkapa.Text = "Prednji D. poklopac točka";
            this.chkPrDeRatkapa.UseVisualStyleBackColor = true;
            this.chkPrDeRatkapa.CheckedChanged += new System.EventHandler(this.chkPrDeRatkapa_CheckedChanged);
            // 
            // chkZaLevaVrata
            // 
            this.chkZaLevaVrata.AutoSize = true;
            this.chkZaLevaVrata.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZaLevaVrata.Location = new System.Drawing.Point(10, 259);
            this.chkZaLevaVrata.Name = "chkZaLevaVrata";
            this.chkZaLevaVrata.Size = new System.Drawing.Size(157, 21);
            this.chkZaLevaVrata.TabIndex = 77;
            this.chkZaLevaVrata.Text = "Zadnja leva vrata";
            this.chkZaLevaVrata.UseVisualStyleBackColor = true;
            this.chkZaLevaVrata.CheckedChanged += new System.EventHandler(this.chkZaLevaVrata_CheckedChanged);
            // 
            // chkDesniRetrovizor
            // 
            this.chkDesniRetrovizor.AutoSize = true;
            this.chkDesniRetrovizor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDesniRetrovizor.Location = new System.Drawing.Point(10, 399);
            this.chkDesniRetrovizor.Name = "chkDesniRetrovizor";
            this.chkDesniRetrovizor.Size = new System.Drawing.Size(146, 21);
            this.chkDesniRetrovizor.TabIndex = 77;
            this.chkDesniRetrovizor.Text = "Desni retrovizor";
            this.chkDesniRetrovizor.UseVisualStyleBackColor = true;
            this.chkDesniRetrovizor.CheckedChanged += new System.EventHandler(this.chkDesniRetrovizor_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtTecnostKocKolicina);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtUljeMenjacKolicina);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.chkFilterUlja);
            this.groupBox2.Controls.Add(this.txtSveciceKolicina);
            this.groupBox2.Controls.Add(this.txtUljeKolicina);
            this.groupBox2.Controls.Add(this.txtFilterKlime);
            this.groupBox2.Controls.Add(this.chkFilterGoriva);
            this.groupBox2.Controls.Add(this.txtPKKais);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.chkTecnostKocenje);
            this.groupBox2.Controls.Add(this.txtSvecice);
            this.groupBox2.Controls.Add(this.chkSvecice);
            this.groupBox2.Controls.Add(this.txtZupKaisLanac);
            this.groupBox2.Controls.Add(this.chkUlje);
            this.groupBox2.Controls.Add(this.txtUljeMenjac);
            this.groupBox2.Controls.Add(this.chkFilterKlime);
            this.groupBox2.Controls.Add(this.chkFilterVazduha);
            this.groupBox2.Controls.Add(this.txtUlje);
            this.groupBox2.Controls.Add(this.chkUljeMenjac);
            this.groupBox2.Controls.Add(this.txtFilterUlja);
            this.groupBox2.Controls.Add(this.chkZupKaisLanac);
            this.groupBox2.Controls.Add(this.txtFilterGoriva);
            this.groupBox2.Controls.Add(this.chkPKKais);
            this.groupBox2.Controls.Add(this.txtTecnostKocenje);
            this.groupBox2.Controls.Add(this.txtFilterVazduha);
            this.groupBox2.Location = new System.Drawing.Point(976, 356);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(298, 343);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sa redovnog";
            // 
            // chkFilterUlja
            // 
            this.chkFilterUlja.AutoSize = true;
            this.chkFilterUlja.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterUlja.Location = new System.Drawing.Point(13, 25);
            this.chkFilterUlja.Name = "chkFilterUlja";
            this.chkFilterUlja.Size = new System.Drawing.Size(98, 21);
            this.chkFilterUlja.TabIndex = 5;
            this.chkFilterUlja.Text = "Filter ulja";
            this.chkFilterUlja.UseVisualStyleBackColor = true;
            this.chkFilterUlja.CheckedChanged += new System.EventHandler(this.chkFilterUlja_CheckedChanged);
            // 
            // txtUljeKolicina
            // 
            this.txtUljeKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtUljeKolicina.Enabled = false;
            this.txtUljeKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUljeKolicina.Location = new System.Drawing.Point(73, 129);
            this.txtUljeKolicina.Name = "txtUljeKolicina";
            this.txtUljeKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUljeKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtUljeKolicina.TabIndex = 4;
            this.txtUljeKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUljeKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkFilterGoriva
            // 
            this.chkFilterGoriva.AutoSize = true;
            this.chkFilterGoriva.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterGoriva.Location = new System.Drawing.Point(13, 52);
            this.chkFilterGoriva.Name = "chkFilterGoriva";
            this.chkFilterGoriva.Size = new System.Drawing.Size(117, 21);
            this.chkFilterGoriva.TabIndex = 5;
            this.chkFilterGoriva.Text = "Filter goriva";
            this.chkFilterGoriva.UseVisualStyleBackColor = true;
            this.chkFilterGoriva.CheckedChanged += new System.EventHandler(this.chkFilterGoriva_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(109, 133);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 17);
            this.label8.TabIndex = 63;
            this.label8.Text = "lit.";
            // 
            // chkTecnostKocenje
            // 
            this.chkTecnostKocenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTecnostKocenje.Location = new System.Drawing.Point(13, 184);
            this.chkTecnostKocenje.Name = "chkTecnostKocenje";
            this.chkTecnostKocenje.Size = new System.Drawing.Size(122, 42);
            this.chkTecnostKocenje.TabIndex = 14;
            this.chkTecnostKocenje.Text = "Tečnost za kočenje";
            this.chkTecnostKocenje.UseVisualStyleBackColor = true;
            this.chkTecnostKocenje.CheckedChanged += new System.EventHandler(this.chkTecnostKocenje_CheckedChanged);
            // 
            // chkSvecice
            // 
            this.chkSvecice.AutoSize = true;
            this.chkSvecice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSvecice.Location = new System.Drawing.Point(13, 160);
            this.chkSvecice.Name = "chkSvecice";
            this.chkSvecice.Size = new System.Drawing.Size(86, 21);
            this.chkSvecice.TabIndex = 15;
            this.chkSvecice.Text = "Svećice";
            this.chkSvecice.UseVisualStyleBackColor = true;
            this.chkSvecice.CheckedChanged += new System.EventHandler(this.chkSvecice_CheckedChanged);
            // 
            // chkUlje
            // 
            this.chkUlje.AutoSize = true;
            this.chkUlje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUlje.Location = new System.Drawing.Point(13, 133);
            this.chkUlje.Name = "chkUlje";
            this.chkUlje.Size = new System.Drawing.Size(58, 21);
            this.chkUlje.TabIndex = 16;
            this.chkUlje.Text = "Ulje";
            this.chkUlje.UseVisualStyleBackColor = true;
            this.chkUlje.CheckedChanged += new System.EventHandler(this.chkUlje_CheckedChanged);
            // 
            // chkFilterKlime
            // 
            this.chkFilterKlime.AutoSize = true;
            this.chkFilterKlime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterKlime.Location = new System.Drawing.Point(13, 106);
            this.chkFilterKlime.Name = "chkFilterKlime";
            this.chkFilterKlime.Size = new System.Drawing.Size(109, 21);
            this.chkFilterKlime.TabIndex = 17;
            this.chkFilterKlime.Text = "Filter klime";
            this.chkFilterKlime.UseVisualStyleBackColor = true;
            this.chkFilterKlime.CheckedChanged += new System.EventHandler(this.chkFilterKlime_CheckedChanged);
            // 
            // chkFilterVazduha
            // 
            this.chkFilterVazduha.AutoSize = true;
            this.chkFilterVazduha.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFilterVazduha.Location = new System.Drawing.Point(13, 79);
            this.chkFilterVazduha.Name = "chkFilterVazduha";
            this.chkFilterVazduha.Size = new System.Drawing.Size(133, 21);
            this.chkFilterVazduha.TabIndex = 18;
            this.chkFilterVazduha.Text = "Filter vazduha";
            this.chkFilterVazduha.UseVisualStyleBackColor = true;
            this.chkFilterVazduha.CheckedChanged += new System.EventHandler(this.chkFilterVazduha_CheckedChanged);
            // 
            // chkZupKaisLanac
            // 
            this.chkZupKaisLanac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZupKaisLanac.Location = new System.Drawing.Point(13, 301);
            this.chkZupKaisLanac.Name = "chkZupKaisLanac";
            this.chkZupKaisLanac.Size = new System.Drawing.Size(176, 38);
            this.chkZupKaisLanac.TabIndex = 79;
            this.chkZupKaisLanac.Text = "Set zupčastog kaiša / lanca";
            this.chkZupKaisLanac.UseVisualStyleBackColor = true;
            this.chkZupKaisLanac.CheckedChanged += new System.EventHandler(this.chkZupKaisLanac_CheckedChanged);
            // 
            // txtPotrosniMaterijal
            // 
            this.txtPotrosniMaterijal.Enabled = false;
            this.txtPotrosniMaterijal.Location = new System.Drawing.Point(1181, 166);
            this.txtPotrosniMaterijal.Name = "txtPotrosniMaterijal";
            this.txtPotrosniMaterijal.Size = new System.Drawing.Size(80, 22);
            this.txtPotrosniMaterijal.TabIndex = 78;
            this.txtPotrosniMaterijal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtLezajTockaZa
            // 
            this.txtLezajTockaZa.Enabled = false;
            this.txtLezajTockaZa.Location = new System.Drawing.Point(1181, 72);
            this.txtLezajTockaZa.Name = "txtLezajTockaZa";
            this.txtLezajTockaZa.Size = new System.Drawing.Size(80, 22);
            this.txtLezajTockaZa.TabIndex = 78;
            this.txtLezajTockaZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtGumiceBalansSta
            // 
            this.txtGumiceBalansSta.Enabled = false;
            this.txtGumiceBalansSta.Location = new System.Drawing.Point(1181, 107);
            this.txtGumiceBalansSta.Name = "txtGumiceBalansSta";
            this.txtGumiceBalansSta.Size = new System.Drawing.Size(80, 22);
            this.txtGumiceBalansSta.TabIndex = 78;
            this.txtGumiceBalansSta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtLezajTockaPr
            // 
            this.txtLezajTockaPr.Enabled = false;
            this.txtLezajTockaPr.Location = new System.Drawing.Point(1181, 31);
            this.txtLezajTockaPr.Name = "txtLezajTockaPr";
            this.txtLezajTockaPr.Size = new System.Drawing.Size(80, 22);
            this.txtLezajTockaPr.TabIndex = 78;
            this.txtLezajTockaPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPopravkaBrenda
            // 
            this.txtPopravkaBrenda.Enabled = false;
            this.txtPopravkaBrenda.Location = new System.Drawing.Point(1181, 139);
            this.txtPopravkaBrenda.Name = "txtPopravkaBrenda";
            this.txtPopravkaBrenda.Size = new System.Drawing.Size(80, 22);
            this.txtPopravkaBrenda.TabIndex = 78;
            this.txtPopravkaBrenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtLamela
            // 
            this.txtLamela.Enabled = false;
            this.txtLamela.Location = new System.Drawing.Point(554, 47);
            this.txtLamela.Name = "txtLamela";
            this.txtLamela.Size = new System.Drawing.Size(80, 22);
            this.txtLamela.TabIndex = 78;
            this.txtLamela.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtKrajSpone
            // 
            this.txtKrajSpone.Enabled = false;
            this.txtKrajSpone.Location = new System.Drawing.Point(888, 176);
            this.txtKrajSpone.Name = "txtKrajSpone";
            this.txtKrajSpone.Size = new System.Drawing.Size(80, 22);
            this.txtKrajSpone.TabIndex = 78;
            this.txtKrajSpone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtStabilizatorZa
            // 
            this.txtStabilizatorZa.Enabled = false;
            this.txtStabilizatorZa.Location = new System.Drawing.Point(888, 147);
            this.txtStabilizatorZa.Name = "txtStabilizatorZa";
            this.txtStabilizatorZa.Size = new System.Drawing.Size(80, 22);
            this.txtStabilizatorZa.TabIndex = 78;
            this.txtStabilizatorZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtStabilizatorPr
            // 
            this.txtStabilizatorPr.Enabled = false;
            this.txtStabilizatorPr.Location = new System.Drawing.Point(888, 107);
            this.txtStabilizatorPr.Name = "txtStabilizatorPr";
            this.txtStabilizatorPr.Size = new System.Drawing.Size(80, 22);
            this.txtStabilizatorPr.TabIndex = 78;
            this.txtStabilizatorPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtSijalica
            // 
            this.txtSijalica.Enabled = false;
            this.txtSijalica.Location = new System.Drawing.Point(888, 73);
            this.txtSijalica.Name = "txtSijalica";
            this.txtSijalica.Size = new System.Drawing.Size(80, 22);
            this.txtSijalica.TabIndex = 78;
            this.txtSijalica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtBaterijaKljuca
            // 
            this.txtBaterijaKljuca.Enabled = false;
            this.txtBaterijaKljuca.Location = new System.Drawing.Point(554, 150);
            this.txtBaterijaKljuca.Name = "txtBaterijaKljuca";
            this.txtBaterijaKljuca.Size = new System.Drawing.Size(80, 22);
            this.txtBaterijaKljuca.TabIndex = 78;
            this.txtBaterijaKljuca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtSetKvacila
            // 
            this.txtSetKvacila.Enabled = false;
            this.txtSetKvacila.Location = new System.Drawing.Point(554, 21);
            this.txtSetKvacila.Name = "txtSetKvacila";
            this.txtSetKvacila.Size = new System.Drawing.Size(80, 22);
            this.txtSetKvacila.TabIndex = 78;
            this.txtSetKvacila.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtAmortizeriZa
            // 
            this.txtAmortizeriZa.Enabled = false;
            this.txtAmortizeriZa.Location = new System.Drawing.Point(888, 21);
            this.txtAmortizeriZa.Name = "txtAmortizeriZa";
            this.txtAmortizeriZa.Size = new System.Drawing.Size(80, 22);
            this.txtAmortizeriZa.TabIndex = 78;
            this.txtAmortizeriZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkLezajTockaZa
            // 
            this.chkLezajTockaZa.AllowDrop = true;
            this.chkLezajTockaZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLezajTockaZa.Location = new System.Drawing.Point(989, 63);
            this.chkLezajTockaZa.Name = "chkLezajTockaZa";
            this.chkLezajTockaZa.Size = new System.Drawing.Size(118, 40);
            this.chkLezajTockaZa.TabIndex = 77;
            this.chkLezajTockaZa.Text = "Ležaj točka zadnji";
            this.chkLezajTockaZa.UseVisualStyleBackColor = true;
            this.chkLezajTockaZa.CheckedChanged += new System.EventHandler(this.chkLezajTockaZa_CheckedChanged);
            // 
            // chkGumiceBalansSta
            // 
            this.chkGumiceBalansSta.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGumiceBalansSta.Location = new System.Drawing.Point(989, 100);
            this.chkGumiceBalansSta.Name = "chkGumiceBalansSta";
            this.chkGumiceBalansSta.Size = new System.Drawing.Size(137, 38);
            this.chkGumiceBalansSta.TabIndex = 77;
            this.chkGumiceBalansSta.Text = "Gumica balans štangle";
            this.chkGumiceBalansSta.UseVisualStyleBackColor = true;
            this.chkGumiceBalansSta.CheckedChanged += new System.EventHandler(this.chkGumiceBalansSta_CheckedChanged);
            // 
            // chkPotrosniMaterijal
            // 
            this.chkPotrosniMaterijal.AutoSize = true;
            this.chkPotrosniMaterijal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPotrosniMaterijal.Location = new System.Drawing.Point(989, 167);
            this.chkPotrosniMaterijal.Name = "chkPotrosniMaterijal";
            this.chkPotrosniMaterijal.Size = new System.Drawing.Size(157, 21);
            this.chkPotrosniMaterijal.TabIndex = 77;
            this.chkPotrosniMaterijal.Text = "Potrošni materijal";
            this.chkPotrosniMaterijal.UseVisualStyleBackColor = true;
            this.chkPotrosniMaterijal.CheckedChanged += new System.EventHandler(this.chkPotrosniMaterijal_CheckedChanged);
            // 
            // chkLezajTockaPr
            // 
            this.chkLezajTockaPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLezajTockaPr.Location = new System.Drawing.Point(989, 23);
            this.chkLezajTockaPr.Name = "chkLezajTockaPr";
            this.chkLezajTockaPr.Size = new System.Drawing.Size(118, 40);
            this.chkLezajTockaPr.TabIndex = 77;
            this.chkLezajTockaPr.Text = "Ležaj točka prednji";
            this.chkLezajTockaPr.UseVisualStyleBackColor = true;
            this.chkLezajTockaPr.CheckedChanged += new System.EventHandler(this.chkLezajTockaPr_CheckedChanged);
            // 
            // txtRashlTecMotora
            // 
            this.txtRashlTecMotora.Enabled = false;
            this.txtRashlTecMotora.Location = new System.Drawing.Point(240, 295);
            this.txtRashlTecMotora.Name = "txtRashlTecMotora";
            this.txtRashlTecMotora.Size = new System.Drawing.Size(80, 22);
            this.txtRashlTecMotora.TabIndex = 78;
            this.txtRashlTecMotora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1173, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 63;
            this.label13.Text = "Cena u DIN";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(881, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 15);
            this.label11.TabIndex = 63;
            this.label11.Text = "Cena u DIN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(551, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 15);
            this.label9.TabIndex = 63;
            this.label9.Text = "Cena u DIN";
            // 
            // chkKrajSpone
            // 
            this.chkKrajSpone.AutoSize = true;
            this.chkKrajSpone.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkKrajSpone.Location = new System.Drawing.Point(661, 178);
            this.chkKrajSpone.Name = "chkKrajSpone";
            this.chkKrajSpone.Size = new System.Drawing.Size(108, 21);
            this.chkKrajSpone.TabIndex = 77;
            this.chkKrajSpone.Text = "Kraj spone";
            this.chkKrajSpone.UseVisualStyleBackColor = true;
            this.chkKrajSpone.CheckedChanged += new System.EventHandler(this.chkKrajSpone_CheckedChanged);
            // 
            // chkPopravkaBrenda
            // 
            this.chkPopravkaBrenda.AutoSize = true;
            this.chkPopravkaBrenda.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPopravkaBrenda.Location = new System.Drawing.Point(989, 140);
            this.chkPopravkaBrenda.Name = "chkPopravkaBrenda";
            this.chkPopravkaBrenda.Size = new System.Drawing.Size(154, 21);
            this.chkPopravkaBrenda.TabIndex = 77;
            this.chkPopravkaBrenda.Text = "Popravka brenda";
            this.chkPopravkaBrenda.UseVisualStyleBackColor = true;
            this.chkPopravkaBrenda.CheckedChanged += new System.EventHandler(this.chkPopravkaBrenda_CheckedChanged);
            // 
            // chkStabilizatorZa
            // 
            this.chkStabilizatorZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStabilizatorZa.Location = new System.Drawing.Point(661, 138);
            this.chkStabilizatorZa.Name = "chkStabilizatorZa";
            this.chkStabilizatorZa.Size = new System.Drawing.Size(118, 40);
            this.chkStabilizatorZa.TabIndex = 77;
            this.chkStabilizatorZa.Text = "Stabilizator zadnji";
            this.chkStabilizatorZa.UseVisualStyleBackColor = true;
            this.chkStabilizatorZa.CheckedChanged += new System.EventHandler(this.chkStabilizatorZa_CheckedChanged);
            // 
            // chkLamela
            // 
            this.chkLamela.AutoSize = true;
            this.chkLamela.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLamela.Location = new System.Drawing.Point(344, 49);
            this.chkLamela.Name = "chkLamela";
            this.chkLamela.Size = new System.Drawing.Size(122, 21);
            this.chkLamela.TabIndex = 77;
            this.chkLamela.Text = "Samo lamela";
            this.chkLamela.UseVisualStyleBackColor = true;
            this.chkLamela.CheckedChanged += new System.EventHandler(this.chkLamela_CheckedChanged);
            // 
            // chkStabilizatorPr
            // 
            this.chkStabilizatorPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStabilizatorPr.Location = new System.Drawing.Point(661, 98);
            this.chkStabilizatorPr.Name = "chkStabilizatorPr";
            this.chkStabilizatorPr.Size = new System.Drawing.Size(118, 40);
            this.chkStabilizatorPr.TabIndex = 77;
            this.chkStabilizatorPr.Text = "Stabilizator prednji";
            this.chkStabilizatorPr.UseVisualStyleBackColor = true;
            this.chkStabilizatorPr.CheckedChanged += new System.EventHandler(this.chkStabilizatorPr_CheckedChanged);
            // 
            // txtAmortizeriPr
            // 
            this.txtAmortizeriPr.Enabled = false;
            this.txtAmortizeriPr.Location = new System.Drawing.Point(888, 47);
            this.txtAmortizeriPr.Name = "txtAmortizeriPr";
            this.txtAmortizeriPr.Size = new System.Drawing.Size(80, 22);
            this.txtAmortizeriPr.TabIndex = 78;
            this.txtAmortizeriPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkSijalica
            // 
            this.chkSijalica.AutoSize = true;
            this.chkSijalica.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSijalica.Location = new System.Drawing.Point(661, 75);
            this.chkSijalica.Name = "chkSijalica";
            this.chkSijalica.Size = new System.Drawing.Size(82, 21);
            this.chkSijalica.TabIndex = 77;
            this.chkSijalica.Text = "Sijalica";
            this.chkSijalica.UseVisualStyleBackColor = true;
            this.chkSijalica.CheckedChanged += new System.EventHandler(this.chkSijalica_CheckedChanged);
            // 
            // txtTecnostStakla
            // 
            this.txtTecnostStakla.Enabled = false;
            this.txtTecnostStakla.Location = new System.Drawing.Point(240, 253);
            this.txtTecnostStakla.Name = "txtTecnostStakla";
            this.txtTecnostStakla.Size = new System.Drawing.Size(80, 22);
            this.txtTecnostStakla.TabIndex = 78;
            this.txtTecnostStakla.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkBaterijaKljuca
            // 
            this.chkBaterijaKljuca.AutoSize = true;
            this.chkBaterijaKljuca.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBaterijaKljuca.Location = new System.Drawing.Point(344, 152);
            this.chkBaterijaKljuca.Name = "chkBaterijaKljuca";
            this.chkBaterijaKljuca.Size = new System.Drawing.Size(133, 21);
            this.chkBaterijaKljuca.TabIndex = 77;
            this.chkBaterijaKljuca.Text = "Baterija ključa";
            this.chkBaterijaKljuca.UseVisualStyleBackColor = true;
            this.chkBaterijaKljuca.CheckedChanged += new System.EventHandler(this.chkBaterijaKljuca_CheckedChanged);
            // 
            // chkSetKvacila
            // 
            this.chkSetKvacila.AutoSize = true;
            this.chkSetKvacila.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSetKvacila.Location = new System.Drawing.Point(344, 23);
            this.chkSetKvacila.Name = "chkSetKvacila";
            this.chkSetKvacila.Size = new System.Drawing.Size(109, 21);
            this.chkSetKvacila.TabIndex = 77;
            this.chkSetKvacila.Text = "Set kvačila";
            this.chkSetKvacila.UseVisualStyleBackColor = true;
            this.chkSetKvacila.CheckedChanged += new System.EventHandler(this.chkSetKvacila_CheckedChanged);
            // 
            // txtAlternator
            // 
            this.txtAlternator.Enabled = false;
            this.txtAlternator.Location = new System.Drawing.Point(554, 125);
            this.txtAlternator.Name = "txtAlternator";
            this.txtAlternator.Size = new System.Drawing.Size(80, 22);
            this.txtAlternator.TabIndex = 78;
            this.txtAlternator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtPodloskaCepa
            // 
            this.txtPodloskaCepa.Enabled = false;
            this.txtPodloskaCepa.Location = new System.Drawing.Point(554, 176);
            this.txtPodloskaCepa.Name = "txtPodloskaCepa";
            this.txtPodloskaCepa.Size = new System.Drawing.Size(80, 22);
            this.txtPodloskaCepa.TabIndex = 78;
            this.txtPodloskaCepa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkAmortizerZa
            // 
            this.chkAmortizerZa.AutoSize = true;
            this.chkAmortizerZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAmortizerZa.Location = new System.Drawing.Point(661, 23);
            this.chkAmortizerZa.Name = "chkAmortizerZa";
            this.chkAmortizerZa.Size = new System.Drawing.Size(151, 21);
            this.chkAmortizerZa.TabIndex = 77;
            this.chkAmortizerZa.Text = "Amortizeri zadnji";
            this.chkAmortizerZa.UseVisualStyleBackColor = true;
            this.chkAmortizerZa.CheckedChanged += new System.EventHandler(this.chkAmortizerZa_CheckedChanged);
            // 
            // chkRashlTecMotora
            // 
            this.chkRashlTecMotora.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRashlTecMotora.Location = new System.Drawing.Point(13, 281);
            this.chkRashlTecMotora.Name = "chkRashlTecMotora";
            this.chkRashlTecMotora.Size = new System.Drawing.Size(167, 48);
            this.chkRashlTecMotora.TabIndex = 77;
            this.chkRashlTecMotora.Text = "Rashladna tečnost motora";
            this.chkRashlTecMotora.UseVisualStyleBackColor = true;
            this.chkRashlTecMotora.CheckedChanged += new System.EventHandler(this.chkRashlTecMotora_CheckedChanged);
            // 
            // txtAnlaser
            // 
            this.txtAnlaser.Enabled = false;
            this.txtAnlaser.Location = new System.Drawing.Point(554, 99);
            this.txtAnlaser.Name = "txtAnlaser";
            this.txtAnlaser.Size = new System.Drawing.Size(80, 22);
            this.txtAnlaser.TabIndex = 78;
            this.txtAnlaser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtMetliceBrisZa
            // 
            this.txtMetliceBrisZa.Enabled = false;
            this.txtMetliceBrisZa.Location = new System.Drawing.Point(240, 224);
            this.txtMetliceBrisZa.Name = "txtMetliceBrisZa";
            this.txtMetliceBrisZa.Size = new System.Drawing.Size(80, 22);
            this.txtMetliceBrisZa.TabIndex = 78;
            this.txtMetliceBrisZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkAmortizerPr
            // 
            this.chkAmortizerPr.AutoSize = true;
            this.chkAmortizerPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAmortizerPr.Location = new System.Drawing.Point(661, 49);
            this.chkAmortizerPr.Name = "chkAmortizerPr";
            this.chkAmortizerPr.Size = new System.Drawing.Size(158, 21);
            this.chkAmortizerPr.TabIndex = 77;
            this.chkAmortizerPr.Text = "Amortizeri prednji";
            this.chkAmortizerPr.UseVisualStyleBackColor = true;
            this.chkAmortizerPr.CheckedChanged += new System.EventHandler(this.chkAmortizerPr_CheckedChanged);
            // 
            // chkTecnostStakla
            // 
            this.chkTecnostStakla.AutoSize = true;
            this.chkTecnostStakla.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTecnostStakla.Location = new System.Drawing.Point(13, 255);
            this.chkTecnostStakla.Name = "chkTecnostStakla";
            this.chkTecnostStakla.Size = new System.Drawing.Size(158, 21);
            this.chkTecnostStakla.TabIndex = 77;
            this.chkTecnostStakla.Text = "Tečnost za stakla";
            this.chkTecnostStakla.UseVisualStyleBackColor = true;
            this.chkTecnostStakla.CheckedChanged += new System.EventHandler(this.chkTecnostStakla_CheckedChanged);
            // 
            // txtZamajac
            // 
            this.txtZamajac.Enabled = false;
            this.txtZamajac.Location = new System.Drawing.Point(554, 73);
            this.txtZamajac.Name = "txtZamajac";
            this.txtZamajac.Size = new System.Drawing.Size(80, 22);
            this.txtZamajac.TabIndex = 78;
            this.txtZamajac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkAlternator
            // 
            this.chkAlternator.AutoSize = true;
            this.chkAlternator.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlternator.Location = new System.Drawing.Point(344, 127);
            this.chkAlternator.Name = "chkAlternator";
            this.chkAlternator.Size = new System.Drawing.Size(102, 21);
            this.chkAlternator.TabIndex = 77;
            this.chkAlternator.Text = "Alternator";
            this.chkAlternator.UseVisualStyleBackColor = true;
            this.chkAlternator.CheckedChanged += new System.EventHandler(this.chkAlternator_CheckedChanged);
            // 
            // txtMetliceBrisPr
            // 
            this.txtMetliceBrisPr.Enabled = false;
            this.txtMetliceBrisPr.Location = new System.Drawing.Point(240, 188);
            this.txtMetliceBrisPr.Name = "txtMetliceBrisPr";
            this.txtMetliceBrisPr.Size = new System.Drawing.Size(80, 22);
            this.txtMetliceBrisPr.TabIndex = 78;
            this.txtMetliceBrisPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkPodloska
            // 
            this.chkPodloska.AutoSize = true;
            this.chkPodloska.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPodloska.Location = new System.Drawing.Point(344, 178);
            this.chkPodloska.Name = "chkPodloska";
            this.chkPodloska.Size = new System.Drawing.Size(193, 21);
            this.chkPodloska.TabIndex = 77;
            this.chkPodloska.Text = "Podloška čepa kartera";
            this.chkPodloska.UseVisualStyleBackColor = true;
            this.chkPodloska.CheckedChanged += new System.EventHandler(this.chkPodloska_CheckedChanged);
            // 
            // chkAnlaser
            // 
            this.chkAnlaser.AutoSize = true;
            this.chkAnlaser.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAnlaser.Location = new System.Drawing.Point(344, 101);
            this.chkAnlaser.Name = "chkAnlaser";
            this.chkAnlaser.Size = new System.Drawing.Size(85, 21);
            this.chkAnlaser.TabIndex = 77;
            this.chkAnlaser.Text = "Anlaser";
            this.chkAnlaser.UseVisualStyleBackColor = true;
            this.chkAnlaser.CheckedChanged += new System.EventHandler(this.chkAnlaser_CheckedChanged);
            // 
            // txtPlocicePakneZa
            // 
            this.txtPlocicePakneZa.Enabled = false;
            this.txtPlocicePakneZa.Location = new System.Drawing.Point(240, 151);
            this.txtPlocicePakneZa.Name = "txtPlocicePakneZa";
            this.txtPlocicePakneZa.Size = new System.Drawing.Size(80, 22);
            this.txtPlocicePakneZa.TabIndex = 78;
            this.txtPlocicePakneZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkMetliceBrisZa
            // 
            this.chkMetliceBrisZa.AutoSize = true;
            this.chkMetliceBrisZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMetliceBrisZa.Location = new System.Drawing.Point(13, 226);
            this.chkMetliceBrisZa.Name = "chkMetliceBrisZa";
            this.chkMetliceBrisZa.Size = new System.Drawing.Size(192, 21);
            this.chkMetliceBrisZa.TabIndex = 77;
            this.chkMetliceBrisZa.Text = "Metlice brisača zadnje";
            this.chkMetliceBrisZa.UseVisualStyleBackColor = true;
            this.chkMetliceBrisZa.CheckedChanged += new System.EventHandler(this.chkMetliceBrisZa_CheckedChanged);
            // 
            // chkZamajac
            // 
            this.chkZamajac.AutoSize = true;
            this.chkZamajac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkZamajac.Location = new System.Drawing.Point(344, 75);
            this.chkZamajac.Name = "chkZamajac";
            this.chkZamajac.Size = new System.Drawing.Size(91, 21);
            this.chkZamajac.TabIndex = 77;
            this.chkZamajac.Text = "Zamajac";
            this.chkZamajac.UseVisualStyleBackColor = true;
            this.chkZamajac.CheckedChanged += new System.EventHandler(this.chkZamajac_CheckedChanged);
            // 
            // txtDiskDobosiZa
            // 
            this.txtDiskDobosiZa.Enabled = false;
            this.txtDiskDobosiZa.Location = new System.Drawing.Point(240, 125);
            this.txtDiskDobosiZa.Name = "txtDiskDobosiZa";
            this.txtDiskDobosiZa.Size = new System.Drawing.Size(80, 22);
            this.txtDiskDobosiZa.TabIndex = 78;
            this.txtDiskDobosiZa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkMetliceBrisPr
            // 
            this.chkMetliceBrisPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMetliceBrisPr.Location = new System.Drawing.Point(13, 178);
            this.chkMetliceBrisPr.Name = "chkMetliceBrisPr";
            this.chkMetliceBrisPr.Size = new System.Drawing.Size(141, 42);
            this.chkMetliceBrisPr.TabIndex = 77;
            this.chkMetliceBrisPr.Text = "Metlice brisača prednje";
            this.chkMetliceBrisPr.UseVisualStyleBackColor = true;
            this.chkMetliceBrisPr.CheckedChanged += new System.EventHandler(this.chkMetliceBrisPr_CheckedChanged);
            // 
            // txtDiskoviPr
            // 
            this.txtDiskoviPr.Enabled = false;
            this.txtDiskoviPr.Location = new System.Drawing.Point(240, 99);
            this.txtDiskoviPr.Name = "txtDiskoviPr";
            this.txtDiskoviPr.Size = new System.Drawing.Size(80, 22);
            this.txtDiskoviPr.TabIndex = 78;
            this.txtDiskoviPr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkPlocicePakneZa
            // 
            this.chkPlocicePakneZa.AutoSize = true;
            this.chkPlocicePakneZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPlocicePakneZa.Location = new System.Drawing.Point(13, 153);
            this.chkPlocicePakneZa.Name = "chkPlocicePakneZa";
            this.chkPlocicePakneZa.Size = new System.Drawing.Size(194, 21);
            this.chkPlocicePakneZa.TabIndex = 77;
            this.chkPlocicePakneZa.Text = "Pločice / pakne zadnje";
            this.chkPlocicePakneZa.UseVisualStyleBackColor = true;
            this.chkPlocicePakneZa.CheckedChanged += new System.EventHandler(this.chkPlocicePakneZa_CheckedChanged);
            // 
            // txtDiskPlocicePr
            // 
            this.txtDiskPlocicePr.Enabled = false;
            this.txtDiskPlocicePr.Location = new System.Drawing.Point(240, 73);
            this.txtDiskPlocicePr.Name = "txtDiskPlocicePr";
            this.txtDiskPlocicePr.Size = new System.Drawing.Size(80, 22);
            this.txtDiskPlocicePr.TabIndex = 78;
            this.txtDiskPlocicePr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkDiskDobosiZa
            // 
            this.chkDiskDobosiZa.AutoSize = true;
            this.chkDiskDobosiZa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiskDobosiZa.Location = new System.Drawing.Point(13, 127);
            this.chkDiskDobosiZa.Name = "chkDiskDobosiZa";
            this.chkDiskDobosiZa.Size = new System.Drawing.Size(193, 21);
            this.chkDiskDobosiZa.TabIndex = 77;
            this.chkDiskDobosiZa.Text = "Diskovi / doboši zadnji";
            this.chkDiskDobosiZa.UseVisualStyleBackColor = true;
            this.chkDiskDobosiZa.CheckedChanged += new System.EventHandler(this.chkDiskDobosiZa_CheckedChanged);
            // 
            // txtAkumulator
            // 
            this.txtAkumulator.Enabled = false;
            this.txtAkumulator.Location = new System.Drawing.Point(240, 47);
            this.txtAkumulator.Name = "txtAkumulator";
            this.txtAkumulator.Size = new System.Drawing.Size(80, 22);
            this.txtAkumulator.TabIndex = 78;
            this.txtAkumulator.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkDiskoviPr
            // 
            this.chkDiskoviPr.AutoSize = true;
            this.chkDiskoviPr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiskoviPr.Location = new System.Drawing.Point(13, 101);
            this.chkDiskoviPr.Name = "chkDiskoviPr";
            this.chkDiskoviPr.Size = new System.Drawing.Size(137, 21);
            this.chkDiskoviPr.TabIndex = 77;
            this.chkDiskoviPr.Text = "Diskovi prednji";
            this.chkDiskoviPr.UseVisualStyleBackColor = true;
            this.chkDiskoviPr.CheckedChanged += new System.EventHandler(this.chkDiskoviPr_CheckedChanged);
            // 
            // txtTecnostKlime
            // 
            this.txtTecnostKlime.Enabled = false;
            this.txtTecnostKlime.Location = new System.Drawing.Point(240, 21);
            this.txtTecnostKlime.Name = "txtTecnostKlime";
            this.txtTecnostKlime.Size = new System.Drawing.Size(80, 22);
            this.txtTecnostKlime.TabIndex = 78;
            this.txtTecnostKlime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtOstaloCena
            // 
            this.txtOstaloCena.Enabled = false;
            this.txtOstaloCena.Location = new System.Drawing.Point(1181, 232);
            this.txtOstaloCena.Name = "txtOstaloCena";
            this.txtOstaloCena.Size = new System.Drawing.Size(80, 22);
            this.txtOstaloCena.TabIndex = 78;
            this.txtOstaloCena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // chkDiskPlocicePr
            // 
            this.chkDiskPlocicePr.AutoSize = true;
            this.chkDiskPlocicePr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiskPlocicePr.Location = new System.Drawing.Point(13, 75);
            this.chkDiskPlocicePr.Name = "chkDiskPlocicePr";
            this.chkDiskPlocicePr.Size = new System.Drawing.Size(177, 21);
            this.chkDiskPlocicePr.TabIndex = 77;
            this.chkDiskPlocicePr.Text = "Disk pločice prednje";
            this.chkDiskPlocicePr.UseVisualStyleBackColor = true;
            this.chkDiskPlocicePr.CheckedChanged += new System.EventHandler(this.chkDiskPlocicePr_CheckedChanged);
            // 
            // chkAkumulator
            // 
            this.chkAkumulator.AutoSize = true;
            this.chkAkumulator.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAkumulator.Location = new System.Drawing.Point(13, 49);
            this.chkAkumulator.Name = "chkAkumulator";
            this.chkAkumulator.Size = new System.Drawing.Size(182, 21);
            this.chkAkumulator.TabIndex = 77;
            this.chkAkumulator.Text = "Zamena akumulatora";
            this.chkAkumulator.UseVisualStyleBackColor = true;
            this.chkAkumulator.CheckedChanged += new System.EventHandler(this.chkAkumulator_CheckedChanged);
            // 
            // chkOstalo
            // 
            this.chkOstalo.AutoSize = true;
            this.chkOstalo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOstalo.Location = new System.Drawing.Point(989, 234);
            this.chkOstalo.Name = "chkOstalo";
            this.chkOstalo.Size = new System.Drawing.Size(77, 21);
            this.chkOstalo.TabIndex = 77;
            this.chkOstalo.Text = "Ostalo";
            this.chkOstalo.UseVisualStyleBackColor = true;
            this.chkOstalo.CheckedChanged += new System.EventHandler(this.chkOstalo_CheckedChanged);
            // 
            // chkTecnostKlime
            // 
            this.chkTecnostKlime.AutoSize = true;
            this.chkTecnostKlime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTecnostKlime.Location = new System.Drawing.Point(13, 23);
            this.chkTecnostKlime.Name = "chkTecnostKlime";
            this.chkTecnostKlime.Size = new System.Drawing.Size(130, 21);
            this.chkTecnostKlime.TabIndex = 77;
            this.chkTecnostKlime.Text = "Tečnost klime";
            this.chkTecnostKlime.UseVisualStyleBackColor = true;
            this.chkTecnostKlime.CheckedChanged += new System.EventHandler(this.chkTecnostKlime_CheckedChanged);
            // 
            // tpDatumServisa
            // 
            this.tpDatumServisa.CustomFormat = "dd.MM.yyyy.";
            this.tpDatumServisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpDatumServisa.Location = new System.Drawing.Point(392, 36);
            this.tpDatumServisa.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.tpDatumServisa.Name = "tpDatumServisa";
            this.tpDatumServisa.Size = new System.Drawing.Size(129, 22);
            this.tpDatumServisa.TabIndex = 1;
            this.tpDatumServisa.Value = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(136, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 17);
            this.label10.TabIndex = 96;
            this.label10.Text = "kom.";
            // 
            // txtSveciceKolicina
            // 
            this.txtSveciceKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtSveciceKolicina.Enabled = false;
            this.txtSveciceKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSveciceKolicina.Location = new System.Drawing.Point(105, 156);
            this.txtSveciceKolicina.Name = "txtSveciceKolicina";
            this.txtSveciceKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSveciceKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtSveciceKolicina.TabIndex = 95;
            this.txtSveciceKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSveciceKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(147, 269);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 17);
            this.label14.TabIndex = 98;
            this.label14.Text = "lit.";
            // 
            // txtUljeMenjacKolicina
            // 
            this.txtUljeMenjacKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtUljeMenjacKolicina.Enabled = false;
            this.txtUljeMenjacKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUljeMenjacKolicina.Location = new System.Drawing.Point(116, 267);
            this.txtUljeMenjacKolicina.Name = "txtUljeMenjacKolicina";
            this.txtUljeMenjacKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtUljeMenjacKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtUljeMenjacKolicina.TabIndex = 97;
            this.txtUljeMenjacKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUljeMenjacKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(168, 199);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 17);
            this.label15.TabIndex = 100;
            this.label15.Text = "lit.";
            // 
            // txtTecnostKocKolicina
            // 
            this.txtTecnostKocKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtTecnostKocKolicina.Enabled = false;
            this.txtTecnostKocKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTecnostKocKolicina.Location = new System.Drawing.Point(137, 194);
            this.txtTecnostKocKolicina.Name = "txtTecnostKocKolicina";
            this.txtTecnostKocKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTecnostKocKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtTecnostKocKolicina.TabIndex = 99;
            this.txtTecnostKocKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTecnostKocKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(189, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 17);
            this.label16.TabIndex = 63;
            this.label16.Text = "lit.";
            // 
            // txtTecnostKlimeKol
            // 
            this.txtTecnostKlimeKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtTecnostKlimeKol.Enabled = false;
            this.txtTecnostKlimeKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTecnostKlimeKol.Location = new System.Drawing.Point(158, 21);
            this.txtTecnostKlimeKol.Name = "txtTecnostKlimeKol";
            this.txtTecnostKlimeKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTecnostKlimeKol.Size = new System.Drawing.Size(30, 22);
            this.txtTecnostKlimeKol.TabIndex = 4;
            this.txtTecnostKlimeKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTecnostKlimeKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtBrisaciKolicina
            // 
            this.txtBrisaciKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtBrisaciKolicina.Enabled = false;
            this.txtBrisaciKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrisaciKolicina.Location = new System.Drawing.Point(158, 188);
            this.txtBrisaciKolicina.Name = "txtBrisaciKolicina";
            this.txtBrisaciKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrisaciKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtBrisaciKolicina.TabIndex = 95;
            this.txtBrisaciKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrisaciKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(189, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 17);
            this.label17.TabIndex = 96;
            this.label17.Text = "kom.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(213, 257);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 17);
            this.label18.TabIndex = 63;
            this.label18.Text = "lit.";
            // 
            // txtTecnostStaklaKol
            // 
            this.txtTecnostStaklaKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtTecnostStaklaKol.Enabled = false;
            this.txtTecnostStaklaKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTecnostStaklaKol.Location = new System.Drawing.Point(177, 253);
            this.txtTecnostStaklaKol.Name = "txtTecnostStaklaKol";
            this.txtTecnostStaklaKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTecnostStaklaKol.Size = new System.Drawing.Size(30, 22);
            this.txtTecnostStaklaKol.TabIndex = 4;
            this.txtTecnostStaklaKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTecnostStaklaKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(213, 299);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 17);
            this.label19.TabIndex = 63;
            this.label19.Text = "lit.";
            // 
            // txtRashlTecMotKol
            // 
            this.txtRashlTecMotKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtRashlTecMotKol.Enabled = false;
            this.txtRashlTecMotKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRashlTecMotKol.Location = new System.Drawing.Point(177, 295);
            this.txtRashlTecMotKol.Name = "txtRashlTecMotKol";
            this.txtRashlTecMotKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRashlTecMotKol.Size = new System.Drawing.Size(30, 22);
            this.txtRashlTecMotKol.TabIndex = 4;
            this.txtRashlTecMotKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRashlTecMotKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // txtSijalicaKolicina
            // 
            this.txtSijalicaKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtSijalicaKolicina.Enabled = false;
            this.txtSijalicaKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSijalicaKolicina.Location = new System.Drawing.Point(789, 73);
            this.txtSijalicaKolicina.Name = "txtSijalicaKolicina";
            this.txtSijalicaKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSijalicaKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtSijalicaKolicina.TabIndex = 95;
            this.txtSijalicaKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSijalicaKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(820, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 17);
            this.label20.TabIndex = 96;
            this.label20.Text = "kom.";
            // 
            // txtStabilizatorPrKol
            // 
            this.txtStabilizatorPrKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtStabilizatorPrKol.Enabled = false;
            this.txtStabilizatorPrKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStabilizatorPrKol.Location = new System.Drawing.Point(789, 107);
            this.txtStabilizatorPrKol.Name = "txtStabilizatorPrKol";
            this.txtStabilizatorPrKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStabilizatorPrKol.Size = new System.Drawing.Size(30, 22);
            this.txtStabilizatorPrKol.TabIndex = 95;
            this.txtStabilizatorPrKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStabilizatorPrKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(820, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 17);
            this.label21.TabIndex = 96;
            this.label21.Text = "kom.";
            // 
            // txtStabilizatorZaKol
            // 
            this.txtStabilizatorZaKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtStabilizatorZaKol.Enabled = false;
            this.txtStabilizatorZaKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStabilizatorZaKol.Location = new System.Drawing.Point(789, 147);
            this.txtStabilizatorZaKol.Name = "txtStabilizatorZaKol";
            this.txtStabilizatorZaKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStabilizatorZaKol.Size = new System.Drawing.Size(30, 22);
            this.txtStabilizatorZaKol.TabIndex = 95;
            this.txtStabilizatorZaKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStabilizatorZaKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(820, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 17);
            this.label22.TabIndex = 96;
            this.label22.Text = "kom.";
            // 
            // txtLezajTockaPrKol
            // 
            this.txtLezajTockaPrKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtLezajTockaPrKol.Enabled = false;
            this.txtLezajTockaPrKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLezajTockaPrKol.Location = new System.Drawing.Point(1106, 31);
            this.txtLezajTockaPrKol.Name = "txtLezajTockaPrKol";
            this.txtLezajTockaPrKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLezajTockaPrKol.Size = new System.Drawing.Size(30, 22);
            this.txtLezajTockaPrKol.TabIndex = 95;
            this.txtLezajTockaPrKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLezajTockaPrKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1137, 36);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 17);
            this.label23.TabIndex = 96;
            this.label23.Text = "kom.";
            // 
            // txtLezajTockaZaKol
            // 
            this.txtLezajTockaZaKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtLezajTockaZaKol.Enabled = false;
            this.txtLezajTockaZaKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLezajTockaZaKol.Location = new System.Drawing.Point(1106, 72);
            this.txtLezajTockaZaKol.Name = "txtLezajTockaZaKol";
            this.txtLezajTockaZaKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtLezajTockaZaKol.Size = new System.Drawing.Size(30, 22);
            this.txtLezajTockaZaKol.TabIndex = 95;
            this.txtLezajTockaZaKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLezajTockaZaKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1137, 77);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(38, 17);
            this.label24.TabIndex = 96;
            this.label24.Text = "kom.";
            // 
            // txtKrajSponeKol
            // 
            this.txtKrajSponeKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtKrajSponeKol.Enabled = false;
            this.txtKrajSponeKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKrajSponeKol.Location = new System.Drawing.Point(789, 176);
            this.txtKrajSponeKol.Name = "txtKrajSponeKol";
            this.txtKrajSponeKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKrajSponeKol.Size = new System.Drawing.Size(30, 22);
            this.txtKrajSponeKol.TabIndex = 95;
            this.txtKrajSponeKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKrajSponeKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(820, 180);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 17);
            this.label25.TabIndex = 96;
            this.label25.Text = "kom.";
            // 
            // txtGumeKolicina
            // 
            this.txtGumeKolicina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtGumeKolicina.Enabled = false;
            this.txtGumeKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGumeKolicina.Location = new System.Drawing.Point(1106, 201);
            this.txtGumeKolicina.Name = "txtGumeKolicina";
            this.txtGumeKolicina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGumeKolicina.Size = new System.Drawing.Size(30, 22);
            this.txtGumeKolicina.TabIndex = 95;
            this.txtGumeKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGumeKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1137, 206);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 17);
            this.label26.TabIndex = 96;
            this.label26.Text = "kom.";
            // 
            // txtInjectorKol
            // 
            this.txtInjectorKol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtInjectorKol.Enabled = false;
            this.txtInjectorKol.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInjectorKol.Location = new System.Drawing.Point(106, 228);
            this.txtInjectorKol.Name = "txtInjectorKol";
            this.txtInjectorKol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtInjectorKol.Size = new System.Drawing.Size(30, 22);
            this.txtInjectorKol.TabIndex = 95;
            this.txtInjectorKol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInjectorKol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTecnostKlime_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(137, 233);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 17);
            this.label27.TabIndex = 96;
            this.label27.Text = "kom.";
            // 
            // VanredniServis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1302, 850);
            this.ControlBox = false;
            this.Controls.Add(this.gbxOsnovni);
            this.Controls.Add(this.btnOdbaci);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtKilometrazaServ);
            this.Controls.Add(this.txtKoJeVrsio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.gbxOstali);
            this.Controls.Add(this.tpDatumServisa);
            this.MinimumSize = new System.Drawing.Size(1320, 868);
            this.Name = "VanredniServis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vanredni servis";
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxOstali.ResumeLayout(false);
            this.gbxOstali.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilterKlime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFilterTecneFaze;
        private System.Windows.Forms.CheckBox chkFilterTecneFaze;
        private System.Windows.Forms.CheckBox chkFilterGasneFaze;
        private System.Windows.Forms.TextBox txtFilterGasneFaze;
        private System.Windows.Forms.TextBox txtPKKais;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKilometrazaServ;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Button btnOdbaci;
        private System.Windows.Forms.TextBox txtSvecice;
        private System.Windows.Forms.TextBox txtZupKaisLanac;
        private System.Windows.Forms.TextBox txtUljeMenjac;
        private System.Windows.Forms.TextBox txtUlje;
        private System.Windows.Forms.TextBox txtFilterUlja;
        private System.Windows.Forms.TextBox txtFilterGoriva;
        private System.Windows.Forms.TextBox txtTecnostKocenje;
        private System.Windows.Forms.TextBox txtFilterVazduha;
        private System.Windows.Forms.TextBox txtZamenaGuma;
        private System.Windows.Forms.TextBox txtKoJeVrsio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOstaloTekst;
        private System.Windows.Forms.CheckBox chkPKKais;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkUljeMenjac;
        private System.Windows.Forms.CheckBox chkZamenaGuma;
        private System.Windows.Forms.GroupBox gbxOstali;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkFilterUlja;
        private System.Windows.Forms.TextBox txtUljeKolicina;
        private System.Windows.Forms.CheckBox chkFilterGoriva;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkTecnostKocenje;
        private System.Windows.Forms.CheckBox chkSvecice;
        private System.Windows.Forms.CheckBox chkUlje;
        private System.Windows.Forms.CheckBox chkFilterKlime;
        private System.Windows.Forms.CheckBox chkFilterVazduha;
        private System.Windows.Forms.CheckBox chkZupKaisLanac;
        private System.Windows.Forms.DateTimePicker tpDatumServisa;
        private System.Windows.Forms.TextBox txtPopravkaBrenda;
        private System.Windows.Forms.TextBox txtLamela;
        private System.Windows.Forms.TextBox txtBaterijaKljuca;
        private System.Windows.Forms.TextBox txtSetKvacila;
        private System.Windows.Forms.TextBox txtAmortizeriZa;
        private System.Windows.Forms.TextBox txtRashlTecMotora;
        private System.Windows.Forms.CheckBox chkPopravkaBrenda;
        private System.Windows.Forms.CheckBox chkLamela;
        private System.Windows.Forms.TextBox txtAmortizeriPr;
        private System.Windows.Forms.TextBox txtTecnostStakla;
        private System.Windows.Forms.CheckBox chkBaterijaKljuca;
        private System.Windows.Forms.CheckBox chkSetKvacila;
        private System.Windows.Forms.TextBox txtAlternator;
        private System.Windows.Forms.TextBox txtPodloskaCepa;
        private System.Windows.Forms.CheckBox chkAmortizerZa;
        private System.Windows.Forms.CheckBox chkRashlTecMotora;
        private System.Windows.Forms.TextBox txtAnlaser;
        private System.Windows.Forms.TextBox txtMetliceBrisZa;
        private System.Windows.Forms.CheckBox chkAmortizerPr;
        private System.Windows.Forms.CheckBox chkTecnostStakla;
        private System.Windows.Forms.TextBox txtZamajac;
        private System.Windows.Forms.CheckBox chkAlternator;
        private System.Windows.Forms.TextBox txtMetliceBrisPr;
        private System.Windows.Forms.CheckBox chkPodloska;
        private System.Windows.Forms.CheckBox chkAnlaser;
        private System.Windows.Forms.TextBox txtPlocicePakneZa;
        private System.Windows.Forms.CheckBox chkMetliceBrisZa;
        private System.Windows.Forms.CheckBox chkZamajac;
        private System.Windows.Forms.TextBox txtDiskDobosiZa;
        private System.Windows.Forms.CheckBox chkMetliceBrisPr;
        private System.Windows.Forms.TextBox txtDiskoviPr;
        private System.Windows.Forms.CheckBox chkPlocicePakneZa;
        private System.Windows.Forms.TextBox txtDiskPlocicePr;
        private System.Windows.Forms.CheckBox chkDiskDobosiZa;
        private System.Windows.Forms.TextBox txtAkumulator;
        private System.Windows.Forms.CheckBox chkDiskoviPr;
        private System.Windows.Forms.TextBox txtTecnostKlime;
        private System.Windows.Forms.CheckBox chkDiskPlocicePr;
        private System.Windows.Forms.CheckBox chkAkumulator;
        private System.Windows.Forms.CheckBox chkTecnostKlime;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtMaskaPr;
        private System.Windows.Forms.CheckBox chkPrLeviFar;
        private System.Windows.Forms.CheckBox chkPrednjaMaska;
        private System.Windows.Forms.CheckBox chkZadnjiBranik;
        private System.Windows.Forms.TextBox txtMigavacLe;
        private System.Windows.Forms.TextBox txtKrov;
        private System.Windows.Forms.TextBox txtPoklMotora;
        private System.Windows.Forms.TextBox txtRatkapaZaDe;
        private System.Windows.Forms.TextBox txtPrednjiBranik;
        private System.Windows.Forms.TextBox txtRatkapaPrDe;
        private System.Windows.Forms.TextBox txtPragDe;
        private System.Windows.Forms.TextBox txtStopLe;
        private System.Windows.Forms.TextBox txtRetrovizorDe;
        private System.Windows.Forms.TextBox txtMaglZaDe;
        private System.Windows.Forms.TextBox txtZaDeKrilo;
        private System.Windows.Forms.CheckBox chkKrov;
        private System.Windows.Forms.TextBox txtVrataZaLe;
        private System.Windows.Forms.TextBox txtFarPrLe;
        private System.Windows.Forms.CheckBox chkPrednjiBranik;
        private System.Windows.Forms.TextBox txtFarPrDe;
        private System.Windows.Forms.TextBox txtZadnjiBranik;
        private System.Windows.Forms.TextBox txtSvetloTablice;
        private System.Windows.Forms.TextBox txtPrLeKrilo;
        private System.Windows.Forms.TextBox txtMaglPrLe;
        private System.Windows.Forms.TextBox txtVrataZaDe;
        private System.Windows.Forms.TextBox txtRatkapaZaLe;
        private System.Windows.Forms.TextBox txtPrDeKrilo;
        private System.Windows.Forms.TextBox txtRatkapaPrLe;
        private System.Windows.Forms.TextBox txtPragLe;
        private System.Windows.Forms.CheckBox chkZaDeSvetlo;
        private System.Windows.Forms.TextBox txtRetrovizorLe;
        private System.Windows.Forms.CheckBox chkZaLeMagl;
        private System.Windows.Forms.CheckBox chkPrLevaVrata;
        private System.Windows.Forms.CheckBox chkPrDesniFar;
        private System.Windows.Forms.CheckBox chkPrDesnaVrata;
        private System.Windows.Forms.CheckBox chkPrDesnaMagl;
        private System.Windows.Forms.CheckBox chkPrLevoKrilo;
        private System.Windows.Forms.TextBox txtMigavacDe;
        private System.Windows.Forms.CheckBox chkZaLevoKrilo;
        private System.Windows.Forms.CheckBox chkPrLevaMagl;
        private System.Windows.Forms.TextBox txtPoklPrtljaznika;
        private System.Windows.Forms.TextBox txtMaglPrDe;
        private System.Windows.Forms.CheckBox chkPrDesnoKrilo;
        private System.Windows.Forms.CheckBox chkZaLeSvetlo;
        private System.Windows.Forms.TextBox txtZaLeKrilo;
        private System.Windows.Forms.TextBox txtStopDe;
        private System.Windows.Forms.CheckBox chkZaDesnoKrilo;
        private System.Windows.Forms.TextBox txtMaglZaLe;
        private System.Windows.Forms.TextBox txtVrataPrLe;
        private System.Windows.Forms.CheckBox chkZaLeRatkapa;
        private System.Windows.Forms.TextBox txtVrataPrDe;
        private System.Windows.Forms.CheckBox chkDesniMigavac;
        private System.Windows.Forms.CheckBox chkLeviPrag;
        private System.Windows.Forms.CheckBox chkPrLeRatkapa;
        private System.Windows.Forms.CheckBox chkPoklPrtljaznika;
        private System.Windows.Forms.CheckBox chkLeviMigavac;
        private System.Windows.Forms.CheckBox chkLeviRetrovizor;
        private System.Windows.Forms.CheckBox chkSvetloTablice;
        private System.Windows.Forms.CheckBox chkPoklMotora;
        private System.Windows.Forms.CheckBox chkZaDeRatkapa;
        private System.Windows.Forms.CheckBox chkZaDesnaVrata;
        private System.Windows.Forms.CheckBox chkZaDeMagl;
        private System.Windows.Forms.CheckBox chkDesniPrag;
        private System.Windows.Forms.CheckBox chkPrDeRatkapa;
        private System.Windows.Forms.CheckBox chkZaLevaVrata;
        private System.Windows.Forms.CheckBox chkDesniRetrovizor;
        private System.Windows.Forms.TextBox txtPotrosniMaterijal;
        private System.Windows.Forms.TextBox txtLezajTockaZa;
        private System.Windows.Forms.TextBox txtGumiceBalansSta;
        private System.Windows.Forms.TextBox txtLezajTockaPr;
        private System.Windows.Forms.TextBox txtKrajSpone;
        private System.Windows.Forms.TextBox txtStabilizatorZa;
        private System.Windows.Forms.TextBox txtStabilizatorPr;
        private System.Windows.Forms.TextBox txtSijalica;
        private System.Windows.Forms.CheckBox chkLezajTockaZa;
        private System.Windows.Forms.CheckBox chkGumiceBalansSta;
        private System.Windows.Forms.CheckBox chkPotrosniMaterijal;
        private System.Windows.Forms.CheckBox chkLezajTockaPr;
        private System.Windows.Forms.CheckBox chkKrajSpone;
        private System.Windows.Forms.CheckBox chkStabilizatorZa;
        private System.Windows.Forms.CheckBox chkStabilizatorPr;
        private System.Windows.Forms.CheckBox chkSijalica;
        private System.Windows.Forms.TextBox txtSenzorPritGasa;
        private System.Windows.Forms.CheckBox chkSenzorPritGasa;
        private System.Windows.Forms.TextBox txtInjector;
        private System.Windows.Forms.CheckBox chkInjector;
        private System.Windows.Forms.TextBox txtSetDelovaRemont;
        private System.Windows.Forms.CheckBox chkSetDelRemontIspa;
        private System.Windows.Forms.CheckBox chkSenzorPokGasa;
        private System.Windows.Forms.TextBox txtSetCrevaVoda;
        private System.Windows.Forms.CheckBox chkMultiventil;
        private System.Windows.Forms.CheckBox chkSetCrevaVoda;
        private System.Windows.Forms.CheckBox chkPriljucakUtakGasa;
        private System.Windows.Forms.TextBox txtSenzorPokazGasa;
        private System.Windows.Forms.TextBox txtMultiventil;
        private System.Windows.Forms.CheckBox chkGumCrevaGasFaze;
        private System.Windows.Forms.TextBox txtPrikljucakUtakGasa;
        private System.Windows.Forms.TextBox txtGumCrevaGasFaze;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOstaloCena;
        private System.Windows.Forms.CheckBox chkOstalo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSveciceKolicina;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUljeMenjacKolicina;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTecnostKocKolicina;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSijalicaKolicina;
        private System.Windows.Forms.TextBox txtBrisaciKolicina;
        private System.Windows.Forms.TextBox txtRashlTecMotKol;
        private System.Windows.Forms.TextBox txtTecnostStaklaKol;
        private System.Windows.Forms.TextBox txtTecnostKlimeKol;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtInjectorKol;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtGumeKolicina;
        private System.Windows.Forms.TextBox txtLezajTockaZaKol;
        private System.Windows.Forms.TextBox txtLezajTockaPrKol;
        private System.Windows.Forms.TextBox txtStabilizatorPrKol;
        private System.Windows.Forms.TextBox txtKrajSponeKol;
        private System.Windows.Forms.TextBox txtStabilizatorZaKol;
    }
}