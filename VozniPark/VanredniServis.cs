﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class VanredniServis : Form
    {
        public float CenaServ = 0;
        public string Radovi = "";
        private int _IDVoz;
        public VanredniServis(int id, string Reg, string Proiz, string Model)
        {
            InitializeComponent();
            _IDVoz = id;
            lblSluzbeniBroj.Text = id.ToString();
            lblRegistracija.Text = Reg;
            lblProizvodjac.Text = Proiz;
            lblModel.Text = Model;
            tpDatumServisa.Value = DateTime.Today;
        }

        private void chkFilterUlja_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterUlja.Checked == true)
            {
                txtFilterUlja.Enabled = true;
            }
            else
            {
                txtFilterUlja.Text = "";
                txtFilterUlja.Enabled = false;
            }
        }
        private void chkFilterGoriva_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterGoriva.Checked == true)
            {
                txtFilterGoriva.Enabled = true;
            }
            else
            {
                txtFilterGoriva.Text = "";
                txtFilterGoriva.Enabled = false;
            }
        }
        private void chkFilterVazduha_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterVazduha.Checked == true)
            {
                txtFilterVazduha.Enabled = true;
            }
            else
            {
                txtFilterVazduha.Text = "";
                txtFilterVazduha.Enabled = false;
            }
        }
        private void chkFilterKlime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterKlime.Checked == true)
            {
                txtFilterKlime.Enabled = true;
            }
            else
            {
                txtFilterKlime.Text = "";
                txtFilterKlime.Enabled = false;
            }
        }
        private void chkTecnostKlime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTecnostKlime.Checked == true)
            {
                txtTecnostKlime.Enabled = true;
                txtTecnostKlimeKol.Enabled = true;
            }
            else
            {
                txtTecnostKlime.Text = "";
                txtTecnostKlimeKol.Text = "";
                txtTecnostKlime.Enabled = false;
                txtTecnostKlimeKol.Enabled = false;
            }
        }
        private void chkAkumulator_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAkumulator.Checked == true)
            {
                txtAkumulator.Enabled = true;
            }
            else
            {
                txtAkumulator.Text = "";
                txtAkumulator.Enabled = false;
            }
        }
        private void chkUlje_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUlje.Checked == true)
            {
                txtUlje.Enabled = true;
                txtUljeKolicina.Enabled = true;
            }
            else
            {
                txtUlje.Text = "";
                txtUljeKolicina.Text = "";
                txtUlje.Enabled = false;
                txtUljeKolicina.Enabled = false;
            }
        }
        private void chkSvecice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSvecice.Checked == true)
            {
                txtSvecice.Enabled = true;
                txtSveciceKolicina.Enabled = true;
            }
            else
            {
                txtSvecice.Text = "";
                txtSveciceKolicina.Text = "";
                txtSvecice.Enabled = false;
                txtSveciceKolicina.Enabled = false;
            }
        }
        private void chkTecnostKocenje_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTecnostKocenje.Checked == true)
            {
                txtTecnostKocenje.Enabled = true;
                txtTecnostKocKolicina.Enabled = true;
            }
            else
            {
                txtTecnostKocenje.Text = "";
                txtTecnostKocKolicina.Text = "";
                txtTecnostKocenje.Enabled = false;
                txtTecnostKocKolicina.Enabled = false;
            }
        }
        private void chkDiskPlocicePr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDiskPlocicePr.Checked == true)
            {
                txtDiskPlocicePr.Enabled = true;
            }
            else
            {
                txtDiskPlocicePr.Text = "";
                txtDiskPlocicePr.Enabled = false;
            }
        }
        private void chkDiskoviPr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDiskoviPr.Checked == true)
            {
                txtDiskoviPr.Enabled = true;
            }
            else
            {
                txtDiskoviPr.Text = "";
                txtDiskoviPr.Enabled = false;
            }
        }
        private void chkDiskDobosiZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDiskDobosiZa.Checked == true)
            {
                txtDiskDobosiZa.Enabled = true;
            }
            else
            {
                txtDiskDobosiZa.Text = "";
                txtDiskDobosiZa.Enabled = false;
            }
        }
        private void chkPlocicePakneZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPlocicePakneZa.Checked == true)
            {
                txtPlocicePakneZa.Enabled = true;
            }
            else
            {
                txtPlocicePakneZa.Text = "";
                txtPlocicePakneZa.Enabled = false;
            }
        }
        private void chkPKKais_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPKKais.Checked == true)
            {
                txtPKKais.Enabled = true;
            }
            else
            {
                txtPKKais.Text = "";
                txtPKKais.Enabled = false;
            }
        }
        private void chkUljeMenjac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUljeMenjac.Checked == true)
            {
                txtUljeMenjac.Enabled = true;
                txtUljeMenjacKolicina.Enabled = true;
            }
            else
            {
                txtUljeMenjac.Text = "";
                txtUljeMenjacKolicina.Text = "";
                txtUljeMenjac.Enabled = false;
                txtUljeMenjacKolicina.Enabled = false;
            }
        }
        private void chkRashlTecMotora_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRashlTecMotora.Checked == true)
            {
                txtRashlTecMotora.Enabled = true;
                txtRashlTecMotKol.Enabled = true;
            }
            else
            {
                txtRashlTecMotora.Text = "";
                txtRashlTecMotKol.Text = "";
                txtRashlTecMotora.Enabled = false;
                txtRashlTecMotKol.Enabled = false;
            }
        }
        private void chkSetKvacila_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSetKvacila.Checked == true)
            {
                txtSetKvacila.Enabled = true;
            }
            else
            {
                txtSetKvacila.Text = "";
                txtSetKvacila.Enabled = false;
            }
        }
        private void chkBaterijaKljuca_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBaterijaKljuca.Checked == true)
            {
                txtBaterijaKljuca.Enabled = true;
            }
            else
            {
                txtBaterijaKljuca.Text = "";
                txtBaterijaKljuca.Enabled = false;
            }
        }
        private void chkStabilizatorPr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStabilizatorPr.Checked == true)
            {
                txtStabilizatorPr.Enabled = true;
                txtStabilizatorPrKol.Enabled = true;
            }
            else
            {
                txtStabilizatorPr.Text = "";
                txtStabilizatorPrKol.Text = "";
                txtStabilizatorPr.Enabled = false;
                txtStabilizatorPrKol.Enabled = false;
            }
        }
        private void chkStabilizatorZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStabilizatorZa.Checked == true)
            {
                txtStabilizatorZa.Enabled = true;
                txtStabilizatorZaKol.Enabled = true;
            }
            else
            {
                txtStabilizatorZa.Text = "";
                txtStabilizatorZaKol.Text = "";
                txtStabilizatorZa.Enabled = false;
                txtStabilizatorZaKol.Enabled = false;
            }
        }
        private void chkKrajSpone_CheckedChanged(object sender, EventArgs e)
        {
            if (chkKrajSpone.Checked == true)
            {
                txtKrajSpone.Enabled = true;
                txtKrajSponeKol.Enabled = true;
            }
            else
            {
                txtKrajSpone.Text = "";
                txtKrajSponeKol.Text = "";
                txtKrajSpone.Enabled = false;
                txtKrajSponeKol.Enabled = false;
            }
        }
        private void chkLezajTockaPr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLezajTockaPr.Checked == true)
            {
                txtLezajTockaPr.Enabled = true;
                txtLezajTockaPrKol.Enabled = true;
            }
            else
            {
                txtLezajTockaPr.Text = "";
                txtLezajTockaPrKol.Text = "";
                txtLezajTockaPr.Enabled = false;
                txtLezajTockaPrKol.Enabled = false;
            }
        }
        private void chkLezajTockaZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLezajTockaZa.Checked == true)
            {
                txtLezajTockaZa.Enabled = true;
                txtLezajTockaZaKol.Enabled = true;
            }
            else
            {
                txtLezajTockaZa.Text = "";
                txtLezajTockaZaKol.Text = "";
                txtLezajTockaZa.Enabled = false;
                txtLezajTockaZaKol.Enabled = false;
            }
        }
        private void chkGumiceBalansSta_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGumiceBalansSta.Checked == true)
            {
                txtGumiceBalansSta.Enabled = true;
            }
            else
            {
                txtGumiceBalansSta.Text = "";
                txtGumiceBalansSta.Enabled = false;
            }
        }
        private void chkPotrosniMaterijal_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPotrosniMaterijal.Checked == true)
            {
                txtPotrosniMaterijal.Enabled = true;
            }
            else
            {
                txtPotrosniMaterijal.Text = "";
                txtPotrosniMaterijal.Enabled = false;
            }
        }
        private void chkZupKaisLanac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZupKaisLanac.Checked == true)
            {
                txtZupKaisLanac.Enabled = true;
            }
            else
            {
                txtZupKaisLanac.Text = "";
                txtZupKaisLanac.Enabled = false;
            }
        }
        private void chkFilterTecneFaze_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterTecneFaze.Checked == true)
            {
                txtFilterTecneFaze.Enabled = true;
            }
            else
            {
                txtFilterTecneFaze.Text = "";
                txtFilterTecneFaze.Enabled = false;
            }
        }
        private void chkFilterGasneFaze_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterGasneFaze.Checked == true)
            {
                txtFilterGasneFaze.Enabled = true;
            }
            else
            {
                txtFilterGasneFaze.Text = "";
                txtFilterGasneFaze.Enabled = false;
            }
        }
        private void chkOstalo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOstalo.Checked == true)
            {
                txtOstaloCena.Enabled = true;
                txtOstaloTekst.Enabled = true;
            }
            else
            {
                txtOstaloCena.Text = "";
                txtOstaloTekst.Text = "";
                txtOstaloCena.Enabled = false;
                txtOstaloTekst.Enabled = false;
            }
        }
        private void chkSetCrevaVoda_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSetCrevaVoda.Checked == true)
            {
                txtSetCrevaVoda.Enabled = true;
            }
            else
            {
                txtSetCrevaVoda.Text = "";
                txtSetCrevaVoda.Enabled = false;
            }
        }
        private void chkGumCrevaGasFaze_CheckedChanged(object sender, EventArgs e)
        {
            if (chkGumCrevaGasFaze.Checked == true)
            {
                txtGumCrevaGasFaze.Enabled = true;
            }
            else
            {
                txtGumCrevaGasFaze.Text = "";
                txtGumCrevaGasFaze.Enabled = false;
            }
        }
        private void chkSetDelRemontIspa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSetDelRemontIspa.Checked == true)
            {
                txtSetDelovaRemont.Enabled = true;
            }
            else
            {
                txtSetDelovaRemont.Text = "";
                txtSetDelovaRemont.Enabled = false;
            }
        }
        private void chkPriljucakUtakGasa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPriljucakUtakGasa.Checked == true)
            {
                txtPrikljucakUtakGasa.Enabled = true;
            }
            else
            {
                txtPrikljucakUtakGasa.Text = "";
                txtPrikljucakUtakGasa.Enabled = false;
            }
        }
        private void chkInjector_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInjector.Checked == true)
            {
                txtInjector.Enabled = true;
                txtInjectorKol.Enabled = true;
            }
            else
            {
                txtInjector.Text = "";
                txtInjectorKol.Text = "";
                txtInjector.Enabled = false;
                txtInjectorKol.Enabled = false;
            }
        }
        private void chkMultiventil_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMultiventil.Checked == true)
            {
                txtMultiventil.Enabled = true;
            }
            else
            {
                txtMultiventil.Text = "";
                txtMultiventil.Enabled = false;
            }
        }
        private void chkSenzorPritGasa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSenzorPritGasa.Checked == true)
            {
                txtSenzorPritGasa.Enabled = true;
            }
            else
            {
                txtSenzorPritGasa.Text = "";
                txtSenzorPritGasa.Enabled = false;
            }
        }
        private void chkSenzorPokGasa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSenzorPokGasa.Checked == true)
            {
                txtSenzorPokazGasa.Enabled = true;
            }
            else
            {
                txtSenzorPokazGasa.Text = "";
                txtSenzorPokazGasa.Enabled = false;
            }
        }
        private void chkMetliceBrisPr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMetliceBrisPr.Checked == true)
            {
                txtMetliceBrisPr.Enabled = true;
                txtBrisaciKolicina.Enabled = true;
            }
            else
            {
                txtMetliceBrisPr.Text = "";
                txtBrisaciKolicina.Text = "";
                txtMetliceBrisPr.Enabled = false;
                txtBrisaciKolicina.Enabled = false;
            }
        }
        private void chkMetliceBrisZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMetliceBrisZa.Checked == true)
            {
                txtMetliceBrisZa.Enabled = true;
            }
            else
            {
                txtMetliceBrisZa.Text = "";
                txtMetliceBrisZa.Enabled = false;
            }
        }
        private void chkTecnostStakla_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTecnostStakla.Checked == true)
            {
                txtTecnostStakla.Enabled = true;
                txtTecnostStaklaKol.Enabled = true;
            }
            else
            {
                txtTecnostStakla.Text = "";
                txtTecnostStaklaKol.Text = "";
                txtTecnostStakla.Enabled = false;
                txtTecnostStaklaKol.Enabled = false;
            }
        }
        private void chkLamela_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLamela.Checked == true)
            {
                txtLamela.Enabled = true;
            }
            else
            {
                txtLamela.Text = "";
                txtLamela.Enabled = false;
            }
        }
        private void chkZamajac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZamajac.Checked == true)
            {
                txtZamajac.Enabled = true;
            }
            else
            {
                txtZamajac.Text = "";
                txtZamajac.Enabled = false;
            }
        }
        private void chkAnlaser_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAnlaser.Checked == true)
            {
                txtAnlaser.Enabled = true;
            }
            else
            {
                txtAnlaser.Text = "";
                txtAnlaser.Enabled = false;
            }
        }
        private void chkAlternator_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAlternator.Checked == true)
            {
                txtAlternator.Enabled = true;
            }
            else
            {
                txtAlternator.Text = "";
                txtAlternator.Enabled = false;
            }
        }
        private void chkPodloska_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPodloska.Checked == true)
            {
                txtPodloskaCepa.Enabled = true;
            }
            else
            {
                txtPodloskaCepa.Text = "";
                txtPodloskaCepa.Enabled = false;
            }
        }
        private void chkAmortizerZa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAmortizerZa.Checked == true)
            {
                txtAmortizeriZa.Enabled = true;
            }
            else
            {
                txtAmortizeriZa.Text = "";
                txtAmortizeriZa.Enabled = false;
            }
        }
        private void chkAmortizerPr_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAmortizerPr.Checked == true)
            {
                txtAmortizeriPr.Enabled = true;
            }
            else
            {
                txtAmortizeriPr.Text = "";
                txtAmortizeriPr.Enabled = false;
            }
        }
        private void chkSijalica_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSijalica.Checked == true)
            {
                txtSijalica.Enabled = true;
                txtSijalicaKolicina.Enabled = true;
            }
            else
            {
                txtSijalica.Text = "";
                txtSijalicaKolicina.Text = "";
                txtSijalica.Enabled = false;
                txtSijalicaKolicina.Enabled = false;
            }
        }
        private void chkPopravkaBrenda_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPopravkaBrenda.Checked == true)
            {
                txtPopravkaBrenda.Enabled = true;
            }
            else
            {
                txtPopravkaBrenda.Text = "";
                txtPopravkaBrenda.Enabled = false;
            }
        }
        private void chkPrednjiBranik_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrednjiBranik.Checked == true)
            {
                txtPrednjiBranik.Enabled = true;
            }
            else
            {
                txtPrednjiBranik.Text = "";
                txtPrednjiBranik.Enabled = false;
            }
        }
        private void chkZadnjiBranik_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZadnjiBranik.Checked == true)
            {
                txtZadnjiBranik.Enabled = true;
            }
            else
            {
                txtZadnjiBranik.Text = "";
                txtZadnjiBranik.Enabled = false;
            }
        }
        private void chkPrLevoKrilo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrLevoKrilo.Checked == true)
            {
                txtPrLeKrilo.Enabled = true;
            }
            else
            {
                txtPrLeKrilo.Text = "";
                txtPrLeKrilo.Enabled = false;
            }
        }
        private void chkPrDesnoKrilo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrDesnoKrilo.Checked == true)
            {
                txtPrDeKrilo.Enabled = true;
            }
            else
            {
                txtPrDeKrilo.Text = "";
                txtPrDeKrilo.Enabled = false;
            }
        }
        private void chkZaLevoKrilo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaLevoKrilo.Checked == true)
            {
                txtZaLeKrilo.Enabled = true;
            }
            else
            {
                txtZaLeKrilo.Text = "";
                txtZaLeKrilo.Enabled = false;
            }
        }
        private void chkZaDesnoKrilo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaDesnoKrilo.Checked == true)
            {
                txtZaDeKrilo.Enabled = true;
            }
            else
            {
                txtZaDeKrilo.Text = "";
                txtZaDeKrilo.Enabled = false;
            }
        }
        private void chkPrLevaVrata_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrLevaVrata.Checked == true)
            {
                txtVrataPrLe.Enabled = true;
            }
            else
            {
                txtVrataPrLe.Text = "";
                txtVrataPrLe.Enabled = false;
            }
        }
        private void chkPrDesnaVrata_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrDesnaVrata.Checked == true)
            {
                txtVrataPrDe.Enabled = true;
            }
            else
            {
                txtVrataPrDe.Text = "";
                txtVrataPrDe.Enabled = false;
            }
        }
        private void chkZaLevaVrata_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaLevaVrata.Checked == true)
            {
                txtVrataZaLe.Enabled = true;
            }
            else
            {
                txtVrataZaLe.Text = "";
                txtVrataZaLe.Enabled = false;
            }
        }
        private void chkZaDesnaVrata_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaDesnaVrata.Checked == true)
            {
                txtVrataZaDe.Enabled = true;
            }
            else
            {
                txtVrataZaDe.Text = "";
                txtVrataZaDe.Enabled = false;
            }
        }
        private void chkPoklMotora_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPoklMotora.Checked == true)
            {
                txtPoklMotora.Enabled = true;
            }
            else
            {
                txtPoklMotora.Text = "";
                txtPoklMotora.Enabled = false;
            }
        }
        private void chkPoklPrtljaznika_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPoklPrtljaznika.Checked == true)
            {
                txtPoklPrtljaznika.Enabled = true;
            }
            else
            {
                txtPoklPrtljaznika.Text = "";
                txtPoklPrtljaznika.Enabled = false;
            }
        }
        private void chkLeviRetrovizor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLeviRetrovizor.Checked == true)
            {
                txtRetrovizorLe.Enabled = true;
            }
            else
            {
                txtRetrovizorLe.Text = "";
                txtRetrovizorLe.Enabled = false;
            }
        }
        private void chkDesniRetrovizor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDesniRetrovizor.Checked == true)
            {
                txtRetrovizorDe.Enabled = true;
            }
            else
            {
                txtRetrovizorDe.Text = "";
                txtRetrovizorDe.Enabled = false;
            }
        }
        private void chkLeviPrag_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLeviPrag.Checked == true)
            {
                txtPragLe.Enabled = true;
            }
            else
            {
                txtPragLe.Text = "";
                txtPragLe.Enabled = false;
            }
        }
        private void chkDesniPrag_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDesniPrag.Checked == true)
            {
                txtPragDe.Enabled = true;
            }
            else
            {
                txtPragDe.Text = "";
                txtPragDe.Enabled = false;
            }
        }
        private void chkPrednjaMaska_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrednjaMaska.Checked == true)
            {
                txtMaskaPr.Enabled = true;
            }
            else
            {
                txtMaskaPr.Text = "";
                txtMaskaPr.Enabled = false;
            }
        }
        private void chkKrov_CheckedChanged(object sender, EventArgs e)
        {
            if (chkKrov.Checked == true)
            {
                txtKrov.Enabled = true;
            }
            else
            {
                txtKrov.Text = "";
                txtKrov.Enabled = false;
            }
        }
        private void chkPrLeviFar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrLeviFar.Checked == true)
            {
                txtFarPrLe.Enabled = true;
            }
            else
            {
                txtFarPrLe.Text = "";
                txtFarPrLe.Enabled = false;
            }
        }
        private void chkPrDesniFar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrDesniFar.Checked == true)
            {
                txtFarPrDe.Enabled = true;
            }
            else
            {
                txtFarPrDe.Text = "";
                txtFarPrDe.Enabled = false;
            }
        }
        private void chkPrLevaMagl_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrLevaMagl.Checked == true)
            {
                txtMaglPrLe.Enabled = true;
            }
            else
            {
                txtMaglPrLe.Text = "";
                txtMaglPrLe.Enabled = false;
            }
        }
        private void chkPrDesnaMagl_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrDesnaMagl.Checked == true)
            {
                txtMaglPrDe.Enabled = true;
            }
            else
            {
                txtMaglPrDe.Text = "";
                txtMaglPrDe.Enabled = false;
            }
        }
        private void chkZaLeSvetlo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaLeSvetlo.Checked == true)
            {
                txtStopLe.Enabled = true;
            }
            else
            {
                txtStopLe.Text = "";
                txtStopLe.Enabled = false;
            }
        }
        private void chkZaDeSvetlo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaDeSvetlo.Checked == true)
            {
                txtStopDe.Enabled = true;
            }
            else
            {
                txtStopDe.Text = "";
                txtStopDe.Enabled = false;
            }
        }
        private void chkZaLeMagl_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaLeMagl.Checked == true)
            {
                txtMaglZaLe.Enabled = true;
            }
            else
            {
                txtMaglZaLe.Text = "";
                txtMaglZaLe.Enabled = false;
            }
        }
        private void chkZaDeMagl_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaDeMagl.Checked == true)
            {
                txtMaglZaDe.Enabled = true;
            }
            else
            {
                txtMaglZaDe.Text = "";
                txtMaglZaDe.Enabled = false;
            }
        }
        private void chkSvetloTablice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSvetloTablice.Checked == true)
            {
                txtSvetloTablice.Enabled = true;
            }
            else
            {
                txtSvetloTablice.Text = "";
                txtSvetloTablice.Enabled = false;
            }
        }
        private void chkLeviMigavac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLeviMigavac.Checked == true)
            {
                txtMigavacLe.Enabled = true;
            }
            else
            {
                txtMigavacLe.Text = "";
                txtMigavacLe.Enabled = false;
            }
        }
        private void chkDesniMigavac_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDesniMigavac.Checked == true)
            {
                txtMigavacDe.Enabled = true;
            }
            else
            {
                txtMigavacDe.Text = "";
                txtMigavacDe.Enabled = false;
            }
        }
        private void chkPrLeRatkapa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrLeRatkapa.Checked == true)
            {
                txtRatkapaPrLe.Enabled = true;
            }
            else
            {
                txtRatkapaPrLe.Text = "";
                txtRatkapaPrLe.Enabled = false;
            }
        }
        private void chkPrDeRatkapa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrDeRatkapa.Checked == true)
            {
                txtRatkapaPrDe.Enabled = true;
            }
            else
            {
                txtRatkapaPrDe.Text = "";
                txtRatkapaPrDe.Enabled = false;
            }
        }
        private void chkZaLeRatkapa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaLeRatkapa.Checked == true)
            {
                txtRatkapaZaLe.Enabled = true;
            }
            else
            {
                txtRatkapaZaLe.Text = "";
                txtRatkapaZaLe.Enabled = false;
            }
        }
        private void chkZaDeRatkapa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZaDeRatkapa.Checked == true)
            {
                txtRatkapaZaDe.Enabled = true;
            }
            else
            {
                txtRatkapaZaDe.Text = "";
                txtRatkapaZaDe.Enabled = false;
            }
        }
        private void chkZamenaGuma_CheckedChanged(object sender, EventArgs e)
        {
            if (chkZamenaGuma.Checked == true)
            {
                txtZamenaGuma.Enabled = true;
                txtGumeKolicina.Enabled = true;
            }
            else
            {
                txtZamenaGuma.Text = "";
                txtGumeKolicina.Text = "";
                txtZamenaGuma.Enabled = false;
                txtGumeKolicina.Enabled = false;
            }
        }

        private void ispitajCekirano()
        {
            CenaServ = 0;
            Radovi = "";
            string str1 = ",";
            string str2 = ".";

            //stavke sa redovnog
            if (chkFilterUlja.Checked == true)
            {
                txtFilterUlja.Text = txtFilterUlja.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterUlja.Text + "  Cena: " + txtFilterUlja.Text + "#";
                if (txtFilterUlja.Text == "") txtFilterUlja.BackColor = Color.Red; else txtFilterUlja.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterUlja.Text);
            }
            if (chkFilterGoriva.Checked == true)
            {
                txtFilterGoriva.Text = txtFilterGoriva.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterGoriva.Text + "  Cena: " + txtFilterGoriva.Text + "#";
                if (txtFilterGoriva.Text == "") txtFilterGoriva.BackColor = Color.Red; else txtFilterGoriva.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterGoriva.Text);
            }
            if (chkFilterVazduha.Checked == true)
            {
                txtFilterVazduha.Text = txtFilterVazduha.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterVazduha.Text + "  Cena: " + txtFilterVazduha.Text + "#";
                if (txtFilterVazduha.Text == "") txtFilterVazduha.BackColor = Color.Red; else txtFilterVazduha.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterVazduha.Text);
            }
            if (chkFilterKlime.Checked == true)
            {
                txtFilterKlime.Text = txtFilterKlime.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterKlime.Text + "  Cena: " + txtFilterKlime.Text + "#";
                if (txtFilterKlime.Text == "") txtFilterKlime.BackColor = Color.Red; else txtFilterKlime.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterKlime.Text);
            }
            if (chkUlje.Checked == true)
            {
                txtUlje.Text = txtUlje.Text.Replace(str1, str2);
                Radovi = Radovi + chkUlje.Text + "  " + txtUljeKolicina.Text + "lit.  Cena: " + txtUlje.Text + "#";
                if (txtUlje.Text == "") txtUlje.BackColor = Color.Red; else txtUlje.BackColor = Color.White;
                if (txtUljeKolicina.Text == "") txtUljeKolicina.BackColor = Color.Red; else txtUljeKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtUlje.Text);
            }
            if (chkSvecice.Checked == true)
            {
                txtSvecice.Text = txtSvecice.Text.Replace(str1, str2);
                Radovi = Radovi + chkSvecice.Text + "  " + txtSveciceKolicina.Text + "kom.  Cena: " + txtSvecice.Text + "#";
                if (txtSvecice.Text == "") txtSvecice.BackColor = Color.Red; else txtSvecice.BackColor = Color.White;
                if (txtSveciceKolicina.Text == "") txtSveciceKolicina.BackColor = Color.Red;
                else txtSveciceKolicina.BackColor = Color.FromArgb(128, 255, 255);
                CenaServ = CenaServ + float.Parse(txtSvecice.Text);
            }
            if (chkTecnostKocenje.Checked == true)
            {
                txtTecnostKocenje.Text = txtTecnostKocenje.Text.Replace(str1, str2);
                Radovi = Radovi + chkTecnostKocenje.Text + "  Cena: " + txtTecnostKocenje.Text + "#";
                if (txtTecnostKocenje.Text == "") txtTecnostKocenje.BackColor = Color.Red; else txtTecnostKocenje.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtTecnostKocenje.Text);
            }
            if (chkPKKais.Checked == true)
            {
                txtPKKais.Text = txtPKKais.Text.Replace(str1, str2);
                Radovi = Radovi + chkPKKais.Text + "  Cena: " + txtPKKais.Text + "#";
                if (txtPKKais.Text == "") txtPKKais.BackColor = Color.Red; else txtPKKais.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPKKais.Text);
            }
            if (chkUljeMenjac.Checked == true)
            {
                txtUljeMenjac.Text = txtUljeMenjac.Text.Replace(str1, str2);
                Radovi = Radovi + chkUljeMenjac.Text + "  Cena: " + txtUljeMenjac.Text + "#";
                if (txtUljeMenjac.Text == "") txtUljeMenjac.BackColor = Color.Red; else txtUljeMenjac.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtUljeMenjac.Text);
            }
            if (chkZupKaisLanac.Checked == true)
            {
                txtZupKaisLanac.Text = txtZupKaisLanac.Text.Replace(str1, str2);
                Radovi = Radovi + chkZupKaisLanac.Text + "  Cena: " + txtZupKaisLanac.Text + "#";
                if (txtZupKaisLanac.Text == "") txtZupKaisLanac.BackColor = Color.Red; else txtZupKaisLanac.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZupKaisLanac.Text);
            }

            //Stavke sa vanrednog
            if (chkTecnostKlime.Checked == true)
            {
                txtTecnostKlime.Text = txtTecnostKlime.Text.Replace(str1, str2);
                Radovi = Radovi + chkTecnostKlime.Text + "  Cena: " + txtTecnostKlime.Text + "#";
                if (txtTecnostKlime.Text == "") txtTecnostKlime.BackColor = Color.Red; else txtTecnostKlime.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtTecnostKlime.Text);
            }
            if (chkAkumulator.Checked == true)
            {
                txtAkumulator.Text = txtAkumulator.Text.Replace(str1, str2);
                Radovi = Radovi + chkAkumulator.Text + "  Cena: " + txtAkumulator.Text + "#";
                if (txtAkumulator.Text == "") txtAkumulator.BackColor = Color.Red; else txtAkumulator.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtAkumulator.Text);
            }
            if (chkDiskPlocicePr.Checked == true)
            {
                txtDiskPlocicePr.Text = txtDiskPlocicePr.Text.Replace(str1, str2);
                Radovi = Radovi + chkDiskPlocicePr.Text + "  Cena: " + txtDiskPlocicePr.Text + "#";
                if (txtDiskPlocicePr.Text == "") txtDiskPlocicePr.BackColor = Color.Red; else txtDiskPlocicePr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtDiskPlocicePr.Text);
            }
            if (chkDiskoviPr.Checked == true)
            {
                txtDiskoviPr.Text = txtDiskoviPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkDiskoviPr.Text + "  Cena: " + txtDiskoviPr.Text + "#";
                if (txtDiskoviPr.Text == "") txtDiskoviPr.BackColor = Color.Red; else txtDiskoviPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtDiskoviPr.Text);
            }
            if (chkDiskDobosiZa.Checked == true)
            {
                txtDiskDobosiZa.Text = txtDiskDobosiZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkDiskDobosiZa.Text + "  Cena: " + txtDiskDobosiZa.Text + "#";
                if (txtDiskDobosiZa.Text == "") txtDiskDobosiZa.BackColor = Color.Red; else txtDiskDobosiZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtDiskDobosiZa.Text);
            }
            if (chkPlocicePakneZa.Checked == true)
            {
                txtPlocicePakneZa.Text = txtPlocicePakneZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkPlocicePakneZa.Text + "  Cena: " + txtPlocicePakneZa.Text + "#";
                if (txtPlocicePakneZa.Text == "") txtPlocicePakneZa.BackColor = Color.Red; else txtPlocicePakneZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPlocicePakneZa.Text);
            }
            if (chkMetliceBrisPr.Checked == true)
            {
                txtMetliceBrisPr.Text = txtMetliceBrisPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkMetliceBrisPr.Text + "  Cena: " + txtMetliceBrisPr.Text + "#";
                if (txtMetliceBrisPr.Text == "") txtMetliceBrisPr.BackColor = Color.Red; else txtMetliceBrisPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMetliceBrisPr.Text);
            }
            if (chkMetliceBrisZa.Checked == true)
            {
                txtMetliceBrisZa.Text = txtMetliceBrisZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkMetliceBrisZa.Text + "  Cena: " + txtMetliceBrisZa.Text + "#";
                if (txtMetliceBrisZa.Text == "") txtMetliceBrisZa.BackColor = Color.Red; else txtMetliceBrisZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMetliceBrisZa.Text);
            }
            if (chkTecnostStakla.Checked == true)
            {
                txtTecnostStakla.Text = txtTecnostStakla.Text.Replace(str1, str2);
                Radovi = Radovi + chkTecnostStakla.Text + "  Cena: " + txtTecnostStakla.Text + "#";
                if (txtTecnostStakla.Text == "") txtTecnostStakla.BackColor = Color.Red; else txtTecnostStakla.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtTecnostStakla.Text);
            }
            if (chkRashlTecMotora.Checked == true)
            {
                txtRashlTecMotora.Text = txtRashlTecMotora.Text.Replace(str1, str2);
                Radovi = Radovi + chkRashlTecMotora.Text + "  Cena: " + txtRashlTecMotora.Text + "#";
                if (txtRashlTecMotora.Text == "") txtRashlTecMotora.BackColor = Color.Red; else txtRashlTecMotora.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRashlTecMotora.Text);
            }
            if (chkSetKvacila.Checked == true)
            {
                txtSetKvacila.Text = txtSetKvacila.Text.Replace(str1, str2);
                Radovi = Radovi + chkSetKvacila.Text + "  Cena: " + txtSetKvacila.Text + "#";
                if (txtSetKvacila.Text == "") txtSetKvacila.BackColor = Color.Red; else txtSetKvacila.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSetKvacila.Text);
            }
            if (chkLamela.Checked == true)
            {
                txtLamela.Text = txtLamela.Text.Replace(str1, str2);
                Radovi = Radovi + chkLamela.Text + "  Cena: " + txtLamela.Text + "#";
                if (txtLamela.Text == "") txtLamela.BackColor = Color.Red; else txtLamela.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtLamela.Text);
            }
            if (chkZamajac.Checked == true)
            {
                txtZamajac.Text = txtZamajac.Text.Replace(str1, str2);
                Radovi = Radovi + chkZamajac.Text + "  Cena: " + txtZamajac.Text + "#";
                if (txtZamajac.Text == "") txtZamajac.BackColor = Color.Red; else txtZamajac.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZamajac.Text);
            }
            if (chkAnlaser.Checked == true)
            {
                txtAnlaser.Text = txtAnlaser.Text.Replace(str1, str2);
                Radovi = Radovi + chkAnlaser.Text + "  Cena: " + txtAnlaser.Text + "#";
                if (txtAnlaser.Text == "") txtAnlaser.BackColor = Color.Red; else txtAnlaser.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtAnlaser.Text);
            }
            if (chkAlternator.Checked == true)
            {
                txtAlternator.Text = txtAlternator.Text.Replace(str1, str2);
                Radovi = Radovi + chkAlternator.Text + "  Cena: " + txtAlternator.Text + "#";
                if (txtAlternator.Text == "") txtAlternator.BackColor = Color.Red; else txtAlternator.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtAlternator.Text);
            }
            if (chkAmortizerPr.Checked == true)
            {
                txtAmortizeriPr.Text = txtAmortizeriPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkAmortizerPr.Text + "  Cena: " + txtAmortizeriPr.Text + "#";
                if (txtAmortizeriPr.Text == "") txtAmortizeriPr.BackColor = Color.Red; else txtAmortizeriPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtAmortizeriPr.Text);
            }
            if (chkAmortizerZa.Checked == true)
            {
                txtAmortizeriZa.Text = txtAmortizeriZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkAmortizerZa.Text + "  Cena: " + txtAmortizeriZa.Text + "#";
                if (txtAmortizeriZa.Text == "") txtAmortizeriZa.BackColor = Color.Red; else txtAmortizeriZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtAmortizeriZa.Text);
            }
            if (chkStabilizatorPr.Checked == true)
            {
                txtStabilizatorPr.Text = txtStabilizatorPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkStabilizatorPr.Text + "  Cena: " + txtStabilizatorPr.Text + "#";
                if (txtStabilizatorPr.Text == "") txtStabilizatorPr.BackColor = Color.Red; else txtStabilizatorPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtStabilizatorPr.Text);
            }
            if (chkStabilizatorZa.Checked == true)
            {
                txtStabilizatorZa.Text = txtStabilizatorZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkStabilizatorZa.Text + "  Cena: " + txtStabilizatorZa.Text + "#";
                if (txtStabilizatorZa.Text == "") txtStabilizatorZa.BackColor = Color.Red; else txtStabilizatorZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtStabilizatorZa.Text);
            }
            if (chkKrajSpone.Checked == true)
            {
                txtKrajSpone.Text = txtKrajSpone.Text.Replace(str1, str2);
                Radovi = Radovi + chkKrajSpone.Text + "  Cena: " + txtKrajSpone.Text + "#";
                if (txtKrajSpone.Text == "") txtKrajSpone.BackColor = Color.Red; else txtKrajSpone.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtKrajSpone.Text);
            }
            if (chkLezajTockaPr.Checked == true)
            {
                txtLezajTockaPr.Text = txtLezajTockaPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkLezajTockaPr.Text + "  Cena: " + txtLezajTockaPr.Text + "#";
                if (txtLezajTockaPr.Text == "") txtLezajTockaPr.BackColor = Color.Red; else txtLezajTockaPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtLezajTockaPr.Text);
            }
            if (chkLezajTockaZa.Checked == true)
            {
                txtLezajTockaZa.Text = txtLezajTockaZa.Text.Replace(str1, str2);
                Radovi = Radovi + chkLezajTockaZa.Text + "  Cena: " + txtLezajTockaZa.Text + "#";
                if (txtLezajTockaZa.Text == "") txtLezajTockaZa.BackColor = Color.Red; else txtLezajTockaZa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtLezajTockaZa.Text);
            }
            if (chkGumiceBalansSta.Checked == true)
            {
                txtGumiceBalansSta.Text = txtGumiceBalansSta.Text.Replace(str1, str2);
                Radovi = Radovi + chkGumiceBalansSta.Text + "  Cena: " + txtGumiceBalansSta.Text + "#";
                if (txtGumiceBalansSta.Text == "") txtGumiceBalansSta.BackColor = Color.Red; else txtGumiceBalansSta.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtGumiceBalansSta.Text);
            }
            if (chkPodloska.Checked == true)
            {
                txtPodloskaCepa.Text = txtPodloskaCepa.Text.Replace(str1, str2);
                Radovi = Radovi + chkPodloska.Text + "  Cena: " + txtPodloskaCepa.Text + "#";
                if (txtPodloskaCepa.Text == "") txtPodloskaCepa.BackColor = Color.Red; else txtPodloskaCepa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPodloskaCepa.Text);
            }
            if (chkSijalica.Checked == true)
            {
                txtSijalica.Text = txtSijalica.Text.Replace(str1, str2);
                Radovi = Radovi + chkSijalica.Text + "  Cena: " + txtSijalica.Text + "#";
                if (txtSijalica.Text == "") txtSijalica.BackColor = Color.Red; else txtSijalica.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSijalica.Text);
            }
            if (chkBaterijaKljuca.Checked == true)
            {
                txtBaterijaKljuca.Text = txtBaterijaKljuca.Text.Replace(str1, str2);
                Radovi = Radovi + chkBaterijaKljuca.Text + "  Cena: " + txtBaterijaKljuca.Text + "#";
                if (txtBaterijaKljuca.Text == "") txtBaterijaKljuca.BackColor = Color.Red; else txtBaterijaKljuca.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtBaterijaKljuca.Text);
            }

            //Karoserija
            if (chkPrednjiBranik.Checked == true)
            {
                txtPrednjiBranik.Text = txtPrednjiBranik.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrednjiBranik.Text + "  Cena: " + txtPrednjiBranik.Text + "#";
                if (txtPrednjiBranik.Text == "") txtPrednjiBranik.BackColor = Color.Red; else txtPrednjiBranik.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPrednjiBranik.Text);
            }
            if (chkZadnjiBranik.Checked == true)
            {
                txtZadnjiBranik.Text = txtZadnjiBranik.Text.Replace(str1, str2);
                Radovi = Radovi + chkZadnjiBranik.Text + "  Cena: " + txtZadnjiBranik.Text + "#";
                if (txtZadnjiBranik.Text == "") txtZadnjiBranik.BackColor = Color.Red; else txtZadnjiBranik.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZadnjiBranik.Text);
            }
            if (chkPrLevoKrilo.Checked == true)
            {
                txtPrLeKrilo.Text = txtPrLeKrilo.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrLevoKrilo.Text + "  Cena: " + txtPrLeKrilo.Text + "#";
                if (txtPrLeKrilo.Text == "") txtPrLeKrilo.BackColor = Color.Red; else txtPrLeKrilo.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPrLeKrilo.Text);
            }
            if (chkPrDesnoKrilo.Checked == true)
            {
                txtPrDeKrilo.Text = txtPrDeKrilo.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrDesnoKrilo.Text + "  Cena: " + txtPrDeKrilo.Text + "#";
                if (txtPrDeKrilo.Text == "") txtPrDeKrilo.BackColor = Color.Red; else txtPrDeKrilo.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPrDeKrilo.Text);
            }
            if (chkZaLevoKrilo.Checked == true)
            {
                txtZaLeKrilo.Text = txtZaLeKrilo.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaLevoKrilo.Text + "  Cena: " + txtZaLeKrilo.Text + "#";
                if (txtZaLeKrilo.Text == "") txtZaLeKrilo.BackColor = Color.Red; else txtZaLeKrilo.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZaLeKrilo.Text);
            }
            if (chkZaDesnoKrilo.Checked == true)
            {
                txtZaDeKrilo.Text = txtZaDeKrilo.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaDesnoKrilo.Text + "  Cena: " + txtZaDeKrilo.Text + "#";
                if (txtZaDeKrilo.Text == "") txtZaDeKrilo.BackColor = Color.Red; else txtZaDeKrilo.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZaDeKrilo.Text);
            }
            if (chkPrLevaVrata.Checked == true)
            {
                txtVrataPrLe.Text = txtVrataPrLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrLevaVrata.Text + "  Cena: " + txtVrataPrLe.Text + "#";
                if (txtVrataPrLe.Text == "") txtVrataPrLe.BackColor = Color.Red; else txtVrataPrLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtVrataPrLe.Text);
            }
            if (chkPrDesnaVrata.Checked == true)
            {
                txtVrataPrDe.Text = txtVrataPrDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrDesnaVrata.Text + "  Cena: " + txtVrataPrDe.Text + "#";
                if (txtVrataPrDe.Text == "") txtVrataPrDe.BackColor = Color.Red; else txtVrataPrDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtVrataPrDe.Text);
            }
            if (chkZaLevaVrata.Checked == true)
            {
                txtVrataZaLe.Text = txtVrataZaLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaLevaVrata.Text + "  Cena: " + txtVrataZaLe.Text + "#";
                if (txtVrataZaLe.Text == "") txtVrataZaLe.BackColor = Color.Red; else txtVrataZaLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtVrataZaLe.Text);
            }
            if (chkZaDesnaVrata.Checked == true)
            {
                txtVrataZaDe.Text = txtVrataZaDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaDesnaVrata.Text + "  Cena: " + txtVrataZaDe.Text + "#";
                if (txtVrataZaDe.Text == "") txtVrataZaDe.BackColor = Color.Red; else txtVrataZaDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtVrataZaDe.Text);
            }
            if (chkPoklMotora.Checked == true)
            {
                txtPoklMotora.Text = txtPoklMotora.Text.Replace(str1, str2);
                Radovi = Radovi + chkPoklMotora.Text + "  Cena: " + txtPoklMotora.Text + "#";
                if (txtPoklMotora.Text == "") txtPoklMotora.BackColor = Color.Red; else txtPoklMotora.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPoklMotora.Text);
            }
            if (chkPoklPrtljaznika.Checked == true)
            {
                txtPoklPrtljaznika.Text = txtPoklPrtljaznika.Text.Replace(str1, str2);
                Radovi = Radovi + chkPoklPrtljaznika.Text + "  Cena: " + txtPoklPrtljaznika.Text + "#";
                if (txtPoklPrtljaznika.Text == "") txtPoklPrtljaznika.BackColor = Color.Red; else txtPoklPrtljaznika.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPoklPrtljaznika.Text);
            }
            if (chkLeviRetrovizor.Checked == true)
            {
                txtRetrovizorLe.Text = txtRetrovizorLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkLeviRetrovizor.Text + "  Cena: " + txtRetrovizorLe.Text + "#";
                if (txtRetrovizorLe.Text == "") txtRetrovizorLe.BackColor = Color.Red; else txtRetrovizorLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRetrovizorLe.Text);
            }
            if (chkDesniRetrovizor.Checked == true)
            {
                txtRetrovizorDe.Text = txtRetrovizorDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkDesniRetrovizor.Text + "  Cena: " + txtRetrovizorDe.Text + "#";
                if (txtRetrovizorDe.Text == "") txtRetrovizorDe.BackColor = Color.Red; else txtRetrovizorDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRetrovizorDe.Text);
            }
            if (chkLeviPrag.Checked == true)
            {
                txtPragLe.Text = txtPragLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkLeviPrag.Text + "  Cena: " + txtPragLe.Text + "#";
                if (txtPragLe.Text == "") txtPragLe.BackColor = Color.Red; else txtPragLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPragLe.Text);
            }
            if (chkDesniPrag.Checked == true)
            {
                txtPragDe.Text = txtPragDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkDesniPrag.Text + "  Cena: " + txtPragDe.Text + "#";
                if (txtPragDe.Text == "") txtPragDe.BackColor = Color.Red; else txtPragDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPragDe.Text);
            }
            if (chkPrednjaMaska.Checked == true)
            {
                txtMaskaPr.Text = txtMaskaPr.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrednjaMaska.Text + "  Cena: " + txtMaskaPr.Text + "#";
                if (txtMaskaPr.Text == "") txtMaskaPr.BackColor = Color.Red; else txtMaskaPr.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMaskaPr.Text);
            }
            if (chkKrov.Checked == true)
            {
                txtKrov.Text = txtKrov.Text.Replace(str1, str2);
                Radovi = Radovi + chkKrov.Text + "  Cena: " + txtKrov.Text + "#";
                if (txtKrov.Text == "") txtKrov.BackColor = Color.Red; else txtKrov.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtKrov.Text);
            }
            if (chkPrLeviFar.Checked == true)
            {
                txtFarPrLe.Text = txtFarPrLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrLeviFar.Text + "  Cena: " + txtFarPrLe.Text + "#";
                if (txtFarPrLe.Text == "") txtFarPrLe.BackColor = Color.Red; else txtFarPrLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFarPrLe.Text);
            }
            if (chkPrDesniFar.Checked == true)
            {
                txtFarPrDe.Text = txtFarPrDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrDesniFar.Text + "  Cena: " + txtFarPrDe.Text + "#";
                if (txtFarPrDe.Text == "") txtFarPrDe.BackColor = Color.Red; else txtFarPrDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFarPrDe.Text);
            }
            if (chkPrLevaMagl.Checked == true)
            {
                txtMaglPrLe.Text = txtMaglPrLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrLevaMagl.Text + "  Cena: " + txtMaglPrLe.Text + "#";
                if (txtMaglPrLe.Text == "") txtMaglPrLe.BackColor = Color.Red; else txtMaglPrLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMaglPrLe.Text);
            }
            if (chkPrDesnaMagl.Checked == true)
            {
                txtMaglPrDe.Text = txtMaglPrDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrDesnaMagl.Text + "  Cena: " + txtMaglPrDe.Text + "#";
                if (txtMaglPrDe.Text == "") txtMaglPrDe.BackColor = Color.Red; else txtMaglPrDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMaglPrDe.Text);
            }
            if (chkZaLeSvetlo.Checked == true)
            {
                txtStopLe.Text = txtStopLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaLeSvetlo.Text + "  Cena: " + txtStopLe.Text + "#";
                if (txtStopLe.Text == "") txtStopLe.BackColor = Color.Red; else txtStopLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtStopLe.Text);
            }
            if (chkZaDeSvetlo.Checked == true)
            {
                txtStopDe.Text = txtStopDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaDeSvetlo.Text + "  Cena: " + txtStopDe.Text + "#";
                if (txtStopDe.Text == "") txtStopDe.BackColor = Color.Red; else txtStopDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtStopDe.Text);
            }
            if (chkZaLeMagl.Checked == true)
            {
                txtMaglZaLe.Text = txtMaglZaLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaLeMagl.Text + "  Cena: " + txtMaglZaLe.Text + "#";
                if (txtMaglZaLe.Text == "") txtMaglZaLe.BackColor = Color.Red; else txtMaglZaLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMaglZaLe.Text);
            }
            if (chkZaDeMagl.Checked == true)
            {
                txtMaglZaDe.Text = txtMaglZaDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaDeMagl.Text + "  Cena: " + txtMaglZaDe.Text + "#";
                if (txtMaglZaDe.Text == "") txtMaglZaDe.BackColor = Color.Red; else txtMaglZaDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMaglZaDe.Text);
            }
            if (chkSvetloTablice.Checked == true)
            {
                txtSvetloTablice.Text = txtSvetloTablice.Text.Replace(str1, str2);
                Radovi = Radovi + chkSvetloTablice.Text + "  Cena: " + txtSvetloTablice.Text + "#";
                if (txtSvetloTablice.Text == "") txtSvetloTablice.BackColor = Color.Red; else txtSvetloTablice.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSvetloTablice.Text);
            }
            if (chkLeviMigavac.Checked == true)
            {
                txtMigavacLe.Text = txtMigavacLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkLeviMigavac.Text + "  Cena: " + txtMigavacLe.Text + "#";
                if (txtMigavacLe.Text == "") txtMigavacLe.BackColor = Color.Red; else txtMigavacLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMigavacLe.Text);
            }
            if (chkDesniMigavac.Checked == true)
            {
                txtMigavacDe.Text = txtMigavacDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkDesniMigavac.Text + "  Cena: " + txtMigavacDe.Text + "#";
                if (txtMigavacDe.Text == "") txtMigavacDe.BackColor = Color.Red; else txtMigavacDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMigavacDe.Text);
            }
            if (chkPrLeRatkapa.Checked == true)
            {
                txtRatkapaPrLe.Text = txtRatkapaPrLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrLeRatkapa.Text + "  Cena: " + txtRatkapaPrLe.Text + "#";
                if (txtRatkapaPrLe.Text == "") txtRatkapaPrLe.BackColor = Color.Red; else txtRatkapaPrLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRatkapaPrLe.Text);
            }
            if (chkPrDeRatkapa.Checked == true)
            {
                txtRatkapaPrDe.Text = txtRatkapaPrDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkPrDeRatkapa.Text + "  Cena: " + txtRatkapaPrDe.Text + "#";
                if (txtRatkapaPrDe.Text == "") txtRatkapaPrDe.BackColor = Color.Red; else txtRatkapaPrDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRatkapaPrDe.Text);
            }
            if (chkZaLeRatkapa.Checked == true)
            {
                txtRatkapaZaLe.Text = txtRatkapaZaLe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaLeRatkapa.Text + "  Cena: " + txtRatkapaZaLe.Text + "#";
                if (txtRatkapaZaLe.Text == "") txtRatkapaZaLe.BackColor = Color.Red; else txtRatkapaZaLe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRatkapaZaLe.Text);
            }
            if (chkZaDeRatkapa.Checked == true)
            {
                txtRatkapaZaDe.Text = txtRatkapaZaDe.Text.Replace(str1, str2);
                Radovi = Radovi + chkZaDeRatkapa.Text + "  Cena: " + txtRatkapaZaDe.Text + "#";
                if (txtRatkapaZaDe.Text == "") txtRatkapaZaDe.BackColor = Color.Red; else txtRatkapaZaDe.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtRatkapaZaDe.Text);
            }

            //TNG  CNG
            if (chkFilterTecneFaze.Checked == true)
            {
                txtFilterTecneFaze.Text = txtFilterTecneFaze.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterTecneFaze.Text + "  Cena: " + txtFilterTecneFaze.Text + "#";
                if (txtFilterTecneFaze.Text == "") txtFilterTecneFaze.BackColor = Color.Red; else txtFilterTecneFaze.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterTecneFaze.Text);
            }
            if (chkFilterGasneFaze.Checked == true)
            {
                txtFilterGasneFaze.Text = txtFilterGasneFaze.Text.Replace(str1, str2);
                Radovi = Radovi + chkFilterGasneFaze.Text + "  Cena: " + txtFilterGasneFaze.Text + "#";
                if (txtFilterGasneFaze.Text == "") txtFilterGasneFaze.BackColor = Color.Red; else txtFilterGasneFaze.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtFilterGasneFaze.Text);
            }
            if (chkSetCrevaVoda.Checked == true)
            {
                txtSetCrevaVoda.Text = txtSetCrevaVoda.Text.Replace(str1, str2);
                Radovi = Radovi + chkSetCrevaVoda.Text + "  Cena: " + txtSetCrevaVoda.Text + "#";
                if (txtSetCrevaVoda.Text == "") txtSetCrevaVoda.BackColor = Color.Red; else txtSetCrevaVoda.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSetCrevaVoda.Text);
            }
            if (chkGumCrevaGasFaze.Checked == true)
            {
                txtGumCrevaGasFaze.Text = txtGumCrevaGasFaze.Text.Replace(str1, str2);
                Radovi = Radovi + chkGumCrevaGasFaze.Text + "  Cena: " + txtGumCrevaGasFaze.Text + "#";
                if (txtGumCrevaGasFaze.Text == "") txtGumCrevaGasFaze.BackColor = Color.Red; else txtGumCrevaGasFaze.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtGumCrevaGasFaze.Text);
            }
            if (chkSetDelRemontIspa.Checked == true)
            {
                txtSetDelovaRemont.Text = txtSetDelovaRemont.Text.Replace(str1, str2);
                Radovi = Radovi + chkSetDelRemontIspa.Text + "  Cena: " + txtSetDelovaRemont.Text + "#";
                if (txtSetDelovaRemont.Text == "") txtSetDelovaRemont.BackColor = Color.Red; else txtSetDelovaRemont.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSetDelovaRemont.Text);
            }
            if (chkPriljucakUtakGasa.Checked == true)
            {
                txtPrikljucakUtakGasa.Text = txtPrikljucakUtakGasa.Text.Replace(str1, str2);
                Radovi = Radovi + chkPriljucakUtakGasa.Text + "  Cena: " + txtPrikljucakUtakGasa.Text + "#";
                if (txtPrikljucakUtakGasa.Text == "") txtPrikljucakUtakGasa.BackColor = Color.Red; else txtPrikljucakUtakGasa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPrikljucakUtakGasa.Text);
            }
            if (chkInjector.Checked == true)
            {
                txtInjector.Text = txtInjector.Text.Replace(str1, str2);
                Radovi = Radovi + chkInjector.Text + "  Cena: " + txtInjector.Text + "#";
                if (txtInjector.Text == "") txtInjector.BackColor = Color.Red; else txtInjector.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtInjector.Text);
            }
            if (chkMultiventil.Checked == true)
            {
                txtMultiventil.Text = txtMultiventil.Text.Replace(str1, str2);
                Radovi = Radovi + chkMultiventil.Text + "  Cena: " + txtMultiventil.Text + "#";
                if (txtMultiventil.Text == "") txtMultiventil.BackColor = Color.Red; else txtMultiventil.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtMultiventil.Text);
            }
            if (chkSenzorPritGasa.Checked == true)
            {
                txtSenzorPritGasa.Text = txtSenzorPritGasa.Text.Replace(str1, str2);
                Radovi = Radovi + chkSenzorPritGasa.Text + "  Cena: " + txtSenzorPritGasa.Text + "#";
                if (txtSenzorPritGasa.Text == "") txtSenzorPritGasa.BackColor = Color.Red; else txtSenzorPritGasa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSenzorPritGasa.Text);
            }
            if (chkSenzorPokGasa.Checked == true)
            {
                txtSenzorPokazGasa.Text = txtSenzorPokazGasa.Text.Replace(str1, str2);
                Radovi = Radovi + chkSenzorPokGasa.Text + "  Cena: " + txtSenzorPokazGasa.Text + "#";
                if (txtSenzorPokazGasa.Text == "") txtSenzorPokazGasa.BackColor = Color.Red; else txtSenzorPokazGasa.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtSenzorPokazGasa.Text);
            } 

            //*********************************
        
            
            if (chkPotrosniMaterijal.Checked == true)
            {
                txtPotrosniMaterijal.Text = txtPotrosniMaterijal.Text.Replace(str1, str2);
                Radovi = Radovi + chkPotrosniMaterijal.Text + "  Cena: " + txtPotrosniMaterijal.Text + "#";
                if (txtPotrosniMaterijal.Text == "") txtPotrosniMaterijal.BackColor = Color.Red; else txtPotrosniMaterijal.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtPotrosniMaterijal.Text);
            }
            if (chkZamenaGuma.Checked == true)
            {
                txtZamenaGuma.Text = txtZamenaGuma.Text.Replace(str1, str2);
                Radovi = Radovi + chkZamenaGuma.Text + "  Cena: " + txtZamenaGuma.Text + "#";
                if (txtZamenaGuma.Text == "") txtZamenaGuma.BackColor = Color.Red; else txtZamenaGuma.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZamenaGuma.Text);
            }
            if (chkOstalo.Checked == true)
            {
                txtZamenaGuma.Text = txtZamenaGuma.Text.Replace(str1, str2);
                Radovi = Radovi + "Ostalo:\n" + txtOstaloTekst.Text + "  Cena: " + txtZamenaGuma.Text + "#";
                if (txtZamenaGuma.Text == "") txtZamenaGuma.BackColor = Color.Red; else txtZamenaGuma.BackColor = Color.White;
                CenaServ = CenaServ + float.Parse(txtZamenaGuma.Text);
            }			
        }

        private void cuvanjeServisa()
        {
            if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Servisi serv = new Servisi();
                serv.KoJeVrsio = txtKoJeVrsio.Text;
                serv.TipServisa = "Vanredni servis";
                serv.DatumServisa = tpDatumServisa.Value.Date;
                serv.KilometrazaServ = Int32.Parse(txtKilometrazaServ.Text);
                serv.CenaServisa = CenaServ.ToString();
                serv.Radovi = Radovi;
                Vozila voz = new Vozila();
                voz.SlBr = _IDVoz;
                serv.Vozilo = voz;
                serv.dodajServis();
                this.Close();
            }
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (txtKilometrazaServ.Text != "" && txtKoJeVrsio.Text != "")
            {
                try
                {
                    ispitajCekirano();
                    if (chkUlje.Checked == true)
                    {
                        if (txtUljeKolicina.Text != "")
                        {
                            cuvanjeServisa();
                        }
                        else MessageBox.Show("Niste uneli količinu ulja.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else cuvanjeServisa();                    
                }
                catch { MessageBox.Show("Morate uneti cene za čekirane stavke!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
            }
            else
            {
                MessageBox.Show("Morate popuniti obavezna polja", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (txtKilometrazaServ.Text == "") txtKilometrazaServ.BackColor = Color.Red; else txtKilometrazaServ.BackColor = Color.White;
                if (txtKoJeVrsio.Text == "") txtKoJeVrsio.BackColor = Color.Red; else txtKoJeVrsio.BackColor = Color.White;
            }
        }

        private void btnOdbaci_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Da li ste sigurni da želiti da odbacite servis?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning ) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void txtTecnostKlime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void txtKilometrazaServ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
