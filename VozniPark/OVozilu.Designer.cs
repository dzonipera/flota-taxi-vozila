﻿namespace VozniPark
{
    partial class OVozilu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OVozilu));
            this.tpRegistrovanDo = new System.Windows.Forms.DateTimePicker();
            this.tpDatKupovine = new System.Windows.Forms.DateTimePicker();
            this.cbxStanje = new System.Windows.Forms.ComboBox();
            this.cbxMProizvodnje = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGodProizvodnje = new System.Windows.Forms.TextBox();
            this.txtMasa = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtBoja = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBrSedista = new System.Windows.Forms.TextBox();
            this.txtBrVrata = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtZapremina = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStandard = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBrBrzina = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRezervoar = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtGorivo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMenjac = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtKilometrazaKupljen = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRegistracija = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtVlasnik = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBrMotora = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBrSasije = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtKaroserija = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            this.gbxOstali = new System.Windows.Forms.GroupBox();
            this.txtKW = new System.Windows.Forms.TextBox();
            this.txtKonjske = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tpParkMarkice = new System.Windows.Forms.DateTimePicker();
            this.tpPregledTaksimetra = new System.Windows.Forms.DateTimePicker();
            this.tpPPAparat = new System.Windows.Forms.DateTimePicker();
            this.tpKasko = new System.Windows.Forms.DateTimePicker();
            this.tpTaksiLegitimacija = new System.Windows.Forms.DateTimePicker();
            this.tpSaobrInspektor = new System.Windows.Forms.DateTimePicker();
            this.tpTehnickiPregled = new System.Windows.Forms.DateTimePicker();
            this.tpOsiguranjeAOiOP = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtKilometraza = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtKomunalniBr = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.btnRedovni = new System.Windows.Forms.Button();
            this.btnVanredni = new System.Windows.Forms.Button();
            this.btnPeriodicniPregled = new System.Windows.Forms.Button();
            this.btnListaServisa = new System.Windows.Forms.Button();
            this.btnPDF = new System.Windows.Forms.Button();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnListaPreg = new System.Windows.Forms.Button();
            this.lblPristup = new System.Windows.Forms.Label();
            this.gbxOsnovni.SuspendLayout();
            this.gbxOstali.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpRegistrovanDo
            // 
            this.tpRegistrovanDo.CustomFormat = "dd. MM. yyyy.";
            this.tpRegistrovanDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpRegistrovanDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpRegistrovanDo.Location = new System.Drawing.Point(436, 171);
            this.tpRegistrovanDo.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpRegistrovanDo.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpRegistrovanDo.Name = "tpRegistrovanDo";
            this.tpRegistrovanDo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpRegistrovanDo.Size = new System.Drawing.Size(136, 22);
            this.tpRegistrovanDo.TabIndex = 23;
            this.tpRegistrovanDo.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpDatKupovine
            // 
            this.tpDatKupovine.CustomFormat = "dd. MM. yyyy.";
            this.tpDatKupovine.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpDatKupovine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpDatKupovine.Location = new System.Drawing.Point(436, 31);
            this.tpDatKupovine.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpDatKupovine.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpDatKupovine.Name = "tpDatKupovine";
            this.tpDatKupovine.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpDatKupovine.Size = new System.Drawing.Size(136, 22);
            this.tpDatKupovine.TabIndex = 18;
            this.tpDatKupovine.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // cbxStanje
            // 
            this.cbxStanje.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxStanje.FormattingEnabled = true;
            this.cbxStanje.Items.AddRange(new object[] {
            "Nov",
            "Polovan"});
            this.cbxStanje.Location = new System.Drawing.Point(436, 57);
            this.cbxStanje.Name = "cbxStanje";
            this.cbxStanje.Size = new System.Drawing.Size(136, 24);
            this.cbxStanje.TabIndex = 19;
            this.cbxStanje.SelectedIndexChanged += new System.EventHandler(this.cbxStanje_SelectedIndexChanged);
            // 
            // cbxMProizvodnje
            // 
            this.cbxMProizvodnje.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMProizvodnje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxMProizvodnje.FormattingEnabled = true;
            this.cbxMProizvodnje.Items.AddRange(new object[] {
            "Januar",
            "Februar",
            "Mart",
            "April",
            "Maj",
            "Jun",
            "Jul",
            "Avgust",
            "Septembar",
            "Oktobar",
            "Novembar",
            "Decembar"});
            this.cbxMProizvodnje.Location = new System.Drawing.Point(168, 171);
            this.cbxMProizvodnje.Name = "cbxMProizvodnje";
            this.cbxMProizvodnje.Size = new System.Drawing.Size(105, 24);
            this.cbxMProizvodnje.TabIndex = 8;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(319, 174);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(104, 17);
            this.label25.TabIndex = 29;
            this.label25.Text = "Registrovan do";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(319, 34);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 17);
            this.label20.TabIndex = 33;
            this.label20.Text = "Datum kupovine";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // txtGodProizvodnje
            // 
            this.txtGodProizvodnje.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGodProizvodnje.Location = new System.Drawing.Point(168, 143);
            this.txtGodProizvodnje.Name = "txtGodProizvodnje";
            this.txtGodProizvodnje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGodProizvodnje.Size = new System.Drawing.Size(105, 22);
            this.txtGodProizvodnje.TabIndex = 7;
            this.txtGodProizvodnje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGodProizvodnje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // txtMasa
            // 
            this.txtMasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMasa.Location = new System.Drawing.Point(68, 115);
            this.txtMasa.Name = "txtMasa";
            this.txtMasa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMasa.Size = new System.Drawing.Size(54, 22);
            this.txtMasa.TabIndex = 5;
            this.txtMasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(319, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 17);
            this.label21.TabIndex = 34;
            this.label21.Text = "Stanje";
            // 
            // txtBoja
            // 
            this.txtBoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoja.Location = new System.Drawing.Point(176, 115);
            this.txtBoja.Name = "txtBoja";
            this.txtBoja.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBoja.Size = new System.Drawing.Size(97, 22);
            this.txtBoja.TabIndex = 6;
            this.txtBoja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 17);
            this.label9.TabIndex = 35;
            this.label9.Text = "Mesec proizvodnje";
            // 
            // txtBrSedista
            // 
            this.txtBrSedista.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrSedista.Location = new System.Drawing.Point(241, 87);
            this.txtBrSedista.Name = "txtBrSedista";
            this.txtBrSedista.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrSedista.Size = new System.Drawing.Size(32, 22);
            this.txtBrSedista.TabIndex = 4;
            this.txtBrSedista.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrSedista.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // txtBrVrata
            // 
            this.txtBrVrata.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrVrata.Location = new System.Drawing.Point(95, 87);
            this.txtBrVrata.Name = "txtBrVrata";
            this.txtBrVrata.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrVrata.Size = new System.Drawing.Size(32, 22);
            this.txtBrVrata.TabIndex = 3;
            this.txtBrVrata.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrVrata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // txtZapremina
            // 
            this.txtZapremina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtZapremina.Location = new System.Drawing.Point(131, 229);
            this.txtZapremina.Name = "txtZapremina";
            this.txtZapremina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtZapremina.Size = new System.Drawing.Size(142, 22);
            this.txtZapremina.TabIndex = 10;
            this.txtZapremina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZapremina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 232);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 17);
            this.label14.TabIndex = 21;
            this.label14.Text = "Zapremina ccm";
            // 
            // txtStandard
            // 
            this.txtStandard.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStandard.Location = new System.Drawing.Point(131, 257);
            this.txtStandard.Name = "txtStandard";
            this.txtStandard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStandard.Size = new System.Drawing.Size(142, 22);
            this.txtStandard.TabIndex = 11;
            this.txtStandard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 260);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 17);
            this.label13.TabIndex = 19;
            this.label13.Text = "Standard";
            // 
            // txtBrBrzina
            // 
            this.txtBrBrzina.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrBrzina.Location = new System.Drawing.Point(220, 396);
            this.txtBrBrzina.Name = "txtBrBrzina";
            this.txtBrBrzina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrBrzina.Size = new System.Drawing.Size(53, 22);
            this.txtBrBrzina.TabIndex = 17;
            this.txtBrBrzina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrBrzina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 399);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(139, 17);
            this.label19.TabIndex = 17;
            this.label19.Text = "Broj stepeni prenosa";
            // 
            // txtRezervoar
            // 
            this.txtRezervoar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRezervoar.Location = new System.Drawing.Point(220, 340);
            this.txtRezervoar.Name = "txtRezervoar";
            this.txtRezervoar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRezervoar.Size = new System.Drawing.Size(53, 22);
            this.txtRezervoar.TabIndex = 15;
            this.txtRezervoar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRezervoar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(20, 343);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(194, 17);
            this.label17.TabIndex = 16;
            this.label17.Text = "Zapremina rezervoara (litara)";
            // 
            // txtGorivo
            // 
            this.txtGorivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGorivo.Location = new System.Drawing.Point(92, 312);
            this.txtGorivo.Name = "txtGorivo";
            this.txtGorivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGorivo.Size = new System.Drawing.Size(181, 22);
            this.txtGorivo.TabIndex = 14;
            this.txtGorivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(20, 315);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 17);
            this.label16.TabIndex = 14;
            this.label16.Text = "Gorivo";
            // 
            // txtMenjac
            // 
            this.txtMenjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMenjac.Location = new System.Drawing.Point(195, 368);
            this.txtMenjac.Name = "txtMenjac";
            this.txtMenjac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMenjac.Size = new System.Drawing.Size(78, 22);
            this.txtMenjac.TabIndex = 16;
            this.txtMenjac.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 371);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(174, 17);
            this.label18.TabIndex = 13;
            this.label18.Text = "Tip menjača (npr. manual)";
            // 
            // txtKilometrazaKupljen
            // 
            this.txtKilometrazaKupljen.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKilometrazaKupljen.Location = new System.Drawing.Point(472, 115);
            this.txtKilometrazaKupljen.Name = "txtKilometrazaKupljen";
            this.txtKilometrazaKupljen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKilometrazaKupljen.Size = new System.Drawing.Size(100, 22);
            this.txtKilometrazaKupljen.TabIndex = 21;
            this.txtKilometrazaKupljen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKilometrazaKupljen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(319, 118);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(147, 17);
            this.label23.TabIndex = 26;
            this.label23.Text = "Kilometraža kupljenog";
            // 
            // txtRegistracija
            // 
            this.txtRegistracija.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegistracija.ForeColor = System.Drawing.Color.Black;
            this.txtRegistracija.Location = new System.Drawing.Point(472, 143);
            this.txtRegistracija.Name = "txtRegistracija";
            this.txtRegistracija.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRegistracija.Size = new System.Drawing.Size(100, 22);
            this.txtRegistracija.TabIndex = 22;
            this.txtRegistracija.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(319, 146);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 17);
            this.label24.TabIndex = 25;
            this.label24.Text = "Registarski broj";
            // 
            // txtVlasnik
            // 
            this.txtVlasnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlasnik.Location = new System.Drawing.Point(436, 87);
            this.txtVlasnik.Name = "txtVlasnik";
            this.txtVlasnik.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVlasnik.Size = new System.Drawing.Size(136, 22);
            this.txtVlasnik.TabIndex = 20;
            this.txtVlasnik.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(319, 90);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 17);
            this.label22.TabIndex = 15;
            this.label22.Text = "Prethodni vlasnik";
            // 
            // txtBrMotora
            // 
            this.txtBrMotora.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrMotora.Location = new System.Drawing.Point(131, 201);
            this.txtBrMotora.Name = "txtBrMotora";
            this.txtBrMotora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrMotora.Size = new System.Drawing.Size(142, 22);
            this.txtBrMotora.TabIndex = 9;
            this.txtBrMotora.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 204);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 17);
            this.label12.TabIndex = 20;
            this.label12.Text = "Broj motora";
            // 
            // txtBrSasije
            // 
            this.txtBrSasije.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrSasije.Location = new System.Drawing.Point(104, 31);
            this.txtBrSasije.Name = "txtBrSasije";
            this.txtBrSasije.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrSasije.Size = new System.Drawing.Size(169, 22);
            this.txtBrSasije.TabIndex = 1;
            this.txtBrSasije.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "Broj šasije";
            // 
            // txtKaroserija
            // 
            this.txtKaroserija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKaroserija.Location = new System.Drawing.Point(120, 59);
            this.txtKaroserija.Name = "txtKaroserija";
            this.txtKaroserija.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKaroserija.Size = new System.Drawing.Size(153, 22);
            this.txtKaroserija.TabIndex = 2;
            this.txtKaroserija.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 30;
            this.label4.Text = "Tip karoserije";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 36;
            this.label5.Text = "Broj vrata";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(155, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 32;
            this.label6.Text = "Broj sedišta";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 17);
            this.label8.TabIndex = 28;
            this.label8.Text = "Godina proizvodnje";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "Masa";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(134, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Boja";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 28);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 56);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 84);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 113);
            this.gbxOsnovni.TabIndex = 60;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // gbxOstali
            // 
            this.gbxOstali.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxOstali.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.gbxOstali.Controls.Add(this.txtKW);
            this.gbxOstali.Controls.Add(this.txtKonjske);
            this.gbxOstali.Controls.Add(this.label15);
            this.gbxOstali.Controls.Add(this.label37);
            this.gbxOstali.Controls.Add(this.label18);
            this.gbxOstali.Controls.Add(this.label7);
            this.gbxOstali.Controls.Add(this.label35);
            this.gbxOstali.Controls.Add(this.label21);
            this.gbxOstali.Controls.Add(this.tpRegistrovanDo);
            this.gbxOstali.Controls.Add(this.label10);
            this.gbxOstali.Controls.Add(this.tpParkMarkice);
            this.gbxOstali.Controls.Add(this.tpPregledTaksimetra);
            this.gbxOstali.Controls.Add(this.tpPPAparat);
            this.gbxOstali.Controls.Add(this.tpKasko);
            this.gbxOstali.Controls.Add(this.tpTaksiLegitimacija);
            this.gbxOstali.Controls.Add(this.tpSaobrInspektor);
            this.gbxOstali.Controls.Add(this.tpTehnickiPregled);
            this.gbxOstali.Controls.Add(this.tpOsiguranjeAOiOP);
            this.gbxOstali.Controls.Add(this.tpDatKupovine);
            this.gbxOstali.Controls.Add(this.label8);
            this.gbxOstali.Controls.Add(this.label34);
            this.gbxOstali.Controls.Add(this.cbxStanje);
            this.gbxOstali.Controls.Add(this.label32);
            this.gbxOstali.Controls.Add(this.label6);
            this.gbxOstali.Controls.Add(this.label36);
            this.gbxOstali.Controls.Add(this.label30);
            this.gbxOstali.Controls.Add(this.label33);
            this.gbxOstali.Controls.Add(this.label25);
            this.gbxOstali.Controls.Add(this.label31);
            this.gbxOstali.Controls.Add(this.label28);
            this.gbxOstali.Controls.Add(this.label29);
            this.gbxOstali.Controls.Add(this.cbxMProizvodnje);
            this.gbxOstali.Controls.Add(this.label27);
            this.gbxOstali.Controls.Add(this.label20);
            this.gbxOstali.Controls.Add(this.label5);
            this.gbxOstali.Controls.Add(this.txtKilometraza);
            this.gbxOstali.Controls.Add(this.txtKilometrazaKupljen);
            this.gbxOstali.Controls.Add(this.label4);
            this.gbxOstali.Controls.Add(this.label38);
            this.gbxOstali.Controls.Add(this.label23);
            this.gbxOstali.Controls.Add(this.txtKaroserija);
            this.gbxOstali.Controls.Add(this.txtRegistracija);
            this.gbxOstali.Controls.Add(this.txtGodProizvodnje);
            this.gbxOstali.Controls.Add(this.label24);
            this.gbxOstali.Controls.Add(this.label11);
            this.gbxOstali.Controls.Add(this.txtKomunalniBr);
            this.gbxOstali.Controls.Add(this.txtVlasnik);
            this.gbxOstali.Controls.Add(this.txtMasa);
            this.gbxOstali.Controls.Add(this.label26);
            this.gbxOstali.Controls.Add(this.label22);
            this.gbxOstali.Controls.Add(this.txtBrSasije);
            this.gbxOstali.Controls.Add(this.label12);
            this.gbxOstali.Controls.Add(this.txtBoja);
            this.gbxOstali.Controls.Add(this.txtBrMotora);
            this.gbxOstali.Controls.Add(this.label9);
            this.gbxOstali.Controls.Add(this.txtMenjac);
            this.gbxOstali.Controls.Add(this.txtBrSedista);
            this.gbxOstali.Controls.Add(this.label16);
            this.gbxOstali.Controls.Add(this.txtBrVrata);
            this.gbxOstali.Controls.Add(this.txtGorivo);
            this.gbxOstali.Controls.Add(this.label17);
            this.gbxOstali.Controls.Add(this.txtRezervoar);
            this.gbxOstali.Controls.Add(this.txtZapremina);
            this.gbxOstali.Controls.Add(this.label19);
            this.gbxOstali.Controls.Add(this.label14);
            this.gbxOstali.Controls.Add(this.txtBrBrzina);
            this.gbxOstali.Controls.Add(this.txtStandard);
            this.gbxOstali.Controls.Add(this.label13);
            this.gbxOstali.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOstali.Location = new System.Drawing.Point(12, 151);
            this.gbxOstali.Name = "gbxOstali";
            this.gbxOstali.Size = new System.Drawing.Size(908, 457);
            this.gbxOstali.TabIndex = 61;
            this.gbxOstali.TabStop = false;
            this.gbxOstali.Text = "Ostali podaci";
            // 
            // txtKW
            // 
            this.txtKW.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKW.Location = new System.Drawing.Point(54, 285);
            this.txtKW.Name = "txtKW";
            this.txtKW.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKW.Size = new System.Drawing.Size(53, 22);
            this.txtKW.TabIndex = 12;
            this.txtKW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // txtKonjske
            // 
            this.txtKonjske.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKonjske.Location = new System.Drawing.Point(220, 285);
            this.txtKonjske.Name = "txtKonjske";
            this.txtKonjske.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKonjske.Size = new System.Drawing.Size(53, 22);
            this.txtKonjske.TabIndex = 13;
            this.txtKonjske.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKonjske.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(113, 288);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(101, 17);
            this.label15.TabIndex = 65;
            this.label15.Text = "Konjske snage";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(20, 288);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 17);
            this.label37.TabIndex = 66;
            this.label37.Text = "kW";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(356, 328);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(74, 17);
            this.label35.TabIndex = 34;
            this.label35.Text = "inspektora";
            // 
            // tpParkMarkice
            // 
            this.tpParkMarkice.CustomFormat = "dd. MM. yyyy.";
            this.tpParkMarkice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpParkMarkice.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpParkMarkice.Location = new System.Drawing.Point(746, 230);
            this.tpParkMarkice.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpParkMarkice.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpParkMarkice.Name = "tpParkMarkice";
            this.tpParkMarkice.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpParkMarkice.Size = new System.Drawing.Size(136, 22);
            this.tpParkMarkice.TabIndex = 33;
            this.tpParkMarkice.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpPregledTaksimetra
            // 
            this.tpPregledTaksimetra.CustomFormat = "dd. MM. yyyy.";
            this.tpPregledTaksimetra.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpPregledTaksimetra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpPregledTaksimetra.Location = new System.Drawing.Point(746, 90);
            this.tpPregledTaksimetra.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpPregledTaksimetra.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpPregledTaksimetra.Name = "tpPregledTaksimetra";
            this.tpPregledTaksimetra.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpPregledTaksimetra.Size = new System.Drawing.Size(136, 22);
            this.tpPregledTaksimetra.TabIndex = 28;
            this.tpPregledTaksimetra.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpPPAparat
            // 
            this.tpPPAparat.CustomFormat = "dd. MM. yyyy.";
            this.tpPPAparat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpPPAparat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpPPAparat.Location = new System.Drawing.Point(746, 202);
            this.tpPPAparat.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpPPAparat.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpPPAparat.Name = "tpPPAparat";
            this.tpPPAparat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpPPAparat.Size = new System.Drawing.Size(136, 22);
            this.tpPPAparat.TabIndex = 32;
            this.tpPPAparat.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpKasko
            // 
            this.tpKasko.CustomFormat = "dd. MM. yyyy.";
            this.tpKasko.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpKasko.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpKasko.Location = new System.Drawing.Point(746, 174);
            this.tpKasko.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpKasko.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpKasko.Name = "tpKasko";
            this.tpKasko.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpKasko.Size = new System.Drawing.Size(136, 22);
            this.tpKasko.TabIndex = 31;
            this.tpKasko.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpTaksiLegitimacija
            // 
            this.tpTaksiLegitimacija.CustomFormat = "dd. MM. yyyy.";
            this.tpTaksiLegitimacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpTaksiLegitimacija.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpTaksiLegitimacija.Location = new System.Drawing.Point(746, 118);
            this.tpTaksiLegitimacija.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpTaksiLegitimacija.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpTaksiLegitimacija.Name = "tpTaksiLegitimacija";
            this.tpTaksiLegitimacija.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpTaksiLegitimacija.Size = new System.Drawing.Size(136, 22);
            this.tpTaksiLegitimacija.TabIndex = 29;
            this.tpTaksiLegitimacija.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpSaobrInspektor
            // 
            this.tpSaobrInspektor.CustomFormat = "dd. MM. yyyy.";
            this.tpSaobrInspektor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpSaobrInspektor.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpSaobrInspektor.Location = new System.Drawing.Point(436, 325);
            this.tpSaobrInspektor.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpSaobrInspektor.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpSaobrInspektor.Name = "tpSaobrInspektor";
            this.tpSaobrInspektor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpSaobrInspektor.Size = new System.Drawing.Size(136, 22);
            this.tpSaobrInspektor.TabIndex = 26;
            this.tpSaobrInspektor.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpTehnickiPregled
            // 
            this.tpTehnickiPregled.CustomFormat = "dd. MM. yyyy.";
            this.tpTehnickiPregled.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpTehnickiPregled.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpTehnickiPregled.Location = new System.Drawing.Point(436, 353);
            this.tpTehnickiPregled.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpTehnickiPregled.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpTehnickiPregled.Name = "tpTehnickiPregled";
            this.tpTehnickiPregled.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpTehnickiPregled.Size = new System.Drawing.Size(136, 22);
            this.tpTehnickiPregled.TabIndex = 27;
            this.tpTehnickiPregled.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // tpOsiguranjeAOiOP
            // 
            this.tpOsiguranjeAOiOP.CustomFormat = "dd. MM. yyyy.";
            this.tpOsiguranjeAOiOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpOsiguranjeAOiOP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpOsiguranjeAOiOP.Location = new System.Drawing.Point(746, 146);
            this.tpOsiguranjeAOiOP.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpOsiguranjeAOiOP.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpOsiguranjeAOiOP.Name = "tpOsiguranjeAOiOP";
            this.tpOsiguranjeAOiOP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpOsiguranjeAOiOP.Size = new System.Drawing.Size(136, 22);
            this.tpOsiguranjeAOiOP.TabIndex = 30;
            this.tpOsiguranjeAOiOP.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(613, 232);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(109, 17);
            this.label34.TabIndex = 33;
            this.label34.Text = "Parking markice";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(613, 92);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(126, 17);
            this.label32.TabIndex = 33;
            this.label32.Text = "Pregled taksimetra";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(760, 47);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(107, 17);
            this.label36.TabIndex = 33;
            this.label36.Text = "Datum isteka:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(613, 204);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 17);
            this.label30.TabIndex = 33;
            this.label30.Text = "PP aparat";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(613, 120);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(115, 17);
            this.label33.TabIndex = 33;
            this.label33.Text = "Taksi legitimacija";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(319, 300);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(196, 17);
            this.label31.TabIndex = 33;
            this.label31.Text = "Sledeći pregled saobraćajnog";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(613, 176);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(117, 17);
            this.label28.TabIndex = 33;
            this.label28.Text = "Kasko osiguranje";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(319, 356);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(113, 17);
            this.label29.TabIndex = 33;
            this.label29.Text = "Tehnički pregled";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(613, 148);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(132, 17);
            this.label27.TabIndex = 33;
            this.label27.Text = "Osiguranje AO i OP";
            // 
            // txtKilometraza
            // 
            this.txtKilometraza.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKilometraza.Location = new System.Drawing.Point(463, 227);
            this.txtKilometraza.Name = "txtKilometraza";
            this.txtKilometraza.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKilometraza.Size = new System.Drawing.Size(109, 22);
            this.txtKilometraza.TabIndex = 25;
            this.txtKilometraza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKilometraza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBrVrata_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(319, 230);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(138, 17);
            this.label38.TabIndex = 26;
            this.label38.Text = "Pređena kilometraža";
            // 
            // txtKomunalniBr
            // 
            this.txtKomunalniBr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKomunalniBr.Location = new System.Drawing.Point(436, 199);
            this.txtKomunalniBr.Name = "txtKomunalniBr";
            this.txtKomunalniBr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKomunalniBr.Size = new System.Drawing.Size(136, 22);
            this.txtKomunalniBr.TabIndex = 24;
            this.txtKomunalniBr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(319, 202);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(102, 17);
            this.label26.TabIndex = 15;
            this.label26.Text = "Komunalni broj";
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZatvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnZatvori.Location = new System.Drawing.Point(775, 465);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(94, 54);
            this.btnZatvori.TabIndex = 41;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // btnRedovni
            // 
            this.btnRedovni.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRedovni.BackColor = System.Drawing.Color.YellowGreen;
            this.btnRedovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRedovni.Location = new System.Drawing.Point(448, 12);
            this.btnRedovni.Name = "btnRedovni";
            this.btnRedovni.Size = new System.Drawing.Size(105, 57);
            this.btnRedovni.TabIndex = 35;
            this.btnRedovni.Text = "Redovni servis";
            this.btnRedovni.UseVisualStyleBackColor = false;
            this.btnRedovni.Click += new System.EventHandler(this.btnRedovni_Click);
            // 
            // btnVanredni
            // 
            this.btnVanredni.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnVanredni.BackColor = System.Drawing.Color.YellowGreen;
            this.btnVanredni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVanredni.Location = new System.Drawing.Point(559, 12);
            this.btnVanredni.Name = "btnVanredni";
            this.btnVanredni.Size = new System.Drawing.Size(105, 57);
            this.btnVanredni.TabIndex = 36;
            this.btnVanredni.Text = "Vanredni servis";
            this.btnVanredni.UseVisualStyleBackColor = false;
            this.btnVanredni.Click += new System.EventHandler(this.btnVanredni_Click);
            // 
            // btnPeriodicniPregled
            // 
            this.btnPeriodicniPregled.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPeriodicniPregled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnPeriodicniPregled.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPeriodicniPregled.Location = new System.Drawing.Point(677, 12);
            this.btnPeriodicniPregled.Name = "btnPeriodicniPregled";
            this.btnPeriodicniPregled.Size = new System.Drawing.Size(105, 57);
            this.btnPeriodicniPregled.TabIndex = 38;
            this.btnPeriodicniPregled.Text = "Interni\r\npregled";
            this.btnPeriodicniPregled.UseVisualStyleBackColor = false;
            this.btnPeriodicniPregled.Click += new System.EventHandler(this.btnPeriodicniPregled_Click);
            // 
            // btnListaServisa
            // 
            this.btnListaServisa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnListaServisa.BackColor = System.Drawing.Color.YellowGreen;
            this.btnListaServisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListaServisa.Location = new System.Drawing.Point(448, 81);
            this.btnListaServisa.Name = "btnListaServisa";
            this.btnListaServisa.Size = new System.Drawing.Size(140, 57);
            this.btnListaServisa.TabIndex = 37;
            this.btnListaServisa.Text = "Lista servisa";
            this.btnListaServisa.UseVisualStyleBackColor = false;
            this.btnListaServisa.Click += new System.EventHandler(this.btnListaServisa_Click);
            // 
            // btnPDF
            // 
            this.btnPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPDF.BackgroundImage = global::VozniPark.Properties.Resources.Adobe_PDF_Document_01__2_;
            this.btnPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPDF.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPDF.Location = new System.Drawing.Point(822, 41);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(72, 72);
            this.btnPDF.TabIndex = 40;
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // btnSnimi
            // 
            this.btnSnimi.BackgroundImage = global::VozniPark.Properties.Resources.Save_01__1_1;
            this.btnSnimi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSnimi.Enabled = false;
            this.btnSnimi.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSnimi.Location = new System.Drawing.Point(346, 39);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(72, 72);
            this.btnSnimi.TabIndex = 34;
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.BackgroundImage = global::VozniPark.Properties.Resources.Text_Edit__1_;
            this.btnIzmeni.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIzmeni.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIzmeni.Location = new System.Drawing.Point(268, 39);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(72, 72);
            this.btnIzmeni.TabIndex = 0;
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnListaPreg
            // 
            this.btnListaPreg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnListaPreg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnListaPreg.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListaPreg.Location = new System.Drawing.Point(642, 81);
            this.btnListaPreg.Name = "btnListaPreg";
            this.btnListaPreg.Size = new System.Drawing.Size(140, 57);
            this.btnListaPreg.TabIndex = 39;
            this.btnListaPreg.Text = "Lista internih pregleda";
            this.btnListaPreg.UseVisualStyleBackColor = false;
            this.btnListaPreg.Click += new System.EventHandler(this.btnListaPreg_Click);
            // 
            // lblPristup
            // 
            this.lblPristup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPristup.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPristup.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPristup.Location = new System.Drawing.Point(12, 151);
            this.lblPristup.Name = "lblPristup";
            this.lblPristup.Size = new System.Drawing.Size(908, 471);
            this.lblPristup.TabIndex = 33;
            this.lblPristup.Text = "NEMATE PRISTUP OVIM PODACIMA!";
            this.lblPristup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPristup.Visible = false;
            // 
            // OVozilu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(932, 635);
            this.ControlBox = false;
            this.Controls.Add(this.btnListaServisa);
            this.Controls.Add(this.btnListaPreg);
            this.Controls.Add(this.btnPeriodicniPregled);
            this.Controls.Add(this.btnVanredni);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnRedovni);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.btnIzmeni);
            this.Controls.Add(this.gbxOstali);
            this.Controls.Add(this.gbxOsnovni);
            this.Controls.Add(this.lblPristup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(950, 682);
            this.Name = "OVozilu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "O Vozilu";
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            this.gbxOstali.ResumeLayout(false);
            this.gbxOstali.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker tpRegistrovanDo;
        private System.Windows.Forms.DateTimePicker tpDatKupovine;
        private System.Windows.Forms.ComboBox cbxStanje;
        private System.Windows.Forms.ComboBox cbxMProizvodnje;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGodProizvodnje;
        private System.Windows.Forms.TextBox txtMasa;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtBoja;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBrSedista;
        private System.Windows.Forms.TextBox txtBrVrata;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtZapremina;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtStandard;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBrBrzina;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtRezervoar;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtGorivo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtMenjac;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtKilometrazaKupljen;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtRegistracija;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtVlasnik;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBrMotora;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBrSasije;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtKaroserija;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.GroupBox gbxOstali;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.Button btnRedovni;
        private System.Windows.Forms.Button btnVanredni;
        private System.Windows.Forms.Button btnPeriodicniPregled;
        private System.Windows.Forms.Button btnListaServisa;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.DateTimePicker tpParkMarkice;
        private System.Windows.Forms.DateTimePicker tpPregledTaksimetra;
        private System.Windows.Forms.DateTimePicker tpPPAparat;
        private System.Windows.Forms.DateTimePicker tpKasko;
        private System.Windows.Forms.DateTimePicker tpTaksiLegitimacija;
        private System.Windows.Forms.DateTimePicker tpSaobrInspektor;
        private System.Windows.Forms.DateTimePicker tpTehnickiPregled;
        private System.Windows.Forms.DateTimePicker tpOsiguranjeAOiOP;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtKomunalniBr;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtKW;
        private System.Windows.Forms.TextBox txtKonjske;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtKilometraza;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btnListaPreg;
        private System.Windows.Forms.Label lblPristup;
    }
}