﻿namespace VozniPark
{
    partial class Naslovna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Naslovna));
            this.dgListaVozila = new System.Windows.Forms.DataGridView();
            this.kolSlBrVozila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolRegistracija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolProizvodjac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lbKorisnik = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnPoruke = new System.Windows.Forms.Button();
            this.btnKorisnici = new System.Windows.Forms.Button();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.btnAlarmi = new System.Windows.Forms.Button();
            this.btnObrisiVozilo = new System.Windows.Forms.Button();
            this.btnOVozilu = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgListaVozila)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgListaVozila
            // 
            this.dgListaVozila.AllowUserToAddRows = false;
            this.dgListaVozila.AllowUserToDeleteRows = false;
            this.dgListaVozila.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListaVozila.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgListaVozila.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kolSlBrVozila,
            this.kolRegistracija,
            this.kolProizvodjac,
            this.kolModel});
            this.dgListaVozila.Location = new System.Drawing.Point(3, 3);
            this.dgListaVozila.Name = "dgListaVozila";
            this.dgListaVozila.ReadOnly = true;
            this.dgListaVozila.RowTemplate.Height = 24;
            this.dgListaVozila.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListaVozila.Size = new System.Drawing.Size(524, 354);
            this.dgListaVozila.TabIndex = 8;
            // 
            // kolSlBrVozila
            // 
            this.kolSlBrVozila.HeaderText = "Službeni broj";
            this.kolSlBrVozila.MinimumWidth = 45;
            this.kolSlBrVozila.Name = "kolSlBrVozila";
            this.kolSlBrVozila.ReadOnly = true;
            // 
            // kolRegistracija
            // 
            this.kolRegistracija.HeaderText = "Registarski broj";
            this.kolRegistracija.MinimumWidth = 100;
            this.kolRegistracija.Name = "kolRegistracija";
            this.kolRegistracija.ReadOnly = true;
            this.kolRegistracija.Width = 150;
            // 
            // kolProizvodjac
            // 
            this.kolProizvodjac.HeaderText = "Proizvođač";
            this.kolProizvodjac.MinimumWidth = 100;
            this.kolProizvodjac.Name = "kolProizvodjac";
            this.kolProizvodjac.ReadOnly = true;
            this.kolProizvodjac.Width = 150;
            // 
            // kolModel
            // 
            this.kolModel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.kolModel.HeaderText = "Model";
            this.kolModel.MinimumWidth = 100;
            this.kolModel.Name = "kolModel";
            this.kolModel.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(893, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Korisnik :";
            // 
            // lbKorisnik
            // 
            this.lbKorisnik.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbKorisnik.AutoSize = true;
            this.lbKorisnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKorisnik.Location = new System.Drawing.Point(988, 9);
            this.lbKorisnik.Name = "lbKorisnik";
            this.lbKorisnik.Size = new System.Drawing.Size(53, 20);
            this.lbKorisnik.TabIndex = 5;
            this.lbKorisnik.Text = "label2";
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Location = new System.Drawing.Point(1008, 38);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(117, 72);
            this.btnLogout.TabIndex = 8;
            this.btnLogout.Text = "Odjavi se";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(41, 153);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.dgListaVozila);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.splitContainer1.Panel2.BackgroundImage = global::VozniPark.Properties.Resources.portal_zrenjanin_poslovi_jul_i_taxi;
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.splitContainer1.Size = new System.Drawing.Size(1084, 360);
            this.splitContainer1.SplitterDistance = 530;
            this.splitContainer1.TabIndex = 7;
            // 
            // btnPoruke
            // 
            this.btnPoruke.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPoruke.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPoruke.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPoruke.Location = new System.Drawing.Point(577, 39);
            this.btnPoruke.Name = "btnPoruke";
            this.btnPoruke.Size = new System.Drawing.Size(130, 72);
            this.btnPoruke.TabIndex = 5;
            this.btnPoruke.Text = "Obaveštenja";
            this.btnPoruke.UseVisualStyleBackColor = true;
            this.btnPoruke.Click += new System.EventHandler(this.btnPoruke_Click);
            // 
            // btnKorisnici
            // 
            this.btnKorisnici.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKorisnici.BackgroundImage = global::VozniPark.Properties.Resources.User_Group_01__1_;
            this.btnKorisnici.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnKorisnici.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKorisnici.Location = new System.Drawing.Point(898, 38);
            this.btnKorisnici.Name = "btnKorisnici";
            this.btnKorisnici.Size = new System.Drawing.Size(72, 72);
            this.btnKorisnici.TabIndex = 7;
            this.btnKorisnici.UseVisualStyleBackColor = true;
            this.btnKorisnici.Click += new System.EventHandler(this.btnKorisnici_Click);
            // 
            // btnPretraga
            // 
            this.btnPretraga.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPretraga.BackgroundImage = global::VozniPark.Properties.Resources.search__2_;
            this.btnPretraga.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPretraga.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPretraga.Location = new System.Drawing.Point(752, 39);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(72, 72);
            this.btnPretraga.TabIndex = 6;
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // btnAlarmi
            // 
            this.btnAlarmi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAlarmi.BackgroundImage = global::VozniPark.Properties.Resources.Alarm;
            this.btnAlarmi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAlarmi.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlarmi.Location = new System.Drawing.Point(479, 39);
            this.btnAlarmi.Name = "btnAlarmi";
            this.btnAlarmi.Size = new System.Drawing.Size(72, 72);
            this.btnAlarmi.TabIndex = 4;
            this.btnAlarmi.UseVisualStyleBackColor = true;
            this.btnAlarmi.Click += new System.EventHandler(this.btnAlarmi_Click);
            // 
            // btnObrisiVozilo
            // 
            this.btnObrisiVozilo.BackgroundImage = global::VozniPark.Properties.Resources.Button_Delete_01__2_;
            this.btnObrisiVozilo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnObrisiVozilo.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnObrisiVozilo.Location = new System.Drawing.Point(305, 39);
            this.btnObrisiVozilo.Name = "btnObrisiVozilo";
            this.btnObrisiVozilo.Size = new System.Drawing.Size(72, 72);
            this.btnObrisiVozilo.TabIndex = 3;
            this.btnObrisiVozilo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnObrisiVozilo.UseVisualStyleBackColor = true;
            this.btnObrisiVozilo.Click += new System.EventHandler(this.btnObrisiVozilo_Click);
            // 
            // btnOVozilu
            // 
            this.btnOVozilu.BackgroundImage = global::VozniPark.Properties.Resources.Button_Info_01;
            this.btnOVozilu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnOVozilu.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOVozilu.Location = new System.Drawing.Point(173, 39);
            this.btnOVozilu.Name = "btnOVozilu";
            this.btnOVozilu.Size = new System.Drawing.Size(72, 72);
            this.btnOVozilu.TabIndex = 2;
            this.btnOVozilu.UseVisualStyleBackColor = true;
            this.btnOVozilu.Click += new System.EventHandler(this.btnOVozilu_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.BackgroundImage = global::VozniPark.Properties.Resources.Button_Add_01__1_;
            this.btnNovo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNovo.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.Location = new System.Drawing.Point(41, 39);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(72, 72);
            this.btnNovo.TabIndex = 1;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // Naslovna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1165, 560);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.lbKorisnik);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnKorisnici);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnPoruke);
            this.Controls.Add(this.btnPretraga);
            this.Controls.Add(this.btnAlarmi);
            this.Controls.Add(this.btnObrisiVozilo);
            this.Controls.Add(this.btnOVozilu);
            this.Controls.Add(this.btnNovo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 607);
            this.Name = "Naslovna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iTaxi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Naslovna_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgListaVozila)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.DataGridView dgListaVozila;
        private System.Windows.Forms.Button btnOVozilu;
        private System.Windows.Forms.Button btnObrisiVozilo;
        private System.Windows.Forms.Button btnAlarmi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbKorisnik;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnKorisnici;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolSlBrVozila;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolRegistracija;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolProizvodjac;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolModel;
        private System.Windows.Forms.Button btnPoruke;
        private System.Windows.Forms.Button btnPretraga;
    }
}

