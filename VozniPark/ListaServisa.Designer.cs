﻿namespace VozniPark
{
    partial class ListaServisa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.dgServisi = new System.Windows.Forms.DataGridView();
            this.btnInfo = new System.Windows.Forms.Button();
            this.txtPojam = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbxOsnovni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgServisi)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblRegistracija);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.label7);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 121);
            this.gbxOsnovni.TabIndex = 78;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 24);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistracija.Location = new System.Drawing.Point(103, 47);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(89, 17);
            this.lblRegistracija.TabIndex = 24;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 70);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registracija:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 93);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // btnObrisi
            // 
            this.btnObrisi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnObrisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnObrisi.Location = new System.Drawing.Point(593, 31);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(78, 68);
            this.btnObrisi.TabIndex = 3;
            this.btnObrisi.Text = "Obriši";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // dgServisi
            // 
            this.dgServisi.AllowUserToAddRows = false;
            this.dgServisi.AllowUserToDeleteRows = false;
            this.dgServisi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgServisi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgServisi.Location = new System.Drawing.Point(12, 150);
            this.dgServisi.Name = "dgServisi";
            this.dgServisi.ReadOnly = true;
            this.dgServisi.RowTemplate.Height = 24;
            this.dgServisi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgServisi.Size = new System.Drawing.Size(685, 398);
            this.dgServisi.TabIndex = 1;
            // 
            // btnInfo
            // 
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInfo.Location = new System.Drawing.Point(491, 31);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(78, 68);
            this.btnInfo.TabIndex = 2;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // txtPojam
            // 
            this.txtPojam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPojam.Location = new System.Drawing.Point(271, 57);
            this.txtPojam.Name = "txtPojam";
            this.txtPojam.Size = new System.Drawing.Size(189, 24);
            this.txtPojam.TabIndex = 1;
            this.txtPojam.TextChanged += new System.EventHandler(this.btnPretraga_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(268, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 18);
            this.label4.TabIndex = 79;
            this.label4.Text = "Pojam za pretragu";
            // 
            // ListaServisa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 560);
            this.Controls.Add(this.txtPojam);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgServisi);
            this.Controls.Add(this.gbxOsnovni);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.btnObrisi);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(727, 607);
            this.Name = "ListaServisa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista servisa";
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgServisi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.DataGridView dgServisi;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.TextBox txtPojam;
        private System.Windows.Forms.Label label4;
    }
}