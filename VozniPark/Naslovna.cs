﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Configuration;

namespace VozniPark
{
    public partial class Naslovna : Form
    {
        List<Vozila> vozilaList = new List<Vozila>();
        List<KorisniciClass> korLIst = new List<KorisniciClass>();
        LogIn _login;
        DateTime LoginTime;
        public string userName;
        public Naslovna(string UN,LogIn _log, DateTime _loginTime)
        {
            LoginTime = _loginTime;
            _login = _log;
            userName = UN;
            InitializeComponent();
            ispitajKorisnika();
            lbKorisnik.Text = userName;
            dgListaVozila.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            prikaziVozilaDGV();
        }

        private void ispitajKorisnika()
        {
            korLIst = new KorisniciClass().ucitajKorisnika(userName);
            if (korLIst[0].UserType != "Admin")
            {
                btnAlarmi.Visible = false;
                btnPoruke.Visible = false;
                btnKorisnici.Visible = false;

                if (korLIst[0].PregledInfo == false)
                    if (korLIst[0].UnosOdrzavanje == false)
                    { btnOVozilu.Enabled = false; }

                if (korLIst[0].Pretraga == false)
                { btnPretraga.Visible = false; }

                if (korLIst[0].UnosBrisVozila == false)
                {
                    btnNovo.Enabled = false;
                    btnObrisiVozilo.Enabled = false;
                }
            }
        }

        public void prikaziVozilaDGV()
        {
            vozilaList = new Vozila().ucitajVozila();
            dgListaVozila.Rows.Clear();
            for (int i = 0; i < vozilaList.Count; i++)
            {
                dgListaVozila.Rows.Add();
                dgListaVozila.Rows[i].Cells["kolSlBrVozila"].Value = vozilaList[i].SlBr;
                dgListaVozila.Rows[i].Cells["kolRegistracija"].Value = vozilaList[i].RegistarskiBr;
                dgListaVozila.Rows[i].Cells["kolProizvodjac"].Value = vozilaList[i].Proizvodjac;
                dgListaVozila.Rows[i].Cells["kolModel"].Value = vozilaList[i].Model;

                dgListaVozila.CurrentCell = null;
            }
        }

        public void OmoguciDugmad()
        {
            btnNovo.Enabled = true;
            btnOVozilu.Enabled = true;
            btnObrisiVozilo.Enabled = true;
            btnAlarmi.Enabled = true;
            btnKorisnici.Enabled = true;
            btnLogout.Enabled = true;
            dgListaVozila.Enabled = true;
        }
        
        private void btnNovo_Click(object sender, EventArgs e)
        {
            btnNovo.Enabled = false;
            btnOVozilu.Enabled = false;
            btnObrisiVozilo.Enabled = false;
            btnAlarmi.Enabled = false;
            btnKorisnici.Enabled = false;
            btnLogout.Enabled = false;
            dgListaVozila.Enabled = false;
            UnosNovogVozila unosNovog = new UnosNovogVozila(this);
            unosNovog.FormBorderStyle = FormBorderStyle.None;
            unosNovog.TopLevel = false;
            unosNovog.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(unosNovog);
            unosNovog.Show();
        }

        private void btnObrisiVozilo_Click(object sender, EventArgs e)
        {
            if (dgListaVozila.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabrano vozilo?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgListaVozila.SelectedRows[0].Cells["kolSlBrVozila"].Value;

                    Vozila selektovanoVozilo = vozilaList.Where(x => x.SlBr == idSelektovanog).FirstOrDefault();

                    if (selektovanoVozilo != null)
                    {
                        selektovanoVozilo.obrisiVozilo();
                    }
                    prikaziVozilaDGV();
                }
            }
        }
        
        private void btnKorisnici_Click(object sender, EventArgs e)
        {
            btnNovo.Enabled = false;
            btnOVozilu.Enabled = false;
            btnObrisiVozilo.Enabled = false;
            btnAlarmi.Enabled = false;
            btnKorisnici.Enabled = false;
            btnLogout.Enabled = false;
            dgListaVozila.Enabled = false;

            Korisnici users = new Korisnici(this);
            users.FormBorderStyle = FormBorderStyle.Sizable;
            users.TopLevel = false;
            users.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(users);
            users.Show();
        }

        public void btnOVozilu_Click(object sender, EventArgs e)
        {
            if ( dgListaVozila.SelectedRows.Count > 0)
            {
                btnNovo.Enabled = false;
                btnOVozilu.Enabled = false;
                btnObrisiVozilo.Enabled = false;
                btnAlarmi.Enabled = false;
                btnKorisnici.Enabled = false;
                btnLogout.Enabled = false;
                dgListaVozila.Enabled = false;

                OVozilu oVozilu = new OVozilu((int)dgListaVozila.SelectedRows[0].Cells["kolSlBrVozila"].Value, this,userName);
                oVozilu.FormBorderStyle = FormBorderStyle.None;
                oVozilu.TopLevel = false;
                oVozilu.AutoScroll = true;
                splitContainer1.Panel2.Controls.Add(oVozilu);
                oVozilu.Show();
            }
            else { MessageBox.Show("Nema podataka ili nijedan red nije odabran!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void btnAlarmi_Click(object sender, EventArgs e)
        {
            btnOVozilu.Enabled = false;
            btnObrisiVozilo.Enabled = false;
            btnNovo.Enabled = false;
            btnAlarmi.Enabled = false;
            btnKorisnici.Enabled = false;
            btnLogout.Enabled = false;
            dgListaVozila.Enabled = false;

            Alarmi alarmi = new Alarmi(this);
            alarmi.FormBorderStyle = FormBorderStyle.None;
            alarmi.TopLevel = false;
            alarmi.AutoScroll = true;
            splitContainer1.Panel2.Controls.Add(alarmi);
            alarmi.Show();
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            PretragaSvih pret = new PretragaSvih();
            pret.Show();
        }
        
        private void Naslovna_FormClosing(object sender, FormClosingEventArgs e)
        {
            SQLiteConnection connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            SQLiteDataAdapter sdaID = new SQLiteDataAdapter("Select UserID from tbLogin where UserName= '" + lbKorisnik.Text + "' ", connection);
            DataTable dtID = new DataTable();
            sdaID.Fill(dtID);
            Logovanja _logovanja = new Logovanja();
            _logovanja.LogIn = LoginTime;
            _logovanja.LogOut = DateTime.Now;
            KorisniciClass kor = new KorisniciClass();
            kor.UserID = Int32.Parse(dtID.Rows[0][0].ToString());
            _logovanja.Korisnik = kor;
            _logovanja.dodajLogovanje();
            List<Form> openForms = new List<Form>();
            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form f in openForms)
            {
                if (f.Name != "LogIn" && f.Name != "Naslovna")
                    f.Close();
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            _login.Show();
            this.Close();
        }

        private void btnPoruke_Click(object sender, EventArgs e)
        {
            Obaveštenja ob = new Obaveštenja();
            ob.Show();
        }
    }
}
