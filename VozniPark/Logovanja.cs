﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    class Logovanja
    {
        private int id;
        private DateTime logIn;
        private DateTime logOut;
        private KorisniciClass korisnik;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public DateTime LogIn
        {
            get { return logIn; }
            set { logIn = value; }
        }
        public DateTime LogOut
        {
            get { return logOut; }
            set { logOut = value; }
        }
        public KorisniciClass Korisnik
        {
            get { return korisnik; }
            set { korisnik = value; }
        }

        public void dodajLogovanje()
        {

            SQLiteConnection _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlADD = @"Insert into tbLogovanja (Login, Logout, KorisnikID)  Values (@Login, @Logout, @KorisnikID)";

            SQLiteCommand command = new SQLiteCommand(sqlADD, _connection);
            command.Parameters.Add(new SQLiteParameter("@Login", LogIn));
            command.Parameters.Add(new SQLiteParameter("@Logout", LogOut));
            command.Parameters.Add(new SQLiteParameter("@KorisnikID", Korisnik.UserID));
            command.ExecuteNonQuery();
        }

        public List<Logovanja> ucitaLogovanja(int _korisnikID)
        {
            List<Logovanja> Log = new List<Logovanja>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbLogovanja JOIN tbLogin WHERE KorisnikID = @UserID AND UserID = @UserID;";

            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@UserID", _korisnikID));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    Logovanja _Log;
                    while (reader.Read())
                    {
                        _Log = new Logovanja();
                        _Log.ID = Int32.Parse(reader["IDLogovanja"].ToString());
                        _Log.LogIn = Convert.ToDateTime(reader["Login"].ToString());
                        _Log.LogOut = Convert.ToDateTime(reader["Logout"].ToString());
                        _Log.Korisnik = new KorisniciClass();
                        _Log.Korisnik.UserName = reader["UserName"].ToString();

                        Log.Add(_Log);
                    }
                }
            }
            return Log;
        }
            
    }
}
