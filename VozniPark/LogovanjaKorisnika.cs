﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class LogovanjaKorisnika : Form
    {

        List<Logovanja> logList = new List<Logovanja>();
        Korisnici _korisnici;
        int _IDKor;
        public LogovanjaKorisnika(Korisnici _kor, int _idKor)
        {
            _korisnici = _kor;
            _IDKor = _idKor;
            InitializeComponent();
            prikaziLogovanjaDGV();
        }

        public void prikaziLogovanjaDGV()
        {
            logList = new Logovanja().ucitaLogovanja(_IDKor);
            dgLogovanja.Rows.Clear();
            for (int i = 0; i < logList.Count; i++)
            {
                string datumStr = logList[i].LogIn.Day+"."+ logList[i].LogIn.Month+"."+ logList[i].LogIn.Year+".";
                string strLogin = logList[i].LogIn.Hour + ":" + logList[i].LogIn.Minute;
                string strLogout = logList[i].LogOut.Hour + ":" + logList[i].LogOut.Minute;
                lblKorisnik.Text = logList[i].Korisnik.UserName;
                dgLogovanja.Rows.Add();
                dgLogovanja.Rows[i].Cells["kolID"].Value = logList[i].ID;
                dgLogovanja.Rows[i].Cells["kolDatum"].Value = datumStr;
                dgLogovanja.Rows[i].Cells["kolLogin"].Value = strLogin;
                dgLogovanja.Rows[i].Cells["kolLogout"].Value = strLogout;

                dgLogovanja.CurrentCell = null;
            }
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            _korisnici.btnEnabled();
            this.Close();
        }
    }
}