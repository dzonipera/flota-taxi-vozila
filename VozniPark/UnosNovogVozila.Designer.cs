﻿using System;

namespace VozniPark
{
    partial class UnosNovogVozila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnosNovogVozila));
            this.label2 = new System.Windows.Forms.Label();
            this.txtProizvodjac = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtGodProizvodnje = new System.Windows.Forms.TextBox();
            this.txtBoja = new System.Windows.Forms.TextBox();
            this.txtKaroserija = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxMProizvodnje = new System.Windows.Forms.ComboBox();
            this.txtSlBrVozila = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMasa = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBrSasije = new System.Windows.Forms.TextBox();
            this.txtBrVrata = new System.Windows.Forms.TextBox();
            this.txtBrSedista = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBrMotora = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtStandard = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtZapremina = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtGorivo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtRezervoar = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtMenjac = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBrBrzina = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tpDatKupovine = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.cbxStanje = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtVlasnik = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtKilometraza = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtRegistracija = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tpRegistrovanDo = new System.Windows.Forms.DateTimePicker();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.txtKW = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtKonjske = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Proizvođač";
            // 
            // txtProizvodjac
            // 
            this.txtProizvodjac.Location = new System.Drawing.Point(125, 67);
            this.txtProizvodjac.Name = "txtProizvodjac";
            this.txtProizvodjac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProizvodjac.Size = new System.Drawing.Size(188, 22);
            this.txtProizvodjac.TabIndex = 2;
            this.txtProizvodjac.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(93, 95);
            this.txtModel.Name = "txtModel";
            this.txtModel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtModel.Size = new System.Drawing.Size(220, 22);
            this.txtModel.TabIndex = 3;
            this.txtModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGodProizvodnje
            // 
            this.txtGodProizvodnje.Location = new System.Drawing.Point(189, 235);
            this.txtGodProizvodnje.Name = "txtGodProizvodnje";
            this.txtGodProizvodnje.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGodProizvodnje.Size = new System.Drawing.Size(124, 22);
            this.txtGodProizvodnje.TabIndex = 10;
            this.txtGodProizvodnje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGodProizvodnje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // txtBoja
            // 
            this.txtBoja.Location = new System.Drawing.Point(216, 207);
            this.txtBoja.Name = "txtBoja";
            this.txtBoja.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBoja.Size = new System.Drawing.Size(97, 22);
            this.txtBoja.TabIndex = 9;
            this.txtBoja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtKaroserija
            // 
            this.txtKaroserija.Location = new System.Drawing.Point(141, 151);
            this.txtKaroserija.Name = "txtKaroserija";
            this.txtKaroserija.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKaroserija.Size = new System.Drawing.Size(172, 22);
            this.txtKaroserija.TabIndex = 5;
            this.txtKaroserija.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "Mesec proizvodnje";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Godina proizvodnje";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(174, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Boja";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(173, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Broj sedišta";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Broj vrata";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tip karoserije";
            // 
            // cbxMProizvodnje
            // 
            this.cbxMProizvodnje.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMProizvodnje.FormattingEnabled = true;
            this.cbxMProizvodnje.Items.AddRange(new object[] {
            "Januar",
            "Februar",
            "Mart",
            "April",
            "Maj",
            "Jun",
            "Jul",
            "Avgust",
            "Septembar",
            "Oktobar",
            "Novembar",
            "Decembar"});
            this.cbxMProizvodnje.Location = new System.Drawing.Point(189, 263);
            this.cbxMProizvodnje.Name = "cbxMProizvodnje";
            this.cbxMProizvodnje.Size = new System.Drawing.Size(124, 24);
            this.cbxMProizvodnje.TabIndex = 11;
            // 
            // txtSlBrVozila
            // 
            this.txtSlBrVozila.Location = new System.Drawing.Point(197, 17);
            this.txtSlBrVozila.Name = "txtSlBrVozila";
            this.txtSlBrVozila.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSlBrVozila.Size = new System.Drawing.Size(116, 22);
            this.txtSlBrVozila.TabIndex = 1;
            this.txtSlBrVozila.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSlBrVozila.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Službeni broj vozila";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 17);
            this.label10.TabIndex = 2;
            this.label10.Text = "Masa(kg)";
            // 
            // txtMasa
            // 
            this.txtMasa.Location = new System.Drawing.Point(114, 207);
            this.txtMasa.Name = "txtMasa";
            this.txtMasa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMasa.Size = new System.Drawing.Size(54, 22);
            this.txtMasa.TabIndex = 8;
            this.txtMasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMasa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(41, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Broj šasije";
            // 
            // txtBrSasije
            // 
            this.txtBrSasije.Location = new System.Drawing.Point(125, 123);
            this.txtBrSasije.Name = "txtBrSasije";
            this.txtBrSasije.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrSasije.Size = new System.Drawing.Size(188, 22);
            this.txtBrSasije.TabIndex = 4;
            this.txtBrSasije.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtBrVrata
            // 
            this.txtBrVrata.Location = new System.Drawing.Point(116, 179);
            this.txtBrVrata.Name = "txtBrVrata";
            this.txtBrVrata.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrVrata.Size = new System.Drawing.Size(51, 22);
            this.txtBrVrata.TabIndex = 6;
            this.txtBrVrata.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrVrata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // txtBrSedista
            // 
            this.txtBrSedista.Location = new System.Drawing.Point(261, 177);
            this.txtBrSedista.Name = "txtBrSedista";
            this.txtBrSedista.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrSedista.Size = new System.Drawing.Size(52, 22);
            this.txtBrSedista.TabIndex = 7;
            this.txtBrSedista.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrSedista.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "Broj motora";
            // 
            // txtBrMotora
            // 
            this.txtBrMotora.Location = new System.Drawing.Point(171, 293);
            this.txtBrMotora.Name = "txtBrMotora";
            this.txtBrMotora.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrMotora.Size = new System.Drawing.Size(142, 22);
            this.txtBrMotora.TabIndex = 12;
            this.txtBrMotora.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(41, 352);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Standard";
            // 
            // txtStandard
            // 
            this.txtStandard.Location = new System.Drawing.Point(171, 349);
            this.txtStandard.Name = "txtStandard";
            this.txtStandard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStandard.Size = new System.Drawing.Size(142, 22);
            this.txtStandard.TabIndex = 14;
            this.txtStandard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 324);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Zapremina ccm";
            // 
            // txtZapremina
            // 
            this.txtZapremina.Location = new System.Drawing.Point(171, 321);
            this.txtZapremina.Name = "txtZapremina";
            this.txtZapremina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtZapremina.Size = new System.Drawing.Size(142, 22);
            this.txtZapremina.TabIndex = 13;
            this.txtZapremina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZapremina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(41, 378);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 17);
            this.label15.TabIndex = 2;
            this.label15.Text = "kW";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(41, 408);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "Gorivo";
            // 
            // txtGorivo
            // 
            this.txtGorivo.Location = new System.Drawing.Point(132, 405);
            this.txtGorivo.Name = "txtGorivo";
            this.txtGorivo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGorivo.Size = new System.Drawing.Size(181, 22);
            this.txtGorivo.TabIndex = 17;
            this.txtGorivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(41, 436);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(194, 17);
            this.label17.TabIndex = 2;
            this.label17.Text = "Zapremina rezervoara (litara)";
            // 
            // txtRezervoar
            // 
            this.txtRezervoar.Location = new System.Drawing.Point(241, 433);
            this.txtRezervoar.Name = "txtRezervoar";
            this.txtRezervoar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRezervoar.Size = new System.Drawing.Size(72, 22);
            this.txtRezervoar.TabIndex = 18;
            this.txtRezervoar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRezervoar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(41, 464);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(174, 17);
            this.label18.TabIndex = 2;
            this.label18.Text = "Tip menjača (npr. manual)";
            // 
            // txtMenjac
            // 
            this.txtMenjac.Location = new System.Drawing.Point(221, 461);
            this.txtMenjac.Name = "txtMenjac";
            this.txtMenjac.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMenjac.Size = new System.Drawing.Size(92, 22);
            this.txtMenjac.TabIndex = 19;
            this.txtMenjac.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(41, 492);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(139, 17);
            this.label19.TabIndex = 2;
            this.label19.Text = "Broj stepeni prenosa";
            // 
            // txtBrBrzina
            // 
            this.txtBrBrzina.Location = new System.Drawing.Point(241, 489);
            this.txtBrBrzina.Name = "txtBrBrzina";
            this.txtBrBrzina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBrBrzina.Size = new System.Drawing.Size(72, 22);
            this.txtBrBrzina.TabIndex = 20;
            this.txtBrBrzina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBrBrzina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(370, 67);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(110, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "Datum kupovine";
            // 
            // tpDatKupovine
            // 
            this.tpDatKupovine.CustomFormat = "dd.MM.yyyy.";
            this.tpDatKupovine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpDatKupovine.Location = new System.Drawing.Point(486, 64);
            this.tpDatKupovine.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpDatKupovine.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpDatKupovine.Name = "tpDatKupovine";
            this.tpDatKupovine.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpDatKupovine.Size = new System.Drawing.Size(152, 22);
            this.tpDatKupovine.TabIndex = 21;
            this.tpDatKupovine.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(369, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 17);
            this.label21.TabIndex = 2;
            this.label21.Text = "Stanje";
            // 
            // cbxStanje
            // 
            this.cbxStanje.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxStanje.FormattingEnabled = true;
            this.cbxStanje.Items.AddRange(new object[] {
            "Nov",
            "Polovan"});
            this.cbxStanje.Location = new System.Drawing.Point(486, 90);
            this.cbxStanje.Name = "cbxStanje";
            this.cbxStanje.Size = new System.Drawing.Size(152, 24);
            this.cbxStanje.TabIndex = 22;
            this.cbxStanje.SelectedIndexChanged += new System.EventHandler(this.cbxStanje_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(369, 123);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Prethodni vlasnik";
            // 
            // txtVlasnik
            // 
            this.txtVlasnik.Location = new System.Drawing.Point(486, 120);
            this.txtVlasnik.Name = "txtVlasnik";
            this.txtVlasnik.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVlasnik.Size = new System.Drawing.Size(152, 22);
            this.txtVlasnik.TabIndex = 23;
            this.txtVlasnik.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(369, 151);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(147, 17);
            this.label23.TabIndex = 2;
            this.label23.Text = "Kilometraža kupljenog";
            // 
            // txtKilometraza
            // 
            this.txtKilometraza.Location = new System.Drawing.Point(522, 148);
            this.txtKilometraza.Name = "txtKilometraza";
            this.txtKilometraza.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKilometraza.Size = new System.Drawing.Size(116, 22);
            this.txtKilometraza.TabIndex = 24;
            this.txtKilometraza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKilometraza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(369, 179);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(133, 17);
            this.label24.TabIndex = 2;
            this.label24.Text = "Registarska oznaka";
            // 
            // txtRegistracija
            // 
            this.txtRegistracija.Location = new System.Drawing.Point(522, 176);
            this.txtRegistracija.Name = "txtRegistracija";
            this.txtRegistracija.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRegistracija.Size = new System.Drawing.Size(116, 22);
            this.txtRegistracija.TabIndex = 25;
            this.txtRegistracija.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(370, 207);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(104, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "Registrovan do";
            // 
            // tpRegistrovanDo
            // 
            this.tpRegistrovanDo.CustomFormat = "dd.MM.yyyy.";
            this.tpRegistrovanDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpRegistrovanDo.Location = new System.Drawing.Point(486, 204);
            this.tpRegistrovanDo.MaxDate = new System.DateTime(4000, 12, 31, 0, 0, 0, 0);
            this.tpRegistrovanDo.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.tpRegistrovanDo.Name = "tpRegistrovanDo";
            this.tpRegistrovanDo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tpRegistrovanDo.Size = new System.Drawing.Size(152, 22);
            this.tpRegistrovanDo.TabIndex = 26;
            this.tpRegistrovanDo.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Location = new System.Drawing.Point(406, 417);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(94, 54);
            this.btnSacuvaj.TabIndex = 27;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnZatvori
            // 
            this.btnZatvori.Location = new System.Drawing.Point(528, 417);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(94, 54);
            this.btnZatvori.TabIndex = 28;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // txtKW
            // 
            this.txtKW.Location = new System.Drawing.Point(74, 377);
            this.txtKW.Name = "txtKW";
            this.txtKW.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKW.Size = new System.Drawing.Size(61, 22);
            this.txtKW.TabIndex = 15;
            this.txtKW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(145, 380);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 17);
            this.label26.TabIndex = 2;
            this.label26.Text = "Konjske snage";
            // 
            // txtKonjske
            // 
            this.txtKonjske.Location = new System.Drawing.Point(252, 377);
            this.txtKonjske.Name = "txtKonjske";
            this.txtKonjske.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtKonjske.Size = new System.Drawing.Size(61, 22);
            this.txtKonjske.TabIndex = 16;
            this.txtKonjske.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKonjske.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlBrVozila_KeyPress);
            // 
            // UnosNovogVozila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.ClientSize = new System.Drawing.Size(664, 533);
            this.ControlBox = false;
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.tpRegistrovanDo);
            this.Controls.Add(this.tpDatKupovine);
            this.Controls.Add(this.cbxStanje);
            this.Controls.Add(this.cbxMProizvodnje);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSlBrVozila);
            this.Controls.Add(this.txtGodProizvodnje);
            this.Controls.Add(this.txtMasa);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtBoja);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtProizvodjac);
            this.Controls.Add(this.txtBrSedista);
            this.Controls.Add(this.txtBrVrata);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtKonjske);
            this.Controls.Add(this.txtKW);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtZapremina);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtStandard);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBrBrzina);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtRezervoar);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtGorivo);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtMenjac);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtKilometraza);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtRegistracija);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtVlasnik);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtBrMotora);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtBrSasije);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtKaroserija);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnosNovogVozila";
            this.Text = "Unos novog vozila";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            _naslovna.OmoguciDugmad();
            this.Close();
        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProizvodjac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtBoja;
        private System.Windows.Forms.TextBox txtKaroserija;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGodProizvodnje;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxMProizvodnje;
        private System.Windows.Forms.TextBox txtSlBrVozila;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMasa;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBrSasije;
        private System.Windows.Forms.TextBox txtBrVrata;
        private System.Windows.Forms.TextBox txtBrSedista;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBrMotora;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtStandard;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtZapremina;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtGorivo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtRezervoar;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtMenjac;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBrBrzina;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker tpDatKupovine;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cbxStanje;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtVlasnik;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtKilometraza;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtRegistracija;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DateTimePicker tpRegistrovanDo;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.TextBox txtKW;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtKonjske;
    }
}