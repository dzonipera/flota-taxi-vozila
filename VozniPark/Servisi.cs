﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    class Servisi
    {
        private int idServisa;
        private Vozila vozilo;
        private string koJeVrsio;
        private DateTime datumServisa;
        private int kilometrazaServ;
        private string cenaServisa;
        private string tipServisa;
        private string radovi;
        
        public int IdServisa
        {
            get { return idServisa; }
            set { idServisa = value; }
        }
        public Vozila Vozilo
        {
            get { return vozilo; }
            set { vozilo = value; }
        }
        public string KoJeVrsio
        {
            get { return koJeVrsio; }
            set { koJeVrsio = value; }
        }
        public DateTime DatumServisa
        {
            get { return datumServisa; }
            set { datumServisa = value; }
        }
        public int KilometrazaServ
        {
            get { return kilometrazaServ; }
            set { kilometrazaServ = value; }    
        }
        public string CenaServisa
        {
            get { return cenaServisa; }
            set { cenaServisa = value; }
        }
        public string TipServisa
        {
            get { return tipServisa; }
            set { tipServisa = value; }
        }
        public string Radovi
        {
            get { return radovi; }
            set { radovi = value; }
        }

        public DateTime strStart;
        public DateTime strEnd;
        public string strPojam;

        public void dodajServis()
        {
            SQLiteConnection _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlADD = @"Insert into tbServisi (TipServisa, DatumServisa, RadoveVrsio, KilometrazaServ, CenaServisa, 
                            VrseniRadovi, IDVozila)  Values (@TipServisa, @DatumServisa, @RadoveVrsio, @KilometrazaServ,
                            @CenaServisa, @VrseniRadovi, @IDVozila)";

            SQLiteCommand command = new SQLiteCommand(sqlADD, _connection);
            command.Parameters.Add(new SQLiteParameter("@TipServisa", TipServisa));
            command.Parameters.Add(new SQLiteParameter("@DatumServisa", DatumServisa));
            command.Parameters.Add(new SQLiteParameter("@RadoveVrsio", KoJeVrsio));
            command.Parameters.Add(new SQLiteParameter("@KilometrazaServ", KilometrazaServ));
            command.Parameters.Add(new SQLiteParameter("@CenaServisa", CenaServisa));
            command.Parameters.Add(new SQLiteParameter("@VrseniRadovi", Radovi));
            command.Parameters.Add(new SQLiteParameter("@IDVozila", Vozilo.SlBr));
            command.ExecuteNonQuery();
        }

        public void obrisiServis()
        {
            string deleteSql = "DELETE FROM tbServisi WHERE IDServisa = @IDServisa";
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SQLiteParameter("@IDServisa", IdServisa));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<Servisi> ucitajServis(int voziloID, int _idServ)
        {
            List<Servisi> Serv = new List<Servisi>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbServisi JOIN tbVozila WHERE IDServisa=@IDServisa AND SlBrVozila=IDVozila  ;  ";

            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@IDServisa", _idServ));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    Servisi _Serv;
                    while (reader.Read())
                    {
                        _Serv = new Servisi();
                        _Serv.IdServisa = Int32.Parse(reader["IDServisa"].ToString());
                        _Serv.KoJeVrsio = reader["RadoveVrsio"].ToString();
                        _Serv.DatumServisa = Convert.ToDateTime(reader["DatumServisa"].ToString());
                        _Serv.KilometrazaServ = Int32.Parse(reader["KilometrazaServ"].ToString());
                        _Serv.CenaServisa = reader["CenaServisa"].ToString();
                        _Serv.TipServisa = reader["TipServisa"].ToString();
                        _Serv.Radovi = reader["VrseniRadovi"].ToString();
                        _Serv.Vozilo = new Vozila();
                        _Serv.Vozilo.SlBr = Int32.Parse(reader["IDVozila"].ToString());
                        _Serv.Vozilo.RegistarskiBr = reader["RegistarskiBr"].ToString();
                        _Serv.Vozilo.Proizvodjac = reader["Proizvodjac"].ToString();
                        _Serv.Vozilo.Model = reader["Model"].ToString();
                        Serv.Add(_Serv);
                    }
                }
            }
            return Serv;
        }

        public List<Servisi> ucitajServise(int voziloID)
        {
            List<Servisi> Serv = new List<Servisi>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbServisi WHERE IDVozila = @SlBrVozila;";
            
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@SlBrVozila", voziloID));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    Servisi _Serv;
                    while (reader.Read())
                    {
                        _Serv = new Servisi();
                        _Serv.IdServisa = Int32.Parse(reader["IDServisa"].ToString());
                        _Serv.KoJeVrsio = reader["RadoveVrsio"].ToString();
                        _Serv.DatumServisa = Convert.ToDateTime(reader["DatumServisa"].ToString());
                        _Serv.KilometrazaServ = Int32.Parse(reader["KilometrazaServ"].ToString());
                        _Serv.CenaServisa = reader["CenaServisa"].ToString();
                        _Serv.TipServisa = reader["TipServisa"].ToString();
                        _Serv.Radovi = reader["VrseniRadovi"].ToString();
                        Serv.Add(_Serv);
                    }
                }
            }
            return Serv;
        }

        public List<Servisi> ucitajServiseOdDo(string _pojam, DateTime _start, DateTime _end)
        {
            strPojam = _pojam;
            strStart = _start;
            strEnd = _end;
            List<Servisi> Serv = new List<Servisi>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbServisi JOIN tbVozila WHERE IDVozila=SlBrVozila;"; 

            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    Servisi _Serv;
                    while (reader.Read())
                    {
                        _Serv = new Servisi();
                        _Serv.IdServisa = Int32.Parse(reader["IDServisa"].ToString());
                        _Serv.KoJeVrsio = reader["RadoveVrsio"].ToString();
                        _Serv.DatumServisa = Convert.ToDateTime(reader["DatumServisa"].ToString());
                        _Serv.KilometrazaServ = Int32.Parse(reader["KilometrazaServ"].ToString());
                        _Serv.CenaServisa = reader["CenaServisa"].ToString();
                        _Serv.TipServisa = reader["TipServisa"].ToString();
                        _Serv.Radovi = reader["VrseniRadovi"].ToString();
                        _Serv.Vozilo = new Vozila();
                        _Serv.Vozilo.SlBr = Int32.Parse(reader["IDVozila"].ToString());
                        _Serv.Vozilo.RegistarskiBr = reader["RegistarskiBr"].ToString();
                        Serv.Add(_Serv);
                    }
                }
            }
            return Serv;
        }

    }
}
