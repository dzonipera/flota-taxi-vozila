﻿namespace VozniPark
{
    partial class UpravljajKorisnikom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.lblAkcija = new System.Windows.Forms.Label();
            this.btnPotvrdi = new System.Windows.Forms.Button();
            this.btnOdbaci = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtImePrezime = new System.Windows.Forms.TextBox();
            this.chkPregledInfo = new System.Windows.Forms.CheckBox();
            this.chkPretraga = new System.Windows.Forms.CheckBox();
            this.chkUnosOdrzavanje = new System.Windows.Forms.CheckBox();
            this.chkUnosBrisVozila = new System.Windows.Forms.CheckBox();
            this.gbOvlastenja = new System.Windows.Forms.GroupBox();
            this.gbOvlastenja.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Korisnicko ime";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lozinka";
            // 
            // txtUser
            // 
            this.txtUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtUser.Location = new System.Drawing.Point(163, 125);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(136, 22);
            this.txtUser.TabIndex = 2;
            // 
            // txtPass
            // 
            this.txtPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPass.Location = new System.Drawing.Point(163, 153);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(136, 22);
            this.txtPass.TabIndex = 3;
            // 
            // lblAkcija
            // 
            this.lblAkcija.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAkcija.AutoSize = true;
            this.lblAkcija.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAkcija.Location = new System.Drawing.Point(27, 23);
            this.lblAkcija.Name = "lblAkcija";
            this.lblAkcija.Size = new System.Drawing.Size(164, 29);
            this.lblAkcija.TabIndex = 0;
            this.lblAkcija.Text = "Novi korisnik";
            // 
            // btnPotvrdi
            // 
            this.btnPotvrdi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPotvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPotvrdi.Location = new System.Drawing.Point(32, 380);
            this.btnPotvrdi.Name = "btnPotvrdi";
            this.btnPotvrdi.Size = new System.Drawing.Size(96, 65);
            this.btnPotvrdi.TabIndex = 8;
            this.btnPotvrdi.Text = "Potvrdi";
            this.btnPotvrdi.UseVisualStyleBackColor = true;
            this.btnPotvrdi.Click += new System.EventHandler(this.btnPotvrdi_Click);
            // 
            // btnOdbaci
            // 
            this.btnOdbaci.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnOdbaci.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOdbaci.Location = new System.Drawing.Point(194, 380);
            this.btnOdbaci.Name = "btnOdbaci";
            this.btnOdbaci.Size = new System.Drawing.Size(96, 65);
            this.btnOdbaci.TabIndex = 9;
            this.btnOdbaci.Text = "Odbaci";
            this.btnOdbaci.UseVisualStyleBackColor = true;
            this.btnOdbaci.Click += new System.EventHandler(this.btnOdbaci_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ime i prezime";
            // 
            // txtImePrezime
            // 
            this.txtImePrezime.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtImePrezime.Location = new System.Drawing.Point(32, 82);
            this.txtImePrezime.Name = "txtImePrezime";
            this.txtImePrezime.Size = new System.Drawing.Size(212, 22);
            this.txtImePrezime.TabIndex = 1;
            // 
            // chkPregledInfo
            // 
            this.chkPregledInfo.AutoSize = true;
            this.chkPregledInfo.Location = new System.Drawing.Point(6, 45);
            this.chkPregledInfo.Name = "chkPregledInfo";
            this.chkPregledInfo.Size = new System.Drawing.Size(240, 21);
            this.chkPregledInfo.TabIndex = 4;
            this.chkPregledInfo.Text = "Pregled osnovnih informacija";
            this.chkPregledInfo.UseVisualStyleBackColor = true;
            // 
            // chkPretraga
            // 
            this.chkPretraga.AutoSize = true;
            this.chkPretraga.Location = new System.Drawing.Point(6, 72);
            this.chkPretraga.Name = "chkPretraga";
            this.chkPretraga.Size = new System.Drawing.Size(93, 21);
            this.chkPretraga.TabIndex = 5;
            this.chkPretraga.Text = "Pretraga";
            this.chkPretraga.UseVisualStyleBackColor = true;
            // 
            // chkUnosOdrzavanje
            // 
            this.chkUnosOdrzavanje.AutoSize = true;
            this.chkUnosOdrzavanje.Location = new System.Drawing.Point(6, 99);
            this.chkUnosOdrzavanje.Name = "chkUnosOdrzavanje";
            this.chkUnosOdrzavanje.Size = new System.Drawing.Size(213, 21);
            this.chkUnosOdrzavanje.TabIndex = 6;
            this.chkUnosOdrzavanje.Text = "Unos o održavanju vozila";
            this.chkUnosOdrzavanje.UseVisualStyleBackColor = true;
            // 
            // chkUnosBrisVozila
            // 
            this.chkUnosBrisVozila.AutoSize = true;
            this.chkUnosBrisVozila.Location = new System.Drawing.Point(6, 126);
            this.chkUnosBrisVozila.Name = "chkUnosBrisVozila";
            this.chkUnosBrisVozila.Size = new System.Drawing.Size(186, 21);
            this.chkUnosBrisVozila.TabIndex = 7;
            this.chkUnosBrisVozila.Text = "Unos i brisanje vozila";
            this.chkUnosBrisVozila.UseVisualStyleBackColor = true;
            // 
            // gbOvlastenja
            // 
            this.gbOvlastenja.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbOvlastenja.Controls.Add(this.chkPregledInfo);
            this.gbOvlastenja.Controls.Add(this.chkUnosBrisVozila);
            this.gbOvlastenja.Controls.Add(this.chkPretraga);
            this.gbOvlastenja.Controls.Add(this.chkUnosOdrzavanje);
            this.gbOvlastenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOvlastenja.Location = new System.Drawing.Point(32, 196);
            this.gbOvlastenja.Name = "gbOvlastenja";
            this.gbOvlastenja.Size = new System.Drawing.Size(267, 160);
            this.gbOvlastenja.TabIndex = 7;
            this.gbOvlastenja.TabStop = false;
            this.gbOvlastenja.Text = "Ovlaštenja";
            // 
            // UpravljajKorisnikom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(327, 463);
            this.ControlBox = false;
            this.Controls.Add(this.gbOvlastenja);
            this.Controls.Add(this.btnOdbaci);
            this.Controls.Add(this.btnPotvrdi);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.txtImePrezime);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblAkcija);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(345, 510);
            this.Name = "UpravljajKorisnikom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dodaj korisnika";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.gbOvlastenja.ResumeLayout(false);
            this.gbOvlastenja.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label lblAkcija;
        private System.Windows.Forms.Button btnPotvrdi;
        private System.Windows.Forms.Button btnOdbaci;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtImePrezime;
        private System.Windows.Forms.CheckBox chkPregledInfo;
        private System.Windows.Forms.CheckBox chkPretraga;
        private System.Windows.Forms.CheckBox chkUnosOdrzavanje;
        private System.Windows.Forms.CheckBox chkUnosBrisVozila;
        private System.Windows.Forms.GroupBox gbOvlastenja;
    }
}