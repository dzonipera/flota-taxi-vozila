﻿namespace VozniPark
{
    partial class PretragaSvih
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PretragaSvih));
            this.tpStart = new System.Windows.Forms.DateTimePicker();
            this.tpEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPojam = new System.Windows.Forms.TextBox();
            this.dgPretrazeni = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnPretrazi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgPretrazeni)).BeginInit();
            this.SuspendLayout();
            // 
            // tpStart
            // 
            this.tpStart.CustomFormat = "dd.MM.yyyy.";
            this.tpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpStart.Location = new System.Drawing.Point(84, 60);
            this.tpStart.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.tpStart.Name = "tpStart";
            this.tpStart.Size = new System.Drawing.Size(101, 22);
            this.tpStart.TabIndex = 0;
            this.tpStart.Value = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            // 
            // tpEnd
            // 
            this.tpEnd.CustomFormat = "dd.MM.yyyy.";
            this.tpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpEnd.Location = new System.Drawing.Point(237, 60);
            this.tpEnd.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.tpEnd.Name = "tpEnd";
            this.tpEnd.Size = new System.Drawing.Size(107, 22);
            this.tpEnd.TabIndex = 1;
            this.tpEnd.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pretraži servise za sva vozila";
            // 
            // txtPojam
            // 
            this.txtPojam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPojam.Location = new System.Drawing.Point(44, 120);
            this.txtPojam.Name = "txtPojam";
            this.txtPojam.Size = new System.Drawing.Size(189, 24);
            this.txtPojam.TabIndex = 3;
            this.txtPojam.TextChanged += new System.EventHandler(this.txtPojam_TextChanged);
            // 
            // dgPretrazeni
            // 
            this.dgPretrazeni.AllowUserToAddRows = false;
            this.dgPretrazeni.AllowUserToDeleteRows = false;
            this.dgPretrazeni.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPretrazeni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPretrazeni.Location = new System.Drawing.Point(13, 188);
            this.dgPretrazeni.Name = "dgPretrazeni";
            this.dgPretrazeni.ReadOnly = true;
            this.dgPretrazeni.RowTemplate.Height = 24;
            this.dgPretrazeni.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPretrazeni.Size = new System.Drawing.Size(595, 354);
            this.dgPretrazeni.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Od";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(200, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Do";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Pojam za pretragu";
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.BackgroundImage = global::VozniPark.Properties.Resources.Button_Info_01__1_;
            this.btnInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnInfo.Location = new System.Drawing.Point(486, 60);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(74, 74);
            this.btnInfo.TabIndex = 6;
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnPretrazi
            // 
            this.btnPretrazi.Location = new System.Drawing.Point(264, 115);
            this.btnPretrazi.Name = "btnPretrazi";
            this.btnPretrazi.Size = new System.Drawing.Size(102, 36);
            this.btnPretrazi.TabIndex = 7;
            this.btnPretrazi.Text = "Pretraži";
            this.btnPretrazi.UseVisualStyleBackColor = true;
            this.btnPretrazi.Click += new System.EventHandler(this.txtPojam_TextChanged);
            // 
            // PretragaSvih
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 554);
            this.Controls.Add(this.btnPretrazi);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.dgPretrazeni);
            this.Controls.Add(this.txtPojam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tpEnd);
            this.Controls.Add(this.tpStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(638, 601);
            this.Name = "PretragaSvih";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pretragas svih servisa";
            ((System.ComponentModel.ISupportInitialize)(this.dgPretrazeni)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker tpStart;
        private System.Windows.Forms.DateTimePicker tpEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPojam;
        private System.Windows.Forms.DataGridView dgPretrazeni;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnPretrazi;
    }
}