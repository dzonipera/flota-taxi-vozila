﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class ListaPregleda : Form
    {
        List<IntPregled> PregList = new List<IntPregled>();
        int _id;
        public ListaPregleda(int id, string reg, string proiz, string model)
        {
            InitializeComponent();
            _id = id;
            lblSluzbeniBroj.Text = id.ToString();
            lblRegistracija.Text = reg;
            lblProizvodjac.Text = proiz;
            lblModel.Text = model;
            dgPerPregledi.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            prikaziPregledeDGV();
        }

        public void prikaziPregledeDGV()
        {
            PregList = new IntPregled().ucitajPreglede(Int32.Parse(lblSluzbeniBroj.Text));
            dgPerPregledi.Rows.Clear();
            for (int i=0; i<PregList.Count; i++)
            {
                dgPerPregledi.Rows.Add();
                string dateString = PregList[i].DatumPregleda.Day + ". " + PregList[i].DatumPregleda.Month + ". " + PregList[i].DatumPregleda.Year + ".";
                dgPerPregledi.Rows[i].Cells["kolID"].Value = PregList[i].IdPregleda;
                dgPerPregledi.Rows[i].Cells["kolDatumPreg"].Value = dateString;
                dgPerPregledi.Rows[i].Cells["kolPregledVrsio"].Value = PregList[i].VrsioPregled;
                dgPerPregledi.CurrentCell = null;
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgPerPregledi.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li želite da obrišete odabrani pregled?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgPerPregledi.SelectedRows[0].Cells["kolID"].Value;

                    IntPregled selektovaniPregled = PregList.Where(x => x.IdPregleda == idSelektovanog).FirstOrDefault();

                    if (selektovaniPregled != null)
                    {
                        selektovaniPregled.obrisiPregled();
                    }
                    prikaziPregledeDGV();
                }
            }
        }
     
        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (dgPerPregledi.SelectedRows.Count > 0)
            {
                int _idPreg = Int32.Parse(dgPerPregledi.SelectedRows[0].Cells["kolID"].Value.ToString());
                OPregledu op = new OPregledu(_id, _idPreg);
                op.Show();
            }
            else { MessageBox.Show("Nema podataka ili nijedan red nije odabran!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }
    }
}
