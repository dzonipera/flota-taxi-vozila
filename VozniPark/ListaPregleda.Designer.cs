﻿namespace VozniPark
{
    partial class ListaPregleda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSluzbeniBroj = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.dgPerPregledi = new System.Windows.Forms.DataGridView();
            this.kolID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolDatumPreg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kolPregledVrsio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbxOsnovni = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgPerPregledi)).BeginInit();
            this.gbxOsnovni.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Službeni broj vozila:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Model:";
            // 
            // lblSluzbeniBroj
            // 
            this.lblSluzbeniBroj.AutoSize = true;
            this.lblSluzbeniBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSluzbeniBroj.Location = new System.Drawing.Point(145, 24);
            this.lblSluzbeniBroj.Name = "lblSluzbeniBroj";
            this.lblSluzbeniBroj.Size = new System.Drawing.Size(61, 17);
            this.lblSluzbeniBroj.TabIndex = 24;
            this.lblSluzbeniBroj.Text = "SL Broj";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistracija.Location = new System.Drawing.Point(103, 47);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(89, 17);
            this.lblRegistracija.TabIndex = 24;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProizvodjac.Location = new System.Drawing.Point(103, 70);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(91, 17);
            this.lblProizvodjac.TabIndex = 24;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(16, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Registracija:";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModel.Location = new System.Drawing.Point(103, 93);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(51, 17);
            this.lblModel.TabIndex = 24;
            this.lblModel.Text = "model";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Proizvođač:";
            // 
            // btnInfo
            // 
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInfo.Location = new System.Drawing.Point(278, 47);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(92, 64);
            this.btnInfo.TabIndex = 1;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnObrisi.Location = new System.Drawing.Point(416, 47);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(92, 64);
            this.btnObrisi.TabIndex = 2;
            this.btnObrisi.Text = "Obriši";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // dgPerPregledi
            // 
            this.dgPerPregledi.AllowUserToAddRows = false;
            this.dgPerPregledi.AllowUserToDeleteRows = false;
            this.dgPerPregledi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPerPregledi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPerPregledi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kolID,
            this.kolDatumPreg,
            this.kolPregledVrsio});
            this.dgPerPregledi.Location = new System.Drawing.Point(12, 150);
            this.dgPerPregledi.Name = "dgPerPregledi";
            this.dgPerPregledi.ReadOnly = true;
            this.dgPerPregledi.RowTemplate.Height = 24;
            this.dgPerPregledi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPerPregledi.Size = new System.Drawing.Size(496, 398);
            this.dgPerPregledi.TabIndex = 79;
            // 
            // kolID
            // 
            this.kolID.HeaderText = "ID";
            this.kolID.Name = "kolID";
            this.kolID.ReadOnly = true;
            this.kolID.Visible = false;
            // 
            // kolDatumPreg
            // 
            this.kolDatumPreg.HeaderText = "Datum pregleda";
            this.kolDatumPreg.Name = "kolDatumPreg";
            this.kolDatumPreg.ReadOnly = true;
            this.kolDatumPreg.Width = 120;
            // 
            // kolPregledVrsio
            // 
            this.kolPregledVrsio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.kolPregledVrsio.HeaderText = "Pregled vršio";
            this.kolPregledVrsio.MinimumWidth = 100;
            this.kolPregledVrsio.Name = "kolPregledVrsio";
            this.kolPregledVrsio.ReadOnly = true;
            // 
            // gbxOsnovni
            // 
            this.gbxOsnovni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.gbxOsnovni.Controls.Add(this.label1);
            this.gbxOsnovni.Controls.Add(this.label3);
            this.gbxOsnovni.Controls.Add(this.lblSluzbeniBroj);
            this.gbxOsnovni.Controls.Add(this.lblRegistracija);
            this.gbxOsnovni.Controls.Add(this.lblProizvodjac);
            this.gbxOsnovni.Controls.Add(this.label7);
            this.gbxOsnovni.Controls.Add(this.lblModel);
            this.gbxOsnovni.Controls.Add(this.label2);
            this.gbxOsnovni.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxOsnovni.Location = new System.Drawing.Point(12, 12);
            this.gbxOsnovni.Name = "gbxOsnovni";
            this.gbxOsnovni.Size = new System.Drawing.Size(234, 121);
            this.gbxOsnovni.TabIndex = 83;
            this.gbxOsnovni.TabStop = false;
            this.gbxOsnovni.Text = "Osnovni podaci";
            // 
            // ListaPregleda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 560);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.dgPerPregledi);
            this.Controls.Add(this.gbxOsnovni);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(538, 607);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(538, 607);
            this.Name = "ListaPregleda";
            this.Text = "Lista pregleda";
            ((System.ComponentModel.ISupportInitialize)(this.dgPerPregledi)).EndInit();
            this.gbxOsnovni.ResumeLayout(false);
            this.gbxOsnovni.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSluzbeniBroj;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.DataGridView dgPerPregledi;
        private System.Windows.Forms.GroupBox gbxOsnovni;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolID;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolDatumPreg;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolPregledVrsio;
    }
}