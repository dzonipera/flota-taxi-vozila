﻿namespace VozniPark
{
    partial class OPregledu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbVrseno = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSlBr = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblVrsio = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblKilometraza = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDatum = new System.Windows.Forms.Label();
            this.lblVrsta = new System.Windows.Forms.Label();
            this.btnPDF = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbVrseno
            // 
            this.lbVrseno.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVrseno.FormattingEnabled = true;
            this.lbVrseno.ItemHeight = 16;
            this.lbVrseno.Location = new System.Drawing.Point(27, 162);
            this.lbVrseno.Name = "lbVrseno";
            this.lbVrseno.Size = new System.Drawing.Size(470, 308);
            this.lbVrseno.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Službeni broj:";
            // 
            // lblSlBr
            // 
            this.lblSlBr.AutoSize = true;
            this.lblSlBr.Location = new System.Drawing.Point(153, 36);
            this.lblSlBr.Name = "lblSlBr";
            this.lblSlBr.Size = new System.Drawing.Size(34, 17);
            this.lblSlBr.TabIndex = 5;
            this.lblSlBr.Text = "SlBr";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(39, 111);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(46, 17);
            this.lblModel.TabIndex = 7;
            this.lblModel.Text = "model";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Location = new System.Drawing.Point(39, 87);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(80, 17);
            this.lblProizvodjac.TabIndex = 8;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Location = new System.Drawing.Point(39, 63);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(77, 17);
            this.lblRegistracija.TabIndex = 9;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblVrsio
            // 
            this.lblVrsio.AutoSize = true;
            this.lblVrsio.Location = new System.Drawing.Point(269, 63);
            this.lblVrsio.Name = "lblVrsio";
            this.lblVrsio.Size = new System.Drawing.Size(38, 17);
            this.lblVrsio.TabIndex = 10;
            this.lblVrsio.Text = "vrsio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Vršio:";
            // 
            // lblKilometraza
            // 
            this.lblKilometraza.AutoSize = true;
            this.lblKilometraza.Location = new System.Drawing.Point(311, 111);
            this.lblKilometraza.Name = "lblKilometraza";
            this.lblKilometraza.Size = new System.Drawing.Size(57, 17);
            this.lblKilometraza.TabIndex = 12;
            this.lblKilometraza.Text = "kilometr";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Kilometraža:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Datum:";
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Location = new System.Drawing.Point(278, 87);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(47, 17);
            this.lblDatum.TabIndex = 15;
            this.lblDatum.Text = "datum";
            // 
            // lblVrsta
            // 
            this.lblVrsta.AutoSize = true;
            this.lblVrsta.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVrsta.Location = new System.Drawing.Point(214, 36);
            this.lblVrsta.Name = "lblVrsta";
            this.lblVrsta.Size = new System.Drawing.Size(190, 17);
            this.lblVrsta.TabIndex = 16;
            this.lblVrsta.Text = "Interni periodični pregled";
            // 
            // btnPDF
            // 
            this.btnPDF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPDF.BackgroundImage = global::VozniPark.Properties.Resources.Adobe_PDF_Document_01;
            this.btnPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPDF.Location = new System.Drawing.Point(423, 38);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(60, 66);
            this.btnPDF.TabIndex = 17;
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // OPregledu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 491);
            this.Controls.Add(this.lbVrseno);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSlBr);
            this.Controls.Add(this.lblModel);
            this.Controls.Add(this.lblProizvodjac);
            this.Controls.Add(this.lblRegistracija);
            this.Controls.Add(this.lblVrsio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblKilometraza);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.lblVrsta);
            this.MaximumSize = new System.Drawing.Size(543, 538);
            this.MinimumSize = new System.Drawing.Size(543, 538);
            this.Name = "OPregledu";
            this.Text = "Periodični pregled - Info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbVrseno;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSlBr;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblVrsio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblKilometraza;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Label lblVrsta;
    }
}