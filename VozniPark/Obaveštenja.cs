﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class Obaveštenja : Form
    {
        List<ObavestenjaClass> _infoList = new List<ObavestenjaClass>();
        public Obaveštenja()
        {
            InitializeComponent();
            prikaziDGV();
        }

        private void prikaziDGV()
        {
            _infoList = new ObavestenjaClass().ucitajObavestenja();
            dgObavestenja.Rows.Clear();
            for (int i = 0; i < _infoList.Count; i++)
            {
                dgObavestenja.Rows.Add();
                dgObavestenja.Rows[i].Cells["ID"].Value = _infoList[i].ID;
                dgObavestenja.Rows[i].Cells["kolVrsta"].Value = _infoList[i].Vrsta;
                dgObavestenja.Rows[i].Cells["kolDatum"].Value =
                            _infoList[i].DatumIsteka.Day+"."+ _infoList[i].DatumIsteka.Month + "."+ _infoList[i].DatumIsteka.Year + ".";
                dgObavestenja.CurrentCell = null;
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgObavestenja.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li zelite da obrisete odabrano obaveštenje?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int idSelektovanog = (int)dgObavestenja.SelectedRows[0].Cells["ID"].Value;

                    ObavestenjaClass selektovano = _infoList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                    if (selektovano != null)
                    {
                        selektovano.obrisiObavestenje();
                    }
                    prikaziDGV();
                }
            }
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            if (dgObavestenja.SelectedRows.Count > 0)
            {
                int idSelektovanog = (int)dgObavestenja.SelectedRows[0].Cells["ID"].Value;
                ObavestenjaClass selektovano = _infoList.Where(x => x.ID == idSelektovanog).FirstOrDefault();

                if (selektovano != null)
                {
                    _infoList = new ObavestenjaClass().ucitajObavestenje(idSelektovanog);
                    MessageBox.Show(_infoList[0].Tekst);
                }
            }
        }
    }
}
