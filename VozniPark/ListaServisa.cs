﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class ListaServisa : Form
    {
        List<Servisi> ServList = new List<Servisi>();
        DataSet ds = new DataSet();
        int _id;
        public ListaServisa(int id, string reg, string proiz, string model)
        {
            InitializeComponent();
            _id = id;
            lblSluzbeniBroj.Text = _id.ToString();
            lblRegistracija.Text = reg;
            lblProizvodjac.Text = proiz;
            lblModel.Text = model;
            dgServisi.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            prikaziServiseDGV();
        }

        public void prikaziServiseDGV()
        {
            ServList = new Servisi().ucitajServise(Int32.Parse(lblSluzbeniBroj.Text));
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("kolRadjeno");
            dt.Columns.Add("kolTipServisa");
            dt.Columns.Add("kolDatumServ");
            dt.Columns.Add("kolKoJeRadio");
            dt.Columns.Add("kolCena");

            for (int i = 0; i < ServList.Count; i++)
            {
                string dateString = ServList[i].DatumServisa.Day + ". " + ServList[i].DatumServisa.Month + ". " + ServList[i].DatumServisa.Year + ".";
                dt.Rows.Add(ServList[i].IdServisa  , ServList[i].Radovi,          //Nastavak u novom redu
                        ServList[i].TipServisa, dateString, ServList[i].KoJeVrsio, ServList[i].CenaServisa);
            }

           // dgServisi.Rows.Clear();
            dgServisi.DataSource = dt;
            ds.Tables.Add(dt);
            dgServisi.Columns["ID"].Visible = false;
            dgServisi.Columns["kolRadjeno"].Visible = false;
            dgServisi.Columns["kolTipServisa"].HeaderText = "Tip servisa";
            dgServisi.Columns["kolDatumServ"].HeaderText = "Datum servisa";
            dgServisi.Columns["kolKoJeRadio"].HeaderText = "Ko je radio";
            dgServisi.Columns["kolCena"].HeaderText = "Cena";
            dgServisi.Columns["kolCena"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgServisi.CurrentCell = null;            
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgServisi.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Da li želite da obrišete odabrani servis?",
                "Potvrda brisanja", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    int idSelektovanog = Int32.Parse(dgServisi.SelectedRows[0].Cells["ID"].Value.ToString());
                    
                    Servisi selektovaniServis = ServList.Where(x => x.IdServisa == idSelektovanog).FirstOrDefault();

                    if (selektovaniServis != null)
                    {
                        selektovaniServis.obrisiServis();
                    }
                    prikaziServiseDGV();
                }
            }
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            DataView dv = ds.Tables[0].DefaultView;
            dv.RowFilter = string.Format("kolRadjeno LIKE '%{0}%'", txtPojam.Text);
            dgServisi.DataSource = dv;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (dgServisi.SelectedRows.Count > 0)
            {
                int _idServisa = Int32.Parse(dgServisi.SelectedRows[0].Cells["ID"].Value.ToString());
                OServisu oServ = new OServisu(_id, _idServisa);
                oServ.Show();
            }
            else { MessageBox.Show("Nema podataka ili nijedan red nije odabran!", "", MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }
    }
}
