﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    class ObavestenjaClass
    {
        private int id;
        private string vrsta;
        private string tekst;
        private DateTime datumIsteka;
        
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Vrsta
        {
            get { return vrsta; }
            set { vrsta = value; }
        }
        public string Tekst
        {
            get { return tekst; }
            set { tekst = value; }
        }
        public DateTime DatumIsteka
        {
            get { return datumIsteka; }
            set { datumIsteka = value; }
        }

        public void dodajObavestenje()
        {
            SQLiteConnection _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlADD = @"Insert into tbObavestenja (VrstaOb, TextOb, DatumOb)  Values (@VrstaOb, @TextOb, @DatumOb)";

            SQLiteCommand command = new SQLiteCommand(sqlADD, _connection);
            command.Parameters.Add(new SQLiteParameter("@VrstaOb", Vrsta));
            command.Parameters.Add(new SQLiteParameter("@TextOb", Tekst));
            command.Parameters.Add(new SQLiteParameter("@DatumOb", DatumIsteka));
            command.ExecuteNonQuery();
        }

        public void obrisiObavestenje()
        {
            string deleteSql = "DELETE FROM tbObavestenja WHERE IDObavestenja = @IDObavestenja";
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = deleteSql;
                command.Parameters.Add(new SQLiteParameter("@IDObavestenja", ID));
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<ObavestenjaClass> ucitajObavestenja()
        {
            List<ObavestenjaClass> obavestenja = new List<ObavestenjaClass>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbObavestenja;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    ObavestenjaClass _info;
                    while (reader.Read())
                    {
                        _info = new ObavestenjaClass();
                        _info.ID = Int32.Parse(reader["IDObavestenja"].ToString());
                        _info.Vrsta = reader["VrstaOb"].ToString();
                        _info.Tekst = reader["TextOb"].ToString();
                        _info.DatumIsteka = Convert.ToDateTime(reader["DatumOb"].ToString());
                        obavestenja.Add(_info);
                    }
                }
            }
            return obavestenja;
        }

        public List<ObavestenjaClass> ucitajObavestenje(int _ID)
        {
            List<ObavestenjaClass> obavestenje = new List<ObavestenjaClass>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbObavestenja WHERE IDObavestenja = @IDObavestenja;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                command.Parameters.Add(new SQLiteParameter("@IDObavestenja", _ID));
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    ObavestenjaClass _info;
                    while (reader.Read())
                    {
                        _info = new ObavestenjaClass();
                        _info.Tekst = reader["TextOb"].ToString();
                        obavestenje.Add(_info);
                    }
                }
            }
            return obavestenje;
        }

    }
}
