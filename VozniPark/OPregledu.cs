﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class OPregledu : Form
    {
        List<IntPregled> pregList = new List<IntPregled>();
        int IDVoz;
        int IDPreg;
        public OPregledu(int _id, int _idPreg)
        {
            IDVoz = _id;
            IDPreg = _idPreg;
            InitializeComponent();
            prikaziPregled();
        }

        private void prikaziPregled()
        {
            pregList = new IntPregled().ucitajPregled(IDVoz, IDPreg);
            lblSlBr.Text = IDVoz.ToString();
            lblProizvodjac.Text = pregList[0].Vozilo.Proizvodjac;
            lblModel.Text = pregList[0].Vozilo.Model;
            lblRegistracija.Text = pregList[0].Vozilo.RegistarskiBr;
            lblVrsio.Text = pregList[0].VrsioPregled;
            lblDatum.Text = pregList[0].DatumPregleda.Day + ". " + pregList[0].DatumPregleda.Month + ". " + pregList[0].DatumPregleda.Year + ".";
            lblKilometraza.Text = pregList[0].KilometrazaPregl.ToString();
            string _pomocni = pregList[0].Pregledano;
            string[] _niz = new string[80];
            int i = 0;

            foreach (char c in _pomocni)
            {
                if (c.ToString() != "#")
                {
                    _niz[i] += c;
                }
                else i++;
            }
            for (int j = 0; j < i; j++)
            {
                lbVrseno.Items.Add(_niz[j]);
            }
            lbVrseno.Items.Add("");
            lbVrseno.Items.Add("StanjeBMB: ");
            lbVrseno.Items.Add(pregList[0].StanjeBMB);
            lbVrseno.Items.Add("Stanje Tng / CNG: ");
            lbVrseno.Items.Add(pregList[0].StanjeTNGCNG);
            lbVrseno.Items.Add("");
            lbVrseno.Items.Add("Primedbe: ");
            lbVrseno.Items.Add(pregList[0].Primedbe);
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            IntPregled selektovaniPregled = pregList.Where(x => x.IdPregleda == IDPreg).FirstOrDefault();
            SaveFileDialog svg = new SaveFileDialog();
            svg.InitialDirectory = @"c:\User\Desktop\";
            svg.FileName = "Interni pregled - " + selektovaniPregled.DatumPregleda.Day + "." + selektovaniPregled.DatumPregleda.Month 
                            + "." + selektovaniPregled.DatumPregleda.Year + "." + selektovaniPregled.Vozilo.SlBr + " - " +
                            selektovaniPregled.Vozilo.RegistarskiBr;
            svg.ShowDialog();

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(svg.FileName + ".pdf", FileMode.Create));
            doc.Open();

            string ARIAL = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIALUNI.TTF");
            BaseFont bf = BaseFont.CreateFont(ARIAL, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            iTextSharp.text.Image Logo = iTextSharp.text.Image.GetInstance("Logo.jpg");
            Logo.ScalePercent(50);
            Logo.Alignment = iTextSharp.text.Image.ALIGN_RIGHT | iTextSharp.text.Image.TEXTWRAP;

            iTextSharp.text.Font fontOsnovni = new iTextSharp.text.Font(bf, 14, iTextSharp.text.Font.BOLDITALIC);
            iTextSharp.text.Font fontOstali = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL);

            string strOsnovni = string.Format("INTERNI PERIODIČNI PREGLED{0}Datum pregleda: {1}.{2}.{3}.{0}Službeni broj vozila: {4}{0}" +
                            "Registarski br.: {5}{6,50}{0}Kilometraža: {7}{8,50}", System.Environment.NewLine, selektovaniPregled.DatumPregleda.Day,
                            selektovaniPregled.DatumPregleda.Month, selektovaniPregled.DatumPregleda.Year, selektovaniPregled.Vozilo.SlBr,
                            selektovaniPregled.Vozilo.RegistarskiBr, "Vozilo: "+selektovaniPregled.Vozilo.Proizvodjac+" "+
                            selektovaniPregled.Vozilo.Model, selektovaniPregled.KilometrazaPregl, "Pregledao vozilo: "+selektovaniPregled.VrsioPregled);
            Phrase Osnovni = new Phrase(strOsnovni, fontOsnovni);
            
            PdfPTable table = new PdfPTable(1);
            table.TotalWidth = doc.PageSize.Width - 72;
            table.LockedWidth = true;

            string _pomocni = selektovaniPregled.Pregledano;
            int i = 0;
            string[] _niz = new string[5];
            foreach (char c in _pomocni) {
                if (c.ToString() != "#")
                {
                    _niz[i] += c;
                }
                else i++;  }

            for (i=0; i<_niz.Length; i++) { table.AddCell(_niz[i]); }
            string strGorivo = string.Format("Stanje goriva: {0}BMB: {1}{0}TNG / CNG: {2}", 
                            Environment.NewLine, selektovaniPregled.StanjeBMB, selektovaniPregled.StanjeTNGCNG);
            table.AddCell(strGorivo);
            string strPrimedbe = string.Format("Primedbe:{0}{1}", Environment.NewLine, selektovaniPregled.Primedbe);
            table.AddCell(strPrimedbe);

            doc.Add(Logo);
            doc.Add(Osnovni);
            doc.Add(table);
            doc.Close();
        }
    }
}
