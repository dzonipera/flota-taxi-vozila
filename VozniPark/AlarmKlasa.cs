﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark
{
    class AlarmKlasa
    {
        private int id;
        private int istekReg;
        private int saobInsp;
        private int tehnicki;
        private int taksimetar;
        private int taksiLeg;
        private int osiguranje;
        private int kasko;
        private int ppAp;
        private int parkMark;
        private DateTime istekRegDT;
        private DateTime saobInspDT;
        private DateTime tehnickiDT;
        private DateTime taksimetarDT;
        private DateTime taksiLegDT;
        private DateTime osiguranjeDT;
        private DateTime kaskoDT;
        private DateTime ppApDT;
        private DateTime parkMarkDT;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int IstekReg
        {
            get { return istekReg; }
            set { istekReg = value; }
        }
        public int SaobInsp
        {
            get { return saobInsp; }
            set { saobInsp = value; }
        }
        public int Tehnicki
        {
            get { return tehnicki; }
            set { tehnicki = value; }
        }
        public int Taksimetar
        {
            get { return taksimetar; }
            set { taksimetar = value; }
        }
        public int TaksiLeg
        {
            get { return taksiLeg; }
            set { taksiLeg = value; }
        }
        public int Osiguranje
        {
            get { return osiguranje; }
            set { osiguranje = value; }
        }
        public int Kasko
        {
            get { return kasko; }
            set { kasko = value; }
        }
        public int PpAp
        {
            get { return ppAp; }
            set { ppAp = value; }
        }
        public int ParkMark
        {
            get { return parkMark; }
            set { parkMark = value; }
        }
        public DateTime IstekRegDT
        {
            get { return istekRegDT; }
            set { istekRegDT = value; }
        }
        public DateTime SaobInspDT
        {
            get { return saobInspDT; }
            set { saobInspDT = value; }
        }
        public DateTime TehnickiDT
        {
            get { return tehnickiDT; }
            set { tehnickiDT = value; }
        }
        public DateTime TaksimetarDT
        {
            get { return taksimetarDT; }
            set { taksimetarDT = value; }
        }
        public DateTime TaksiLegDT
        {
            get { return taksiLegDT; }
            set { taksiLegDT = value; }
        }
        public DateTime OsiguranjeDT
        {
            get { return osiguranjeDT; }
            set { osiguranjeDT = value; }
        }
        public DateTime KaskoDT
        {
            get { return kaskoDT; }
            set { kaskoDT = value; }
        }
        public DateTime PpApDT
        {
            get { return ppApDT; }
            set { ppApDT = value; }
        }
        public DateTime ParkMarkDT
        {
            get { return parkMarkDT; }
            set { parkMarkDT = value; }
        }

        public void dodajAlarm()
        {
            SQLiteConnection conn = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            conn.Open();
            string sqlADD = @"insert into tbAlarmi (IdAlarma, IstekRegAl, SaobInspAl, TehnickiAl, TaksimetarAl, TaksiLegAl,
                            OsiguranjeAl, KaskoAl, PPAparatAl, ParkMarkAl, IstekRegDT, SaobInspDT, TehnickiDT, TaksimetarDT, TaksiLegDT,
                            OsiguranjeDT, KaskoDT, PPAparatDT, ParkMarkDT) VALUES (@IdAlarma, @IstekRegAl, @SaobInspAl,
                            @TehnickiAl, @TaksimetarAl, @TaksiLegAl, @OsiguranjeAl, @KaskoAl, @PPAparatAl, @ParkMarkAl, @IstekRegDT, @SaobInspDT,
                            @TehnickiDT, @TaksimetarDT, @TaksiLegDT, @OsiguranjeDT, @KaskoDT, @PPAparatDT, @ParkMarkDT)";
  
            SQLiteCommand command = new SQLiteCommand(sqlADD, conn);
            command.Parameters.Add(new SQLiteParameter("@IdAlarma", Id));
            command.Parameters.Add(new SQLiteParameter("@IstekRegAl", IstekReg));
            command.Parameters.Add(new SQLiteParameter("@SaobInspAl", SaobInsp));
            command.Parameters.Add(new SQLiteParameter("@TehnickiAl", Tehnicki));
            command.Parameters.Add(new SQLiteParameter("@TaksimetarAl", Taksimetar));
            command.Parameters.Add(new SQLiteParameter("@TaksiLegAl", TaksiLeg));
            command.Parameters.Add(new SQLiteParameter("@OsiguranjeAl", Osiguranje));
            command.Parameters.Add(new SQLiteParameter("@KaskoAl", Kasko));
            command.Parameters.Add(new SQLiteParameter("@PPAparatAl", PpAp));
            command.Parameters.Add(new SQLiteParameter("@ParkMarkAl", ParkMark));
            command.Parameters.Add(new SQLiteParameter("@IstekRegDT", IstekRegDT));
            command.Parameters.Add(new SQLiteParameter("@SaobInspDT", SaobInspDT));
            command.Parameters.Add(new SQLiteParameter("@TehnickiDT", TehnickiDT));
            command.Parameters.Add(new SQLiteParameter("@TaksimetarDT", TaksimetarDT));
            command.Parameters.Add(new SQLiteParameter("@TaksiLegDT", TaksiLegDT));
            command.Parameters.Add(new SQLiteParameter("@OsiguranjeDT", OsiguranjeDT));
            command.Parameters.Add(new SQLiteParameter("@KaskoDT", KaskoDT));
            command.Parameters.Add(new SQLiteParameter("@PPAparatDT", PpApDT));
            command.Parameters.Add(new SQLiteParameter("@ParkMarkDT", ParkMarkDT));
            command.ExecuteNonQuery();
        }

        public void azurirajAlarme()
        {
            SQLiteConnection _connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            _connection.Open();
            string sqlUpdate = @"Update tbAlarmi SET IstekRegAl=@IstekRegAl, SaobInspAl=@SaobInspAl, TehnickiAl=@TehnickiAl,
                            TaksimetarAl=@TaksimetarAl, TaksiLegAl=@TaksiLegAl, OsiguranjeAl=@OsiguranjeAl, KaskoAl=@KaskoAl,
                            PPAparatAl=@PPAparatAl, ParkMarkAl=@ParkMarkAl, IstekRegDT=@IstekRegDT, SaobInspDT=@SaobInspDT, TehnickiDT=@TehnickiDT,
                            TaksimetarDT=@TaksimetarDT, TaksiLegDT=@TaksiLegDT, OsiguranjeDT=@OsiguranjeDT, KaskoDT=@KaskoDT,
                            PPAparatDT=@PPAparatDT, ParkMarkDT=@ParkMarkDT WHERE IdAlarma=@IdAlarma";

            SQLiteCommand command = new SQLiteCommand(sqlUpdate, _connection);
            command.Parameters.Add(new SQLiteParameter("@IdAlarma", Id));
            command.Parameters.Add(new SQLiteParameter("@IstekRegAl", IstekReg));
            command.Parameters.Add(new SQLiteParameter("@SaobInspAl", SaobInsp));
            command.Parameters.Add(new SQLiteParameter("@TehnickiAl", Tehnicki));
            command.Parameters.Add(new SQLiteParameter("@TaksimetarAl", Taksimetar));
            command.Parameters.Add(new SQLiteParameter("@TaksiLegAl", TaksiLeg));
            command.Parameters.Add(new SQLiteParameter("@OsiguranjeAl", Osiguranje));
            command.Parameters.Add(new SQLiteParameter("@KaskoAl", Kasko));
            command.Parameters.Add(new SQLiteParameter("@PPAparatAl", PpAp));
            command.Parameters.Add(new SQLiteParameter("@ParkMarkAl", ParkMark));
            command.Parameters.Add(new SQLiteParameter("@IstekRegDT", IstekRegDT));
            command.Parameters.Add(new SQLiteParameter("@SaobInspDT", SaobInspDT));
            command.Parameters.Add(new SQLiteParameter("@TehnickiDT", TehnickiDT));
            command.Parameters.Add(new SQLiteParameter("@TaksimetarDT", TaksimetarDT));
            command.Parameters.Add(new SQLiteParameter("@TaksiLegDT", TaksiLegDT));
            command.Parameters.Add(new SQLiteParameter("@OsiguranjeDT", OsiguranjeDT));
            command.Parameters.Add(new SQLiteParameter("@KaskoDT", KaskoDT));
            command.Parameters.Add(new SQLiteParameter("@PPAparatDT", PpApDT));
            command.Parameters.Add(new SQLiteParameter("@ParkMarkDT", ParkMarkDT));
            command.ExecuteNonQuery();
        }

        public List<AlarmKlasa> ucitajAlarm()
        {
            List<AlarmKlasa> alarmi = new List<AlarmKlasa>();
            string conn = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            string queryString = "SELECT * FROM tbAlarmi;";
            using (SQLiteConnection connection = new SQLiteConnection(conn))
            {
                SQLiteCommand command = new SQLiteCommand(queryString, connection);
                connection.Open();
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    AlarmKlasa _alarm;
                    while (reader.Read())
                    {
                        _alarm = new AlarmKlasa();
                        _alarm.Id = Int32.Parse(reader["IdAlarma"].ToString());
                        _alarm.IstekReg = Int32.Parse(reader["IstekRegAl"].ToString());
                        _alarm.SaobInsp = Int32.Parse(reader["SaobInspAl"].ToString());
                        _alarm.Tehnicki = Int32.Parse(reader["TehnickiAl"].ToString());
                        _alarm.Taksimetar = Int32.Parse(reader["TaksimetarAl"].ToString());
                        _alarm.TaksiLeg = Int32.Parse(reader["TaksiLegAl"].ToString());
                        _alarm.Osiguranje = Int32.Parse(reader["OsiguranjeAl"].ToString());
                        _alarm.Kasko = Int32.Parse(reader["KaskoAl"].ToString());
                        _alarm.PpAp = Int32.Parse(reader["PPAparatAl"].ToString());
                        _alarm.ParkMark = Int32.Parse(reader["ParkMarkAl"].ToString());
                        _alarm.IstekRegDT = Convert.ToDateTime(reader["IstekRegDT"]);
                        _alarm.SaobInspDT = Convert.ToDateTime(reader["SaobInspDT"]);
                        _alarm.TehnickiDT = Convert.ToDateTime(reader["TehnickiDT"]);
                        _alarm.TaksimetarDT = Convert.ToDateTime(reader["TaksimetarDT"]);
                        _alarm.TaksiLegDT = Convert.ToDateTime(reader["TaksiLegDT"]);
                        _alarm.OsiguranjeDT = Convert.ToDateTime(reader["OsiguranjeDT"]);
                        _alarm.KaskoDT = Convert.ToDateTime(reader["KaskoDT"]);
                        _alarm.PpApDT = Convert.ToDateTime(reader["PPAparatDT"]);
                        _alarm.ParkMarkDT = Convert.ToDateTime(reader["ParkMarkDT"]);
                        alarmi.Add(_alarm);
                    }
                }
            }
            return alarmi;
        }

        
    }
}
