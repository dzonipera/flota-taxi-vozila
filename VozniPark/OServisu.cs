﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf.draw;

namespace VozniPark
{
    public partial class OServisu : Form
    {
        List<Servisi> servList = new List<Servisi>();
        int IDVoz;
        int IDServ;
        public OServisu(int _id, int _idServ)
        {
            IDVoz = _id;
            IDServ = _idServ;
            InitializeComponent();
            servList = new Servisi().ucitajServis(IDVoz, IDServ);
            prikaziServ();
        }

        private void prikaziServ()
        {
            lblSlBr.Text = IDVoz.ToString();
            lblProizvodjac.Text = servList[0].Vozilo.Proizvodjac;
            lblModel.Text = servList[0].Vozilo.Model;
            lblRegistracija.Text = servList[0].Vozilo.RegistarskiBr;
            lblVrsta.Text = servList[0].TipServisa;
            lblVrsio.Text = servList[0].KoJeVrsio;
            lblDatum.Text = servList[0].DatumServisa.Day + ". " + servList[0].DatumServisa.Month + ". " + servList[0].DatumServisa.Year + ".";
            lblKilometraza.Text = servList[0].KilometrazaServ.ToString();
            lblCena.Text = "Cena: "+ servList[0].CenaServisa;
            string _pomocni = servList[0].Radovi;
            string[] _niz = new string[100];
            int i=0;

            foreach (char c in _pomocni)
            {
                if (c.ToString() != "#")
                {
                    _niz[i] += c;
                }
                else i++;                
            }
            
            for (int j=0; j<i; j++)
            {
                lbVrseno.Items.Add(_niz[j]);
            }            
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            Servisi selektovaniServis = servList.Where(x => x.IdServisa == IDServ).FirstOrDefault();
            SaveFileDialog svg = new SaveFileDialog();
            svg.InitialDirectory = @"c:\User\Desktop\";
            svg.FileName = "" + selektovaniServis.TipServisa + " - " +selektovaniServis.DatumServisa.Day+ "." + selektovaniServis.DatumServisa.Month 
                            + "." + selektovaniServis.DatumServisa.Year + ". - " + selektovaniServis.Vozilo.SlBr + " - " +
                            selektovaniServis.Vozilo.RegistarskiBr;
            svg.ShowDialog();

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(svg.FileName + ".pdf", FileMode.Create));
            doc.Open();

            string TNR = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "TIMES.TTF");
            BaseFont bfOsnovni = BaseFont.CreateFont(TNR, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            string ARIAL = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIALUNI.TTF");
            BaseFont bfOstali = BaseFont.CreateFont(ARIAL, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            iTextSharp.text.Image Logo = iTextSharp.text.Image.GetInstance("Logo.jpg");
            Logo.ScalePercent(50);
            Logo.Alignment = iTextSharp.text.Image.ALIGN_RIGHT | iTextSharp.text.Image.TEXTWRAP;

            iTextSharp.text.Font fontOsnovni = new iTextSharp.text.Font(bfOsnovni, 14, iTextSharp.text.Font.BOLDITALIC);
            iTextSharp.text.Font fontOstali = new iTextSharp.text.Font(bfOstali, 12, iTextSharp.text.Font.NORMAL);

            string strOsnovni = string.Format("OSNOVNI PODACI O SERVISU{0}Tip servisa: {1}{0}Datum servisa: {2}.{3}.{4}.",
                            System.Environment.NewLine, selektovaniServis.TipServisa, selektovaniServis.DatumServisa.Day, selektovaniServis.DatumServisa.Month,
                            selektovaniServis.DatumServisa.Year);
            Phrase Osnovni = new Phrase(strOsnovni, fontOsnovni);

            PdfPTable tabOsnovne = new PdfPTable(2);
            tabOsnovne.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            tabOsnovne.TotalWidth = doc.PageSize.Width - 72;
            tabOsnovne.LockedWidth = true;
            tabOsnovne.AddCell(new Phrase(string.Format("Službeni broj vozila: {1}{0}Registarski br.: {2}{0}Cena servisa: {3}", System.Environment.NewLine,
                            selektovaniServis.Vozilo.SlBr, selektovaniServis.Vozilo.RegistarskiBr, selektovaniServis.CenaServisa),fontOsnovni));
            tabOsnovne.AddCell(new Phrase(string.Format("Vozilo: {1} {2}{0}Kilometraža: {3}{0}Servis vršio: {4}{0}", System.Environment.NewLine,
                            selektovaniServis.Vozilo.Proizvodjac, selektovaniServis.Vozilo.Model, selektovaniServis.KilometrazaServ,
                            selektovaniServis.KoJeVrsio),fontOsnovni));                                    

            string _pomocni = selektovaniServis.Radovi;
            string[] _niz = new string[100];
            int i = 0;
            int _kol;

            foreach (char c in _pomocni)
            {
                if (c.ToString() != "#")
                {
                    _niz[i] += c;
                }
                else i++;
            }

            string strOstali = "";
            if (i <= 20) _kol = 1;
            else if (i <= 40) { _kol = 2; }
            else _kol = 3;
            
            PdfPTable table = new PdfPTable(_kol);
            table.TotalWidth = doc.PageSize.Width - 72;
            table.LockedWidth = true;

            int m = 0;
            for (int k=0; k<_kol; k++)
            {
                strOstali = "";
                for (int j = 0; j <=i/_kol; j++)
                {
                    strOstali += _niz[m] + Environment.NewLine + Environment.NewLine;
                    m++;
                }
                Phrase Ostali = new Phrase(strOstali, fontOstali);
                table.AddCell(Ostali);
            }
                        
            doc.Add(Logo);
            doc.Add(Osnovni);
            doc.Add(tabOsnovne);
            doc.Add(table);
            doc.Close();

        }
    }
}
