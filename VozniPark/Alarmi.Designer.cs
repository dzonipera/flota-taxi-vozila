﻿namespace VozniPark
{
    partial class Alarmi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numRegIstice = new System.Windows.Forms.NumericUpDown();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numSaobInsp = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numTehnicki = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numTaksimetar = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numTaksiLeg = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numOsiguranje = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numKasko = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numPPAparat = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.numParkMark = new System.Windows.Forms.NumericUpDown();
            this.dtIstekReg = new System.Windows.Forms.DateTimePicker();
            this.dtSaobInsp = new System.Windows.Forms.DateTimePicker();
            this.dtTehnicki = new System.Windows.Forms.DateTimePicker();
            this.dtTaksimetar = new System.Windows.Forms.DateTimePicker();
            this.dtTaksiLeg = new System.Windows.Forms.DateTimePicker();
            this.dtOsiguranje = new System.Windows.Forms.DateTimePicker();
            this.dtKasko = new System.Windows.Forms.DateTimePicker();
            this.dtPPAparat = new System.Windows.Forms.DateTimePicker();
            this.dtParkMark = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numRegIstice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaobInsp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTehnicki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTaksimetar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTaksiLeg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOsiguranje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numKasko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPAparat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParkMark)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Istek registracije";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.Location = new System.Drawing.Point(256, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "Koliko dana ranije";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // numRegIstice
            // 
            this.numRegIstice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numRegIstice.Location = new System.Drawing.Point(283, 89);
            this.numRegIstice.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numRegIstice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRegIstice.Name = "numRegIstice";
            this.numRegIstice.Size = new System.Drawing.Size(64, 22);
            this.numRegIstice.TabIndex = 1;
            this.numRegIstice.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnSnimi
            // 
            this.btnSnimi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSnimi.Location = new System.Drawing.Point(131, 370);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(95, 59);
            this.btnSnimi.TabIndex = 2;
            this.btnSnimi.Text = "Sačuvaj";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnZatvori.Location = new System.Drawing.Point(279, 370);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(95, 59);
            this.btnZatvori.TabIndex = 2;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Pregled saobraćajnog inspektora";
            // 
            // numSaobInsp
            // 
            this.numSaobInsp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numSaobInsp.Location = new System.Drawing.Point(283, 117);
            this.numSaobInsp.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numSaobInsp.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSaobInsp.Name = "numSaobInsp";
            this.numSaobInsp.Size = new System.Drawing.Size(64, 22);
            this.numSaobInsp.TabIndex = 1;
            this.numSaobInsp.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(110, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tehnički pregled vozila";
            // 
            // numTehnicki
            // 
            this.numTehnicki.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numTehnicki.Location = new System.Drawing.Point(283, 145);
            this.numTehnicki.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numTehnicki.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTehnicki.Name = "numTehnicki";
            this.numTehnicki.Size = new System.Drawing.Size(64, 22);
            this.numTehnicki.TabIndex = 1;
            this.numTehnicki.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(137, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Pregled taksimetra";
            // 
            // numTaksimetar
            // 
            this.numTaksimetar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numTaksimetar.Location = new System.Drawing.Point(283, 173);
            this.numTaksimetar.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numTaksimetar.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTaksimetar.Name = "numTaksimetar";
            this.numTaksimetar.Size = new System.Drawing.Size(64, 22);
            this.numTaksimetar.TabIndex = 1;
            this.numTaksimetar.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(99, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Obnova taksi legitimacije";
            // 
            // numTaksiLeg
            // 
            this.numTaksiLeg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numTaksiLeg.Location = new System.Drawing.Point(283, 201);
            this.numTaksiLeg.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numTaksiLeg.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTaksiLeg.Name = "numTaksiLeg";
            this.numTaksiLeg.Size = new System.Drawing.Size(64, 22);
            this.numTaksiLeg.TabIndex = 1;
            this.numTaksiLeg.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(133, 231);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Osiguranje AP i OP";
            // 
            // numOsiguranje
            // 
            this.numOsiguranje.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numOsiguranje.Location = new System.Drawing.Point(283, 229);
            this.numOsiguranje.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numOsiguranje.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numOsiguranje.Name = "numOsiguranje";
            this.numOsiguranje.Size = new System.Drawing.Size(64, 22);
            this.numOsiguranje.TabIndex = 1;
            this.numOsiguranje.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(146, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Kasko osiguranje";
            // 
            // numKasko
            // 
            this.numKasko.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numKasko.Location = new System.Drawing.Point(283, 257);
            this.numKasko.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numKasko.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numKasko.Name = "numKasko";
            this.numKasko.Size = new System.Drawing.Size(64, 22);
            this.numKasko.TabIndex = 1;
            this.numKasko.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(139, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "provera PP aparat";
            // 
            // numPPAparat
            // 
            this.numPPAparat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numPPAparat.Location = new System.Drawing.Point(283, 285);
            this.numPPAparat.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numPPAparat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPPAparat.Name = "numPPAparat";
            this.numPPAparat.Size = new System.Drawing.Size(64, 22);
            this.numPPAparat.TabIndex = 1;
            this.numPPAparat.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(109, 315);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Obnova parkin markice";
            // 
            // numParkMark
            // 
            this.numParkMark.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numParkMark.Location = new System.Drawing.Point(283, 313);
            this.numParkMark.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numParkMark.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParkMark.Name = "numParkMark";
            this.numParkMark.Size = new System.Drawing.Size(64, 22);
            this.numParkMark.TabIndex = 1;
            this.numParkMark.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // dtIstekReg
            // 
            this.dtIstekReg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtIstekReg.CustomFormat = "HH:mm";
            this.dtIstekReg.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtIstekReg.Location = new System.Drawing.Point(377, 89);
            this.dtIstekReg.Name = "dtIstekReg";
            this.dtIstekReg.ShowUpDown = true;
            this.dtIstekReg.Size = new System.Drawing.Size(64, 22);
            this.dtIstekReg.TabIndex = 3;
            this.dtIstekReg.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtSaobInsp
            // 
            this.dtSaobInsp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtSaobInsp.CustomFormat = "HH:mm";
            this.dtSaobInsp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSaobInsp.Location = new System.Drawing.Point(377, 117);
            this.dtSaobInsp.Name = "dtSaobInsp";
            this.dtSaobInsp.ShowUpDown = true;
            this.dtSaobInsp.Size = new System.Drawing.Size(64, 22);
            this.dtSaobInsp.TabIndex = 3;
            this.dtSaobInsp.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtTehnicki
            // 
            this.dtTehnicki.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtTehnicki.CustomFormat = "HH:mm";
            this.dtTehnicki.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTehnicki.Location = new System.Drawing.Point(377, 145);
            this.dtTehnicki.Name = "dtTehnicki";
            this.dtTehnicki.ShowUpDown = true;
            this.dtTehnicki.Size = new System.Drawing.Size(64, 22);
            this.dtTehnicki.TabIndex = 3;
            this.dtTehnicki.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtTaksimetar
            // 
            this.dtTaksimetar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtTaksimetar.CustomFormat = "HH:mm";
            this.dtTaksimetar.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTaksimetar.Location = new System.Drawing.Point(377, 173);
            this.dtTaksimetar.Name = "dtTaksimetar";
            this.dtTaksimetar.ShowUpDown = true;
            this.dtTaksimetar.Size = new System.Drawing.Size(64, 22);
            this.dtTaksimetar.TabIndex = 3;
            this.dtTaksimetar.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtTaksiLeg
            // 
            this.dtTaksiLeg.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtTaksiLeg.CustomFormat = "HH:mm";
            this.dtTaksiLeg.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTaksiLeg.Location = new System.Drawing.Point(377, 201);
            this.dtTaksiLeg.Name = "dtTaksiLeg";
            this.dtTaksiLeg.ShowUpDown = true;
            this.dtTaksiLeg.Size = new System.Drawing.Size(64, 22);
            this.dtTaksiLeg.TabIndex = 3;
            this.dtTaksiLeg.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtOsiguranje
            // 
            this.dtOsiguranje.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtOsiguranje.CustomFormat = "HH:mm";
            this.dtOsiguranje.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtOsiguranje.Location = new System.Drawing.Point(377, 229);
            this.dtOsiguranje.Name = "dtOsiguranje";
            this.dtOsiguranje.ShowUpDown = true;
            this.dtOsiguranje.Size = new System.Drawing.Size(64, 22);
            this.dtOsiguranje.TabIndex = 3;
            this.dtOsiguranje.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtKasko
            // 
            this.dtKasko.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtKasko.CustomFormat = "HH:mm";
            this.dtKasko.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtKasko.Location = new System.Drawing.Point(377, 257);
            this.dtKasko.Name = "dtKasko";
            this.dtKasko.ShowUpDown = true;
            this.dtKasko.Size = new System.Drawing.Size(64, 22);
            this.dtKasko.TabIndex = 3;
            this.dtKasko.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtPPAparat
            // 
            this.dtPPAparat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtPPAparat.CustomFormat = "HH:mm";
            this.dtPPAparat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPPAparat.Location = new System.Drawing.Point(377, 285);
            this.dtPPAparat.Name = "dtPPAparat";
            this.dtPPAparat.ShowUpDown = true;
            this.dtPPAparat.Size = new System.Drawing.Size(64, 22);
            this.dtPPAparat.TabIndex = 3;
            this.dtPPAparat.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // dtParkMark
            // 
            this.dtParkMark.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtParkMark.CustomFormat = "HH:mm";
            this.dtParkMark.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtParkMark.Location = new System.Drawing.Point(377, 313);
            this.dtParkMark.Name = "dtParkMark";
            this.dtParkMark.ShowUpDown = true;
            this.dtParkMark.Size = new System.Drawing.Size(64, 22);
            this.dtParkMark.TabIndex = 3;
            this.dtParkMark.Value = new System.DateTime(2016, 8, 11, 9, 0, 0, 0);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.Location = new System.Drawing.Point(374, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 39);
            this.label11.TabIndex = 0;
            this.label11.Text = "Vreme podsetnika";
            this.label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // Alarmi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(505, 480);
            this.ControlBox = false;
            this.Controls.Add(this.dtParkMark);
            this.Controls.Add(this.dtPPAparat);
            this.Controls.Add(this.dtKasko);
            this.Controls.Add(this.dtOsiguranje);
            this.Controls.Add(this.dtTaksiLeg);
            this.Controls.Add(this.dtTaksimetar);
            this.Controls.Add(this.dtTehnicki);
            this.Controls.Add(this.dtSaobInsp);
            this.Controls.Add(this.dtIstekReg);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.numParkMark);
            this.Controls.Add(this.numPPAparat);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numKasko);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numOsiguranje);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numTaksiLeg);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numTaksimetar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numTehnicki);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numSaobInsp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numRegIstice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(523, 527);
            this.Name = "Alarmi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Podešavanje podsetnika";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.numRegIstice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaobInsp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTehnicki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTaksimetar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTaksiLeg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOsiguranje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numKasko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPPAparat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParkMark)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numRegIstice;
        private System.Windows.Forms.Button btnSnimi;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numSaobInsp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numTehnicki;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numTaksimetar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numTaksiLeg;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numOsiguranje;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numKasko;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numPPAparat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numParkMark;
        private System.Windows.Forms.DateTimePicker dtIstekReg;
        private System.Windows.Forms.DateTimePicker dtSaobInsp;
        private System.Windows.Forms.DateTimePicker dtTehnicki;
        private System.Windows.Forms.DateTimePicker dtTaksimetar;
        private System.Windows.Forms.DateTimePicker dtTaksiLeg;
        private System.Windows.Forms.DateTimePicker dtOsiguranje;
        private System.Windows.Forms.DateTimePicker dtKasko;
        private System.Windows.Forms.DateTimePicker dtPPAparat;
        private System.Windows.Forms.DateTimePicker dtParkMark;
        private System.Windows.Forms.Label label11;
    }
}