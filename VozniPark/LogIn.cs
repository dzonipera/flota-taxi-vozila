﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class LogIn : Form
    {
        private Icon ico;
        List<Vozila> vozilaList = new List<Vozila>();
        List<AlarmKlasa> AlarmList = new List<AlarmKlasa>();

        private int da, ma, ga;
        string IstekRegSpisak;
        string SaobInspSpisak;
        string TehnickiSpisak;
        string TaksimetarSpisak;
        string TaksiLegSpisak;
        string OsiguranjeSpisak;
        string KaskoSpisak;
        string PPApSpisak;
        string ParkMarkSpisak;
        bool IstekRegPrikaz = false;
        bool SaobInspPrikaz = false;
        bool TehnickiPrikaz = false;
        bool TaksimetarPrikaz = false;
        bool TaksiLegPrikaz = false;
        bool OsiguranjePrikaz = false;
        bool KaskoPrikaz = false;
        bool PPApPrikaz = false;
        bool ParkMarkPrikaz = false;
        int jIR;
        int jSI;
        int jTEH;
        int jTM;
        int jTL;
        int jOS;
        int jKO;
        int jPP;
        int jPM;

        public DateTime timeLogin;

        public LogIn()
        {
            InitializeComponent();
            txtPass.UseSystemPasswordChar = true;
            txtUser.Clear();
            txtPass.Clear();
            this.AcceptButton = btnLogin;
            ispitajAlarme();
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string strUserName;
            SQLiteConnection connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            SQLiteDataAdapter sdaUName = new SQLiteDataAdapter("Select UserName from tbLogin where UserName= '" 
                                            + txtUser.Text + "' and Password= '" + txtPass.Text + "' ", connection);
            DataTable dtUName = new DataTable();
            sdaUName.Fill(dtUName);

            if (dtUName.Rows.Count == 1)
            {
                strUserName = dtUName.Rows[0][0].ToString();
                txtUser.Clear();
                txtPass.Clear();
                txtUser.Focus();
                this.Hide();
                timeLogin = DateTime.Now;
                Naslovna naslovna = new Naslovna(strUserName, this, timeLogin);
                naslovna.Show();
            }
            else
            {
                MessageBox.Show("Pogrešno korisničko ime ili lozinka!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUser.Clear();
                txtPass.Clear();
                txtUser.Focus();
            }
        }
        
        private void tmIstReg_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].IstekRegDT.Hour)
            {
                if (DateTime.Now.Minute >= AlarmList[0].IstekRegDT.Minute)
                {
                    tmIstReg.Stop();
                    MessageBox.Show("Vozila kojima ističe registracija za " + AlarmList[0].IstekReg + " dana:\n" + IstekRegSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    IstekRegPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Istek registracije";
                    info.Tekst = string.Format("Vozila kojima ističe registracija {0:dd.MM.yyyy.}\n {1};", vozilaList[jIR].RegistrIstice, IstekRegSpisak);
                    info.DatumIsteka = vozilaList[jIR].RegistrIstice;
                    info.dodajObavestenje();
                }
            }
        }
        private void tmSaobInsp_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].SaobInspDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].SaobInspDT.Minute)
                {
                    tmSaobInsp.Stop();
                    MessageBox.Show("Vozila kojima treba pregled Saobraćajnog inspektora za " + AlarmList[0].SaobInsp + " dana:\n" + SaobInspSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SaobInspPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Pregled saob. inspektora";
                    info.Tekst = string.Format("Vozila kojima trreba pregled Saobraćajnog inspektora {0:dd.MM.yyyy.}\n {1};", vozilaList[jSI].SaobrInspektor, SaobInspSpisak);
                    info.DatumIsteka = vozilaList[jSI].SaobrInspektor;
                    info.dodajObavestenje();
                }
        }
        private void tmTehnicki_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].TehnickiDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].TehnickiDT.Minute)
                {
                    tmTehnicki.Stop();
                    MessageBox.Show("Vozila koja trebaju na tehnički pregled za " + AlarmList[0].Tehnicki + " dana:\n" + TehnickiSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TehnickiPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Tehnički pregled";
                    info.Tekst = string.Format("Vozila koja trebaju na tehnički pregled {0:dd.MM.yyyy.}\n {1};", vozilaList[jTEH].TehnickiPregled, TehnickiSpisak);
                    info.DatumIsteka = vozilaList[jTEH].TehnickiPregled;
                    info.dodajObavestenje();
                }
        }
        private void tmTaksimetar_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].TaksimetarDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].TaksimetarDT.Minute)
                {
                    tmTaksimetar.Stop();
                    MessageBox.Show("Vozila koja trebaju na pregled taksimetra za " + AlarmList[0].Taksimetar + " dana:\n" + TaksimetarSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TaksimetarPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Pregled taksimetra";
                    info.Tekst = string.Format("Vozila koja trebaju na pregled taksimetra {0:dd.MM.yyyy.}\n {1};", vozilaList[jTM].PregledTaksimetra, TaksimetarSpisak);
                    info.DatumIsteka = vozilaList[jTM].PregledTaksimetra;
                    info.dodajObavestenje();
                }
        }
        private void tmTaksiLeg_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].TaksiLegDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].TaksiLegDT.Minute)
                {
                    tmTaksiLeg.Stop();
                    MessageBox.Show("Vozila kojima treba obnova taksi legitimacije za " + AlarmList[0].TaksiLeg + " dana:\n" + TaksiLegSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TaksiLegPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Taksi legitimacija";
                    info.Tekst = string.Format("Vozila kojima ističe registracija {0:dd.MM.yyyy.}\n {1};", vozilaList[jTL].TaksiLegitimacija, TaksiLegSpisak);
                    info.DatumIsteka = vozilaList[jTL].TaksiLegitimacija;
                    info.dodajObavestenje();
                }
        }
        private void tmOsiguranje_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].OsiguranjeDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].OsiguranjeDT.Minute)
                {
                    tmOsiguranje.Stop();
                    MessageBox.Show("Vozila kojima ističe osiguranje AO i OP za " + AlarmList[0].Osiguranje + " dana:\n" + OsiguranjeSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OsiguranjePrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Osiguranje AO i OP";
                    info.Tekst = string.Format("Vozila kojima ističe osiguranje AO i OP {0:dd.MM.yyyy.}\n {1};", vozilaList[jOS].OsiguranjeAOOP, OsiguranjeSpisak);
                    info.DatumIsteka = vozilaList[jOS].OsiguranjeAOOP;
                    info.dodajObavestenje();
                }
        }
        private void tmKasko_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].KaskoDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].KaskoDT.Minute)
                {
                    tmKasko.Stop();
                    MessageBox.Show("Vozila kojima ističe kasko osiguranje za " + AlarmList[0].Kasko + " dana:\n" + KaskoSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    KaskoPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Kasko osiguranje";
                    info.Tekst = string.Format("Vozila kojima ističe kasko osiguranje {0:dd.MM.yyyy.}\n {1};", vozilaList[jKO].Kasko, KaskoSpisak);
                    info.DatumIsteka = vozilaList[jKO].Kasko;
                    info.dodajObavestenje();
                }
        }
        private void tmPPAparat_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].PpApDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].PpApDT.Minute)
                {
                    tmPPAparat.Stop();
                    MessageBox.Show("Vozila kojima treba pregled PP aparata za " + AlarmList[0].Kasko + " dana:\n" + PPApSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    PPApPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "PP Aparat";
                    info.Tekst = string.Format("Vozila kojima treba pregled PP aparata {0:dd.MM.yyyy.}\n {1};", vozilaList[jPP].PPAparat, PPApSpisak);
                    info.DatumIsteka = vozilaList[jPP].PPAparat;
                    info.dodajObavestenje();
                }
        }
        private void tmParkMark_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour >= AlarmList[0].ParkMarkDT.Hour)
                if (DateTime.Now.Minute >= AlarmList[0].ParkMarkDT.Minute)
                {
                    tmParkMark.Stop();
                    MessageBox.Show("Vozila kojima treba obnova parking markice za " + AlarmList[0].ParkMark + " dana:\n" + ParkMarkSpisak + ";", "",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ParkMarkPrikaz = true;

                    ObavestenjaClass info = new ObavestenjaClass();
                    info.Vrsta = "Parking markica";
                    info.Tekst = string.Format("Vozila kojima treba obnova parking markice {0:dd.MM.yyyy.}\n {1};", vozilaList[jPM].ParkMarkice, ParkMarkSpisak);
                    info.DatumIsteka = vozilaList[jPM].ParkMarkice;
                    info.dodajObavestenje();
                }
        }

        private void chkPrikaziLozinku_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPrikaziLozinku.Checked == true)
                txtPass.UseSystemPasswordChar = false;
            else txtPass.UseSystemPasswordChar = true;
        }
        
        private void otvoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            ico = notifyIcon1.Icon;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void zatvoriProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li želite zatvorite program \"Vozni Park\"?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void datumAlarma(int f, int d, int m, int g)
        {
            if (d - f > 0)
            {
                da = d - f;
                ma = m;
                ga = g;
            }
            else if (m - 1 > 0)
            {
                ma = m - 1; ga = g;
                switch (ma)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                        da = 31 + d - f;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        da = 30 + d - f;
                        break;
                    case 2:
                        if (g % 4 == 0) da = 29 + d - f;
                        else da = 28 + d - f;
                        break;
                }
            }
            else
            {
                da = 31 + d - f; ma = 12; ga = g - 1;
            }
            //return 
        }

        public void ispitajAlarme()
        {
            IstekRegSpisak = "";
            SaobInspSpisak = "";
            TehnickiSpisak = "";
            TaksimetarSpisak = "";
            TaksiLegSpisak = "";
            OsiguranjeSpisak = "";
            KaskoSpisak = "";
            PPApSpisak = "";
            ParkMarkSpisak = "";
            vozilaList = new Vozila().ucitajVozila();
            AlarmList = new AlarmKlasa().ucitajAlarm();

            if (AlarmList.Count > 0)
            {
                for (int i = 0; i < vozilaList.Count; i++)
                {
                    //Ispitivanje datuma alarma za registraciju
                    if (IstekRegPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].IstekReg, vozilaList[i].RegistrIstice.Day, vozilaList[i].RegistrIstice.Month, vozilaList[i].RegistrIstice.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (IstekRegSpisak == "")
                            {
                                IstekRegSpisak += vozilaList[i].SlBr;
                                jIR = i;
                            }
                            else IstekRegSpisak += ", " + vozilaList[i].SlBr;
                            tmIstReg.Start();
                        }
                    }

                    //Ispitivanje datuma alarma za pregled saobraćajnog inspektora
                    if (SaobInspPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].SaobInsp,
                                            vozilaList[i].SaobrInspektor.Day, vozilaList[i].SaobrInspektor.Month, vozilaList[i].SaobrInspektor.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (SaobInspSpisak == "")
                            {
                                SaobInspSpisak += vozilaList[i].SlBr;
                                jSI = i;
                            }
                            else SaobInspSpisak += ", " + vozilaList[i].SlBr;
                            tmSaobInsp.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za tehnički pregled
                    if (TehnickiPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].Tehnicki,
                                            vozilaList[i].TehnickiPregled.Day, vozilaList[i].TehnickiPregled.Month, vozilaList[i].TehnickiPregled.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (TehnickiSpisak == "")
                            {
                                TehnickiSpisak += vozilaList[i].SlBr;
                                jTEH = i;
                            }
                            else TehnickiSpisak += ", " + vozilaList[i].SlBr;
                            tmTehnicki.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za pregled taksimetra
                    if (TaksimetarPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].Taksimetar,
                                            vozilaList[i].PregledTaksimetra.Day, vozilaList[i].PregledTaksimetra.Month, vozilaList[i].PregledTaksimetra.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (TaksimetarSpisak == "")
                            {
                                TaksimetarSpisak += vozilaList[i].SlBr;
                                jTM = i;
                            }
                            else TaksimetarSpisak += ", " + vozilaList[i].SlBr;
                            tmTaksimetar.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za obnovu taksi legitimacije
                    if (TaksiLegPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].TaksiLeg,
                                            vozilaList[i].TaksiLegitimacija.Day, vozilaList[i].TaksiLegitimacija.Month, vozilaList[i].TaksiLegitimacija.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (TaksiLegSpisak == "")
                            {
                                TaksiLegSpisak += vozilaList[i].SlBr;
                                jTL = i;
                            }
                            else TaksiLegSpisak += ", " + vozilaList[i].SlBr;
                            tmTaksiLeg.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za isteko siguranja AO i OP
                    if (OsiguranjePrikaz == false)
                    {
                        datumAlarma(AlarmList[0].Osiguranje,
                                            vozilaList[i].OsiguranjeAOOP.Day, vozilaList[i].OsiguranjeAOOP.Month, vozilaList[i].OsiguranjeAOOP.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (OsiguranjeSpisak == "")
                            {
                                OsiguranjeSpisak += vozilaList[i].SlBr;
                                jOS = i;
                            }
                            else OsiguranjeSpisak += ", " + vozilaList[i].SlBr;
                            tmOsiguranje.Start();
                        }
                    }
                    //Ispitivanje datuma alarma za istek kasko osiguranja
                    if (KaskoPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].Kasko,
                                            vozilaList[i].Kasko.Day, vozilaList[i].Kasko.Month, vozilaList[i].Kasko.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (KaskoSpisak == "")
                            {
                                KaskoSpisak += vozilaList[i].SlBr;
                                jKO = i;
                            }
                            else KaskoSpisak += ", " + vozilaList[i].SlBr;
                            tmKasko.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za pregled PP aparata
                    if (PPApPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].PpAp,
                                            vozilaList[i].PPAparat.Day, vozilaList[i].PPAparat.Month, vozilaList[i].PPAparat.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (PPApSpisak == "")
                            {
                                PPApSpisak += vozilaList[i].SlBr;
                                jPP = i;
                            }
                            else PPApSpisak += ", " + vozilaList[i].SlBr;
                            tmPPAparat.Start();
                        }
                    }
                    
                    //Ispitivanje datuma alarma za obnovu parking markica
                    if (ParkMarkPrikaz == false)
                    {
                        datumAlarma(AlarmList[0].ParkMark,
                                            vozilaList[i].ParkMarkice.Day, vozilaList[i].ParkMarkice.Month, vozilaList[i].ParkMarkice.Year);
                        if (DateTime.Today.Day == da && DateTime.Today.Month == ma && DateTime.Today.Year == ga)
                        {
                            if (ParkMarkSpisak == "")
                            {
                                ParkMarkSpisak += vozilaList[i].SlBr;
                                jPM = i;
                            }
                            else ParkMarkSpisak += ", " + vozilaList[i].SlBr;
                            tmParkMark.Start();
                        }
                    }
                }
            }
        }
    }
}
