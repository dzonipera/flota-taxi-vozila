﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VozniPark
{
    public partial class Alarmi : Form
    {
        List<AlarmKlasa> alarmLista = new List<AlarmKlasa>();
        private Naslovna _nas;

        public Alarmi(Naslovna naslovna)
        {
            alarmLista = new AlarmKlasa().ucitajAlarm();
            _nas = naslovna;
            InitializeComponent();
            if (alarmLista.Count != 0)
            {
                numRegIstice.Value = alarmLista[0].IstekReg;
                numSaobInsp.Value = alarmLista[0].SaobInsp;
                numTehnicki.Value = alarmLista[0].Tehnicki;
                numTaksimetar.Value = alarmLista[0].Taksimetar;
                numTaksiLeg.Value = alarmLista[0].TaksiLeg;
                numOsiguranje.Value = alarmLista[0].Osiguranje;
                numKasko.Value = alarmLista[0].Kasko;
                numPPAparat.Value = alarmLista[0].PpAp;
                numParkMark.Value = alarmLista[0].ParkMark;
                dtIstekReg.Value = alarmLista[0].IstekRegDT;
                dtSaobInsp.Value = alarmLista[0].SaobInspDT;
                dtTehnicki.Value = alarmLista[0].TehnickiDT;
                dtTaksimetar.Value = alarmLista[0].TaksimetarDT;
                dtTaksiLeg.Value = alarmLista[0].TaksiLegDT;
                dtOsiguranje.Value = alarmLista[0].OsiguranjeDT;
                dtKasko.Value = alarmLista[0].KaskoDT;
                dtPPAparat.Value = alarmLista[0].PpApDT;
                dtParkMark.Value = alarmLista[0].ParkMarkDT;
            }
        }
        
        private void btnSnimi_Click(object sender, EventArgs e)
        {
            alarmLista = new AlarmKlasa().ucitajAlarm();
            if (alarmLista.Count == 0)
            {
                if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    AlarmKlasa _alarmi = new AlarmKlasa();
                    _alarmi.Id = 1;
                    _alarmi.IstekReg = Int32.Parse(numRegIstice.Value.ToString());
                    _alarmi.SaobInsp = Int32.Parse(numSaobInsp.Value.ToString());
                    _alarmi.Tehnicki = Int32.Parse(numTehnicki.Value.ToString());
                    _alarmi.Taksimetar = Int32.Parse(numTaksimetar.Value.ToString());
                    _alarmi.TaksiLeg = Int32.Parse(numTaksiLeg.Value.ToString());
                    _alarmi.Osiguranje = Int32.Parse(numOsiguranje.Value.ToString());
                    _alarmi.Kasko = Int32.Parse(numKasko.Value.ToString());
                    _alarmi.PpAp = Int32.Parse(numPPAparat.Value.ToString());
                    _alarmi.ParkMark = Int32.Parse(numParkMark.Value.ToString());
                    _alarmi.IstekRegDT = Convert.ToDateTime(dtIstekReg.Value.ToString());
                    _alarmi.SaobInspDT = Convert.ToDateTime(dtSaobInsp.Value.ToString());
                    _alarmi.TehnickiDT = Convert.ToDateTime(dtTehnicki.Value.ToString());
                    _alarmi.TaksimetarDT = Convert.ToDateTime(dtTaksimetar.Value.ToString());
                    _alarmi.TaksiLegDT = Convert.ToDateTime(dtTaksiLeg.Value.ToString());
                    _alarmi.OsiguranjeDT = Convert.ToDateTime(dtOsiguranje.Value.ToString());
                    _alarmi.KaskoDT = Convert.ToDateTime(dtKasko.Value.ToString());
                    _alarmi.PpApDT = Convert.ToDateTime(dtPPAparat.Value.ToString());
                    _alarmi.ParkMarkDT = Convert.ToDateTime(dtParkMark.Value.ToString());
                    _alarmi.dodajAlarm();
                }
            }
            else
            {
                if (MessageBox.Show("Da li ste sigurni da želiti da sačuvate?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    AlarmKlasa _alarm = alarmLista.Where(x => x.Id == 1).FirstOrDefault();
                    _alarm.IstekReg = Int32.Parse(numRegIstice.Value.ToString());
                    _alarm.SaobInsp = Int32.Parse(numSaobInsp.Value.ToString());
                    _alarm.Tehnicki = Int32.Parse(numTehnicki.Value.ToString());
                    _alarm.Taksimetar = Int32.Parse(numTaksimetar.Value.ToString());
                    _alarm.TaksiLeg = Int32.Parse(numTaksiLeg.Value.ToString());
                    _alarm.Osiguranje = Int32.Parse(numOsiguranje.Value.ToString());
                    _alarm.Kasko = Int32.Parse(numKasko.Value.ToString());
                    _alarm.PpAp = Int32.Parse(numPPAparat.Value.ToString());
                    _alarm.ParkMark = Int32.Parse(numParkMark.Value.ToString());
                    _alarm.IstekRegDT = Convert.ToDateTime(dtIstekReg.Value.ToString());
                    _alarm.SaobInspDT = Convert.ToDateTime(dtSaobInsp.Value.ToString());
                    _alarm.TehnickiDT = Convert.ToDateTime(dtTehnicki.Value.ToString());
                    _alarm.TaksimetarDT = Convert.ToDateTime(dtTaksimetar.Value.ToString());
                    _alarm.TaksiLegDT = Convert.ToDateTime(dtTaksiLeg.Value.ToString());
                    _alarm.OsiguranjeDT = Convert.ToDateTime(dtOsiguranje.Value.ToString());
                    _alarm.KaskoDT = Convert.ToDateTime(dtKasko.Value.ToString());
                    _alarm.PpApDT = Convert.ToDateTime(dtPPAparat.Value.ToString());
                    _alarm.ParkMarkDT = Convert.ToDateTime(dtParkMark.Value.ToString());
                    _alarm.azurirajAlarme();
                }
            }
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            _nas.OmoguciDugmad();
            this.Close();
        }
        
    }
}
