﻿namespace VozniPark
{
    partial class OServisu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OServisu));
            this.lblVrsta = new System.Windows.Forms.Label();
            this.lblSlBr = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRegistracija = new System.Windows.Forms.Label();
            this.lblProizvodjac = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblDatum = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVrsio = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblKilometraza = new System.Windows.Forms.Label();
            this.lblCena = new System.Windows.Forms.Label();
            this.lbVrseno = new System.Windows.Forms.ListBox();
            this.btnPDF = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblVrsta
            // 
            this.lblVrsta.AutoSize = true;
            this.lblVrsta.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVrsta.Location = new System.Drawing.Point(219, 36);
            this.lblVrsta.Name = "lblVrsta";
            this.lblVrsta.Size = new System.Drawing.Size(44, 17);
            this.lblVrsta.TabIndex = 0;
            this.lblVrsta.Text = "vrsta";
            // 
            // lblSlBr
            // 
            this.lblSlBr.AutoSize = true;
            this.lblSlBr.Location = new System.Drawing.Point(153, 36);
            this.lblSlBr.Name = "lblSlBr";
            this.lblSlBr.Size = new System.Drawing.Size(34, 17);
            this.lblSlBr.TabIndex = 0;
            this.lblSlBr.Text = "SlBr";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Službeni broj:";
            // 
            // lblRegistracija
            // 
            this.lblRegistracija.AutoSize = true;
            this.lblRegistracija.Location = new System.Drawing.Point(39, 63);
            this.lblRegistracija.Name = "lblRegistracija";
            this.lblRegistracija.Size = new System.Drawing.Size(77, 17);
            this.lblRegistracija.TabIndex = 0;
            this.lblRegistracija.Text = "registracija";
            // 
            // lblProizvodjac
            // 
            this.lblProizvodjac.AutoSize = true;
            this.lblProizvodjac.Location = new System.Drawing.Point(39, 87);
            this.lblProizvodjac.Name = "lblProizvodjac";
            this.lblProizvodjac.Size = new System.Drawing.Size(80, 17);
            this.lblProizvodjac.TabIndex = 0;
            this.lblProizvodjac.Text = "proizvodjac";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(39, 111);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(46, 17);
            this.lblModel.TabIndex = 0;
            this.lblModel.Text = "model";
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Location = new System.Drawing.Point(278, 87);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(47, 17);
            this.lblDatum.TabIndex = 0;
            this.lblDatum.Text = "datum";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Vršio:";
            // 
            // lblVrsio
            // 
            this.lblVrsio.AutoSize = true;
            this.lblVrsio.Location = new System.Drawing.Point(269, 63);
            this.lblVrsio.Name = "lblVrsio";
            this.lblVrsio.Size = new System.Drawing.Size(38, 17);
            this.lblVrsio.TabIndex = 0;
            this.lblVrsio.Text = "vrsio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(219, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Datum:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Kilometraža:";
            // 
            // lblKilometraza
            // 
            this.lblKilometraza.AutoSize = true;
            this.lblKilometraza.Location = new System.Drawing.Point(311, 111);
            this.lblKilometraza.Name = "lblKilometraza";
            this.lblKilometraza.Size = new System.Drawing.Size(57, 17);
            this.lblKilometraza.TabIndex = 0;
            this.lblKilometraza.Text = "kilometr";
            // 
            // lblCena
            // 
            this.lblCena.AutoSize = true;
            this.lblCena.Location = new System.Drawing.Point(219, 135);
            this.lblCena.Name = "lblCena";
            this.lblCena.Size = new System.Drawing.Size(45, 17);
            this.lblCena.TabIndex = 0;
            this.lblCena.Text = "Cena:";
            // 
            // lbVrseno
            // 
            this.lbVrseno.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVrseno.FormattingEnabled = true;
            this.lbVrseno.ItemHeight = 16;
            this.lbVrseno.Location = new System.Drawing.Point(27, 162);
            this.lbVrseno.Name = "lbVrseno";
            this.lbVrseno.Size = new System.Drawing.Size(470, 308);
            this.lbVrseno.TabIndex = 3;
            // 
            // btnPDF
            // 
            this.btnPDF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPDF.BackgroundImage = global::VozniPark.Properties.Resources.Adobe_PDF_Document_01;
            this.btnPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPDF.Location = new System.Drawing.Point(423, 38);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(60, 66);
            this.btnPDF.TabIndex = 2;
            this.btnPDF.UseVisualStyleBackColor = true;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // OServisu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(525, 491);
            this.Controls.Add(this.lbVrseno);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSlBr);
            this.Controls.Add(this.lblCena);
            this.Controls.Add(this.lblModel);
            this.Controls.Add(this.lblProizvodjac);
            this.Controls.Add(this.lblRegistracija);
            this.Controls.Add(this.lblVrsio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblKilometraza);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.lblVrsta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(543, 538);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(543, 538);
            this.Name = "OServisu";
            this.Text = "O Servisu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVrsta;
        private System.Windows.Forms.Label lblSlBr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRegistracija;
        private System.Windows.Forms.Label lblProizvodjac;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVrsio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblKilometraza;
        private System.Windows.Forms.Label lblCena;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.ListBox lbVrseno;
    }
}